package com.skgjst;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.google.gson.Gson;
import com.skgjst.model.MainUser;
import com.skgjst.utils.MySharedPref;

public class BaseActivity extends AppCompatActivity {

    public MainUser user;
    public Gson gson;
    public MySharedPref mySharedPref;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        gson = new Gson();
        mySharedPref = new MySharedPref(this);
        user = gson.fromJson(mySharedPref.getUserModel(), MainUser.class);
    }
}
