package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by vidhi-techmishty on 11/4/18.
 */

public class Country implements Serializable {

    public String LocationName="";
    public int countryid;

    public int getCountryid() {
        return countryid;
    }

    public void setCountryid(int countryid) {
        this.countryid = countryid;
    }


    public String getLocationName() {
        return LocationName;
    }

    public void setLocationName(String locationName) {
        LocationName = locationName;
    }
}
