package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 10/16/2018.
 */

public class BusinessAd implements Serializable {
    public int BusinessID;
    public String PhotoURL = "";

    public int getBusinessID() {
        return BusinessID;
    }

    public void setBusinessID(int businessID) {
        BusinessID = businessID;
    }

    public String getPhotoURL() {
        return PhotoURL;
    }

    public void setPhotoURL(String photoURL) {
        PhotoURL = photoURL;
    }

    public String getWebsiteURL() {
        return WebsiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        WebsiteURL = websiteURL;
    }

    public String WebsiteURL = "";
}
