package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by vidhi-techmishty on 11/4/18.
 */

public class DaughterinLawWife implements Serializable {


    public int DaughterInLawID;
    public String Name = "", MothName = "", FathName = "", GFathName = "", Surname = "", MobileNo = "", RealtionID = "", Place = "", Village = "";

    public int getDaughterInLawID() {
        return DaughterInLawID;
    }

    public void setDaughterInLawID(int daughterInLawID) {
        DaughterInLawID = daughterInLawID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getMothName() {
        return MothName;
    }

    public void setMothName(String mothName) {
        MothName = mothName;
    }

    public String getFathName() {
        return FathName;
    }

    public void setFathName(String fathName) {
        FathName = fathName;
    }

    public String getGFathName() {
        return GFathName;
    }

    public void setGFathName(String GFathName) {
        this.GFathName = GFathName;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getRealtionID() {
        return RealtionID;
    }

    public void setRealtionID(String realtionID) {
        RealtionID = realtionID;
    }

    public String getPlace() {
        return Place;
    }

    public void setPlace(String place) {
        Place = place;
    }

    public String getVillage() {
        return Village;
    }

    public void setVillage(String village) {
        Village = village;
    }
}
