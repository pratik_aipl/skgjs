package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 9/22/2018.
 */

public class Profile implements Serializable {
    public String FName = "";
    public String MName = "";
    public String SurName = "";
    public String Gender = "";
    public String PhotoURL = "";
    public String DOB = "";
    public String BloodGroupName = "";
    public String Maritalstatus = "";
    public String MarriageDate = "";
    public String Education = "";
    public String IndustryName = "";
    public String BusinessDetail = "";
    public String OfficeName = "";
    public String OfficeAddress = "";
    public String WebsiteURL = "";
    public String MemberRelationID = "";
    public String ModifiedOn = "";
    public String FathersSurname = "";
    public String FathersVillage = "";
    public String MotherName = "";
    public int IsMotherProfile =0;
    public int IsFatherProfile =0;

    public String getMotherName() {
        return MotherName;
    }

    public void setMotherName(String motherName) {
        MotherName = motherName;
    }

    public int getIsMotherProfile() {
        return IsMotherProfile;
    }

    public void setIsMotherProfile(int isMotherProfile) {
        IsMotherProfile = isMotherProfile;
    }

    public int getIsFatherProfile() {
        return IsFatherProfile;
    }

    public void setIsFatherProfile(int isFatherProfile) {
        IsFatherProfile = isFatherProfile;
    }

    public String getFathersSurname() {
        return FathersSurname;
    }

    public void setFathersSurname(String fathersSurname) {
        FathersSurname = fathersSurname;
    }

    public String getFathersVillage() {
        return FathersVillage;
    }

    public void setFathersVillage(String fathersVillage) {
        FathersVillage = fathersVillage;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public String FatherName = "";

    public int getIsSpouseProfile() {
        return IsSpouseProfile;
    }

    public void setIsSpouseProfile(int isSpouseProfile) {
        IsSpouseProfile = isSpouseProfile;
    }

    public String ModifiedBy = "";
    public boolean MIsActive = false;
    public boolean IsAlive = false;
    public int IsSpouseProfile;

    public String getPlayerID() {
        return playerID;
    }

    public void setPlayerID(String playerID) {
        this.playerID = playerID;
    }

    public String playerID = "";

    public boolean isMIsActive() {
        return MIsActive;
    }

    public void setMIsActive(boolean MIsActive) {
        this.MIsActive = MIsActive;
    }

    public boolean isAlive() {
        return IsAlive;
    }

    public void setAlive(boolean alive) {
        IsAlive = alive;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getMemberRelationID() {
        return MemberRelationID;
    }

    public void setMemberRelationID(String memberRelationID) {
        MemberRelationID = memberRelationID;
    }

    public String getNativePlace() {
        return NativePlace;
    }

    public void setNativePlace(String nativePlace) {
        NativePlace = nativePlace;
    }

    public String WorkProfession = "";
    public String NativePlace = "";

    public String getFName() {
        return FName;
    }

    public void setFName(String FName) {
        this.FName = FName;
    }

    public String getMName() {
        return MName;
    }

    public void setMName(String MName) {
        this.MName = MName;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String surName) {
        SurName = surName;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getPhotoURL() {
        return PhotoURL;
    }

    public void setPhotoURL(String photoURL) {
        PhotoURL = photoURL;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getBloodGroupName() {
        return BloodGroupName;
    }

    public void setBloodGroupName(String bloodGroupName) {
        BloodGroupName = bloodGroupName;
    }

    public String getMaritalstatus() {
        return Maritalstatus;
    }

    public void setMaritalstatus(String maritalstatus) {
        Maritalstatus = maritalstatus;
    }

    public String getMarriageDate() {
        return MarriageDate;
    }

    public void setMarriageDate(String marriageDate) {
        MarriageDate = marriageDate;
    }

    public String getEducation() {
        return Education;
    }

    public void setEducation(String education) {
        Education = education;
    }

    public String getIndustryName() {
        return IndustryName;
    }

    public void setIndustryName(String industryName) {
        IndustryName = industryName;
    }

    public String getBusinessDetail() {
        return BusinessDetail;
    }

    public void setBusinessDetail(String businessDetail) {
        BusinessDetail = businessDetail;
    }

    public String getOfficeName() {
        return OfficeName;
    }

    public void setOfficeName(String officeName) {
        OfficeName = officeName;
    }

    public String getOfficeAddress() {
        return OfficeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        OfficeAddress = officeAddress;
    }

    public String getWebsiteURL() {
        return WebsiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        WebsiteURL = websiteURL;
    }

    public String getWorkProfession() {
        return WorkProfession;
    }

    public void setWorkProfession(String workProfession) {
        WorkProfession = workProfession;
    }
}
