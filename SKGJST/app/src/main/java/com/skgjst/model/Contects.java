package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 9/22/2018.
 */

public class Contects implements Serializable {

    public String getContactNo() {
        return ContactNo;
    }

    public void setContactNo(String contactNo) {
        ContactNo = contactNo;
    }

    public String getOfficeNumber1() {
        return OfficeNumber1;
    }

    public void setOfficeNumber1(String officeNumber1) {
        OfficeNumber1 = officeNumber1;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getHomeContact() {
        return HomeContact;
    }

    public void setHomeContact(String homeContact) {
        HomeContact = homeContact;
    }

    public String getOfficeNumber2() {
        return OfficeNumber2;
    }

    public void setOfficeNumber2(String officeNumber2) {
        OfficeNumber2 = officeNumber2;
    }

    public String getWing() {
        return Wing;
    }

    public void setWing(String wing) {
        Wing = wing;
    }

    public String getRoomNo() {
        return RoomNo;
    }

    public void setRoomNo(String roomNo) {
        RoomNo = roomNo;
    }

    public String getBuildingName() {
        return BuildingName;
    }

    public void setBuildingName(String buildingName) {
        BuildingName = buildingName;
    }

    public String getPlotNo() {
        return PlotNo;
    }

    public void setPlotNo(String plotNo) {
        PlotNo = plotNo;
    }

    public String getRoadName() {
        return RoadName;
    }

    public void setRoadName(String roadName) {
        RoadName = roadName;
    }

    public String getLandMark() {
        return LandMark;
    }

    public void setLandMark(String landMark) {
        LandMark = landMark;
    }

    public String getSuburbName() {
        return SuburbName;
    }

    public void setSuburbName(String suburbName) {
        SuburbName = suburbName;
    }

    public String getDistrictName() {
        return DistrictName;
    }

    public void setDistrictName(String districtName) {
        DistrictName = districtName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPincode() {
        return Pincode;
    }

    public void setPincode(String pincode) {
        Pincode = pincode;
    }
    public String ModifiedOn = "";
    public String ModifiedBy = "";

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String ContactNo = "";
    public String OfficeNumber1 = "";
    public String EmailID = "";
    public String HomeContact = "";
    public String OfficeNumber2 = "";
    public String Wing = "";
    public String RoomNo = "";
    public String BuildingName = "";
    public String PlotNo = "";

    public String getEducation() {
        return Education;
    }

    public void setEducation(String education) {
        Education = education;
    }

    public String getBloodGroupName() {
        return BloodGroupName;
    }

    public void setBloodGroupName(String bloodGroupName) {
        BloodGroupName = bloodGroupName;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String RoadName = "";
    public String LandMark = "";
    public String SuburbName = "";
    public String DistrictName = "";
    public String country = "";
    public String state = "";
    public String city = "";

    public String getMaritalstatus() {
        return Maritalstatus;
    }

    public void setMaritalstatus(String maritalstatus) {
        Maritalstatus = maritalstatus;
    }

    public String Pincode = "";
    public String Education="";
    public String BloodGroupName="";
    public String DOB="";
    public String Maritalstatus="";


}
