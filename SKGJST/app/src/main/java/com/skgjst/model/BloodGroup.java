package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by vidhi-techmishty on 11/4/18.
 */

public class BloodGroup implements Serializable {

    public String BloodGroupName = "";
    public int BloodGroupID;

    public String getBloodGroupName() {
        return BloodGroupName;
    }

    public void setBloodGroupName(String bloodGroupName) {
        BloodGroupName = bloodGroupName;
    }

    public int getBloodGroupID() {
        return BloodGroupID;
    }

    public void setBloodGroupID(int bloodGroupID) {
        BloodGroupID = bloodGroupID;
    }
}
