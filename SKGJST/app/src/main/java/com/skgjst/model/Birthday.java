package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by vidhi-techmishty on 19/4/18.
 */

public class Birthday implements Serializable {

    public String FName = "";
    public String MName = "";
    public String SurName = "";
    public int IsWish;
    public int wishCount;

    public int getIsWish() {
        return IsWish;
    }

    public void setIsWish(int isWish) {
        IsWish = isWish;
    }

    public int getWishCount() {
        return wishCount;
    }

    public void setWishCount(int wishCount) {
        this.wishCount = wishCount;
    }

    public int getFamilyID() {
        return FamilyID;
    }

    public void setFamilyID(int familyID) {
        FamilyID = familyID;
    }

    public int FamilyID = -1;

    public String getNativePlace() {
        return NativePlace;
    }

    public void setNativePlace(String nativePlace) {
        NativePlace = nativePlace;
    }

    public String PhotoURL = "";
    public String NativePlace = "";
    public String CurrentCity = "";

    public String getCurrentCity() {
        return CurrentCity;
    }

    public void setCurrentCity(String currentCity) {
        CurrentCity = currentCity;
    }

    public int getSfamilydetailid() {
        return Sfamilydetailid;
    }

    public void setSfamilydetailid(int sfamilydetailid) {
        Sfamilydetailid = sfamilydetailid;
    }

    public int Sfamilydetailid;


    public String getPhotoURL() {
        return PhotoURL;
    }

    public void setPhotoURL(String photoURL) {
        PhotoURL = photoURL;
    }

    public String getFName() {
        return FName;
    }

    public void setFName(String FName) {
        this.FName = FName;
    }

    public String getMName() {
        return MName;
    }

    public void setMName(String MName) {
        this.MName = MName;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String surName) {
        SurName = surName;
    }
}
