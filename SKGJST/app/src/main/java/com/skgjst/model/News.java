package com.skgjst.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 10/15/2018.
 */

public class News implements Serializable {
    public String NewsTitle = "";

    public String getNewsTitle() {
        return NewsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        NewsTitle = newsTitle;
    }

    public String getNewsDesc() {
        return NewsDesc;
    }

    public void setNewsDesc(String newsDesc) {
        NewsDesc = newsDesc;
    }

    public String NewsDesc = "";
    public ArrayList<News> newsSubArray=new ArrayList<>();
}
