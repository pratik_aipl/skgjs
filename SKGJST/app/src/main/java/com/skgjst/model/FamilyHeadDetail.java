package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by vidhi-techmishty on 11/4/18.
 */

public class FamilyHeadDetail implements Serializable {


    public String GrandFatherName = "", NativePlace = "", Wing = "", RoomNo = "", BuildingName = "",
            PlotNo = "", RoadName = "", LandMark = "", SubrubName = "", DistrictName = "", City = "",
            State = "", Country = "", Pincode = "", HomeContactNo = "", OfficeContactNo = "", BloodGroupName="",RelationName="", BusinessTypeName="",
            OfficeName = "", OfficeAddress = "", International = "", FName = "", MName = "", SurName = "", DOB = "", Gender = "", MarriageDate = "",
            Education = "", BusinessType = "", BusinessDetail = "", ContactNo = "", EmailID = "", PhotoURL = "", WebsiteURL = "", WorkProfession = "",
            CityID="", StateID="", CountryID="";

    public int RalationID, BloodGroupID, MaritalStatusID;

    public String getCityID() {
        return CityID;
    }

    public void setCityID(String cityID) {
        CityID = cityID;
    }

    public String getStateID() {
        return StateID;
    }

    public void setStateID(String stateID) {
        StateID = stateID;
    }

    public String getCountryID() {
        return CountryID;
    }

    public void setCountryID(String countryID) {
        CountryID = countryID;
    }

    public String getBusinessTypeName() {
        return BusinessTypeName;
    }

    public void setBusinessTypeName(String businessTypeName) {
        BusinessTypeName = businessTypeName;
    }

    public String getRelationName() {
        return RelationName;
    }

    public void setRelationName(String relationName) {
        RelationName = relationName;
    }

    public String getBloodGroupName() {
        return BloodGroupName;
    }

    public void setBloodGroupName(String bloodGroupName) {
        BloodGroupName = bloodGroupName;
    }

    public String getGrandFatherName() {
        return GrandFatherName;
    }

    public void setGrandFatherName(String grandFatherName) {
        GrandFatherName = grandFatherName;
    }

    public String getNativePlace() {
        return NativePlace;
    }

    public void setNativePlace(String nativePlace) {
        NativePlace = nativePlace;
    }

    public String getWing() {
        return Wing;
    }

    public void setWing(String wing) {
        Wing = wing;
    }

    public String getRoomNo() {
        return RoomNo;
    }

    public void setRoomNo(String roomNo) {
        RoomNo = roomNo;
    }

    public String getBuildingName() {
        return BuildingName;
    }

    public void setBuildingName(String buildingName) {
        BuildingName = buildingName;
    }

    public String getPlotNo() {
        return PlotNo;
    }

    public void setPlotNo(String plotNo) {
        PlotNo = plotNo;
    }

    public String getRoadName() {
        return RoadName;
    }

    public void setRoadName(String roadName) {
        RoadName = roadName;
    }

    public String getLandMark() {
        return LandMark;
    }

    public void setLandMark(String landMark) {
        LandMark = landMark;
    }

    public String getSubrubName() {
        return SubrubName;
    }

    public void setSubrubName(String subrubName) {
        SubrubName = subrubName;
    }

    public String getDistrictName() {
        return DistrictName;
    }

    public void setDistrictName(String districtName) {
        DistrictName = districtName;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getPincode() {
        return Pincode;
    }

    public void setPincode(String pincode) {
        Pincode = pincode;
    }

    public String getHomeContactNo() {
        return HomeContactNo;
    }

    public void setHomeContactNo(String homeContactNo) {
        HomeContactNo = homeContactNo;
    }

    public String getOfficeContactNo() {
        return OfficeContactNo;
    }

    public void setOfficeContactNo(String officeContactNo) {
        OfficeContactNo = officeContactNo;
    }

    public String getOfficeName() {
        return OfficeName;
    }

    public void setOfficeName(String officeName) {
        OfficeName = officeName;
    }

    public String getOfficeAddress() {
        return OfficeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        OfficeAddress = officeAddress;
    }

    public String getInternational() {
        return International;
    }

    public void setInternational(String international) {
        International = international;
    }

    public String getFName() {
        return FName;
    }

    public void setFName(String FName) {
        this.FName = FName;
    }

    public String getMName() {
        return MName;
    }

    public void setMName(String MName) {
        this.MName = MName;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String surName) {
        SurName = surName;
    }


    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public int getRalationID() {
        return RalationID;
    }

    public void setRalationID(int ralationID) {
        RalationID = ralationID;
    }

    public int getBloodGroupID() {
        return BloodGroupID;
    }

    public void setBloodGroupID(int bloodGroupID) {
        BloodGroupID = bloodGroupID;
    }

    public int getMaritalStatusID() {
        return MaritalStatusID;
    }

    public void setMaritalStatusID(int maritalStatusID) {
        MaritalStatusID = maritalStatusID;
    }

    public String getMarriageDate() {
        return MarriageDate;
    }

    public void setMarriageDate(String marriageDate) {
        MarriageDate = marriageDate;
    }

    public String getEducation() {
        return Education;
    }

    public void setEducation(String education) {
        Education = education;
    }

    public String getBusinessType() {
        return BusinessType;
    }

    public void setBusinessType(String businessType) {
        BusinessType = businessType;
    }

    public String getBusinessDetail() {
        return BusinessDetail;
    }

    public void setBusinessDetail(String businessDetail) {
        BusinessDetail = businessDetail;
    }

    public String getContactNo() {
        return ContactNo;
    }

    public void setContactNo(String contactNo) {
        ContactNo = contactNo;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getPhotoURL() {
        return PhotoURL;
    }

    public void setPhotoURL(String photoURL) {
        PhotoURL = photoURL;
    }

    public String getWebsiteURL() {
        return WebsiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        WebsiteURL = websiteURL;
    }

    public String getWorkProfession() {
        return WorkProfession;
    }

    public void setWorkProfession(String workProfession) {
        WorkProfession = workProfession;
    }
}
