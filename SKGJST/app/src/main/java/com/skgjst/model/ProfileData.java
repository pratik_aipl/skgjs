package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 9/26/2018.
 */

public class ProfileData implements Serializable {
    public int FamilyDetailID ;
    public int FamilyID;
    public int MemberRelationID;
    public String Gender = "";
    public String PhotoURL = "";
    public String DOB = "";
    public boolean MIsActive = false;
    public boolean IsAlive = false;

    public int getMemberRelationID() {
        return MemberRelationID;
    }

    public void setMemberRelationID(int memberRelationID) {
        MemberRelationID = memberRelationID;
    }

    public boolean isMIsActive() {
        return MIsActive;
    }

    public void setMIsActive(boolean MIsActive) {
        this.MIsActive = MIsActive;
    }

    public boolean isAlive() {
        return IsAlive;
    }

    public void setAlive(boolean alive) {
        IsAlive = alive;
    }

    public String getContactNo() {
        return ContactNo;
    }

    public void setContactNo(String contactNo) {
        ContactNo = contactNo;
    }

    public String ContactNo= "";


    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getPhotoURL() {
        return PhotoURL;
    }

    public void setPhotoURL(String photoURL) {
        PhotoURL = photoURL;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }


    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getBusinessType() {
        return BusinessType;
    }

    public void setBusinessType(String businessType) {
        BusinessType = businessType;
    }

    public String getBusinessDetail() {
        return BusinessDetail;
    }

    public void setBusinessDetail(String businessDetail) {
        BusinessDetail = businessDetail;
    }

    public String getWorkProfession() {
        return WorkProfession;
    }

    public void setWorkProfession(String workProfession) {
        WorkProfession = workProfession;
    }

    public String getOfficeName() {
        return OfficeName;
    }

    public void setOfficeName(String officeName) {
        OfficeName = officeName;
    }

    public String getOfficeAddress() {
        return OfficeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        OfficeAddress = officeAddress;
    }

    public String getOfficeContNo() {
        return OfficeContNo;
    }

    public void setOfficeContNo(String officeContNo) {
        OfficeContNo = officeContNo;
    }

    public String getWebsiteURL() {
        return WebsiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        WebsiteURL = websiteURL;
    }

    public String getWing() {
        return wing;
    }

    public void setWing(String wing) {
        this.wing = wing;
    }

    public String getRoomNo() {
        return RoomNo;
    }

    public void setRoomNo(String roomNo) {
        RoomNo = roomNo;
    }

    public String getPlotno() {
        return plotno;
    }

    public void setPlotno(String plotno) {
        this.plotno = plotno;
    }

    public String getBuildingName() {
        return BuildingName;
    }

    public void setBuildingName(String buildingName) {
        BuildingName = buildingName;
    }

    public String getRoadname() {
        return roadname;
    }

    public void setRoadname(String roadname) {
        this.roadname = roadname;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getSubrubName() {
        return SubrubName;
    }

    public void setSubrubName(String subrubName) {
        SubrubName = subrubName;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getPincode() {
        return Pincode;
    }

    public void setPincode(String pincode) {
        Pincode = pincode;
    }

    public String getDistrictName() {
        return DistrictName;
    }

    public void setDistrictName(String districtName) {
        DistrictName = districtName;
    }

    public int getFamilyDetailID() {
        return FamilyDetailID;
    }

    public void setFamilyDetailID(int familyDetailID) {
        FamilyDetailID = familyDetailID;
    }

    public int getFamilyID() {
        return FamilyID;
    }

    public void setFamilyID(int familyID) {
        FamilyID = familyID;
    }

    public void setBloodgroupid(int bloodgroupid) {
        this.bloodgroupid = bloodgroupid;
    }

    public int getBloodgroupid() {
        return bloodgroupid;
    }

    public int bloodgroupid;
    public String EmailID = "";
    public String education = "";
    public String BusinessType = "";
    public String BusinessDetail = "";
    public String WorkProfession = "";
    public String OfficeName = "";
    public String OfficeAddress = "";
    public String OfficeContNo = "";
    public String WebsiteURL = "";
    public String wing = "";
    public String RoomNo = "";
    public String plotno = "";
    public String BuildingName = "";
    public String roadname = "";
    public String landmark = "";
    public String SubrubName = "";
    public String Country = "";
    public String State = "";
    public String City = "";
    public String Pincode = "";
    public String DistrictName = "";
}
