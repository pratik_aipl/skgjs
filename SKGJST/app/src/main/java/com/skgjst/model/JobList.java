package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 9/19/2018.
 */

public class JobList implements Serializable {

    public String Joblocation = "";
    public String CreatedOn = "";
    public String CreatedBy = "";
    public String ModifiedOn = "";
    public String ModifiedBy = "";
    public String ExpiryDate = "";
    public String experience = "";
    public String companyname = "";
    public String jobpost = "";
    public int FamilyDetailid=-1;

    public int getFamilyDetailid() {
        return FamilyDetailid;
    }

    public void setFamilyDetailid(int familyDetailid) {
        FamilyDetailid = familyDetailid;
    }

    public String getJobdescription() {
        return jobdescription;
    }

    public void setJobdescription(String jobdescription) {
        this.jobdescription = jobdescription;
    }

    public String jobdescription="";

    public int familyid=-1,jobid=-1;

    public String getJoblocation() {
        return Joblocation;
    }

    public void setJoblocation(String joblocation) {
        Joblocation = joblocation;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }

    public String getExpiryDate() {
        return ExpiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        ExpiryDate = expiryDate;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getJobpost() {
        return jobpost;
    }

    public void setJobpost(String jobpost) {
        this.jobpost = jobpost;
    }

    public int getFamilyid() {
        return familyid;
    }

    public void setFamilyid(int familyid) {
        this.familyid = familyid;
    }

    public int getJobid() {
        return jobid;
    }

    public void setJobid(int jobid) {
        this.jobid = jobid;
    }
}
