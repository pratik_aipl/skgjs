package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 10/9/2018.
 */

public class Notifications implements Serializable {

    public String message = "";
    public String CreatedDate = "";
    public boolean IsActive;

    public int getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(int notification_id) {
        this.notification_id = notification_id;
    }

    public String FName = "";
    public int notification_id=-1;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String createdDate) {
        CreatedDate = createdDate;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public String getFName() {
        return FName;
    }

    public void setFName(String FName) {
        this.FName = FName;
    }

    public String getMName() {
        return MName;
    }

    public void setMName(String MName) {
        this.MName = MName;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String surName) {
        SurName = surName;
    }

    public String getNotification_type() {
        return Notification_type;
    }

    public void setNotification_type(String notification_type) {
        Notification_type = notification_type;
    }

    public String MName = "";
    public String SurName = "";
    public String Notification_type = "";
}
