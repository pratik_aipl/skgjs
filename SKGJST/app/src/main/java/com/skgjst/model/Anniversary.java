package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 10/4/2018.
 */

public class Anniversary implements Serializable {
    public String HusbandName = "";
    public int HFamilyDetailID;
    public String HPhotoURl = "";
    public int FamilyID;
    public int IsWish;
    public int wishCount;

    public int getIsWish() {
        return IsWish;
    }

    public void setIsWish(int isWish) {
        IsWish = isWish;
    }

    public int getWishCount() {
        return wishCount;
    }

    public void setWishCount(int wishCount) {
        this.wishCount = wishCount;
    }

    public int getFamilyID() {
        return FamilyID;
    }

    public void setFamilyID(int familyID) {
        FamilyID = familyID;
    }

    public String getCurrentCity() {
        return CurrentCity;
    }

    public void setCurrentCity(String currentCity) {
        CurrentCity = currentCity;
    }

    public String CurrentCity="";

    public String getNativePlace() {
        return NativePlace;
    }

    public void setNativePlace(String nativePlace) {
        NativePlace = nativePlace;
    }

    public String NativePlace="";

    public String getHusbandName() {
        return HusbandName;
    }

    public void setHusbandName(String husbandName) {
        HusbandName = husbandName;
    }

    public int getHFamilyDetailID() {
        return HFamilyDetailID;
    }

    public void setHFamilyDetailID(int HFamilyDetailID) {
        this.HFamilyDetailID = HFamilyDetailID;
    }

    public String getHPhotoURl() {
        return HPhotoURl;
    }

    public void setHPhotoURl(String HPhotoURl) {
        this.HPhotoURl = HPhotoURl;
    }

    public int getYears() {
        return years;
    }

    public void setYears(int years) {
        this.years = years;
    }

    public String getWifeName() {
        return WifeName;
    }

    public void setWifeName(String wifeName) {
        WifeName = wifeName;
    }

    public int getWFamilyDetailID() {
        return WFamilyDetailID;
    }

    public void setWFamilyDetailID(int WFamilyDetailID) {
        this.WFamilyDetailID = WFamilyDetailID;
    }

    public String getWPhotoURL() {
        return WPhotoURL;
    }

    public void setWPhotoURL(String WPhotoURL) {
        this.WPhotoURL = WPhotoURL;
    }

    public int years;
    public String WifeName = "";
    public int WFamilyDetailID;
    public String WPhotoURL;
}
