package com.skgjst.model;

import java.io.Serializable;

public class MemberDetail implements Serializable {

    public String FirstName ="", LastName="",  MobileNumber="", EmailID="",  RelationID="",  DOb="";
    public boolean isFooter = false;
    public int isSelected;


    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getRelationID() {
        return RelationID;
    }

    public void setRelationID(String relationID) {
        RelationID = relationID;
    }

    public String getDOb() {
        return DOb;
    }

    public void setDOb(String DOb) {
        this.DOb = DOb;
    }
}
