package com.skgjst.model;

import java.io.Serializable;
import java.util.ArrayList;

public class DemiseType implements Serializable {
    public String Demise_name;

    public String getNotification_name() {
        return Demise_name;
    }

    public void setNotification_name(String notification_name) {
        this.Demise_name = notification_name;
    }

    public ArrayList<DemiseDataNew> demiseDataArray = new ArrayList<>();

}
