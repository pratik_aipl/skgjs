package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by vidhi-techmishty on 11/4/18.
 */

public class Village implements Serializable {

    public String Villagename="";

    public int VillageID;

    public int getVillageID() {
        return VillageID;
    }

    public void setVillageID(int villageID) {
        VillageID = villageID;
    }

    public String getVillagename() {
        return Villagename;
    }

    public void setVillagename(String villagename) {
        Villagename = villagename;
    }
}
