package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by vidhi-techmishty on 11/4/18.
 */

public class MaritalStatus implements Serializable {

    public String  Maritalstatus="";
    public int MaritalstatusID;

    public int getMaritalstatusID() {
        return MaritalstatusID;
    }

    public void setMaritalstatusID(int maritalstatusID) {
        MaritalstatusID = maritalstatusID;
    }

    public String getMaritalstatus() {
        return Maritalstatus;
    }

    public void setMaritalstatus(String maritalstatus) {
        Maritalstatus = maritalstatus;
    }
}
