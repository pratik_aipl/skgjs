package com.skgjst.model;

import java.io.Serializable;

public class CurrentCity implements Serializable {
    public int LocationID;
    public String city = "";

    public int getLocationID() {
        return LocationID;
    }

    public void setLocationID(int locationID) {
        LocationID = locationID;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
