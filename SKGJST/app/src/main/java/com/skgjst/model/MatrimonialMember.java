package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by Krupa Kakkad on 10 July 2018
 */
public class MatrimonialMember implements Serializable {

    public int FamilyDetailID = 0;
    public String FName = "";

    public int getFamilyDetailID() {
        return FamilyDetailID;
    }

    public void setFamilyDetailID(int familyDetailID) {
        FamilyDetailID = familyDetailID;
    }

    public String getFName() {
        return FName;
    }

    public void setFName(String FName) {
        this.FName = FName;
    }
}
