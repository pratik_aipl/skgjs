package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by vidhi-techmishty on 19/4/18.
 */

public class DiksharthiDetail implements Serializable{

    public String SansariName = "",DikshaName="",DikshaDate="",Samudaay="",Dob="";
    public int RealtionID,DikshaDetailsID;

    public int getDikshaDetailID() {
        return DikshaDetailsID;
    }

    public void setDikshaDetailID(int dikshaDetailID) {
        DikshaDetailsID = dikshaDetailID;
    }

    public String getSansariName() {
        return SansariName;
    }

    public void setSansariName(String sansariName) {
        SansariName = sansariName;
    }

    public String getDikshaName() {
        return DikshaName;
    }

    public void setDikshaName(String dikshaName) {
        DikshaName = dikshaName;
    }

    public String getDikshaDate() {
        return DikshaDate;
    }

    public void setDikshaDate(String dikshaDate) {
        DikshaDate = dikshaDate;
    }

    public String getSamudaay() {
        return Samudaay;
    }

    public void setSamudaay(String samudaay) {
        Samudaay = samudaay;
    }

    public String getDob() {
        return Dob;
    }

    public void setDob(String dob) {
        Dob = dob;
    }

    public int getRealtionID() {
        return RealtionID;
    }

    public void setRealtionID(int realtionID) {
        RealtionID = realtionID;
    }
}
