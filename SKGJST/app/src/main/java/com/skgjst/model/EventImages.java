package com.skgjst.model;

import java.io.Serializable;

public class EventImages implements Serializable {

    public String ImageURL = "";

    public String getCoverImage() {
        return CoverImage;
    }

    public void setCoverImage(String coverImage) {
        CoverImage = coverImage;
    }

    public String CoverImage="";

    public String getImageURL() {
        return ImageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }
}
