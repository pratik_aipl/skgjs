package com.skgjst.model;


import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 9/21/2018.
 */

public class RegisterData implements Serializable {
    public String Surname ="";
    public String Firstname="";
    public String Middlename="";
    public String NativePlace="";

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public String PhotoUrl="";
    public String filename="";

    public String getFirstname() {
        return Firstname;
    }

    public void setFirstname(String firstname) {
        Firstname = firstname;
    }

    public String getMiddlename() {
        return Middlename;
    }

    public void setMiddlename(String middlename) {
        Middlename = middlename;
    }

    public String getNativePlace() {
        return NativePlace;
    }

    public void setNativePlace(String nativePlace) {
        NativePlace = nativePlace;
    }

    public String getPhotoUrl() {
        return PhotoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        PhotoUrl = photoUrl;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getBloodgroupid() {
        return bloodgroupid;
    }

    public void setBloodgroupid(String bloodgroupid) {
        this.bloodgroupid = bloodgroupid;
    }

    public String getMaritalstatusid() {
        return maritalstatusid;
    }

    public void setMaritalstatusid(String maritalstatusid) {
        this.maritalstatusid = maritalstatusid;
    }

    public String getMarriagedate() {
        return marriagedate;
    }

    public void setMarriagedate(String marriagedate) {
        this.marriagedate = marriagedate;
    }

    public String getContactno() {
        return contactno;
    }

    public void setContactno(String contactno) {
        this.contactno = contactno;
    }

    public String getEducation() {
        return Education;
    }

    public void setEducation(String education) {
        Education = education;
    }

    public String getWorkprofession() {
        return workprofession;
    }

    public void setWorkprofession(String workprofession) {
        this.workprofession = workprofession;
    }

    public String getBusstype() {
        return busstype;
    }

    public void setBusstype(String busstype) {
        this.busstype = busstype;
    }

    public String getBussdetails() {
        return bussdetails;
    }

    public void setBussdetails(String bussdetails) {
        this.bussdetails = bussdetails;
    }

    public String getOfficename() {
        return officename;
    }

    public void setOfficename(String officename) {
        this.officename = officename;
    }

    public String getOffaddress() {
        return offaddress;
    }

    public void setOffaddress(String offaddress) {
        this.offaddress = offaddress;
    }

    public String getOfficecontactNo() {
        return officecontactNo;
    }

    public void setOfficecontactNo(String officecontactNo) {
        this.officecontactNo = officecontactNo;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getWebsiteurl() {
        return Websiteurl;
    }

    public void setWebsiteurl(String websiteurl) {
        Websiteurl = websiteurl;
    }

    public String getWing() {
        return wing;
    }

    public void setWing(String wing) {
        this.wing = wing;
    }

    public String getRoomno() {
        return roomno;
    }

    public void setRoomno(String roomno) {
        this.roomno = roomno;
    }

    public String getPlotNo() {
        return PlotNo;
    }

    public void setPlotNo(String plotNo) {
        PlotNo = plotNo;
    }

    public String getBuildingname() {
        return Buildingname;
    }

    public void setBuildingname(String buildingname) {
        Buildingname = buildingname;
    }

    public String getRoadname() {
        return roadname;
    }

    public void setRoadname(String roadname) {
        this.roadname = roadname;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getSuburbname() {
        return suburbname;
    }

    public void setSuburbname(String suburbname) {
        this.suburbname = suburbname;
    }

    public String getHomecontactno() {
        return homecontactno;
    }

    public void setHomecontactno(String homecontactno) {
        this.homecontactno = homecontactno;
    }

    public String getDistrictname() {
        return districtname;
    }

    public void setDistrictname(String districtname) {
        this.districtname = districtname;
    }

    public String getCountryid() {
        return countryid;
    }

    public void setCountryid(String countryid) {
        this.countryid = countryid;
    }

    public String getStateid() {
        return stateid;
    }

    public void setStateid(String stateid) {
        this.stateid = stateid;
    }

    public String getCityid() {
        return cityid;
    }

    public void setCityid(String cityid) {
        this.cityid = cityid;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getInternational() {
        return international;
    }

    public void setInternational(String international) {
        this.international = international;
    }

    public String getCountrycode() {
        return countrycode;
    }

    public void setCountrycode(String countrycode) {
        this.countrycode = countrycode;
    }

    public String getFathername() {
        return fathername;
    }

    public void setFathername(String fathername) {
        this.fathername = fathername;
    }

    public String getMothername() {
        return mothername;
    }

    public void setMothername(String mothername) {
        this.mothername = mothername;
    }

    public String getGrandfathername() {
        return grandfathername;
    }

    public void setGrandfathername(String grandfathername) {
        this.grandfathername = grandfathername;
    }

    public String getGrandmothername() {
        return grandmothername;
    }

    public void setGrandmothername(String grandmothername) {
        this.grandmothername = grandmothername;
    }

    public String getMfathername() {
        return mfathername;
    }

    public void setMfathername(String mfathername) {
        this.mfathername = mfathername;
    }

    public String getMmothername() {
        return mmothername;
    }

    public void setMmothername(String mmothername) {
        this.mmothername = mmothername;
    }

    public String getMfathersurname() {
        return mfathersurname;
    }

    public void setMfathersurname(String mfathersurname) {
        this.mfathersurname = mfathersurname;
    }

    public String getMfathervillage() {
        return mfathervillage;
    }

    public void setMfathervillage(String mfathervillage) {
        this.mfathervillage = mfathervillage;
    }

    public String getFathersurname() {
        return fathersurname;
    }

    public void setFathersurname(String fathersurname) {
        this.fathersurname = fathersurname;
    }

    public String getFathervillage() {
        return fathervillage;
    }

    public void setFathervillage(String fathervillage) {
        this.fathervillage = fathervillage;
    }

    public String getSpousename() {
        return spousename;
    }

    public void setSpousename(String spousename) {
        this.spousename = spousename;
    }

    public String getSpousefathername() {
        return spousefathername;
    }

    public void setSpousefathername(String spousefathername) {
        this.spousefathername = spousefathername;
    }

    public String getSpousemothername() {
        return spousemothername;
    }

    public void setSpousemothername(String spousemothername) {
        this.spousemothername = spousemothername;
    }

    public String getWifefathersurname() {
        return wifefathersurname;
    }

    public void setWifefathersurname(String wifefathersurname) {
        this.wifefathersurname = wifefathersurname;
    }

    public String getWifefathervillage() {
        return wifefathervillage;
    }

    public void setWifefathervillage(String wifefathervillage) {
        this.wifefathervillage = wifefathervillage;
    }

    public String gender="";
    public String dob="";
    public String bloodgroupid="";
    public String maritalstatusid="";
    public String marriagedate="";
    public String contactno="";
    public String Education="";
    public String workprofession="";
    public String busstype="";
    public String bussdetails="";
    public String officename="";
    public String offaddress="";
    public String officecontactNo="";
    public String EmailID="";
    public String Websiteurl="";
    public String wing="";
    public String roomno="";
    public String PlotNo="";
    public String Buildingname="";
    public String roadname="";
    public String landmark="";
    public String suburbname="";
    public String homecontactno="";
    public String districtname="";
    public String countryid="";
    public String stateid="";
    public String cityid="";
    public String pincode="";
    public String international="";
    public String countrycode="";
    public String fathername="";
    public String mothername="";
    public String grandfathername="";
    public String grandmothername="";
    public String mfathername="";
    public String mmothername="";
    public String mfathersurname="";
    public String mfathervillage="";
    public String fathersurname="";
    public String fathervillage="";
    public String spousename="";
    public String spousefathername="";
    public String spousemothername="";
    public String wifefathersurname="";
    public String wifefathervillage="";
}
