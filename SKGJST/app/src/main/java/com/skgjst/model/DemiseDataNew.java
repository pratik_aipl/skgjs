package com.skgjst.model;

import java.io.Serializable;

public class DemiseDataNew implements Serializable {

    public String DemisePrathnaID = "";
    public String Name = "";
    public String FatherName = "";
    public String GrandFatherName = "";
    public String Surname = "";
    public String NativePlace = "";
    public String CurrenPlace = "";
    public String DemiseDate = "";
    public String PDType = "";
    public String IsPrathnaAdded = "";
    public String PDDate = "";
    public String PDTime = "";
    public String PDPlace = "";
    public String PostedByFDID = "";
    public String ImageURL = "";
    public String Age = "";
    public String ContactPerson = "";
    public String OtherInfo = "";
    public String ContactNumber = "";
    public String PostedBy="";

    public String getPostedBy() {
        return PostedBy;
    }

    public void setPostedBy(String postedBy) {
        PostedBy = postedBy;
    }


    public String getIseditable() {
        return iseditable;
    }

    public void setIseditable(String iseditable) {
        this.iseditable = iseditable;
    }

    public String iseditable="";

    public String getDemisePrathnaID() {
        return DemisePrathnaID;
    }

    public void setDemisePrathnaID(String demisePrathnaID) {
        DemisePrathnaID = demisePrathnaID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public String getGrandFatherName() {
        return GrandFatherName;
    }

    public void setGrandFatherName(String grandFatherName) {
        GrandFatherName = grandFatherName;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public String getNativePlace() {
        return NativePlace;
    }

    public void setNativePlace(String nativePlace) {
        NativePlace = nativePlace;
    }

    public String getCurrenPlace() {
        return CurrenPlace;
    }

    public void setCurrenPlace(String currenPlace) {
        CurrenPlace = currenPlace;
    }

    public String getDemiseDate() {
        return DemiseDate;
    }

    public void setDemiseDate(String demiseDate) {
        DemiseDate = demiseDate;
    }

    public String getPDType() {
        return PDType;
    }

    public void setPDType(String PDType) {
        this.PDType = PDType;
    }

    public String getIsPrathnaAdded() {
        return IsPrathnaAdded;
    }

    public void setIsPrathnaAdded(String isPrathnaAdded) {
        IsPrathnaAdded = isPrathnaAdded;
    }

    public String getPDDate() {
        return PDDate;
    }

    public void setPDDate(String PDDate) {
        this.PDDate = PDDate;
    }

    public String getPDTime() {
        return PDTime;
    }

    public void setPDTime(String PDTime) {
        this.PDTime = PDTime;
    }

    public String getPDPlace() {
        return PDPlace;
    }

    public void setPDPlace(String PDPlace) {
        this.PDPlace = PDPlace;
    }

    public String getPostedByFDID() {
        return PostedByFDID;
    }

    public void setPostedByFDID(String postedByFDID) {
        PostedByFDID = postedByFDID;
    }

    public String getImageURL() {
        return ImageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }

    public String getAge() {
        return Age;
    }

    public void setAge(String age) {
        Age = age;
    }

    public String getContactPerson() {
        return ContactPerson;
    }

    public void setContactPerson(String contactPerson) {
        ContactPerson = contactPerson;
    }

    public String getOtherInfo() {
        return OtherInfo;
    }

    public void setOtherInfo(String otherInfo) {
        OtherInfo = otherInfo;
    }

    public String getContactNumber() {
        return ContactNumber;
    }

    public void setContactNumber(String contactNumber) {
        ContactNumber = contactNumber;
    }
}
