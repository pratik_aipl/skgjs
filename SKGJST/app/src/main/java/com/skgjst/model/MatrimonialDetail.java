package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by Krupa Kakkad on 10 July 2018
 */
public class MatrimonialDetail implements Serializable {
    public int MatrimonialID;
    public int FamilydetailID;
    public String Height = "";
    public String Weight = "";
    public String Skincolor = "";
    public String Handicap = "";
    public String HandicapDetail = "";
    public String Manglik = "";
public String MaritalStatus="";

    public String getCurrentCity() {
        return CurrentCity;
    }

    public void setCurrentCity(String currentCity) {
        CurrentCity = currentCity;
    }

    public String CurrentCity="";

    public String getMaritalStatus() {
        return MaritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        MaritalStatus = maritalStatus;
    }

    public String getNativePlace() {

        return NativePlace;
    }

    public void setNativePlace(String nativePlace) {
        NativePlace = nativePlace;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String FullName="";
    public String NativePlace="";
    public String City="";

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public int getMatrimonialID() {
        return MatrimonialID;
    }

    public void setMatrimonialID(int matrimonialID) {
        MatrimonialID = matrimonialID;
    }

    public int getFamilydetailID() {
        return FamilydetailID;
    }

    public void setFamilydetailID(int familydetailID) {
        FamilydetailID = familydetailID;
    }

    public String getHeight() {
        return Height;
    }

    public void setHeight(String height) {
        Height = height;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public String getSkincolor() {
        return Skincolor;
    }

    public void setSkincolor(String skincolor) {
        Skincolor = skincolor;
    }

    public String getHandicap() {
        return Handicap;
    }

    public void setHandicap(String handicap) {
        Handicap = handicap;
    }

    public String getHandicapDetail() {
        return HandicapDetail;
    }

    public void setHandicapDetail(String handicapDetail) {
        HandicapDetail = handicapDetail;
    }

    public String getManglik() {
        return Manglik;
    }

    public void setManglik(String manglik) {
        Manglik = manglik;
    }

    public String getPhotourl() {
        return Photourl;
    }

    public void setPhotourl(String photourl) {
        Photourl = photourl;
    }

    public String getOtherinfo() {
        return otherinfo;
    }

    public void setOtherinfo(String otherinfo) {
        this.otherinfo = otherinfo;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    public int getUFamilyDetailID() {
        return UFamilyDetailID;
    }

    public void setUFamilyDetailID(int UFamilyDetailID) {
        this.UFamilyDetailID = UFamilyDetailID;
    }

    public String Photourl = "";
    public String otherinfo = "";
    public String isactive = "";
    public int UFamilyDetailID;


}
