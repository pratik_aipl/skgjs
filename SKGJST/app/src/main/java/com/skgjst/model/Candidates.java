package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 9/21/2018.
 */

public class Candidates implements Serializable {


    public int applyid, familyid, jobid, Familydetailid;
    public String firstname = "", lastname = "", experience = "", presentlocation = "", noticeperiod = "", currentctc = "",
            expectedctc = "", resume = "", CreatedOn = "", CreatedBy = "", ModifiedOn = "", ModifiedBy = "";

    public int getApplyid() {
        return applyid;
    }

    public void setApplyid(int applyid) {
        this.applyid = applyid;
    }

    public int getFamilyid() {
        return familyid;
    }

    public void setFamilyid(int familyid) {
        this.familyid = familyid;
    }

    public int getJobid() {
        return jobid;
    }

    public void setJobid(int jobid) {
        this.jobid = jobid;
    }

    public int getFamilydetailid() {
        return Familydetailid;
    }

    public void setFamilydetailid(int familydetailid) {
        Familydetailid = familydetailid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getPresentlocation() {
        return presentlocation;
    }

    public void setPresentlocation(String presentlocation) {
        this.presentlocation = presentlocation;
    }

    public String getNoticeperiod() {
        return noticeperiod;
    }

    public void setNoticeperiod(String noticeperiod) {
        this.noticeperiod = noticeperiod;
    }

    public String getCurrentctc() {
        return currentctc;
    }

    public void setCurrentctc(String currentctc) {
        this.currentctc = currentctc;
    }

    public String getExpectedctc() {
        return expectedctc;
    }

    public void setExpectedctc(String expectedctc) {
        this.expectedctc = expectedctc;
    }

    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public String getModifiedBy() {
        return ModifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        ModifiedBy = modifiedBy;
    }
}
