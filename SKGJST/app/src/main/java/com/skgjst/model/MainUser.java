package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 9/18/2018.
 */

public class MainUser implements Serializable {


    public String MUserID = "";

    public String getMUserID() {
        return MUserID;
    }

    public void setMUserID(String MUserID) {
        this.MUserID = MUserID;
    }

    public int getFamilyDetailID() {
        return FamilyDetailID;
    }

    public void setFamilyDetailID(int familyDetailID) {
        FamilyDetailID = familyDetailID;
    }

    public int FamilyDetailID;
    public String UserID = "";
    public String FName = "";
    public String MName = "";
    public String SurName = "";
    public String EmailID = "";
    public String PhotoURL = "";
    public String ContactNo = "";
    public String Gender = "";
    public String NativePlace = "";
    public String FatherName="";
    public String MotherName="";

    public String getMotherName() {
        return MotherName;
    }

    public void setMotherName(String motherName) {
        MotherName = motherName;
    }

    public String getFatherName() {
        return FatherName;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public String getSpouseName() {
        return SpouseName;
    }

    public void setSpouseName(String spouseName) {
        SpouseName = spouseName;
    }

    public String getFathersSurname() {
        return FathersSurname;
    }

    public void setFathersSurname(String fathersSurname) {
        FathersSurname = fathersSurname;
    }

    public String getFathersVillage() {
        return FathersVillage;
    }

    public void setFathersVillage(String fathersVillage) {
        FathersVillage = fathersVillage;
    }

    public String SpouseName="";
    public String FathersSurname="";
    public String FathersVillage="";

    public String getNativePlace() {
        return NativePlace;
    }

    public void setNativePlace(String nativePlace) {
        NativePlace = nativePlace;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getUserID() {
        return UserID;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public int FamilyID;

    public String getFName() {
        return FName;
    }

    public void setFName(String FName) {
        this.FName = FName;
    }

    public String getMName() {
        return MName;
    }

    public void setMName(String MName) {
        this.MName = MName;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String surName) {
        SurName = surName;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getPhotoURL() {
        return PhotoURL;
    }

    public void setPhotoURL(String photoURL) {
        PhotoURL = photoURL;
    }

    public String getContactNo() {
        return ContactNo;
    }

    public void setContactNo(String contactNo) {
        ContactNo = contactNo;
    }

    public int getFamilyID() {
        return FamilyID;
    }

    public void setFamilyID(int familyID) {
        FamilyID = familyID;
    }
}
