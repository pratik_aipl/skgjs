package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by vidhi-techmishty on 11/4/18.
 */

public class Profession implements Serializable {

    public String ProfessionID ="", ProfessionName="";

    public String getProfessionID() {
        return ProfessionID;
    }

    public void setProfessionID(String professionID) {
        ProfessionID = professionID;
    }

    public String getProfessionName() {
        return ProfessionName;
    }

    public void setProfessionName(String professionName) {
        ProfessionName = professionName;
    }
}
