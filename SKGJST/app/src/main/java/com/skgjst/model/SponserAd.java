package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 10/16/2018.
 */

public class SponserAd implements Serializable {
    public int img_logo;

    public int getImg_logo() {
        return img_logo;
    }

    public void setImg_logo(int img_logo) {
        this.img_logo = img_logo;
    }

    public int getImg_sponser() {
        return img_sponser;
    }

    public void setImg_sponser(int img_sponser) {
        this.img_sponser = img_sponser;
    }

    public int img_sponser;

    public SponserAd(int img_logo, int img_sponser) {
        this.img_logo = img_logo;
        this.img_sponser = img_sponser;

    }
}
