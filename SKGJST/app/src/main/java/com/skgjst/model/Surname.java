package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by vidhi-techmishty on 11/4/18.
 */

public class Surname implements Serializable {

    public String Surname="";
    public int mSurnameid;

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public int getmSurnameid() {
        return mSurnameid;
    }

    public void setmSurnameid(int mSurnameid) {
        this.mSurnameid = mSurnameid;
    }
}
