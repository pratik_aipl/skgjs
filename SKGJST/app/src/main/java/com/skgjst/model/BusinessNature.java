package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by vidhi-techmishty on 11/4/18.
 */

public class BusinessNature implements Serializable {

    public String IndustryName = "";
    public int IndustryID;

    public int getIndustryID() {
        return IndustryID;
    }

    public void setIndustryID(int industryID) {
        IndustryID = industryID;
    }

    public String getIndustryName() {
        return IndustryName;
    }

    public void setIndustryName(String industryName) {
        IndustryName = industryName;
    }
}
