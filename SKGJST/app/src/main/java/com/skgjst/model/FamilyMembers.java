package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by Karan - Empiere on 9/22/2018.
 */

public class FamilyMembers implements Serializable {

    public String FName = "", MName = "", SurName = "", Gender = "", PhotoURL = "", RelationName = "", Maritalstatus = "", FatherName = "", FathersSurname = "", FathersVillage = "", NativePlace = "", playerID = "";
    public int FamilyID = -1, FamilyDetailID = -1;

    public boolean MIsActive = false, IsAlive = false;

    public String getFatherName() {
        return FatherName;
    }

    public String getFathersVillage() {
        return FathersVillage;
    }

    public void setFathersVillage(String fathersVillage) {
        FathersVillage = fathersVillage;
    }

    public void setFatherName(String fatherName) {
        FatherName = fatherName;
    }

    public String getFathersSurname() {
        return FathersSurname;
    }

    public void setFathersSurname(String fathersSurname) {
        FathersSurname = fathersSurname;
    }


    public String getMaritalstatus() {
        return Maritalstatus;
    }

    public void setMaritalstatus(String maritalstatus) {
        Maritalstatus = maritalstatus;
    }


    public String getPlayerID() {
        return playerID;
    }

    public void setPlayerID(String playerID) {
        this.playerID = playerID;
    }

    public boolean isMIsActive() {
        return MIsActive;
    }

    public void setMIsActive(boolean MIsActive) {
        this.MIsActive = MIsActive;
    }

    public boolean isAlive() {
        return IsAlive;
    }

    public void setAlive(boolean alive) {
        IsAlive = alive;
    }

    public int getFamilyID() {
        return FamilyID;
    }

    public String getNativePlace() {
        return NativePlace;
    }

    public void setNativePlace(String nativePlace) {
        NativePlace = nativePlace;
    }

    public void setFamilyID(int familyID) {
        FamilyID = familyID;
    }

    public int getFamilyDetailID() {
        return FamilyDetailID;
    }

    public void setFamilyDetailID(int familyDetailID) {
        FamilyDetailID = familyDetailID;
    }


    public String getFName() {
        return FName;
    }

    public void setFName(String FName) {
        this.FName = FName;
    }

    public String getMName() {
        return MName;
    }

    public void setMName(String MName) {
        this.MName = MName;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String surName) {
        SurName = surName;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getPhotoURL() {
        return PhotoURL;
    }

    public void setPhotoURL(String photoURL) {
        PhotoURL = photoURL;
    }

    public String getRelationName() {
        return RelationName;
    }

    public void setRelationName(String relationName) {
        RelationName = relationName;
    }
}
