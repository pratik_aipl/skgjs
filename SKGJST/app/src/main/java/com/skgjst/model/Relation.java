package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by vidhi-techmishty on 11/4/18.
 */

public class Relation implements Serializable {

    public String RelationName="";

    public int RelationID;

    public int getRelationID() {
        return RelationID;
    }

    public void setRelationID(int relationID) {
        RelationID = relationID;
    }

    public String getRelationName() {
        return RelationName;
    }

    public void setRelationName(String relationName) {
        RelationName = relationName;
    }
}
