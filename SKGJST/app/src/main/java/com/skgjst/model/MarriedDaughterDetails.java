package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by vidhi-techmishty on 11/4/18.
 */

public class MarriedDaughterDetails implements Serializable {


    public String Surname = "", Name = "", HusbandName = "", FatherInLawName = "", village = "", MobileNo = "", Place = "", CountryCode = "";
    public int RelationID;

    public int getMarriedDaugDetailID() {
        return MarriedDaugDetailID;
    }

    public void setMarriedDaugDetailID(int marriedDaugDetailID) {
        MarriedDaugDetailID = marriedDaugDetailID;
    }

    public int MarriedDaugDetailID;


    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getHusbandName() {
        return HusbandName;
    }

    public void setHusbandName(String husbandName) {
        HusbandName = husbandName;
    }

    public String getFatherInLawName() {
        return FatherInLawName;
    }

    public void setFatherInLawName(String fatherInLawName) {
        FatherInLawName = fatherInLawName;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getPlace() {
        return Place;
    }

    public void setPlace(String place) {
        Place = place;
    }

    public int getRelationID() {
        return RelationID;
    }

    public void setRelationID(int relationID) {
        RelationID = relationID;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }
}
