package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 9/26/2018.
 */

public class DemiseData implements Serializable {

    public int DemiseID ;
    public String fullname = "";
    public int age;
    public String deathdate = "";
    public String deathinfo = "";
    public String photourl = "";

    public int getDemiseID() {
        return DemiseID;
    }

    public int getAge() {
        return age;
    }

    public int getFamilydetailid() {
        return familydetailid;
    }

    public int getPrathnaID() {
        return PrathnaID;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }


    public String personinfo= "";

    public String getPersoninfo() {
        return personinfo;
    }

    public void setPersoninfo(String personinfo) {
        this.personinfo = personinfo;
    }

    public String getDeathdate() {
        return deathdate;
    }

    public void setDeathdate(String deathdate) {
        this.deathdate = deathdate;
    }

    public String getDeathinfo() {
        return deathinfo;
    }

    public void setDeathinfo(String deathinfo) {
        this.deathinfo = deathinfo;
    }

    public String getPhotourl() {
        return photourl;
    }

    public void setPhotourl(String photourl) {
        this.photourl = photourl;
    }



    public String getPrathnaPlace() {
        return PrathnaPlace;
    }

    public void setPrathnaPlace(String prathnaPlace) {
        PrathnaPlace = prathnaPlace;
    }

    public String getPrathnainfo() {
        return Prathnainfo;
    }

    public void setPrathnainfo(String prathnainfo) {
        Prathnainfo = prathnainfo;
    }

    public String getPrathnaDate() {
        return PrathnaDate;
    }

    public void setPrathnaDate(String prathnaDate) {
        PrathnaDate = prathnaDate;
    }

    public void setDemiseID(int demiseID) {
        DemiseID = demiseID;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setFamilydetailid(int familydetailid) {
        this.familydetailid = familydetailid;
    }

    public void setPrathnaID(int prathnaID) {
        PrathnaID = prathnaID;
    }

    public int familydetailid ;
    public int PrathnaID ;
    public String PrathnaPlace = "";
    public String Prathnainfo = "";
    public String PrathnaDate = "";
}
