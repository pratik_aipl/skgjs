package com.skgjst.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 10/9/2018.
 */

public class NotificationType  implements Serializable{
    public String notification_name;

    public String getNotification_name() {
        return notification_name;
    }

    public void setNotification_name(String notification_name) {
        this.notification_name = notification_name;
    }

    public ArrayList<Notifications> notificationsArray = new ArrayList<>();

}
