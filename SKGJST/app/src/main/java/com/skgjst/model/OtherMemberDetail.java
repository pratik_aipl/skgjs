package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by vidhi-techmishty on 11/4/18.
 */

public class OtherMemberDetail implements Serializable {


    public String FName = "", MName = "", SurName = "", DateOfBirth = "",
            Gender = "", MarriageDate = "", Education = "", BusinessType = "",
            BusinessDetail = "", OfficeName = "", OfficeContNo = "", OfficeAddress = "", ContactNo = "",
            EmailID = "", PhotoURL = "", WebsiteURL = "", WorkProfession = "";

    public int RalationID, BloodGroupID, MaritalStatusID;

    public int FamilyDetailID;

    public int getFamilyDetailID() {
        return FamilyDetailID;
    }

    public void setFamilyDetailID(int familyDetailID) {
        FamilyDetailID = familyDetailID;
    }

    public String getFName() {
        return FName;
    }

    public void setFName(String FName) {
        this.FName = FName;
    }

    public String getMName() {
        return MName;
    }

    public void setMName(String MName) {
        this.MName = MName;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String surName) {
        SurName = surName;
    }

    public String getDOB() {
        return DateOfBirth;
    }

    public void setDOB(String DOB) {
        this.DateOfBirth = DOB;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getMarriageDate() {
        return MarriageDate;
    }

    public void setMarriageDate(String marriageDate) {
        MarriageDate = marriageDate;
    }

    public String getEducation() {
        return Education;
    }

    public void setEducation(String education) {
        Education = education;
    }

    public String getBusinessType() {
        return BusinessType;
    }

    public void setBusinessType(String businessType) {
        BusinessType = businessType;
    }

    public String getBusinessDetail() {
        return BusinessDetail;
    }

    public void setBusinessDetail(String businessDetail) {
        BusinessDetail = businessDetail;
    }

    public String getOfficeName() {
        return OfficeName;
    }

    public void setOfficeName(String officeName) {
        OfficeName = officeName;
    }

    public String getOfficeContNo() {
        return OfficeContNo;
    }

    public void setOfficeContNo(String officeContNo) {
        OfficeContNo = officeContNo;
    }

    public String getOfficeAddress() {
        return OfficeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        OfficeAddress = officeAddress;
    }

    public String getContactNo() {
        return ContactNo;
    }

    public void setContactNo(String contactNo) {
        ContactNo = contactNo;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getPhotoURL() {
        return PhotoURL;
    }

    public void setPhotoURL(String photoURL) {
        PhotoURL = photoURL;
    }

    public String getWebsiteURL() {
        return WebsiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        WebsiteURL = websiteURL;
    }

    public String getWorkProfession() {
        return WorkProfession;
    }

    public void setWorkProfession(String workProfession) {
        WorkProfession = workProfession;
    }

    public int getRalationID() {
        return RalationID;
    }

    public void setRalationID(int ralationID) {
        RalationID = ralationID;
    }

    public int getBloodGroupID() {
        return BloodGroupID;
    }

    public void setBloodGroupID(int bloodGroupID) {
        BloodGroupID = bloodGroupID;
    }

    public int getMaritalStatusID() {
        return MaritalStatusID;
    }

    public void setMaritalStatusID(int maritalStatusID) {
        MaritalStatusID = maritalStatusID;
    }
}
