package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by vidhi-techmishty on 19/4/18.
 */

public class BusinessList implements Serializable {


    public String FName = "", MName = "", SurName = "", ContactNo = "", EmailID = "", IndustryName = "", OfficeAddress = "", OfficeContNo = "", OfficeName = "", WorkProfession = "";
public String PhotoURL="";
public int FamilyDetailID=-1;
public int FamilyID=-1;

    public int getFamilyID() {
        return FamilyID;
    }

    public void setFamilyID(int familyID) {
        FamilyID = familyID;
    }

    public int getFamilyDetailID() {
        return FamilyDetailID;
    }

    public void setFamilyDetailID(int familyDetailID) {
        FamilyDetailID = familyDetailID;
    }

    public String getPhotoURL() {
        return PhotoURL;
    }

    public void setPhotoURL(String photoURL) {
        PhotoURL = photoURL;
    }

    public String getWorkProfession() {
        return WorkProfession;
    }

    public void setWorkProfession(String workProfession) {
        WorkProfession = workProfession;
    }

    public String getOfficeAddress() {
        return OfficeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        OfficeAddress = officeAddress;
    }

    public String getOfficeContNo() {
        return OfficeContNo;
    }

    public void setOfficeContNo(String officeContNo) {
        OfficeContNo = officeContNo;
    }

    public String getOfficeName() {
        return OfficeName;
    }

    public void setOfficeName(String officeName) {
        OfficeName = officeName;
    }

    public String getFName() {
        return FName;
    }

    public void setFName(String FName) {
        this.FName = FName;
    }

    public String getMName() {
        return MName;
    }

    public void setMName(String MName) {
        this.MName = MName;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String surName) {
        SurName = surName;
    }

    public String getContactNo() {
        return ContactNo;
    }

    public void setContactNo(String contactNo) {
        ContactNo = contactNo;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getIndustryName() {
        return IndustryName;
    }

    public void setIndustryName(String industryName) {
        IndustryName = industryName;
    }
}
