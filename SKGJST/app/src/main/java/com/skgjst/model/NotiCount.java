package com.skgjst.model;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 10/19/2018.
 */

public class NotiCount implements Serializable {
    public int notid = -1;
    public int familydetailid = -1;
    public int countvalue = -1;

    public int getNotid() {
        return notid;
    }

    public void setNotid(int notid) {
        this.notid = notid;
    }

    public int getFamilydetailid() {
        return familydetailid;
    }

    public void setFamilydetailid(int familydetailid) {
        this.familydetailid = familydetailid;
    }

    public int getCountvalue() {
        return countvalue;
    }

    public void setCountvalue(int countvalue) {
        this.countvalue = countvalue;
    }

    public String getNotification_type() {
        return notification_type;
    }

    public void setNotification_type(String notification_type) {
        this.notification_type = notification_type;
    }

    public String notification_type = "";
}
