package com.skgjst.fragment;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;

import com.skgjst.BaseFragment;
import com.skgjst.adapter.NotificationAdapter;
import com.skgjst.App;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.callinterface.NotificationListener;
import com.skgjst.model.NotificationType;
import com.skgjst.model.Notifications;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.skgjst.activities.DashBoardActivity.frame_slecte;
import static com.skgjst.activities.DashBoardActivity.tv_mydemise;
import static com.skgjst.activities.DashBoardActivity.tv_title;

/**
 * Created by Karan - Empiere on 9/19/2018.
 */

public class NotificationFragment extends BaseFragment implements AsynchTaskListner,NotificationListener {
    public View view;
    public RecyclerView rv_product_list;
    public LinearLayout lin_empty;
    public JsonParserUniversal jParser;
    public ArrayList<Notifications> birthdayArrayList = new ArrayList<>();
    public ArrayList<NotificationType> headerArrayList = new ArrayList<>();
    public NotificationType notiTypeObj;
    List<String> listDataHeader;
    HashMap<String, ArrayList<Notifications>> listDataChild;
    ExpandableListView expListView;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_notification, container, false);
        tv_title.setText("NOTIFICATION");
        jParser = new JsonParserUniversal();
        frame_slecte.setVisibility(View.VISIBLE);
        tv_mydemise.setVisibility(View.GONE);
        expListView = (ExpandableListView) view.findViewById(R.id.lvExp);
        App.isAdded=false;
        listDataHeader = new ArrayList<String>();

        for (int i = 0; i < 3; i++) {
            notiTypeObj = new NotificationType();
            if (i == 0)
                notiTypeObj.setNotification_name("Birthday");

            if (i == 1)
                notiTypeObj.setNotification_name("Anniversary");
            if (i == 2)
                notiTypeObj.setNotification_name("General");

            headerArrayList.add(notiTypeObj);
        }

        new CallRequest(this).getNotifications(user);
        return view;
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            //    Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getNotifications:
                    birthdayArrayList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                         //   if (jObj.getJSONArray("Notificationdata") != null && jObj.getJSONArray("Notificationdata").length() > 0) {
//                                lin_empty.setVisibility(View.GONE);
                                //   rv_product_list.setVisibility(View.VISIBLE);
                                JSONArray userData = jObj.getJSONArray("Notificationdata");
                                for (int i = 0; i < userData.length(); i++) {
                                    JSONObject obj = userData.getJSONObject(i);
                                    Notifications notiObj = (Notifications) jParser.parseJson(obj, new Notifications());
                                    if (notiObj.getNotification_type().equalsIgnoreCase("Birthday")) {
                                        headerArrayList.get(0).notificationsArray.add(notiObj);
                                    }
                                    if (notiObj.getNotification_type().equalsIgnoreCase("Anniversary")) {
                                        headerArrayList.get(1).notificationsArray.add(notiObj);
                                    }

                                    if (notiObj.getNotification_type().equalsIgnoreCase("Other")) {
                                        headerArrayList.get(2).notificationsArray.add(notiObj);
                                    }


                                }
                                  NotificationAdapter listAdapter = new NotificationAdapter(NotificationFragment.this, headerArrayList,expListView,user);

                                expListView.setAdapter(listAdapter);

                           /* } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("UMessage"), getActivity());
                                //   lin_empty.setVisibility(View.VISIBLE);
                                //    rv_product_list.setVisibility(View.GONE);
                            }*/


                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), getActivity());
                            // lin_empty.setVisibility(View.VISIBLE);
                            //  rv_product_list.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {

                        Utils.hideProgressDialog();
                        Utils.showToast("UMessage", getActivity());
                        //   lin_empty.setVisibility(View.VISIBLE);
                        //   rv_product_list.setVisibility(View.GONE);
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case getWatched:
                    Utils.hideProgressDialog();
                   JSONObject jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            //   Utils.showToast(jObj.getString("UMessage"), this);
                            new CallRequest(this).getNotifications(user);

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

    @Override
    public void notificationWatch(int notification_id) {
        new CallRequest(this).getWatched(user, String.valueOf(notification_id));

    }
}
