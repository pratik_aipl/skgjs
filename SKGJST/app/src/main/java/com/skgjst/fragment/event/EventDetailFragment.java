package com.skgjst.fragment.event;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.skgjst.BaseFragment;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.EventImages;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventDetailFragment extends BaseFragment implements AsynchTaskListner {

    public TextView tvDate, tvLocation, tvEventTitle, tvDescription;
    //public ImageView ivImage;
    public int eventId;
    public JsonParserUniversal jParser;
    RecyclerView rvImages;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<EventImages> imageList = new ArrayList<>();
    private EventDetailsAdapter eventDetailsAdapter;

    public EventDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        Bundle bundle = getArguments();

        View view = inflater.inflate(R.layout.fragment_event_detail, container, false);

        tvEventTitle = view.findViewById(R.id.tvEventTitle);
        tvDate = view.findViewById(R.id.tvDate);
        tvLocation = view.findViewById(R.id.tvLocation);
        tvDescription = view.findViewById(R.id.tvDescription);
        rvImages = view.findViewById(R.id.rvImages);
        //ivImage = view.findViewById(R.id.ivImage);

        jParser = new JsonParserUniversal();
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rvImages.setLayoutManager(layoutManager);

        if (bundle != null) {
            eventId = bundle.getInt("id");
            tvEventTitle.setText(bundle.getString("title"));
            tvDate.setText(bundle.getString("date"));
            tvLocation.setText(bundle.getString("location"));
            tvDescription.setText(bundle.getString("description"));

            new CallRequest(EventDetailFragment.this).GetEventImagesList(eventId);

        }


        return view;
    }

    public void switchFragment(Fragment fragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, fragment, fragment.getClass().getSimpleName());
        transaction.addToBackStack(fragment.getClass().getSimpleName());
        transaction.commit();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            Utils.hideProgressDialog();
            switch (request) {
                case GetEventImages:
                    imageList.clear();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            JSONArray jObjData = jObj.getJSONArray("data");
                            if (jObjData != null) {
                                for (int i = 0; i < jObjData.length(); i++) {
                                    JSONObject jPackag = jObjData.getJSONObject(i);
                                    EventImages pastEvent = (EventImages) jParser.parseJson(jPackag, new EventImages());
                                    imageList.add(pastEvent);
                                }

                                System.out.println("size" + imageList.size());

                         //       eventDetailsAdapter = new EventDetailsAdapter(EventDetailFragment.this, imageList);
                                rvImages.setAdapter(eventDetailsAdapter);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
