package com.skgjst.fragment.member;


import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.skgjst.BaseFragment;
import com.skgjst.activities.membersearch.MemberList;
import com.skgjst.R;
import com.skgjst.utils.ImagePopup;
import com.skgjst.utils.PicassoTrustAll;
import com.squareup.picasso.Callback;

/**
 * A simple {@link Fragment} subclass.
 */
public class MemberSearchDetailFragment extends BaseFragment {

    ImageView ivUser;
    TextView tvFirstName, tvMiddleName, tvSurname, tvGrandFatherName, tvNativePlace, tvDateOfBirth, tvGender, tvWing, tvRoomNumber,
            tvBuildingName, tvRoadName, tvSuburbName, tvCity, tvPincode, tvContactNumber;

    MemberList memberList;
    ImageView imgCall;

    public MemberSearchDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_member_search_detail, container, false);

        Bundle bundle = getArguments();
        if (bundle != null) {
            memberList = (MemberList) bundle.getSerializable("member_search");
        }

        ivUser = view.findViewById(R.id.ivUser);
        tvFirstName = view.findViewById(R.id.tvFirstName);
        tvMiddleName = view.findViewById(R.id.tvMiddleName);
        tvSurname = view.findViewById(R.id.tvSurname);
        tvGrandFatherName = view.findViewById(R.id.tvGrandFatherName);
        tvNativePlace = view.findViewById(R.id.tvNativePlace);
        tvDateOfBirth = view.findViewById(R.id.tvDateOfBirth);
        tvGender = view.findViewById(R.id.tvGender);
        tvWing = view.findViewById(R.id.tvWing);
        tvRoomNumber = view.findViewById(R.id.tvRoomNumber);
        tvBuildingName = view.findViewById(R.id.tvBuildingName);
        tvRoadName = view.findViewById(R.id.tvRoadName);
        tvSuburbName = view.findViewById(R.id.tvSuburbName);
        tvCity = view.findViewById(R.id.tvCity);
        tvPincode = view.findViewById(R.id.tvPincode);
        tvContactNumber = view.findViewById(R.id.tvContactNumber);
        imgCall = view.findViewById(R.id.imgCall);
        
        setData();

        return view;
    }

    private void setData() {

        if (!TextUtils.isEmpty(memberList.getPhotoURL())) {
            try {
                PicassoTrustAll.getInstance(getActivity())
                        .load(memberList.getPhotoURL().replace(" ", "%20"))
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar)
                        .resize(256, 256)
                        .into(ivUser, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        final ImagePopup imagePopup = new ImagePopup(getActivity());
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.BLACK);  // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(true);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional

        imagePopup.initiatePopupWithPicasso(memberList.getPhotoURL().replace(" ", "%20"));

        ivUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagePopup.viewPopup();
            }
        });

        if (memberList.getFName() != null && !TextUtils.isEmpty(memberList.getFName())) {
            tvFirstName.setText(memberList.getFName());
        } else {
            tvFirstName.setText("Not available");
        }

        if (memberList.getMName() != null && !TextUtils.isEmpty(memberList.getMName())) {
            tvMiddleName.setText(memberList.getMName());
        } else {
            tvMiddleName.setText("Not available");
        }

        if (memberList.getSurName() != null && !TextUtils.isEmpty(memberList.getSurName())) {
            tvSurname.setText(memberList.getSurName());
        } else {
            tvSurname.setText("Not available");
        }

        if (memberList.getGrandFatherName() != null && !TextUtils.isEmpty(memberList.getGrandFatherName())) {
            tvGrandFatherName.setText(memberList.getGrandFatherName());
        } else {
            tvGrandFatherName.setText("Not available");
        }

        if (memberList.getNativePlace() != null && !TextUtils.isEmpty(memberList.getNativePlace())) {
            tvNativePlace.setText(memberList.getNativePlace());
        } else {
            tvNativePlace.setText("Not available");
        }

        if (memberList.getAge() != 0) {
            tvDateOfBirth.setText(String.valueOf(memberList.getAge()));
        } else {
            tvDateOfBirth.setText("Not available");
        }

        if (memberList.getGender() != null && !TextUtils.isEmpty(memberList.getGender())) {
            tvGender.setText(memberList.getGender());
        } else {
            tvGender.setText("Not available");
        }

        if (memberList.getWing() != null && !TextUtils.isEmpty(memberList.getWing())) {
            tvWing.setText(memberList.getWing());
        } else {
            tvWing.setText("Not available");
        }

        if (memberList.getRoomNo() != null && !TextUtils.isEmpty(memberList.getRoomNo())) {
            tvRoomNumber.setText(memberList.getRoomNo());
        } else {
            tvRoomNumber.setText("Not available");
        }

        if (memberList.getBuildingName() != null && !TextUtils.isEmpty(memberList.getBuildingName())) {
            tvBuildingName.setText(memberList.getBuildingName());
        } else {
            tvBuildingName.setText("Not available");
        }

        if (memberList.getRoadName() != null && !TextUtils.isEmpty(memberList.getRoadName())) {
            tvRoadName.setText(memberList.getRoadName());
        } else {
            tvRoadName.setText("Not available");
        }

        if (memberList.getSuburbName() != null && !TextUtils.isEmpty(memberList.getSuburbName())) {
            tvSuburbName.setText(memberList.getSuburbName());
        } else {
            tvSuburbName.setText("Not available");
        }

        if (memberList.getLocationName() != null && !TextUtils.isEmpty(memberList.getLocationName())) {
            tvCity.setText(memberList.getLocationName());
        } else {
            tvCity.setText("Not available");
        }

        if (memberList.getPincode() != null && !TextUtils.isEmpty(memberList.getPincode())) {
            tvPincode.setText(memberList.getPincode());
        } else {
            tvPincode.setText("Not available");
        }

        if (memberList.getContactNo() != null && !TextUtils.isEmpty(memberList.getContactNo())) {
            tvContactNumber.setText(memberList.getContactNo());
            imgCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", memberList.getContactNo(), null));
                    startActivity(intent);
                }
            });
        } else {
            tvContactNumber.setText("Not available");
        }
        
    }

}
