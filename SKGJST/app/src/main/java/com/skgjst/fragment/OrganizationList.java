package com.skgjst.fragment;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Krupa Kakkad on 16 June 2018
 */
public class OrganizationList implements Serializable{

    public String OrgName="", OrgAddress="", OrgCntPersonName="", OrgContctNo="", OrgContEmail="", OrgLogo="", OrgDesc="", WebsiteUrl="", Place="", OrgContctNo2="";

    public ArrayList<TrusteeDetails> Trustee_Details;

    public ArrayList<TrusteeDetails> getTrustee_Details() {
        return Trustee_Details;
    }

    public void setTrustee_Details(ArrayList<TrusteeDetails> trustee_Details) {
        Trustee_Details = trustee_Details;
    }

    public String getOrgName() {
        return OrgName;
    }

    public void setOrgName(String orgName) {
        OrgName = orgName;
    }

    public String getOrgAddress() {
        return OrgAddress;
    }

    public void setOrgAddress(String orgAddress) {
        OrgAddress = orgAddress;
    }

    public String getOrgCntPersonName() {
        return OrgCntPersonName;
    }

    public void setOrgCntPersonName(String orgCntPersonName) {
        OrgCntPersonName = orgCntPersonName;
    }

    public String getOrgContctNo() {
        return OrgContctNo;
    }

    public void setOrgContctNo(String orgContctNo) {
        OrgContctNo = orgContctNo;
    }

    public String getOrgContEmail() {
        return OrgContEmail;
    }

    public void setOrgContEmail(String orgContEmail) {
        OrgContEmail = orgContEmail;
    }

    public String getOrgLogo() {
        return OrgLogo;
    }

    public void setOrgLogo(String orgLogo) {
        OrgLogo = orgLogo;
    }

    public String getOrgDesc() {
        return OrgDesc;
    }

    public void setOrgDesc(String orgDesc) {
        OrgDesc = orgDesc;
    }

    public String getWebsiteUrl() {
        return WebsiteUrl;
    }

    public void setWebsiteUrl(String websiteUrl) {
        WebsiteUrl = websiteUrl;
    }

    public String getPlace() {
        return Place;
    }

    public void setPlace(String place) {
        Place = place;
    }

    public String getOrgContctNo2() {
        return OrgContctNo2;
    }

    public void setOrgContctNo2(String orgContctNo2) {
        OrgContctNo2 = orgContctNo2;
    }
}
