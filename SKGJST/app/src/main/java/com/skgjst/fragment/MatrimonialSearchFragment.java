package com.skgjst.fragment;


import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.skgjst.App;
import com.skgjst.BaseFragment;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.Gender;
import com.skgjst.model.MaritalStatus;
import com.skgjst.model.MatrimonialList;
import com.skgjst.model.Village;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.skgjst.activities.DashBoardActivity.tv_title;

/**
 * Created by vidhi-techmishty on 2/4/18.
 */

public class MatrimonialSearchFragment extends BaseFragment implements AsynchTaskListner {

    public View v;
    public RecyclerView listView;
    public MatrimonialSearchAdapter matrimonialSearchAdapter;
    public ImageView imgSearch, imgClose;
    public String genderId = "", villageId = "", maritalStatusId = "", manglikId = "", skinColorId = "", handicapId = "";
    public Gender gender;
    public Village village;
    public MaritalStatus maritalStatus;

    public JsonParserUniversal jParser;
    public Spinner spGender, spMaritalStatus, spVillage, spSkinColor, spisManglik, spHandicap, spMinAge, spMaxAge;
    public LinearLayout llFilter;
    //public EditText etFromAge, etToAge;
    public Dialog dialog;
    public ArrayList<String> manglikList = new ArrayList<>();
    public ArrayList<String> skinColorList = new ArrayList<>();
    public ArrayList<String> handicapList = new ArrayList<>();
    public ArrayList<MatrimonialList> matrimonialArrayList = new ArrayList<>();
    public TextView tvCount, tvNoSearchItems;
    int checkGender = 0;
    private String minAge, maxAge;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<String> minAgeList = new ArrayList<>();
    private ArrayList<String> maxAgeList = new ArrayList<>();
    public FloatingActionButton float_add;

    @Override

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_matrimonial_search, container, false);
        jParser = new JsonParserUniversal();
        float_add = v.findViewById(R.id.float_add);

        listView = (RecyclerView) v.findViewById(R.id.listView);
        tvCount = v.findViewById(R.id.tvCount);
        tvNoSearchItems = v.findViewById(R.id.tvNoSearchItems);

        layoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(layoutManager);

        new CallRequest(MatrimonialSearchFragment.this).MatrimonialList();

        if (matrimonialArrayList.size() > 0) {
            tvCount.setText(String.valueOf(matrimonialArrayList.size()) + " " + "Members");
            layoutManager = new LinearLayoutManager(getActivity());
            listView.setLayoutManager(layoutManager);
          //  matrimonialSearchAdapter = new MatrimonialSearchAdapter(MatrimonialSearchFragment.this, getActivity(), matrimonialArrayList);
            listView.setAdapter(matrimonialSearchAdapter);
            tvNoSearchItems.setVisibility(View.GONE);
        }

        imgSearch = v.findViewById(R.id.imgSearch);
        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog = new Dialog(getActivity());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.dialog_matrimonial_search);

                spGender = dialog.findViewById(R.id.spinnerGender);
              //  spMaritalStatus = dialog.findViewById(R.id.spinnerStatus);
                spVillage = dialog.findViewById(R.id.spinnerVillage);
                spHandicap = dialog.findViewById(R.id.spinnerHandicap);
             //   spisManglik = dialog.findViewById(R.id.spinnerManklik);
                spSkinColor = dialog.findViewById(R.id.spinnerSkinColor);
                spMinAge = dialog.findViewById(R.id.spinnerMinAge);
                spMaxAge = dialog.findViewById(R.id.spinnerMaxAge);

                /*etFromAge = dialog.findViewById(R.id.etFromAge);
                etToAge = dialog.findViewById(R.id.etToAge);*/

                minAgeList.clear();
                maxAgeList.clear();

                minAgeList.add("From Age");
                maxAgeList.add("To Age");

                for (int i = 18; i < 70; i++) {
                    minAgeList.add(String.valueOf(i));
                }

                for (int i = 19; i <= 70; i++) {
                    maxAgeList.add(String.valueOf(i));
                }

                // for min age
                ArrayAdapter aaMinAge = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, minAgeList);
                aaMinAge.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spMinAge.setAdapter(aaMinAge);

                // for max age
                ArrayAdapter aaMaxAge = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, maxAgeList);
                aaMaxAge.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spMaxAge.setAdapter(aaMaxAge);

                spMinAge.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        try {
                            if (i != 0) {
                                minAge = String.valueOf(minAgeList.get(i));
                                System.out.println("Min age::::" + minAge);

                                maxAgeList.clear();
                                maxAgeList.add("To Age");
                                for (int j = Integer.parseInt(minAgeList.get(i)) + 1; j <= 70; j++) {
                                    maxAgeList.add(String.valueOf(j));
                                }

                                ArrayAdapter aaMaxAge = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, maxAgeList);
                                aaMaxAge.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spMaxAge.setAdapter(aaMaxAge);

                            } else {
                                minAge = "0";

                                maxAgeList.clear();
                                maxAgeList.add("To Age");
                                for (int j = 19; j <= 70; j++) {
                                    maxAgeList.add(String.valueOf(j));
                                }
                                // for max age
                                ArrayAdapter aaMaxAge = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, maxAgeList);
                                aaMaxAge.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spMaxAge.setAdapter(aaMaxAge);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        minAge = "0";
                    }
                });

                spMaxAge.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        try {
                            if (i != 0) {
                                maxAge = String.valueOf(maxAgeList.get(i));
                                System.out.println("Min age::::" + maxAge);
                            } else {
                                maxAge = "0";
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        maxAge = "0";
                    }
                });

                manglikList.clear();
                manglikList.add("Select Manglik");
                manglikList.add("No");
                manglikList.add("Yes");

                ArrayAdapter aaManglik = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, manglikList);
                aaManglik.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spisManglik.setAdapter(aaManglik);

                spisManglik.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                        System.out.println("**ManglikId::::" + i);

                        if (i == 0) {
                            manglikId = "0";
                        } else if (i == 1) {
                            manglikId = "NO";
                        } else if (i == 2) {
                            manglikId = "YES";
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }

                });


                skinColorList.clear();
                skinColorList.add("Select Skin Color");
                skinColorList.add("Fair");
                skinColorList.add("Moderate");
                skinColorList.add("Brownish");
                skinColorList.add("Blackish");
                skinColorList.add("Dark");

                ArrayAdapter aaSkinColor = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, skinColorList);
                aaSkinColor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spSkinColor.setAdapter(aaSkinColor);
                spSkinColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                        System.out.println("**SkinColor::::" + i);

                        if (i == 0) {
                            skinColorId = "0";
                        } else if (i == 1) {
                            skinColorId = "Fair";
                        } else if (i == 2) {
                            skinColorId = "Moderate";
                        } else if (i == 3) {
                            skinColorId = "Brownish";
                        } else if (i == 4) {
                            skinColorId = "Blackish";
                        } else if (i == 5) {
                            skinColorId = "Dark";
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }

                });
                handicapList.clear();
                handicapList.add("Select Handicap");
                handicapList.add("No");
                handicapList.add("Yes");
                ArrayAdapter aaHandicap = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, handicapList);
                aaHandicap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spHandicap.setAdapter(aaHandicap);

                spHandicap.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                        System.out.println("**handicap id::::" + i);

                        if (i == 0) {
                            handicapId = "0";
                        } else if (i == 1) {
                            handicapId = "NO";
                        } else if (i == 2) {
                            handicapId = "YES";
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }

                });

               // llFilter = dialog.findViewById(R.id.ll_Fliter);

                if (App.villageArrayList.size() == 0) {
                    new CallRequest(MatrimonialSearchFragment.this).VillageList();

                } else {
                    ArrayAdapter aaVillage = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, App.villageArrayName);
                    aaVillage.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spVillage.setAdapter(aaVillage);
                    //   spVillage.setAdapter(new NothingSelectedSpinnerAdapter(aaVillage, R.layout.spinner_nothing_selected, getActivity(), "Select Village"));
                    spVillage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            try {
                                if (i != 0) {
                                    villageId = String.valueOf(App.villageArrayList.get(i).getVillagename());
                                    System.out.println("VollageId::::" + villageId);
                                } else {
                                    villageId = "0";
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });

                }


                if (App.maritalStatusArrayList.size() == 0) {
                    new CallRequest(MatrimonialSearchFragment.this).MaritalStatus();

                } else {

                    ArrayAdapter aaState = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, App.maritalStatusArrayName);
                    aaState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spMaritalStatus.setAdapter(aaState);

                    spMaritalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            try {
                                if (i != 0) {
                                    maritalStatusId = String.valueOf(App.maritalStatusArrayList.get(i).getMaritalstatusID());
                                    System.out.println("MaritalId::::" + maritalStatusId);
                                } else {
                                    maritalStatusId = "0";
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                        }
                    });
                }

                if (App.genderArrayList.size() == 0) {
                    new CallRequest(MatrimonialSearchFragment.this).Gender_List();

                } else {

                    ArrayAdapter aaCity = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, App.genderArrayName);
                    aaCity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spGender.setAdapter(aaCity);
                    spGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            try {
                                if (i != 0) {
                                    genderId = App.genderArrayList.get(i).Gender;
                                    System.out.println("GenderId::::" + genderId);
                                } else {
                                    genderId = "0";
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                genderId = "0";
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                            genderId = "0";
                        }

                    });
                }


                llFilter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        /*String fromAge = etFromAge.getText().toString();
                        String tomAge = etToAge.getText().toString();
                        try {

                            if (!fromAge.isEmpty() && !tomAge.isEmpty())
                                if (Integer.parseInt(tomAge) < Integer.parseInt(fromAge)) {
                                    Utils.showToast("Please enter proper data in from age and to age", getActivity());
                                    return;
                                }
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                            return;
                        }

                        fromAge = ((fromAge.isEmpty()) ? "0" : fromAge);
                        tomAge = ((tomAge.isEmpty()) ? "0" : tomAge);*/

                        new CallRequest(MatrimonialSearchFragment.this).Matrimonialbysearch(manglikId, handicapId, minAge, maxAge, villageId, genderId, skinColorId);


                    }
                });

                imgClose = dialog.findViewById(R.id.imgClose);
                imgClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        genderId = "";
                        villageId = "";
                        maritalStatusId = "";
                        manglikId = "";
                        skinColorId = "";
                        handicapId = "";
                        dialog.dismiss();
                    }
                });
                dialog.show();

            }
        });
        tv_title.setText("MATRIMONIAL SEARCH");
        App.editProfile = "";
float_add.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        switchFragment(new MatrimonialDetailFragment());
    }
});
        return v;
    }

    public void switchFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, fragment, fragment.getClass().getSimpleName());
        transaction.addToBackStack(fragment.getClass().getSimpleName());
        transaction.commit();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {

                case VillageList:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jBranchArray = jObj.getJSONArray("data");
                                Utils.addDefaultVillage("Select Village");
                                for (int i = 0; i < jBranchArray.length(); i++) {
                                    JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    village = (Village) jParser.parseJson(jPackag, new Village());
                                    App.villageArrayList.add(village);
                                    App.villageArrayName.add(village.getVillagename());
                                }

                                System.out.println("LIst size::::" + App.villageArrayList.size());

                                ArrayAdapter aaVillage = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, App.villageArrayName);
                                aaVillage.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spVillage.setAdapter(aaVillage);
                                spVillage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        try {
                                            if (i != 0) {
                                                villageId = String.valueOf(App.villageArrayList.get(i).getVillagename());
                                                System.out.println("VollageId::::" + villageId);
                                            } else {
                                                villageId = "";
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {
                                    }
                                });
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;


                case MaritalStatus:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jBranchArray = jObj.getJSONArray("data");
                                Utils.addDefaultMaritalStatus();
                                for (int i = 0; i < jBranchArray.length(); i++) {
                                    JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    maritalStatus = (MaritalStatus) jParser.parseJson(jPackag, new MaritalStatus());
                                    App.maritalStatusArrayList.add(maritalStatus);
                                    App.maritalStatusArrayName.add(maritalStatus.getMaritalstatus());

                                }
                                ArrayAdapter aaState = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, App.maritalStatusArrayName);
                                aaState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spMaritalStatus.setAdapter(aaState);


                                spMaritalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        try {
                                            maritalStatusId = String.valueOf(App.maritalStatusArrayList.get(i).getMaritalstatusID());
                                            System.out.println("MaritalId::::" + maritalStatusId);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {
                                    }
                                });
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case Gender_List:
                    Utils.hideProgressDialog();
                    /*genderArrayList.clear();
                    genderArrayName.clear();*/
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jBranchArray = jObj.getJSONArray("data");

                                Utils.addDefaultGender();

                                for (int i = 0; i < jBranchArray.length(); i++) {

                                    JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    String gender = jPackag.getString("gender");
                                    String Gender = jPackag.getString("Gender");

                                    Gender gender1 = new Gender();
                                    gender1.gender = gender;
                                    gender1.Gender = Gender;

                                    App.genderArrayList.add(gender1);
                                    App.genderArrayName.add(gender);

                                    /*JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    gender = (Gender) jParser.parseJson(jPackag, new Gender());
                                    App.genderArrayList.add(gender);
                                    App.genderArrayName.add(gender.getGender());
                                */
                                }

                                ArrayAdapter aaCity = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, App.genderArrayName);
                                aaCity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spGender.setAdapter(aaCity);

                                spGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        try {
                                            if (i != 0) {
                                                genderId = App.genderArrayList.get(i).gender;
                                                System.out.println("GenderId::::" + genderId);
                                            } else {
                                                genderId = "0";
                                                ;
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {
                                        genderId = "";
                                    }
                                });
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;


                case MatrimonialList:
                    Utils.hideProgressDialog();
                    matrimonialArrayList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            JSONArray jBranchArray = jObj.getJSONArray("data");


                            for (int i = 0; i < jBranchArray.length(); i++) {
                                JSONObject jPackag = jBranchArray.getJSONObject(i);
                                MatrimonialList matrimonialList = (MatrimonialList) jParser.parseJson(jPackag, new MatrimonialList());
                                matrimonialArrayList.add(matrimonialList);
                            }


                            System.out.println("size" + matrimonialArrayList.size());

                       //     matrimonialSearchAdapter = new MatrimonialSearchAdapter(MatrimonialSearchFragment.this, getActivity(), matrimonialArrayList);
                            listView.setAdapter(matrimonialSearchAdapter);

                            tvCount.setText(String.valueOf(matrimonialArrayList.size()) + " members");

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;

                case Matrimonialbysearch:
                    matrimonialArrayList.clear();
                    tvNoSearchItems.setVisibility(View.GONE);
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            JSONArray jBranchArray = jObj.getJSONArray("data");

                            if (jBranchArray.length() > 0) {
                                for (int i = 0; i < jBranchArray.length(); i++) {
                                    JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    MatrimonialList matrimonialList = (MatrimonialList) jParser.parseJson(jPackag, new MatrimonialList());
                                    matrimonialArrayList.add(matrimonialList);
                                }

                            } else {
                                Utils.showToast(jObj.getString("Message"), getActivity());
                            }

                            dialog.dismiss();
                            genderId = "";
                            villageId = "";
                            maritalStatusId = "";
                            manglikId = "";
                            skinColorId = "";
                            handicapId = "";
                            System.out.println("size" + matrimonialArrayList.size());

                     //       matrimonialSearchAdapter = new MatrimonialSearchAdapter(MatrimonialSearchFragment.this, getActivity(), matrimonialArrayList);
                            listView.setAdapter(matrimonialSearchAdapter);
                            tvCount.setText(String.valueOf(matrimonialArrayList.size()) + " members");

                        } else {
                            Utils.showToast(jObj.getString("Message"), getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
            }
        }
    }
}

