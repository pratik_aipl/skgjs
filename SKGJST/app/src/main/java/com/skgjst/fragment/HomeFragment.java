package com.skgjst.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ViewFlipper;

import com.skgjst.App;
import com.skgjst.BaseFragment;
import com.skgjst.R;
import com.skgjst.adapter.HomeAdapter;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.utils.Constant;
import com.skgjst.utils.ImagePopup;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.Collections;

import static com.skgjst.activities.DashBoardActivity.frame_slecte;
import static com.skgjst.activities.DashBoardActivity.tv_mydemise;
import static com.skgjst.activities.DashBoardActivity.tv_title;

/**
 * Created by Karan - Empiere on 9/18/2018.
 */

public class HomeFragment extends BaseFragment implements AsynchTaskListner {
    public RecyclerView recycler_img;
    public View view;
    int[] imageId = {
            R.drawable.member_search,
            R.drawable.business,
            R.drawable.matrimonial,
            R.drawable.events,
            R.drawable.job_icon,
            R.drawable.organization};
    public Runnable mRunnable, mRunnableGold, mRunnableBusi;
    public Handler handler, handlerGold, handlerBusi;
    public int currentDiamondPos, currentGoldPos, currentBusiPos;
    public ImageView img_gold_ad, img_business_ad, img_diamond_ad;
    String[] textViewId = {"MEMBER SEARCH", "BUSINESS", "MATRIMONIAL", "EVENTS",
            "JOBS", "ORGANIZATIONS"};

    int[] backgroundColor = {R.color.businessback, R.color.eventsback, R.color.matrimonialback,
            R.color.photogalaryback, R.color.newsback, R.color.jarvidhiback};
    public ProgressBar pbar_business, pbar_gold, pbar_diamond;
    public static Dialog dialog;
    public Context context;
    public ProgressBar imageProgress;
    private ViewFlipper viewFlipper;
    private float initialX;

    @Override
    public void onResume() {
        super.onResume();
        startDiamondAdAnimation();
        startGoldAdAnimation();
        startBusinessAdAnimation();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_home, container, false);
        context = getActivity();
        tv_title.setText("HOME");
        App.isAdded = false;
        recycler_img = view.findViewById(R.id.recycler_img);
        img_gold_ad = view.findViewById(R.id.img_gold_ad);
        img_business_ad = view.findViewById(R.id.img_business_ad);
        img_diamond_ad = view.findViewById(R.id.img_diamond_ad);
        tv_mydemise.setVisibility(View.GONE);
        frame_slecte.setVisibility(View.VISIBLE);
        pbar_gold = view.findViewById(R.id.pbar_gold);
        pbar_business = view.findViewById(R.id.pbar_business);
        pbar_diamond = view.findViewById(R.id.pbar_diamond);

        StaggeredGridLayoutManager sgaggeredGridLayoutManager = new StaggeredGridLayoutManager(3, 1);
        recycler_img.setLayoutManager(sgaggeredGridLayoutManager);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_right);
        recycler_img.setLayoutAnimation(controller);
        recycler_img.setHasFixedSize(true);

        final HomeAdapter adapter = new HomeAdapter(getActivity(), imageId, textViewId, backgroundColor);
        recycler_img.setAdapter(adapter);


        if (App.isfirst && App.platinumAdvertiseMent.size() > 0) {
            App.isfirst = false;
            openBusinessDialog();
        }

        startDiamondAdAnimation();
        startGoldAdAnimation();
        startBusinessAdAnimation();

        return view;
    }

    @SuppressLint("ClickableViewAccessibility")
    public void openBusinessDialog() {
        createDialog();
        initDialogComponents();

        Collections.shuffle(App.platinumAdvertiseMent);

        for (int i = 0; i < App.platinumAdvertiseMent.size(); i++) {
            final ImageView image = new ImageView(context);
            image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

            imageProgress = new ProgressBar(context);
            if (!TextUtils.isEmpty(App.platinumAdvertiseMent.get(i).getFilepath())) {
                try {
                    Picasso.with(context).load(App.platinumAdvertiseMent.get(i).getFilepath().replace(" ", "%20")).resize(1048, 1048).into(image, new Callback() {
                        @Override
                        public void onSuccess() {
                            imageProgress.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            image.setImageResource(R.drawable.no_image);
                        }

                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                image.setImageResource(R.drawable.no_image);
            }

            viewFlipper.addView(image);
        }


        viewFlipper.setInAnimation(getActivity(), android.R.anim.fade_in);
        viewFlipper.setOutAnimation(getActivity(), android.R.anim.fade_out);

        viewFlipper.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent touchevent) {
                switch (touchevent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = touchevent.getX();
                        break;
                    case MotionEvent.ACTION_UP:
                        float finalX = touchevent.getX();
                        if (initialX > finalX) {
                            if (viewFlipper.getDisplayedChild() == 1)
                                break;


                            viewFlipper.showNext();
                        } else {
                            if (viewFlipper.getDisplayedChild() == 0)
                                break;


                            viewFlipper.showPrevious();
                        }
                        break;
                }
                return false;
            }


        });

        viewFlipper.startFlipping();
        viewFlipper.setAutoStart(true);
        viewFlipper.setFlipInterval(3000);
    }

    public void createDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.cancel();
        }
        dialog = new Dialog(context);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        // Include dialog.xml file
        dialog.setContentView(R.layout.popup_platinum);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);

        ImageButton ivClose = dialog.findViewById(R.id.ivClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        if (!dialog.isShowing()) {
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    dialog.show();
                }
            };
            r.run();
        }

    }

    public void initDialogComponents() {
        viewFlipper = dialog.findViewById(R.id.flipper);
        ImageButton ivClose = dialog.findViewById(R.id.ivClose);
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void startDiamondAdAnimation() {
        handler = new Handler();
        mRunnable = new Runnable() {
            public void run() {
                try {
                    if (App.diamondAdvertiseMent.size() > 0) {
                        if (currentDiamondPos >= App.diamondAdvertiseMent.size() - 1) {
                            currentDiamondPos = 0;
                        } else {
                            currentDiamondPos++;
                        }


                        Animation fadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fed_in);
                        img_diamond_ad.startAnimation(fadeIn);

                        Picasso.with(getActivity()).load(App.diamondAdvertiseMent.get(currentDiamondPos).getFilepath().replace(" ", "%20")).resize(4092, 572).into(img_diamond_ad, new Callback() {
                            @Override
                            public void onSuccess() {
                                pbar_diamond.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                img_diamond_ad.setImageResource(R.drawable.no_image);
                            }

                        });

                        final ImagePopup imagePopup = new ImagePopup(getActivity());
                        //  imagePopup.setWindowHeight(800); // Optional
                        // imagePopup.setWindowWidth(800); // Optional
                        imagePopup.setBackgroundColor(Color.BLACK);  // Optional
                        imagePopup.setFullScreen(true); // Optional
                        imagePopup.setHideCloseIcon(true);  // Optional
                        imagePopup.setImageOnClickClose(true);  // Optional


                        imagePopup.initiatePopupWithPicasso(App.diamondAdvertiseMent.get(currentDiamondPos).getFilepath());

                        img_diamond_ad.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                /** Initiate Popup view **/
                                imagePopup.viewPopup();

                            }
                        });

                        fadeIn.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                handler.postDelayed(mRunnable, 5000);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        handler.postDelayed(mRunnable, 10);


    }

    public void startGoldAdAnimation() {
        handlerGold = new Handler();
        mRunnableGold = new Runnable() {
            public void run() {
                try {
                    if (App.goldAdvertiseMent.size() > 0) {
                        if (currentGoldPos >= App.goldAdvertiseMent.size() - 1) {
                            currentGoldPos = 0;
                        } else {
                            currentGoldPos++;
                        }


                        Animation fadeIn = AnimationUtils.loadAnimation(context, R.anim.fed_in);
                        img_gold_ad.startAnimation(fadeIn);

                        Picasso.with(getActivity()).load(App.goldAdvertiseMent.get(currentGoldPos).getFilepath().replace(" ", "%20")).resize(512, 512).into(img_gold_ad, new Callback() {
                            @Override
                            public void onSuccess() {
                                pbar_gold.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                img_gold_ad.setImageResource(R.drawable.no_image);
                            }

                        });

                        final ImagePopup imagePopup = new ImagePopup(getActivity());
                        //  imagePopup.setWindowHeight(800); // Optional
                        // imagePopup.setWindowWidth(800); // Optional
                        imagePopup.setBackgroundColor(Color.BLACK);  // Optional
                        imagePopup.setFullScreen(true); // Optional
                        imagePopup.setHideCloseIcon(true);  // Optional
                        imagePopup.setImageOnClickClose(true);  // Optional


                        imagePopup.initiatePopupWithPicasso(App.goldAdvertiseMent.get(currentGoldPos).getFilepath());

                        img_gold_ad.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                /** Initiate Popup view **/
                                imagePopup.viewPopup();

                            }
                        });

                        fadeIn.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                handlerGold.postDelayed(mRunnableGold, 5000);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        handlerGold.postDelayed(mRunnableGold, 10);
    }

    public void startBusinessAdAnimation() {
        handlerBusi = new Handler();
        mRunnableBusi = new Runnable() {
            public void run() {
                try {
                    if (App.businessAdvertiseMent.size() > 0) {
                        if (currentBusiPos >= App.businessAdvertiseMent.size() - 1) {
                            currentBusiPos = 0;
                        } else {
                            currentBusiPos++;
                        }


                        Animation fadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fed_in);
                        img_business_ad.startAnimation(fadeIn);

                        Picasso.with(getActivity()).load(App.businessAdvertiseMent.get(currentBusiPos).getFilepath().replace(" ", "%20")).resize(512, 512).into(img_business_ad, new Callback() {
                            @Override
                            public void onSuccess() {
                                pbar_business.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                img_business_ad.setImageResource(R.drawable.no_image);
                            }

                        });

                        final ImagePopup imagePopup = new ImagePopup(getActivity());
                        //    imagePopup.setWindowHeight(800); // Optional
                        //   imagePopup.setWindowWidth(800); // Optional
                        imagePopup.setBackgroundColor(Color.BLACK);  // Optional
                        imagePopup.setFullScreen(true); // Optional
                        imagePopup.setHideCloseIcon(true);  // Optional
                        imagePopup.setImageOnClickClose(true);  // Optional


                        imagePopup.initiatePopupWithPicasso(App.businessAdvertiseMent.get(currentBusiPos).getFilepath());

                        img_business_ad.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                /** Initiate Popup view **/
                                imagePopup.viewPopup();

                            }
                        });

                        fadeIn.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                handlerBusi.postDelayed(mRunnableBusi, 5000);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        handlerBusi.postDelayed(mRunnableBusi, 10);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

    }
}
