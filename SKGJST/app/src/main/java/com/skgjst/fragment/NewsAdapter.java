package com.skgjst.fragment;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.skgjst.R;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by empiere-php on 12/26/2017.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder> {

    //  public ArrayList<EquipmentsModel> list;
    public ArrayList<News> myList = new ArrayList<>();
    public Context context;
    public String emailId, token, userId;

    public NewsAdapter(JarVidhiFragment newsFragment, FragmentActivity activity, ArrayList<News> newsArrayList) {
        context = activity;
        myList = newsArrayList;
    }
    public NewsAdapter(NewsFragment newsFragment, FragmentActivity activity, ArrayList<News> newsArrayList) {
        context = activity;
        myList = newsArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_news_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tvTitle.setText(myList.get(position).getNewsTitle());
        holder.tvDescription.setText(myList.get(position).getNewsDesc());
        holder.tvDate.setText(getFormattedTime(myList.get(position).getDate()));

    }

    public String getFormattedTime(String date) {
        /// android  /// 2018-04-25T08:40:13.16

        try {
            Log.i("DATE", "IN Android FORMAT");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS");

            SimpleDateFormat newFormat = new SimpleDateFormat("MMM dd, yyyy");
            Date currentDate = new Date();

            currentDate = format.parse(date);
            return newFormat.format(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }


    }


    @Override
    public int getItemCount() {
        return myList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvTitle, tvDescription, tvDate;

        public MyViewHolder(View view) {
            super(view);
            tvTitle = view.findViewById(R.id.tvTitle);
            tvDescription = view.findViewById(R.id.tvDescription);
            tvDate = view.findViewById(R.id.tvDate);
        }
    }
}

