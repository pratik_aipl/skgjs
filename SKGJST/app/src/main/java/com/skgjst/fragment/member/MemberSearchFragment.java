package com.skgjst.fragment.member;


import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.skgjst.BaseFragment;
import com.skgjst.activities.membersearch.MemberList;
import com.skgjst.App;
import com.skgjst.fonts.MyCustomTypeface;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.BloodGroup;
import com.skgjst.model.MaritalStatus;
import com.skgjst.model.Surname;
import com.skgjst.model.Village;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.skgjst.activities.DashBoardActivity.tv_title;

/**
 * Created by vidhi-techmishty on 2/4/18.
 */

public class MemberSearchFragment extends BaseFragment implements AsynchTaskListner {

    public View v;
    public RecyclerView listView;
    public MemberSearchAdapter memberSearchAdapter;
    public ImageView imgSearch, imgClose;
    public EditText et_area, et_first_name, etEducation/*, etMinAge, etMaxAge*/;
    public Spinner sp_surname, sp_blood_group, sp_village, sp_mat_status, sp_gender, sp_min_age, sp_max_age;
    public LinearLayout llFilter;
    public TextView tvMemberCount, tvNoSearchItems;
    public String surnameId = "", villageId = "", bloodGroup_id = "0", minAge = "0", maxAge = "0", genderStr = "";
    public JsonParserUniversal jParser;
    public Dialog dialog;
    public ArrayList<MemberList> memberArrayList = new ArrayList<>();
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<String> minAgeList = new ArrayList<>();
    private ArrayList<String> maxAgeList = new ArrayList<>();
    public Surname surname;
    public ArrayList<Surname> surnameArray = new ArrayList<>();
    public ArrayList<String> strSurnameArray = new ArrayList<>();
    public String surname_id = "", surname_name = "";
    public MemberSearchFragment instance;
    public BloodGroup bloodGroup;
    public ArrayList<BloodGroup> bloodGroupArray = new ArrayList<>();
    public ArrayList<String> strBloodGroupArray = new ArrayList<>();
    public String blood_group_id = "";
    public Village village;
    public ArrayList<Village> villageArray = new ArrayList<>();
    public ArrayList<String> strVillageArray = new ArrayList<>();
    public String village_id = "", village_name = "";
    public MaritalStatus maritalStatus;
    public ArrayList<MaritalStatus> maritalStatusArray = new ArrayList<>();
    public ArrayList<String> strMaritalStatusArray = new ArrayList<>();
    public String marital_status_id = "";
    public String selectGender = "";
    public RecyclerView rv_member_list;
    public LinearLayout lin_empty;
    public TextView tv_message,tv_count;
    public FloatingActionButton float_add;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_member_search, container, false);

        jParser = new JsonParserUniversal();
        instance = MemberSearchFragment.this;
        float_add = v.findViewById(R.id.float_add);
        rv_member_list =v.findViewById(R.id.rv_member_list);
        // openFilter();
        tv_title.setText("MEMBER SEARCH");
        return v;
    }

    private void openFilter() {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dailog_member_filter);
        et_area = dialog.findViewById(R.id.et_area);
        et_first_name = dialog.findViewById(R.id.et_first_name);
        sp_surname = dialog.findViewById(R.id.sp_surname);
        sp_blood_group = dialog.findViewById(R.id.sp_blood_group);
        sp_village = dialog.findViewById(R.id.sp_village);
        sp_mat_status = dialog.findViewById(R.id.sp_mat_status);
        sp_gender = dialog.findViewById(R.id.sp_gender);
        sp_max_age = dialog.findViewById(R.id.sp_max_age);
        sp_min_age = dialog.findViewById(R.id.sp_min_age);
        surname = new Surname();
        bloodGroup = new BloodGroup();
        village = new Village();
        maritalStatus = new MaritalStatus();
        minAgeList.clear();
        maxAgeList.clear();

        minAgeList.add("Min Age");
        maxAgeList.add("Max Age");

        for (int i = 18; i < 100; i++) {
            minAgeList.add(String.valueOf(i));
        }

        for (int i = 18; i <= 100; i++) {
            maxAgeList.add(String.valueOf(i));
        }

        sp_min_age.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, minAgeList));
        sp_max_age.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, maxAgeList));


        new CallRequest(MemberSearchFragment.this).getSurnameListFragment();
        new CallRequest(instance).getVillageListFragment();
        new CallRequest(instance).getBloodGroupFragment();
        new CallRequest(instance).getMaritalStatusFragment();

        sp_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/Lato-Regular_0.ttf")));
                        if (position == 1) {
                            selectGender = "M";
                        }
                        if (position == 2) {
                            selectGender = "F";
                        }

                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_mat_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/Lato-Regular_0.ttf")));
                        marital_status_id = String.valueOf(maritalStatusArray.get(position).getMaritalstatusID());

                    } else {
                        marital_status_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_surname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/Lato-Regular_0.ttf")));
                        surname_id = String.valueOf(surnameArray.get(position).getmSurnameid());
                        surname_name = surnameArray.get(position).getSurname();
                    } else {
                        surname_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_blood_group.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/Lato-Regular_0.ttf")));
                        blood_group_id = String.valueOf(bloodGroupArray.get(position).getBloodGroupID());

                    } else {
                        blood_group_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_village.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/Lato-Regular_0.ttf")));
                        village_id = String.valueOf(villageArray.get(position).getVillageID());
                        village_name = villageArray.get(position).getVillagename();

                    } else {
                        village_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_min_age.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/Lato-Regular_0.ttf")));

                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_max_age.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(getActivity(), "fonts/Lato-Regular_0.ttf")));

                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        imgClose = dialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                surnameId = "";
                marital_status_id = "0";
                bloodGroup_id = "0";
                villageId = "";
                dialog.dismiss();

            }
        });

        dialog.show();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getSurnameList:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strSurnameArray.clear();
                            surnameArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                surname.setSurname(getResources().getString(R.string.surname));
                                surname.setmSurnameid(0);
                                strSurnameArray.add(surname.getSurname());
                                surnameArray.add(surname);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        surname = (Surname) jParser.parseJson(jSurnameArray.getJSONObject(i), new Surname());
                                        surnameArray.add(surname);
                                        strSurnameArray.add(surname.getSurname());
                                    }
                                    sp_surname.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, strSurnameArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), getActivity());
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), getActivity());
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", getActivity());
                        e.printStackTrace();
                    }
                    break;

                case getMaritalStatus:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strMaritalStatusArray.clear();
                            maritalStatusArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                maritalStatus.setMaritalstatus(getResources().getString(R.string.MaritalStatus));
                                maritalStatus.setMaritalstatusID(0);
                                strMaritalStatusArray.add(maritalStatus.getMaritalstatus());
                                maritalStatusArray.add(maritalStatus);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        maritalStatus = (MaritalStatus) jParser.parseJson(jSurnameArray.getJSONObject(i), new MaritalStatus());
                                        maritalStatusArray.add(maritalStatus);
                                        strMaritalStatusArray.add(maritalStatus.getMaritalstatus());
                                    }
                                    sp_mat_status.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, strMaritalStatusArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), getActivity());
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), getActivity());
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", getActivity());
                        e.printStackTrace();
                    }
                    break;


                case getBloodGroup:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strBloodGroupArray.clear();
                            bloodGroupArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                bloodGroup.setBloodGroupName(getResources().getString(R.string.bloodgroup));
                                bloodGroup.setBloodGroupID(0);
                                strBloodGroupArray.add(bloodGroup.getBloodGroupName());
                                bloodGroupArray.add(bloodGroup);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        bloodGroup = (BloodGroup) jParser.parseJson(jSurnameArray.getJSONObject(i), new BloodGroup());
                                        bloodGroupArray.add(bloodGroup);
                                        strBloodGroupArray.add(bloodGroup.getBloodGroupName());
                                    }
                                    sp_blood_group.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, strBloodGroupArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), getActivity());
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), getActivity());
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", getActivity());
                        e.printStackTrace();
                    }
                    break;


                case getVillageList:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strVillageArray.clear();
                            villageArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                village.setVillagename(getResources().getString(R.string.village));
                                village.setVillageID(0);
                                strVillageArray.add(village.getVillagename());
                                villageArray.add(village);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        village = (Village) jParser.parseJson(jSurnameArray.getJSONObject(i), new Village());
                                        villageArray.add(village);
                                        strVillageArray.add(village.getVillagename());
                                    }
                                    sp_village.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, strVillageArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), getActivity());
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), getActivity());
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", getActivity());
                        e.printStackTrace();
                    }
                    break;
                case getMember:
                    Utils.hideProgressDialog();
                    memberArrayList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jBranchArray = jObj.getJSONArray("data");

                                for (int i = 0; i < jBranchArray.length(); i++) {
                                    JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    MemberList memberList = (MemberList) jParser.parseJson(jPackag, new MemberList());
                                    memberArrayList.add(memberList);
                                }
                                App.memberCount = String.valueOf(memberArrayList.size());

                                tvMemberCount.setText(String.valueOf(memberArrayList.size()) + " " + "Members");
                                layoutManager = new LinearLayoutManager(getActivity());
                                listView.setLayoutManager(layoutManager);
                              //  memberSearchAdapter = new MemberSearchAdapter(MemberSearchFragment.this, getActivity(), memberArrayList);
                                listView.setAdapter(memberSearchAdapter);

                                // Utils.showToast(jObj.getString("Message"), getActivity());

                            }
                        } else {
                            Utils.showToast(jObj.getString("Message"), getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case MemberSearch:
                    Utils.hideProgressDialog();
                    tvNoSearchItems.setVisibility(View.GONE);
                    try {
                        JSONObject jObj = new JSONObject(result);
                        memberArrayList.clear();
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jBranchArray = jObj.getJSONArray("data");
                                for (int i = 0; i < jBranchArray.length(); i++) {
                                    JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    MemberList memberList = (MemberList) jParser.parseJson(jPackag, new MemberList());
                                    memberArrayList.add(memberList);
                                }

                                tvMemberCount.setText(String.valueOf(memberArrayList.size()) + " " + "Members");
                                dialog.dismiss();

                                layoutManager = new LinearLayoutManager(getActivity());
                                listView.setLayoutManager(layoutManager);
                          //      memberSearchAdapter = new MemberSearchAdapter(MemberSearchFragment.this, getActivity(), memberArrayList);
                                listView.setAdapter(memberSearchAdapter);
                                // Utils.showToast(jObj.getString("Message"), getActivity());

                            } else {
                                Utils.showToast(jObj.getString("Message"), getActivity());
                                dialog.dismiss();

                                tvMemberCount.setText(String.valueOf(memberArrayList.size()) + " " + "Members");
                                layoutManager = new LinearLayoutManager(getActivity());
                                listView.setLayoutManager(layoutManager);
                            //    memberSearchAdapter = new MemberSearchAdapter(MemberSearchFragment.this, getActivity(), memberArrayList);
                                listView.setAdapter(memberSearchAdapter);
                            }
                        } else {
                            Utils.showToast(jObj.getString("Message"), getActivity());
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    dialog.dismiss();

                    break;
            }
        }
    }
}

