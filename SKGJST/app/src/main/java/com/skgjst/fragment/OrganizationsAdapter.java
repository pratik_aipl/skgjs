package com.skgjst.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.skgjst.activities.OraganizationDetailActivity;
import com.skgjst.R;
import com.skgjst.utils.ImagePopup;
import com.skgjst.utils.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by empiere-php on 12/26/2017.
 */

public class OrganizationsAdapter extends RecyclerView.Adapter<OrganizationsAdapter.MyViewHolder> {

    public Context context;
    public ArrayList<OrganizationList> myLists;

    public OrganizationsAdapter(ArrayList<OrganizationList> memberArrayList, Context context) {
        this.context = context;
        this.myLists = memberArrayList;
    }

    @Override
    public OrganizationsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_organization_item, parent, false);

        return new OrganizationsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OrganizationsAdapter.MyViewHolder holder, final int position) {

        holder.tv_name.setText(myLists.get(position).getOrgName());

        if (myLists.get(position).getPlace() != null && !TextUtils.isEmpty(myLists.get(position).getPlace()) && !myLists.get(position).getPlace().equalsIgnoreCase("null")) {
            holder.tvAge.setText(myLists.get(position).getPlace());
        } else {
            holder.tvAge.setText("NOT AVAILABLE");
        }

        if (!TextUtils.isEmpty(myLists.get(position).getOrgLogo())) {
            try {
                PicassoTrustAll.getInstance(context)
                        .load(myLists.get(position).getOrgLogo().replace(" ", "%20"))
                        .placeholder(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        .resize(256, 256)
                        .into(holder.ivLogo, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        final ImagePopup imagePopup = new ImagePopup(context);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.BLACK);  // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(true);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional

        imagePopup.initiatePopupWithPicasso(myLists.get(position).getOrgLogo());

        holder.ivLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagePopup.viewPopup();
            }
        });
        holder.tv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, OraganizationDetailActivity.class)
                        .putExtra("obj", myLists.get(position))
                );
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, OraganizationDetailActivity.class)
                        .putExtra("obj", myLists.get(position))
                );
            }
        });

        holder.itemView.setTag(position);

    }


    @Override
    public int getItemCount() {
        return myLists.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_name, tvAge;
        public CircleImageView ivLogo;
        public ImageButton tv_send;

        public MyViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.tv_name);
            tvAge = view.findViewById(R.id.tvAge);
            ivLogo = view.findViewById(R.id.img_profile);
            tv_send = view.findViewById(R.id.tv_send);
        }
    }

}

