package com.skgjst.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skgjst.BaseFragment;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.skgjst.activities.DashBoardActivity.tv_title;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrganizationsFragment extends BaseFragment implements AsynchTaskListner {

    public RecyclerView listView;
    public JsonParserUniversal jParser;
    public ArrayList<OrganizationList> organizationLists = new ArrayList<>();
    public OrganizationsAdapter organizationsAdapter;
    private RecyclerView.LayoutManager layoutManager;

    public OrganizationsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_organizations, container, false);

        jParser = new JsonParserUniversal();
        listView = (RecyclerView) view.findViewById(R.id.listView);

        new CallRequest(OrganizationsFragment.this).getOrganizationList();

        tv_title.setText("ORGANIZATIONS");

        return view;
    }

    public void switchFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, fragment, fragment.getClass().getSimpleName());
        transaction.addToBackStack(fragment.getClass().getSimpleName());
        transaction.commit();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        Utils.hideProgressDialog();
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getOrganizationList:
                    // Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONObject("data") != null && jObj.getJSONObject("data").length() > 0) {
                                JSONObject object = jObj.getJSONObject("data");
                                JSONObject jsonObject = object.getJSONObject("Organizations");
                                JSONArray array = jsonObject.getJSONArray("Organisation");

                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object1 = array.getJSONObject(i);
                                    OrganizationList organizationList = new OrganizationList();
                                    organizationList.setOrgName(object1.getString("OrgName"));
                                    organizationList.setOrgAddress(object1.getString("OrgAddress"));
                                    organizationList.setOrgCntPersonName(object1.getString("OrgCntPersonName"));
                                    organizationList.setOrgContctNo(object1.getString("OrgContctNo"));
                                    organizationList.setOrgContEmail(object1.getString("OrgContEmail"));
                                    organizationList.setOrgLogo(object1.getString("OrgLogo"));
                                    organizationList.setOrgDesc(object1.getString("OrgDesc"));
                                    organizationList.setWebsiteUrl(object1.getString("WebsiteUrl"));
                                    organizationList.setPlace(object1.getString("Place"));
                                    organizationList.setOrgContctNo2(object1.getString("OrgContctNo2"));

                                    ArrayList<TrusteeDetails> list = new ArrayList<>();
                                    if (object1.has("Trustee_Details")) {
                                        JSONArray jsonArray = object1.getJSONArray("Trustee_Details");
                                        for (int j = 0; j < jsonArray.length(); j++) {
                                            JSONObject object2 = jsonArray.getJSONObject(j);
                                            if (object2.length() > 0) {
                                                TrusteeDetails trusteeDetails = (TrusteeDetails) jParser.parseJson(object2, new TrusteeDetails());
                                                list.add(trusteeDetails);
                                            }
                                        }
                                    }

                                    organizationList.setTrustee_Details(list);

                                    organizationLists.add(organizationList);
                                }

                                layoutManager = new LinearLayoutManager(getActivity());
                                listView.setLayoutManager(layoutManager);
                             //   organizationsAdapter = new OrganizationsAdapter(OrganizationsFragment.this, getActivity(), organizationLists);
                                listView.setAdapter(organizationsAdapter);

                            } else {
                                layoutManager = new LinearLayoutManager(getActivity());
                                listView.setLayoutManager(layoutManager);
                             //   organizationsAdapter = new OrganizationsAdapter(OrganizationsFragment.this, getActivity(), organizationLists);
                                listView.setAdapter(organizationsAdapter);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
