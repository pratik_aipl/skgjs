package com.skgjst.fragment.event;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.androidquery.AQuery;
import com.skgjst.App;
import com.skgjst.fragment.ImageDisplayFragment;
import com.skgjst.model.EventImages;
import com.skgjst.model.PastEvent;
import com.skgjst.R;
import com.skgjst.utils.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by empiere-php on 12/26/2017.
 */

public class EventDetailsAdapter extends RecyclerView.Adapter<EventDetailsAdapter.MyViewHolder> {

    public ArrayList<EventImages> myLists;
    public Context context;
    public String emailId, token, userId;
    public AQuery aqIMG;

    public EventDetailsAdapter(Context context, ArrayList<EventImages> pastEventArrayList) {

        this.context = context;
        this.myLists = pastEventArrayList;
        aqIMG = new AQuery(context);

    }

    @Override
    public EventDetailsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_event_detail, parent, false);

        return new EventDetailsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final EventDetailsAdapter.MyViewHolder holder, final int position) {

        if (myLists.get(position).getImageURL() != null) {
            System.out.println("image:::" + myLists.get(position).getImageURL());
            /*Picasso.with(context).load(myLists.get(position).getImageURL().replace(" ", "%20")).resize(572, 572).into(holder.ivImage, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    holder.ivImage.setImageResource(R.drawable.no_image);
                }

            });*/

            try {
                PicassoTrustAll.getInstance(context)
                        .load(myLists.get(position).getImageURL().replace(" ", "%20"))
                        .error(R.drawable.no_image)
                        .placeholder(R.drawable.no_image)
                        .resize(572,572)
                        .into(holder.ivImage, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                                holder.ivImage.setImageResource(R.drawable.no_image);
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }

            holder.itemView.setTag(position);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (int) v.getTag();
                    context.startActivity(new Intent(context, ImageDisplayFragment.class)
                            .putExtra("position", pos)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    );

                    App.eventImagesArrayList = myLists;

                }
            });

            /*final ImagePopup imagePopup = new ImagePopup(context);
            imagePopup.setWindowHeight(800); // Optional
            imagePopup.setWindowWidth(800); // Optional
            imagePopup.setBackgroundColor(Color.BLACK);  // Optional
            imagePopup.setFullScreen(true); // Optional
            imagePopup.setHideCloseIcon(true);  // Optional
            imagePopup.setImageOnClickClose(true);  // Optional

            imagePopup.initiatePopupWithPicasso(myLists.get(position).getImageURL().replace(" ", "%20"));

            holder.ivImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imagePopup.viewPopup();
                }
            });*/

        }

    }

    public String getFormattedTime(String date) {
        /// android  /// 2018-04-25T08:40:13.16

        try {
            Log.i("DATE", "IN Android FORMAT");
            SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");

            SimpleDateFormat newFormat = new SimpleDateFormat("MMM dd, yyyy");
            Date currentDate = new Date();

            currentDate = format.parse(date);
            return newFormat.format(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }


    }

    @Override
    public int getItemCount() {
        return myLists.size();
    }


    public interface OnItemClickListener {
        void onItemClick(PastEvent item);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivImage;

        public MyViewHolder(View view) {
            super(view);

            ivImage = view.findViewById(R.id.ivImage);
        }
    }
}

