package com.skgjst.fragment.business;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.skgjst.model.BusinessList;
import com.skgjst.R;
import java.util.ArrayList;

/**
 * Created by empiere-php on 12/26/2017.
 */

public class BusinessSearchAdapter extends RecyclerView.Adapter<BusinessSearchAdapter.MyViewHolder> {

    public Context context;
    public ArrayList<BusinessList> myLists;
    private BusinessSearchFragment fragment;

    public BusinessSearchAdapter(BusinessSearchFragment businessSearchFragment, FragmentActivity activity, ArrayList<BusinessList> businessArrayList) {
        context = businessSearchFragment.getActivity();
        this.myLists = businessArrayList;
        fragment = businessSearchFragment;

    }

    @Override
    public BusinessSearchAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_business_search_item, parent, false);

        return new BusinessSearchAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BusinessSearchAdapter.MyViewHolder holder, final int position) {


        holder.tvName.setText(myLists.get(position).getFName() + " " + myLists.get(position).getMName() + " " + myLists.get(position).getSurName());

        if (myLists.get(position).getIndustryName() !=null && !TextUtils.isEmpty(myLists.get(position).getIndustryName())) {
            holder.tvBusinessName.setText(myLists.get(position).getIndustryName());
        } else {
            holder.tvBusinessName.setText("Not available");
        }

        if (myLists.get(position).getWorkProfession() !=null && !TextUtils.isEmpty(myLists.get(position).getWorkProfession())) {
            holder.tvProfession.setText(myLists.get(position).getWorkProfession());
        } else {
            holder.tvProfession.setText("Not available");
        }

        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int) v.getTag();
                Bundle bundle = new Bundle();
                bundle.putSerializable("business_search", myLists.get(pos));
                BusinessSearchDetailFragment detailFragment = new BusinessSearchDetailFragment();
                detailFragment.setArguments(bundle);

                fragment.switchFragment(detailFragment);
            }
        });

    }


    @Override
    public int getItemCount() {
        return myLists.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvName, tvBusinessName, tvProfession;

        public MyViewHolder(View view) {
            super(view);

            tvName = view.findViewById(R.id.tvName);
            tvBusinessName = view.findViewById(R.id.tvBusinessName);
            tvProfession = view.findViewById(R.id.tvProfession);
        }
    }
}

