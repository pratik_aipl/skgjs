package com.skgjst.fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.skgjst.App;
import com.skgjst.BaseFragment;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.Surname;
import com.skgjst.model.Village;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by vidhi-techmishty on 2/4/18.
 */

public class JarVidhiFragment extends BaseFragment implements AsynchTaskListner {
    public static final int progress_bar_type = 0;
    //private ProgressDialog pDialog;
    public static ProgressDialog pDialog;
    public static ArrayList<Surname> surnameArrayList = new ArrayList<>();
    public static ArrayList<String> surnameArrayName = new ArrayList<>();
    @SuppressLint("StaticFieldLeak")
    static Context context;
    static String certificate;
    static File fileURL;
    //public static final int progress_bar_type = 0;
    public View v;
    public RecyclerView listView;
    public NewsAdapter newsAdapter;
    public Spinner spSurname, spVillage;
    public LinearLayout ll_Download_LagnaVidhi, ll_Download_Chandlavidhi;
    public RelativeLayout rel_no_record;
    public TextView tvNotification;
    public Surname surname;
    public String surnameId = "0", villageId = "0", filePath = "";
    public JsonParserUniversal jParser;
    public LinearLayout llDownload;
    public String name;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_jar_vidhi, container, false);
        jParser = new JsonParserUniversal();

        context = getActivity();

        spSurname = v.findViewById(R.id.spinnerSurname);
        spVillage = v.findViewById(R.id.spinnerVillage);
        ll_Download_LagnaVidhi = v.findViewById(R.id.ll_Download_LagnaVidhi);
        ll_Download_Chandlavidhi = v.findViewById(R.id.ll_Download_Chandlavidhi);

        ll_Download_LagnaVidhi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = "lagnavidhi";
//                new DownloadFile().execute("http://skgjsedirectory.org/Upload/JarVidhiFiles/lagnavidhi.pdf");
//                Utils.showToast("File Downloaded SuccessFully",getActivity());

                String url = "http://skgjsedirectory.org/Upload/JarVidhiFiles/lagnavidhi.pdf";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });

        ll_Download_Chandlavidhi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = "Chandlavidhi";
                //   new DownloadFile().execute("http://skgjsedirectory.org/Upload/JarVidhiFiles/Chandlavidhi.pdf");
                String url = "http://skgjsedirectory.org/Upload/JarVidhiFiles/Chandlavidhi.pdf";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);


            }
        });

        llDownload = v.findViewById(R.id.ll_Download_Pdf);
        llDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CallRequest(JarVidhiFragment.this).JarVidhi(villageId, surnameId);
            }
        });


        new CallRequest(JarVidhiFragment.this).getSurnameListFra();


        return v;
    }

    public void switchFragment(Fragment fragment) {

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, fragment, fragment.getClass().getSimpleName());
        transaction.addToBackStack(fragment.getClass().getSimpleName());
        transaction.commit();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getSurnameList:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jBranchArray = jObj.getJSONArray("data");
                                //Utils.addDefaultSurname();

                                Surname surname = new Surname();
                                surname.Surname = "All Surnames";
                                surnameArrayList.add(surname);
                                surnameArrayName.add(surname.getSurname());

                                for (int i = 0; i < jBranchArray.length(); i++) {
                                    JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    surname = (Surname) jParser.parseJson(jPackag, new Surname());
                                    surnameArrayList.add(surname);
                                    surnameArrayName.add(surname.getSurname());

                                }

                                System.out.println("List size::::" + surnameArrayList.size());

                                ArrayAdapter aaProfession = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, surnameArrayName);
                                aaProfession.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spSurname.setAdapter(aaProfession);

                                spSurname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        try {
                                            if (i != 0) {
                                                surnameId = String.valueOf(surnameArrayList.get(i).getmSurnameid());
                                                System.out.println("SurnameId::::" + surnameId);
                                            } else {
                                                surnameId = "0";
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {
                                    }
                                });

                            }
                        }
                        new CallRequest(JarVidhiFragment.this).VillageList();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;



                case JarVidhi:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {

                                JSONArray jBranchArray = jObj.getJSONArray("data");

                                for (int i = 0; i < jBranchArray.length(); i++) {
                                    JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    filePath = jPackag.getString("filepath").replace(" ", "%20");

                                }
                                if (!filePath.isEmpty()) {
                                    name = "";

                                    /*certificate = filePath;

                                    if (Build.VERSION.SDK_INT >= 23) {
                                        String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
                                        if (!hasPermissions(getContext(), PERMISSIONS)) {
                                            ActivityCompat.requestPermissions((Activity) getContext(), PERMISSIONS, 112);
                                        } else {
                                            System.out.println("Going to Create PDF file");

                                            createPdFFile();
                                        }
                                    } else {

                                        System.out.println("Going to Create PDF file");

                                        createPdFFile();
                                    }*/

                                    String url = filePath;
                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                    i.setData(Uri.parse(url));
                                    startActivity(i);

                                    //new DownloadFile().execute(filePath);
                                    //Utils.showToast("File Downloaded SuccessFully", getActivity());
                                }
                            } else {
                                Utils.showToast(jObj.getString("Message"), getActivity());
                            }
                        } else {
                            Utils.showAlert(jObj.getString("Message"), getActivity());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case VillageList:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jBranchArray = jObj.getJSONArray("data");
                                Utils.addDefaultVillage("All Villages");
                                for (int i = 0; i < jBranchArray.length(); i++) {
                                    JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    Village village = (Village) jParser.parseJson(jPackag, new Village());
                                    App.villageArrayList.add(village);
                                    App.villageArrayName.add(village.getVillagename());
                                }

                                System.out.println("LIst size::::" + App.villageArrayList.size());

                                ArrayAdapter aaVillage = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, App.villageArrayName);
                                aaVillage.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spVillage.setAdapter(aaVillage);

                                spVillage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        try {
                                            if (i != 0) {
                                                villageId = String.valueOf(App.villageArrayList.get(i).getVillageID());
                                                System.out.println("VillageId::::" + villageId);
                                            } else {
                                                villageId = "0";
                                            }

                                        } catch (IndexOutOfBoundsException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {

                                    }
                                });
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;


            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == 112) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                createPdFFile();

            } else {
                Utils.showToast("Permission not granted", getContext());
            }
        }
    }

    private void createPdFFile() {

        File dir = new File(Environment.getExternalStorageDirectory().getPath() + "/skgjst/pdf");
        if (!dir.exists()) {
            System.out.println("Directory not exist");

            dir.mkdirs();
        }
        if (name.isEmpty()) {
            fileURL = new File(dir.getAbsolutePath(), "Rituals_" + System.currentTimeMillis() + ".pdf");
        } else {
            fileURL = new File(dir.getAbsolutePath(), name + ".pdf");
        }
        try {
            System.out.println("Creating file " + fileURL.getAbsolutePath());

            fileURL.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("File URL" + filePath);

        new Downloader().execute();
    }

    public static class Downloader extends AsyncTask<Void, String, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        private void showDialog(int id) {
            switch (id) {
                case progress_bar_type:
                    pDialog = new ProgressDialog(context);
                    pDialog.setMessage("Downloading file. Please wait...");
                    pDialog.setIndeterminate(false);
                    pDialog.setMax(100);
                    pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    pDialog.setCancelable(true);
                    pDialog.show();
                    return;
                default:
            }
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            pDialog.dismiss();

        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {

                FileOutputStream f = new FileOutputStream(fileURL);
                URL u = new URL(certificate);
                HttpURLConnection c = (HttpURLConnection) u.openConnection();
                c.setRequestMethod("GET");
                /*c.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                c.setRequestProperty("charset", "utf-8");*/
                //c.setDoOutput(true);
                c.connect();

                int reponse_code = c.getResponseCode();
                InputStream error = c.getErrorStream();
                System.out.println("code:::" + reponse_code);
                System.out.println("response code error" + error);

                int lengthOfFile = c.getContentLength();

                InputStream in = c.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;
                long total = 0;

                while ((len1 = in.read(buffer)) > 0) {
                    total += len1;
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    f.write(buffer, 0, len1);
                }

                f.close();

                System.out.println("completed downloading");

                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(
                        new Runnable() {
                            @Override
                            public void run() {
                                Utils.showToast("File downloaded successfully", context);
                            }
                        }
                );
            } catch (Exception e) {
                e.printStackTrace();
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(
                        new Runnable() {
                            @Override
                            public void run() {
                                Utils.showToast("Something went wrong", context);
                            }
                        }
                );
            }

            return null;
        }

        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }
    }
}