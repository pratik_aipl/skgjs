package com.skgjst.fragment.business;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.skgjst.BaseFragment;
import com.skgjst.model.BusinessList;
import com.skgjst.R;
import com.skgjst.utils.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class BusinessSearchDetailFragment extends BaseFragment {

    TextView tvFirstName, tvMiddleName, tvSurname, tvTypeOfBusiness, tvNameOfOffice, tvOfficeAddress, tvOfficeContactNumber, tvEmailAddress, tvMobileNumber;
    BusinessList businessList;
    ImageView imgCall;

    public BusinessSearchDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_business_search_detail, container, false);

        Bundle bundle = getArguments();
        if (bundle != null) {
            businessList = (BusinessList) bundle.getSerializable("business_search");
        }

        tvFirstName = view.findViewById(R.id.tvFirstName);
        tvMiddleName = view.findViewById(R.id.tvMiddleName);
        tvSurname = view.findViewById(R.id.tvSurname);
        tvTypeOfBusiness = view.findViewById(R.id.tvTypeOfBusiness);
        tvNameOfOffice = view.findViewById(R.id.tvNameOfOffice);
        tvOfficeAddress = view.findViewById(R.id.tvOfficeAddress);
        tvOfficeContactNumber = view.findViewById(R.id.tvOfficeContactNumber);
        tvEmailAddress = view.findViewById(R.id.tvEmailAddress);
        tvMobileNumber = view.findViewById(R.id.tvMobileNumber);
        imgCall = view.findViewById(R.id.imgCall);

        setData();

        return view;
    }

    private void setData() {

        if (businessList.getFName() != null && !TextUtils.isEmpty(businessList.getFName())) {
            tvFirstName.setText(businessList.getFName());
        } else {
            tvFirstName.setText("Not available");
        }

        if (businessList.getMName() != null && !TextUtils.isEmpty(businessList.getMName())) {
            tvMiddleName.setText(businessList.getMName());
        } else {
            tvMiddleName.setText("Not available");
        }

        if (businessList.getSurName() != null && !TextUtils.isEmpty(businessList.getSurName())) {
            tvSurname.setText(businessList.getSurName());
        } else {
            tvSurname.setText("Not available");
        }

        if (businessList.getIndustryName() != null && !TextUtils.isEmpty(businessList.getIndustryName())) {
            tvTypeOfBusiness.setText(businessList.getIndustryName());
        } else {
            tvTypeOfBusiness.setText("Not available");
        }

        if (businessList.getOfficeName() != null && !TextUtils.isEmpty(businessList.getOfficeName())) {
            tvNameOfOffice.setText(businessList.getOfficeName());
        } else {
            tvNameOfOffice.setText("Not available");
        }

        if (businessList.getOfficeAddress() != null && !TextUtils.isEmpty(businessList.getOfficeAddress())) {
            tvOfficeAddress.setText(businessList.getOfficeAddress());
        } else {
            tvOfficeAddress.setText("Not available");
        }

        if (businessList.getOfficeContNo() != null && !TextUtils.isEmpty(businessList.getOfficeContNo())) {
            tvOfficeContactNumber.setText(businessList.getOfficeContNo());
        } else {
            tvOfficeContactNumber.setText("Not available");
        }

        if (businessList.getEmailID() != null && !TextUtils.isEmpty(businessList.getEmailID())) {
            tvEmailAddress.setText(businessList.getEmailID());
            tvEmailAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + businessList.getEmailID()));
                        intent.putExtra(Intent.EXTRA_SUBJECT, "");
                        intent.putExtra(Intent.EXTRA_TEXT, "");
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        //TODO smth
                        Utils.showToast("There is no app for email", getActivity());
                    }
                }
            });
        } else {
            tvEmailAddress.setText("Not available");
            tvEmailAddress.setTextColor(getResources().getColor(R.color.black));
        }

        if (businessList.getContactNo() != null && !TextUtils.isEmpty(businessList.getContactNo())) {
            tvMobileNumber.setText(businessList.getContactNo());
            imgCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", businessList.getContactNo(), null));
                    startActivity(intent);
                }
            });
        } else {
            tvMobileNumber.setText("Not available");
        }

    }

}
