package com.skgjst.fragment;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skgjst.activities.matrimonial.MatrimonialDetailActivity;
import com.skgjst.model.MatrimonialList;
import com.skgjst.R;
import com.skgjst.utils.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by empiere-php on 12/26/2017.
 */

public class MatrimonialSearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<MatrimonialList> mArrayList;
    public Context context;
    public static final int VIEW_TYPE_ITEM = 1;

    public MatrimonialSearchAdapter(ArrayList<MatrimonialList> moviesList, Context context) {
        mArrayList = moviesList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name,tv_Marital_Status,tvAge,tv_status;
        public LinearLayout lin_info;
        public CircleImageView img_profile;
        public ImageButton tv_send;

        public MyViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.tv_name);
            tv_Marital_Status = view.findViewById(R.id.tv_Marital_Status);
            tv_status = view.findViewById(R.id.tv_status);
            tvAge = view.findViewById(R.id.tvAge);
            img_profile = view.findViewById(R.id.img_profile);
            tv_send = view.findViewById(R.id.tv_send);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new MyViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_matrimonal_raw, parent, false));

    }




    public void bindMyViewHolder(final MyViewHolder holder, final int pos) {
        final MatrimonialList Obj = mArrayList.get(pos);
        holder.tv_name.setText(Obj.getFName()+" "+Obj.getMName()+" "+Obj.getSurName());
        if(Obj.getNativePlace().equalsIgnoreCase("")){
            holder.tvAge.setText("Native Place : Not available"+Obj.getNativePlace());

        }else{
            holder.tvAge.setText("Native Place : "+Obj.getNativePlace());

        }
        if(Obj.getNativePlace().equalsIgnoreCase("")) {

            holder.tv_Marital_Status.setText("N/A");
        }
        else{
            holder.tv_Marital_Status.setText(Obj.getCity());

        }

        holder.tv_status.setText(Obj.getMaritalStatus());
        if (Obj.getPhotoURL() != null && !TextUtils.isEmpty(Obj.getPhotoURL())) {
            try {
                PicassoTrustAll.getInstance(context)
                        .load(Obj.getPhotoURL().replace(" ", "%20"))
                        .error(R.drawable.avatar)
                        .resize(256,256)
                        .into(holder.img_profile, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        holder.tv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, MatrimonialDetailActivity.class)
                        .putExtra("obj", Obj)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                );
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, MatrimonialDetailActivity.class)
                        .putExtra("obj", Obj)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                );
            }
        });

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                 MyViewHolder addrHolder = (MyViewHolder) holder;
                bindMyViewHolder(addrHolder, position);


    }




    @Override
    public int getItemCount() {
        return mArrayList.size();
    }



}

