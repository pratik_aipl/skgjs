package com.skgjst.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skgjst.App;
import com.skgjst.BaseFragment;
import com.skgjst.R;
import com.skgjst.adapter.BirthdayAdapter;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.callinterface.BirthdaywishListener;
import com.skgjst.model.Birthday;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.skgjst.activities.DashBoardActivity.frame_slecte;
import static com.skgjst.activities.DashBoardActivity.tv_mydemise;
import static com.skgjst.activities.DashBoardActivity.tv_title;

/**
 * Created by Karan - Empiere on 9/19/2018.
 */

public class BirthdayFragment extends BaseFragment implements AsynchTaskListner, BirthdaywishListener {
    public View view;
    public RecyclerView rv_product_list;
    public LinearLayout lin_empty;
    public TextView tv_message;
    public JsonParserUniversal jParser;
    public ArrayList<Birthday> birthdayArrayList = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_birthday, container, false);
        tv_title.setText("TODAY'S BIRTHDAY");
        jParser = new JsonParserUniversal();

        App.isAdded = false;

        rv_product_list = view.findViewById(R.id.rcyclerView);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);
        rv_product_list.setLayoutAnimation(controller);
        rv_product_list.scheduleLayoutAnimation();
        rv_product_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rv_product_list.setHasFixedSize(true);
        rv_product_list.setItemViewCacheSize(20);
        rv_product_list.setDrawingCacheEnabled(true);
        rv_product_list.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        frame_slecte.setVisibility(View.VISIBLE);
        tv_mydemise.setVisibility(View.GONE);
        lin_empty = view.findViewById(R.id.lin_empty);
        tv_message = view.findViewById(R.id.tv_message);

        new CallRequest(this).birthdayList(user);

        return view;
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            //    Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case BirthdayList:
                    birthdayArrayList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("BirthdayList") != null && jObj.getJSONArray("BirthdayList").length() > 0) {
                                lin_empty.setVisibility(View.GONE);
                                rv_product_list.setVisibility(View.VISIBLE);
                                JSONArray userData = jObj.getJSONArray("BirthdayList");
                                for (int i = 0; i < userData.length(); i++) {
                                    JSONObject obj = userData.getJSONObject(i);
                                    Birthday birthday = (Birthday) jParser.parseJson(obj, new Birthday());
                                    birthdayArrayList.add(birthday);
                                }
                                Birthday birthday = new Birthday();
                                birthday.setFName("blank");
                                birthdayArrayList.add(birthday);
                                System.out.println("birthday list::::" + birthdayArrayList.size());
                                BirthdayAdapter birthdayAdapter = new BirthdayAdapter(birthdayArrayList, getActivity(), BirthdayFragment.this);
                                rv_product_list.setAdapter(birthdayAdapter);


                            } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("UMessage"), getActivity());
                                lin_empty.setVisibility(View.VISIBLE);
                                rv_product_list.setVisibility(View.GONE);
                            }


                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), getActivity());
                            lin_empty.setVisibility(View.VISIBLE);
                            rv_product_list.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {

                        Utils.hideProgressDialog();
                        Utils.showToast("UMessage", getActivity());
                        lin_empty.setVisibility(View.VISIBLE);
                        rv_product_list.setVisibility(View.GONE);
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case getBirthdayWishes:
                    Utils.hideProgressDialog();
                    JSONObject jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            Utils.showToast(jObj.getString("UMessage"), getActivity());
                            new CallRequest(this).birthdayList(user);

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;


            }
        }
    }

    @Override
    public void birthdayWish(String messageText, int familydetailid) {
        new CallRequest(this).getBirthdayWishes(user, messageText, familydetailid);
    }
}
