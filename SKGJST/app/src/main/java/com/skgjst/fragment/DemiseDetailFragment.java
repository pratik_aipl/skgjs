package com.skgjst.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skgjst.BaseFragment;
import com.skgjst.adapter.DemiseExpAdapter;
import com.skgjst.App;
import com.skgjst.R;
import com.skgjst.utils.JsonParserUniversal;
import static com.skgjst.activities.DashBoardActivity.tv_title;

public class DemiseDetailFragment extends BaseFragment {

    public JsonParserUniversal jParser;
    ExpandableListView expListView;
    public LinearLayout lin_empty;
    public TextView tv_message, tv_count;
    public View view;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_demise_new, container, false);
        tv_title.setText("DEMISE");
        Log.i("Type","==>"+App.isDemiseType);
        jParser = new JsonParserUniversal();
        expListView = (ExpandableListView) view.findViewById(R.id.lvExp);
        lin_empty = view.findViewById(R.id.lin_empty);
        tv_message = view.findViewById(R.id.tv_message);

        if (App.headerArrayList.size() > 0) {


            lin_empty.setVisibility(View.GONE);
            expListView.setVisibility(View.VISIBLE);
            DemiseExpAdapter listAdapter = new DemiseExpAdapter(this, App.headerArrayList, expListView);
            expListView.setAdapter(listAdapter);
            /*for (int i = 0; i <= App.headerArrayList.size(); i++) {
                expListView.isGroupExpanded(i);
            }*/
        } else {
            lin_empty.setVisibility(View.VISIBLE);
            expListView.setVisibility(View.GONE);
        }
        return view;
    }


}
