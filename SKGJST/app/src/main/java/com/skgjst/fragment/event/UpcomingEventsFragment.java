package com.skgjst.fragment.event;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.skgjst.BaseFragment;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.UpcomingEvent;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by vidhi-techmishty on 2/4/18.
 */

public class UpcomingEventsFragment extends BaseFragment implements AsynchTaskListner {

    public View v;
    private RecyclerView.LayoutManager layoutManager;
    public RecyclerView listView;
    public UpcomingEventsAdapter upcomingEventsAdapter;
    public ArrayList<UpcomingEvent> upcomingEventArrayList = new ArrayList<>();
    public JsonParserUniversal jParser;
    public LinearLayout lin_empty;


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_upcoming_events, container, false);

        jParser=new JsonParserUniversal();
        listView = (RecyclerView) v.findViewById(R.id.listView);

        layoutManager = new LinearLayoutManager(getActivity());
        lin_empty = v.findViewById(R.id.lin_empty);
        listView.setLayoutManager(layoutManager);

        new CallRequest(UpcomingEventsFragment.this).GetEventList();

        return v;
    }

    
     public UpcomingEventsAdapter.OnItemClickListener eventItemClickListner = new UpcomingEventsAdapter.OnItemClickListener() {
         @Override
         public void onItemClick(UpcomingEvent item) {

             EventDetailFragment eventDetailFragment = new EventDetailFragment();
             Bundle bundle = new Bundle();
             bundle.putString("title", item.getEventTitle());
             bundle.putString("date", item.getEventDate());
             bundle.putString("location", item.getVenue());
             bundle.putString("description", item.getEventDescription());
             bundle.putString("image", item.getCoverImage());
             bundle.putInt("id", item.getEventID());
             eventDetailFragment.setArguments(bundle);
             switchFragment(eventDetailFragment);
             
         }
     };
    
    public void switchFragment(Fragment fragment) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, fragment, fragment.getClass().getSimpleName());
        transaction.addToBackStack(fragment.getClass().getSimpleName());
        transaction.commit();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case GetEventList:
                    upcomingEventArrayList.clear();
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj != null) {
                                if (jObj.getJSONArray("Futureeventdata") != null && jObj.getJSONArray("Futureeventdata").length() > 0) {
                                    lin_empty.setVisibility(View.GONE);
                                    listView.setVisibility(View.VISIBLE);
                                    JSONArray jBranchArray = jObj.getJSONArray("Futureeventdata");
                                    for (int i = 0; i < jBranchArray.length(); i++) {
                                        JSONObject jPackag = jBranchArray.getJSONObject(i);
                                        UpcomingEvent upcomingEvent = (UpcomingEvent) jParser.parseJson(jPackag, new UpcomingEvent());
                                        upcomingEventArrayList.add(upcomingEvent);
                                    }
                                    upcomingEventsAdapter = new UpcomingEventsAdapter(UpcomingEventsFragment.this, upcomingEventArrayList,eventItemClickListner);
                                    listView.setAdapter(upcomingEventsAdapter);

                                }else {

                                    //   Utils.showToast(jObj.getString("UMessage"), this);
                                    lin_empty.setVisibility(View.VISIBLE);
                                    listView.setVisibility(View.GONE);

                                    Utils.showToast(jObj.getString("UMessage"), getActivity());
                                }

                                System.out.println("size" + upcomingEventArrayList.size());

                               }
                        }else {

                            //   Utils.showToast(jObj.getString("UMessage"), this);
                            lin_empty.setVisibility(View.VISIBLE);
                            listView.setVisibility(View.GONE);

                            Utils.showToast(jObj.getString("UMessage"), getActivity());
                        }
                    }catch (JSONException e) {
                        e.printStackTrace();
                        lin_empty.setVisibility(View.VISIBLE);
                        listView.setVisibility(View.GONE);
                        Utils.showToast("Something getting wrong! Please try again later.", getActivity());
                    }
                    break;
            }
        }
    }
}
