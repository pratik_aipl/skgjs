package com.skgjst.fragment;


import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.skgjst.BaseFragment;
import com.skgjst.model.MatrimonialList;
import com.skgjst.R;
import com.skgjst.utils.ImagePopup;
import com.skgjst.utils.PicassoTrustAll;
import com.squareup.picasso.Callback;

import static com.skgjst.activities.DashBoardActivity.tv_title;

/**
 * A simple {@link Fragment} subclass.
 */
public class MatrimonialSearchDetailFragment extends BaseFragment {

    ImageView ivUser;
    TextView tvFirstName, tvMiddleName, tvSurname, tvHeight, tvWeight, tvSkinColor, tvIsHandicap, tvIsManglik, tvOtherInfo, tvDOB, tvGender, tvMaritalStatus,
            tvEducation, tvBloodGroup, tvProfession, tvNatureOfBusiness, tvContactNumber, tvNativePlace;
    MatrimonialList matrimonialList;

    public MatrimonialSearchDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_matrimonial_search_detail, container, false);

        Bundle bundle = getArguments();
        if (bundle != null) {
            matrimonialList = (MatrimonialList) bundle.getSerializable("matrimonial_search");
        }

        ivUser = view.findViewById(R.id.ivUser);
        tvFirstName = view.findViewById(R.id.tvFirstName);
        tvMiddleName = view.findViewById(R.id.tvMiddleName);
        tvSurname = view.findViewById(R.id.tvSurname);
        tvHeight = view.findViewById(R.id.tvHeight);
        tvWeight = view.findViewById(R.id.tvWeight);
        tvSkinColor = view.findViewById(R.id.tvSkinColor);
        tvIsHandicap = view.findViewById(R.id.tvIsHandicap);
        tvIsManglik = view.findViewById(R.id.tvIsManglik);
        tvOtherInfo = view.findViewById(R.id.tvOtherInfo);
        tvDOB = view.findViewById(R.id.tvDOB);
        tvGender = view.findViewById(R.id.tvGender);
        tvMaritalStatus = view.findViewById(R.id.tvMaritalStatus);
        tvEducation = view.findViewById(R.id.tvEducation);
        tvBloodGroup = view.findViewById(R.id.tvBloodGroup);
        tvProfession = view.findViewById(R.id.tvProfession);
        tvNatureOfBusiness = view.findViewById(R.id.tvNatureOfBusiness);
        tvContactNumber = view.findViewById(R.id.tvContactNumber);
        tvNativePlace = view.findViewById(R.id.tvNativePlace);

        setData();
tv_title.setText("MATRIMONIAL DETAIL");

        return view;
    }

    private void setData() {

        if (!TextUtils.isEmpty(matrimonialList.getPhotoURL())) {
            try {
                PicassoTrustAll.getInstance(getActivity())
                        .load(matrimonialList.getPhotoURL().replace(" ", "%20"))
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar)
                        .resize(256, 256)
                        .into(ivUser, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        final ImagePopup imagePopup = new ImagePopup(getActivity());
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.BLACK);  // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(true);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional

        imagePopup.initiatePopupWithPicasso(matrimonialList.getPhotoURL().replace(" ", "%20"));

        ivUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagePopup.viewPopup();
            }
        });

        if (matrimonialList.getFName() != null && !TextUtils.isEmpty(matrimonialList.getFName())) {
            tvFirstName.setText(matrimonialList.getFName());
        } else {
            tvFirstName.setText("Not available");
        }

        if (matrimonialList.getMName() != null && !TextUtils.isEmpty(matrimonialList.getMName())) {
            tvMiddleName.setText(matrimonialList.getMName());
        } else {
            tvMiddleName.setText("Not available");
        }

        if (matrimonialList.getSurName() != null && !TextUtils.isEmpty(matrimonialList.getSurName())) {
            tvSurname.setText(matrimonialList.getSurName());
        } else {
            tvSurname.setText("Not available");
        }

        if (matrimonialList.getHeight() != null && !TextUtils.isEmpty(matrimonialList.getHeight())) {
            tvHeight.setText(matrimonialList.getHeight());
        } else {
            tvHeight.setText("Not available");
        }

        if (matrimonialList.getWeight() != null && !TextUtils.isEmpty(matrimonialList.getWeight())) {
            tvWeight.setText(matrimonialList.getWeight());
        } else {
            tvWeight.setText("Not available");
        }

        if (matrimonialList.getSkincolor() != null && !TextUtils.isEmpty(matrimonialList.getSkincolor())) {
            tvSkinColor.setText(matrimonialList.getSkincolor());
        } else {
            tvSkinColor.setText("Not available");
        }

        if (matrimonialList.getHandicap() != null && !TextUtils.isEmpty(matrimonialList.getHandicap())) {
            tvIsHandicap.setText(matrimonialList.getHandicap());
        } else {
            tvIsHandicap.setText("Not available");
        }

        if (matrimonialList.getManglik() != null && !TextUtils.isEmpty(matrimonialList.getManglik())) {
            tvIsManglik.setText(matrimonialList.getManglik());
        } else {
            tvIsManglik.setText("Not available");
        }

        if (matrimonialList.getOtherinfo() != null && !TextUtils.isEmpty(matrimonialList.getOtherinfo())) {
            tvOtherInfo.setText(matrimonialList.getOtherinfo());
        } else {
            tvOtherInfo.setText("Not available");
        }

        if (matrimonialList.getNativePlace() != null && !TextUtils.isEmpty(matrimonialList.getNativePlace())) {
            tvNativePlace.setText(matrimonialList.getNativePlace());
        } else {
            tvNativePlace.setText("Not available");
        }

        if (matrimonialList.getDateOfBirth() != null && !TextUtils.isEmpty(matrimonialList.getDateOfBirth())) {
            tvDOB.setText(matrimonialList.getDateOfBirth());
        } else {
            tvDOB.setText("Not available");
        }

        if (matrimonialList.getGender() != null && !TextUtils.isEmpty(matrimonialList.getGender())) {
            tvGender.setText(matrimonialList.getGender());
        } else {
            tvGender.setText("Not available");
        }

        if (matrimonialList.getMaritalStatus() != null && !TextUtils.isEmpty(matrimonialList.getMaritalStatus())) {
            tvMaritalStatus.setText(matrimonialList.getMaritalStatus());
        } else {
            tvMaritalStatus.setText("Not available");
        }

        if (matrimonialList.getEducation() != null && !TextUtils.isEmpty(matrimonialList.getEducation())) {
            tvEducation.setText(matrimonialList.getEducation());
        } else {
            tvEducation.setText("Not available");
        }

        if (matrimonialList.getBloodGroupName() != null && !TextUtils.isEmpty(matrimonialList.getBloodGroupName())) {
            tvBloodGroup.setText(matrimonialList.getBloodGroupName());
        } else {
            tvBloodGroup.setText("Not available");
        }

        if (matrimonialList.getProfession() != null && !TextUtils.isEmpty(matrimonialList.getProfession())) {
            tvProfession.setText(matrimonialList.getProfession());
        } else {
            tvProfession.setText("Not available");
        }

        if (matrimonialList.getIndustryName() != null && !TextUtils.isEmpty(matrimonialList.getIndustryName())) {
            tvNatureOfBusiness.setText(matrimonialList.getIndustryName());
        } else {
            tvNatureOfBusiness.setText("Not available");
        }

        if (matrimonialList.getContactNo() != null && !TextUtils.isEmpty(matrimonialList.getContactNo())) {
            tvContactNumber.setText(matrimonialList.getContactNo());
            tvContactNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", matrimonialList.getContactNo(), null));
                    startActivity(intent);
                }
            });
        } else {
            tvContactNumber.setText("Not available");
        }

    }

}
