package com.skgjst.fragment;

import java.io.Serializable;

/**
 * Created by Krupa Kakkad on 16 June 2018
 */
public class TrusteeDetails implements Serializable {

    public String TrusteeName = "", TrusteeEmailID = "", TrusteePhoneNumber = "", TrusteePhoto = "", Post = "";

    public String getTrusteeName() {
        return TrusteeName;
    }

    public void setTrusteeName(String trusteeName) {
        TrusteeName = trusteeName;
    }

    public String getTrusteeEmailID() {
        return TrusteeEmailID;
    }

    public void setTrusteeEmailID(String trusteeEmailID) {
        TrusteeEmailID = trusteeEmailID;
    }

    public String getTrusteePhoneNumber() {
        return TrusteePhoneNumber;
    }

    public void setTrusteePhoneNumber(String trusteePhoneNumber) {
        TrusteePhoneNumber = trusteePhoneNumber;
    }

    public String getTrusteePhoto() {
        return TrusteePhoto;
    }

    public void setTrusteePhoto(String trusteePhoto) {
        TrusteePhoto = trusteePhoto;
    }

    public String getPost() {
        return Post;
    }

    public void setPost(String post) {
        Post = post;
    }
}
