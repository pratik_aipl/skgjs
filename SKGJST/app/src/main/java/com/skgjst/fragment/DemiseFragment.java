package com.skgjst.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skgjst.App;
import com.skgjst.BaseFragment;
import com.skgjst.R;
import com.skgjst.activities.demise.DemiseAddActivity;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.DemiseDataNew;
import com.skgjst.model.DemiseType;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.skgjst.activities.DashBoardActivity.frame_slecte;
import static com.skgjst.activities.DashBoardActivity.tv_mydemise;
import static com.skgjst.activities.DashBoardActivity.tv_title;

/**
 * Created by Karan - Empiere on 9/19/2018.
 */

public class DemiseFragment extends BaseFragment implements AsynchTaskListner {
    public View view;
    public JsonParserUniversal jParser;
    public FloatingActionButton float_add;
    public ViewPager viewPager;
    private TabLayout tabLayout;
    public ViewPagerAdapter adapter;
    public ArrayList<DemiseDataNew> DemiseArrayList = new ArrayList<>();
    public ImageView img_my_job;
    public TextView tv_message, tv_count;
    public LinearLayout lin_empty;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_demise, container, false);
        tv_title.setText("DEMISE");
        jParser = new JsonParserUniversal();
        img_my_job = view.findViewById(R.id.img_my_job);
        img_my_job.setVisibility(View.VISIBLE);
        tv_count = view.findViewById(R.id.tv_count);

        float_add = view.findViewById(R.id.float_add);
        frame_slecte.setVisibility(View.GONE);
        tv_mydemise.setVisibility(View.VISIBLE);

        tv_mydemise.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!App.isAdded) {
                    App.isAdded = true;
                    tv_mydemise.setImageResource(R.drawable.b_demise);
                    new CallRequest(DemiseFragment.this).getPersonallyAddedPrathnaDemiseList(user);
                    App.isDemiseType = "My";
                } else {
                    App.isAdded = false;
                    new CallRequest(DemiseFragment.this).getPrathnaDemiseList();
                    tv_mydemise.setImageResource(R.drawable.pray);
                    App.isDemiseType = "All";
                }

            }
        });


        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);


        //  new CallRequest(this).getPrathnaDemiseList();
        viewPager = view.findViewById(R.id.viewpager);

        float_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title = "ADD DEMISE";
                startActivity(new Intent(getActivity(), DemiseAddActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                );
            }
        });


        tabLayout = view.findViewById(R.id.tabLayout);
        img_my_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CallRequest(DemiseFragment.this).getMyDemiseList(user);
            }
        });
        if (App.isAdded) {
            tv_mydemise.setImageResource(R.drawable.b_demise);
            new CallRequest(DemiseFragment.this).getPersonallyAddedPrathnaDemiseList(user);
            App.isDemiseType = "My";
        } else {
            new CallRequest(DemiseFragment.this).getPrathnaDemiseList();
            tv_mydemise.setImageResource(R.drawable.pray);
            App.isDemiseType = "All";
        }
        return view;
    }

    protected void changeTabsFont(TabLayout tabLayout) {
        Log.i("In change tab font", "");
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        Log.i("Tab count--->", String.valueOf(tabsCount));
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof AppCompatTextView) {
                    Log.i("In font", "");

                }
            }
        }
    }

    private void setupViewPager(ViewPager viewPager, String type) {
        // viewPager.removeAllViews();
        //  viewPager.setAdapter(null);
        if (getActivity() != null && viewPager != null) {
            adapter = new ViewPagerAdapter(getChildFragmentManager());
            adapter.addFragment(new DemiseDetailFragment(), "DEMISE", type);
            adapter.addFragment(new PrathanaFragment(), "PRATHANA", type);
            viewPager.setAdapter(adapter);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        private final List<String> mFragmentTypList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title, String type) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
            mFragmentTypList.add(type);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            //    Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getPrathnaDemiseList:
                    DemiseArrayList.clear();
                    App.headerArrayList.clear();
                    App.PrathnasArrayList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            try {
                                if (jObj.getJSONObject("records").getJSONObject("DemisePrathna").has("NewDemise")) {
                                    DemiseArrayList.clear();
                                    JSONObject jNewDemise = jObj.getJSONObject("records").getJSONObject("DemisePrathna").getJSONObject("NewDemise");
                                    if (jNewDemise.getJSONArray("Demises") != null && jNewDemise.getJSONArray("Demises").length() > 0) {
                                        JSONArray jdemiseArray = jNewDemise.getJSONArray("Demises");
                                        for (int i = 0; i < jdemiseArray.length(); i++) {
                                            JSONObject jdemi = jdemiseArray.getJSONObject(i);
                                            DemiseDataNew birthday = (DemiseDataNew) jParser.parseJson(jdemi, new DemiseDataNew());
                                            DemiseArrayList.add(birthday);
                                        }
                                        DemiseType obj = new DemiseType();
                                        obj.setNotification_name("NewDemise");
                                        obj.demiseDataArray.addAll(DemiseArrayList);
                                        App.headerArrayList.add(obj);
                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if (jObj.getJSONObject("records").getJSONObject("DemisePrathna").has("PastDemise")) {
                                    DemiseArrayList.clear();
                                    JSONObject jNewDemise = jObj.getJSONObject("records").getJSONObject("DemisePrathna").getJSONObject("PastDemise");
                                    if (jNewDemise.getJSONArray("Demises") != null && jNewDemise.getJSONArray("Demises").length() > 0) {
                                        JSONArray jdemiseArray = jNewDemise.getJSONArray("Demises");
                                        for (int i = 0; i < jdemiseArray.length(); i++) {
                                            JSONObject jdemi = jdemiseArray.getJSONObject(i);
                                            DemiseDataNew birthday = (DemiseDataNew) jParser.parseJson(jdemi, new DemiseDataNew());
                                            DemiseArrayList.add(birthday);
                                        }
                                        DemiseType obj = new DemiseType();
                                        obj.setNotification_name("PastDemise");
                                        obj.demiseDataArray.addAll(DemiseArrayList);
                                        App.headerArrayList.add(obj);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if (jObj.getJSONObject("records").getJSONObject("DemisePrathna").has("ALLPrathna")) {
                                    App.PrathnasArrayList.clear();
                                    JSONObject jNewDemise = jObj.getJSONObject("records").getJSONObject("DemisePrathna").getJSONObject("ALLPrathna");
                                    if (jNewDemise != null && jNewDemise.has("Prathnas")) {
                                        JSONArray jdemiseArray = jNewDemise.getJSONArray("Prathnas");
                                        for (int i = 0; i < jdemiseArray.length(); i++) {
                                            JSONObject jdemi = jdemiseArray.getJSONObject(i);
                                            DemiseDataNew birthday = (DemiseDataNew) jParser.parseJson(jdemi, new DemiseDataNew());
                                            App.PrathnasArrayList.add(birthday);
                                        }
                                    }


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), getActivity());

                        }
                        setupViewPager(viewPager, "All");
                        tabLayout.setupWithViewPager(viewPager);
                        changeTabsFont(tabLayout);
                        App.isDemiseType = "ALL";
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Utils.hideProgressDialog();
                        Utils.showToast("UMessage", getActivity());
                    }
                    Utils.hideProgressDialog();
                    break;
                case getPersonallyAddedPrathnaDemiseList:

                    DemiseArrayList.clear();
                    App.headerArrayList.clear();
                    App.PrathnasArrayList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getString("Status").equalsIgnoreCase("Success")) {
                            try {
                                if (jObj.getJSONObject("records").getJSONObject("DemisePrathna").has("AddedDemise")) {
                                    DemiseArrayList.clear();
                                    JSONObject jNewDemise = jObj.getJSONObject("records").getJSONObject("DemisePrathna").getJSONObject("AddedDemise");
                                    if (jNewDemise.getJSONArray("Demises") != null && jNewDemise.getJSONArray("Demises").length() > 0) {
                                        JSONArray jdemiseArray = jNewDemise.getJSONArray("Demises");
                                        for (int i = 0; i < jdemiseArray.length(); i++) {
                                            JSONObject jdemi = jdemiseArray.getJSONObject(i);
                                            DemiseDataNew birthday = (DemiseDataNew) jParser.parseJson(jdemi, new DemiseDataNew());
                                            DemiseArrayList.add(birthday);
                                        }
                                        DemiseType obj = new DemiseType();
                                        obj.setNotification_name("Added Demise");
                                        obj.demiseDataArray.addAll(DemiseArrayList);
                                        App.headerArrayList.add(obj);
                                    }

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if (jObj.getJSONObject("records").getJSONObject("DemisePrathna").has("PastDemise")) {
                                    DemiseArrayList.clear();
                                    JSONObject jNewDemise = jObj.getJSONObject("records").getJSONObject("DemisePrathna").getJSONObject("PastDemise");
                                    if (jNewDemise.getJSONArray("Demises") != null && jNewDemise.getJSONArray("Demises").length() > 0) {
                                        JSONArray jdemiseArray = jNewDemise.getJSONArray("Demises");
                                        for (int i = 0; i < jdemiseArray.length(); i++) {
                                            JSONObject jdemi = jdemiseArray.getJSONObject(i);
                                            DemiseDataNew birthday = (DemiseDataNew) jParser.parseJson(jdemi, new DemiseDataNew());
                                            DemiseArrayList.add(birthday);
                                        }
                                        DemiseType obj = new DemiseType();
                                        obj.setNotification_name("PastDemise");
                                        obj.demiseDataArray.addAll(DemiseArrayList);
                                        App.headerArrayList.add(obj);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if (jObj.getJSONObject("records").getJSONObject("DemisePrathna").has("AddedPrathnas")) {
                                    App.PrathnasArrayList.clear();
                                    JSONObject jNewDemise = jObj.getJSONObject("records").getJSONObject("DemisePrathna").getJSONObject("AddedPrathnas");
                                    if (jNewDemise.getJSONArray("Prathnas") != null && jNewDemise.getJSONArray("Prathnas").length() > 0) {
                                        JSONArray jdemiseArray = jNewDemise.getJSONArray("Prathnas");
                                        DemiseDataNew demiseDataNew;
                                        for (int i = 0; i < jdemiseArray.length(); i++) {
                                            JSONObject jdemi = jdemiseArray.getJSONObject(i);
                                            demiseDataNew = (DemiseDataNew) jParser.parseJson(jdemi, new DemiseDataNew());
                                            App.PrathnasArrayList.add(demiseDataNew);
                                        }
                                        Log.i("Size", "==>" + App.PrathnasArrayList.size() + "");
                                        // DemiseDataNew birthday = new DemiseDataNew();
                                        //     App.PrathnasArrayList.add(birthday);

                                    }


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), getActivity());

                        }
                        setupViewPager(viewPager, "my");
                        tabLayout.setupWithViewPager(viewPager);
                        changeTabsFont(tabLayout);
                        App.isDemiseType = "My";
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Utils.hideProgressDialog();
                        Utils.showToast("UMessage", getActivity());
                    }
                    Utils.hideProgressDialog();
                    break;
            }
        }
    }

}
