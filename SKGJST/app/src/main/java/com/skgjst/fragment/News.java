package com.skgjst.fragment;

import java.io.Serializable;

public class News implements Serializable {

    public String NewsTitle = "", NewsDesc = "", CreatedOn = "";

    public String getNewsTitle() {
        return NewsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        NewsTitle = newsTitle;
    }

    public String getNewsDesc() {
        return NewsDesc;
    }

    public void setNewsDesc(String newsDesc) {
        NewsDesc = newsDesc;
    }

    public String getDate() {
        return CreatedOn;
    }

    public void setDate(String date) {
        CreatedOn = date;
    }
}