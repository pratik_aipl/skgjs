package com.skgjst.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skgjst.BaseFragment;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.R;
import com.skgjst.utils.Constant;

import static com.skgjst.activities.DashBoardActivity.tv_title;

/**
 * Created by Karan - Empiere on 9/19/2018.
 */

public class SponserdFragment extends BaseFragment implements AsynchTaskListner {
    public View view;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_anniversary, container, false);
        tv_title.setText("Sponsored Ads");
        return view;
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

    }
}
