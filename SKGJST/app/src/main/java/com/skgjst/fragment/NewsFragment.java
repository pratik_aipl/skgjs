package com.skgjst.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.skgjst.App;
import com.skgjst.BaseFragment;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.skgjst.activities.DashBoardActivity.tv_title;

/**
 * Created by vidhi-techmishty on 2/4/18.
 */

public class NewsFragment extends BaseFragment implements AsynchTaskListner {
    public View v;
    private RecyclerView.LayoutManager layoutManager;
    public RecyclerView listView;
    public NewsAdapter newsAdapter;
    public News news;
    public ArrayList<News> newsArrayList = new ArrayList<>();
    public RelativeLayout rel_no_record;
    public TextView tvNotification;
    public JsonParserUniversal jParser;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_news, container, false);

        listView = (RecyclerView) v.findViewById(R.id.listView);

        jParser=new JsonParserUniversal();

        layoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(layoutManager);

        new CallRequest(NewsFragment.this).NewsList("");

        tv_title.setText("NEWS");

        App.editProfile = "";
        return v;
    }

    public void switchFragment(Fragment fragment) {

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, fragment, fragment.getClass().getSimpleName());
        transaction.addToBackStack(fragment.getClass().getSimpleName());
        transaction.commit();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            //    Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case NewsList:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray userData = jObj.getJSONArray("data");
                                for (int i = 0; i < userData.length(); i++) {
                                    JSONObject obj = userData.getJSONObject(i);
                                    News news = (News) jParser.parseJson(obj, new News());
                                    newsArrayList.add(news);
                                }

                                System.out.println("news list::::"+newsArrayList.size());
                                newsAdapter = new NewsAdapter(NewsFragment.this, getActivity(),newsArrayList);
                                listView.setAdapter(newsAdapter);
                                newsAdapter.notifyDataSetChanged();
                            }
                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
            }
        }
    }
}
