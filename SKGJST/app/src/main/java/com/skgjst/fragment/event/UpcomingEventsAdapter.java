package com.skgjst.fragment.event;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.skgjst.activities.events.EventDetailActivity;
import com.skgjst.model.UpcomingEvent;
import com.skgjst.R;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by empiere-php on 12/26/2017.
 */

public class UpcomingEventsAdapter extends RecyclerView.Adapter<UpcomingEventsAdapter.MyViewHolder> {
    public ArrayList<UpcomingEvent> myLists;
    public Context context;
    public String emailId, token, userId;
    public AQuery aqIMG;
    UpcomingEventsFragment upcomingEventsFragment;
    private OnItemClickListener listener;

    public UpcomingEventsAdapter(UpcomingEventsFragment upcomingEventsFragment, ArrayList<UpcomingEvent> pastEventArrayList,
                                 OnItemClickListener listener) {
        context = upcomingEventsFragment.getActivity();
        this.myLists = pastEventArrayList;
        aqIMG = new AQuery(context);
        this.upcomingEventsFragment = upcomingEventsFragment;
        this.listener = listener;

    }

    @Override
    public UpcomingEventsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_upcoming_event_item, parent, false);

        return new UpcomingEventsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final UpcomingEventsAdapter.MyViewHolder holder, final int position) {

        try {
            holder.tvEventTitle.setText(myLists.get(position).getEventTitle());

            holder.tvDate.setText(getFormattedTime(myLists.get(position).getEventDate()));

            holder.tvLocation.setText(myLists.get(position).getVenue());

            Log.i("Image Url","==>"+myLists.get(position).getCoverImage());

            Utils.setRoundedImage(context,myLists.get(position).getCoverImage(),0,0,holder.imgPastEvent,holder.prg);

            holder.itemView.setTag(position);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(context, EventDetailActivity.class)
                            .putExtra("obj", myLists.get(position))
                            .putExtra("title","Upcoming")

                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    );
                }
            });

        } catch (OutOfMemoryError e){
            e.printStackTrace();
        }
        //  imgDownload.id(holder.imgPastEvent).progress(holder.prg).image(myLists.get(position).getCoverImage(), true, true, 0, R.drawable.no_image, null, 0, 0.0f);

    }

    public String getFormattedTime(String date) {
        /// android  /// 2018-04-25T08:40:13.16

        try {
            Log.i("DATE", "IN Android FORMAT");
            SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");

            SimpleDateFormat newFormat = new SimpleDateFormat("MMM dd, yyyy");
            Date currentDate = new Date();

            currentDate = format.parse(date);
            return newFormat.format(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }


    }

    @Override
    public int getItemCount() {
        return myLists.size();
    }


    public interface OnItemClickListener {
        void onItemClick(UpcomingEvent item);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvDate, tvLocation, tvEventTitle;
        public ImageView imgPastEvent;
        public ProgressBar prg;

        public MyViewHolder(View view) {
            super(view);
            tvEventTitle = view.findViewById(R.id.tvEventTitle);
            tvDate = view.findViewById(R.id.tvDate);
            tvLocation = view.findViewById(R.id.tvLocation);
            imgPastEvent = view.findViewById(R.id.imgPastEvent);
            prg = view.findViewById(R.id.prg);
            ;
        }
    }
}

