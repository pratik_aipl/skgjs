package com.skgjst.fragment;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.skgjst.R;
import com.skgjst.utils.ImagePopup;
import com.skgjst.utils.PicassoTrustAll;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;

import java.util.ArrayList;

/**
 * Created by empiere-php on 12/26/2017.
 */

public class OrganizationDetailsAdapter extends RecyclerView.Adapter<OrganizationDetailsAdapter.MyViewHolder> {

    public Context context;
    public ArrayList<TrusteeDetails> myLists;
    private OrganizationDetailFragment fragment;


    public OrganizationDetailsAdapter(Context context, ArrayList<TrusteeDetails> memberArrayList) {
        this.context = context;
        this.myLists = memberArrayList;
    }

    @Override
    public OrganizationDetailsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_organization_detail, parent, false);

        return new OrganizationDetailsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OrganizationDetailsAdapter.MyViewHolder holder, final int position) {

        holder.tvTrusteeName.setText(myLists.get(position).getTrusteeName());

        if (myLists.get(position).getTrusteePhoneNumber() != null && !TextUtils.isEmpty(myLists.get(position).getTrusteePhoneNumber())) {
            holder.tvMobileNumber.setText(myLists.get(position).getTrusteePhoneNumber());
            holder.tvMobileNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", myLists.get(position).getTrusteePhoneNumber(), null));
                    context.startActivity(intent);
                }
            });
        } else {
            holder.tvMobileNumber.setText("Not available");
        }

        if (myLists.get(position).getTrusteeEmailID() != null && !TextUtils.isEmpty(myLists.get(position).getTrusteeEmailID())) {
            holder.tvEmailAddress.setText(myLists.get(position).getTrusteeEmailID());
            holder.tvEmailAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + myLists.get(position).getTrusteeEmailID()));
                        intent.putExtra(Intent.EXTRA_SUBJECT, "");
                        intent.putExtra(Intent.EXTRA_TEXT, "");
                        context.startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        Utils.showToast("There is no app for email", context);
                    }
                }
            });
        } else {
            holder.tvEmailAddress.setText("Not available");
        }

        if (myLists.get(position).getPost() != null && !TextUtils.isEmpty(myLists.get(position).getPost())) {
            holder.tvPost.setText(myLists.get(position).getPost());
        } else {
            holder.tvPost.setText("Not available");
        }

        if (!TextUtils.isEmpty(myLists.get(position).getTrusteePhoto())) {
            try {
                PicassoTrustAll.getInstance(context)
                        .load(myLists.get(position).getTrusteePhoto().replace(" ", "%20"))
                        .placeholder(R.drawable.avatar)
                        .error(R.drawable.avatar)
                        //.resize(256, 256)
                        .into(holder.ivTrustee, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        final ImagePopup imagePopup = new ImagePopup(context);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.BLACK);  // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(true);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional

        imagePopup.initiatePopupWithPicasso(myLists.get(position).getTrusteePhoto().replace(" ", "%20"));

        holder.ivTrustee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagePopup.viewPopup();
            }
        });

    }


    @Override
    public int getItemCount() {
        return myLists.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvTrusteeName, tvEmailAddress, tvPost, tvMobileNumber;
        public ImageView ivTrustee;

        public MyViewHolder(View view) {
            super(view);
            tvTrusteeName = view.findViewById(R.id.tvTrusteeName);
            tvEmailAddress = view.findViewById(R.id.tvEmailAddress);
            tvPost = view.findViewById(R.id.tvPost);
            tvMobileNumber = view.findViewById(R.id.tvMobileNumber);
            ivTrustee = view.findViewById(R.id.ivTrustee);
        }
    }
}

