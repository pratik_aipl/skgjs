package com.skgjst.fragment;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.skgjst.BaseFragment;
import com.skgjst.R;
import com.skgjst.utils.ImagePopup;
import com.skgjst.utils.PicassoTrustAll;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;

import static com.skgjst.activities.DashBoardActivity.tv_title;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrganizationDetailFragment extends BaseFragment {

    private ImageView ivOrganization;
    private TextView tvOrganizationName, tvAddress, tvContactPerson, tvContactNumber, tvEmailAddress, tvWebsiteURL, tvOrganizationDescription, tvNoDetails;
    private RecyclerView rvTrustee;
    private OrganizationList organizationList;
    private OrganizationDetailsAdapter organizationDetailsAdapter;
    private LinearLayoutManager layoutManager;

    public OrganizationDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_organization_detail, container, false);

        Bundle bundle = getArguments();
        if (bundle != null) {
            organizationList = (OrganizationList) bundle.getSerializable("organization_detail");
        }

        ivOrganization = view.findViewById(R.id.ivOrganization);
        tvOrganizationName = view.findViewById(R.id.tvOrganizationName);
        tvOrganizationDescription = view.findViewById(R.id.tvOrganizationDescription);
        tvAddress = view.findViewById(R.id.tvAddress);
        tvContactPerson = view.findViewById(R.id.tvContactPerson);
        tvContactNumber = view.findViewById(R.id.tvContactNumber);
        tvEmailAddress = view.findViewById(R.id.tvEmailAddress);
        tvWebsiteURL = view.findViewById(R.id.tvWebsiteURL);
        tvNoDetails = view.findViewById(R.id.tvNoDetails);
        rvTrustee = view.findViewById(R.id.rvTrustee);

        layoutManager = new LinearLayoutManager(getActivity());
        rvTrustee.setLayoutManager(layoutManager);

        setData();
tv_title.setText("ORGANIZATION DETAIL");

        return view;
    }

    private void setData() {

        if (!TextUtils.isEmpty(organizationList.getOrgLogo())) {
            try {
                PicassoTrustAll.getInstance(getActivity())
                        .load(organizationList.getOrgLogo().replace(" ", "%20"))
                        .placeholder(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        //.resize(256, 256)
                        .into(ivOrganization, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        final ImagePopup imagePopup = new ImagePopup(getActivity());
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.BLACK);  // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(true);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional

        imagePopup.initiatePopupWithPicasso(organizationList.getOrgLogo().replace(" ", "%20"));

        ivOrganization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagePopup.viewPopup();
            }
        });

        if (organizationList.getOrgName() != null && !TextUtils.isEmpty(organizationList.getOrgName())) {
            tvOrganizationName.setText(organizationList.getOrgName());
        } else {
            tvOrganizationName.setText("Not available");
        }

        if (organizationList.getOrgDesc() != null && !TextUtils.isEmpty(organizationList.getOrgDesc()) && !organizationList.getOrgDesc().equalsIgnoreCase("null")) {
            tvOrganizationDescription.setText(organizationList.getOrgDesc());
        } else {
            tvOrganizationDescription.setText("Not available");
        }

        if (organizationList.getOrgAddress() != null && !TextUtils.isEmpty(organizationList.getOrgAddress()) && !organizationList.getOrgAddress().equalsIgnoreCase("null")) {
            tvAddress.setText(organizationList.getOrgAddress());
        } else {
            tvAddress.setText("Not available");
        }

        if (organizationList.getOrgCntPersonName() != null && !TextUtils.isEmpty(organizationList.getOrgCntPersonName()) && !organizationList.getOrgCntPersonName().equalsIgnoreCase("null")) {
            tvContactPerson.setText(organizationList.getOrgCntPersonName());
        } else {
            tvContactPerson.setText("Not available");
        }

        if (organizationList.getOrgContctNo() != null && !TextUtils.isEmpty(organizationList.getOrgContctNo()) && !organizationList.getOrgContctNo().equalsIgnoreCase("null")) {
            tvContactNumber.setText(organizationList.getOrgContctNo());
            tvContactNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", organizationList.getOrgContctNo(), null));
                    startActivity(intent);
                }
            });
        } else {
            tvContactNumber.setText("Not available");
        }

        if (organizationList.getOrgContEmail() != null && !TextUtils.isEmpty(organizationList.getOrgContEmail()) && !organizationList.getOrgContEmail().equalsIgnoreCase("null")) {
            tvEmailAddress.setText(organizationList.getOrgContEmail());
            tvEmailAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + organizationList.getOrgContEmail()));
                        intent.putExtra(Intent.EXTRA_SUBJECT, "");
                        intent.putExtra(Intent.EXTRA_TEXT, "");
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        //TODO smth
                        Utils.showToast("There is no app for email", getActivity());
                    }
                }
            });
        } else {
            tvEmailAddress.setText("Not available");
        }

        if (organizationList.getWebsiteUrl() != null && !TextUtils.isEmpty(organizationList.getWebsiteUrl()) && !organizationList.getWebsiteUrl().equalsIgnoreCase("null")) {
            tvWebsiteURL.setText(organizationList.getWebsiteUrl());
            tvWebsiteURL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                        browserIntent.setData(Uri.parse("http://" + organizationList.getWebsiteUrl()));
                        startActivity(browserIntent);
                    } catch (ActivityNotFoundException e){
                        Utils.showToast("There is no app for url", getActivity());
                    }
                }
            });
        } else {
            tvWebsiteURL.setText("Not available");
        }

        System.out.println("trustee size:::::" + organizationList.getTrustee_Details().size());
        if (organizationList.getTrustee_Details().size() > 0) {
            tvNoDetails.setVisibility(View.GONE);
            rvTrustee.setVisibility(View.VISIBLE);

          //  organizationDetailsAdapter = new OrganizationDetailsAdapter(OrganizationDetailFragment.this, getActivity(), organizationList.getTrustee_Details());
            rvTrustee.setAdapter(organizationDetailsAdapter);
            rvTrustee.setNestedScrollingEnabled(false);

        } else {
            tvNoDetails.setVisibility(View.VISIBLE);
            rvTrustee.setVisibility(View.GONE);
        }

    }

}
