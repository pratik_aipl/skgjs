package com.skgjst.fragment;


import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.skgjst.App;
import com.skgjst.BaseFragment;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.MatrimonialDetail;
import com.skgjst.model.MatrimonialMember;
import com.skgjst.model.MemberDetailProfileInterface;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.PicassoTrustAll;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;
import static com.skgjst.utils.CallRequest.COMMON_URL;

/**
 * A simple {@link Fragment} subclass.
 */
public class MatrimonialDetailFragment extends BaseFragment implements MemberDetailProfileInterface, AsynchTaskListner {

    private static final int SELECT_PICTURE = 1;
    private static final int RESULT_CROP_DP = 5;
    public JsonParserUniversal jParser;
    public ArrayList<String> manglikList = new ArrayList<>();
    public ArrayList<String> skinColorList = new ArrayList<>();
    public ArrayList<String> handicapList = new ArrayList<>();
    public ArrayList<String> activateList = new ArrayList<>();
    public String filePathFirst, filePath, fileName = "";
    private CircleImageView imgProfile;
    private Spinner spNames, spinnerManglik, spinnerSkinColor, spinnerHandicap, spinnerActivate;
    private EditText etHeight, etWeight, etHandicapInfo, etOtherInfo;
    private RecyclerView listView;
    private Button btnSave, btnAdd, btnReset;
    private String manglikId = "", skinColorId = "", handicapId = "", activateId = "", nameId = "";
    private MatrimonialDetailAdapter matrimonialDetailAdapter;
    private RecyclerView.LayoutManager layoutManager;

    public MatrimonialDetailFragment() {
        // Required empty public constructor
    }

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat,
                                        int quality) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    public static Bitmap decodeSampledBitmapFromPath(String path, int reqWidth,
                                                     int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap bmp = BitmapFactory.decodeFile(path, options);
        return bmp;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_matrimonial_detail, container, false);

        jParser = new JsonParserUniversal();

        imgProfile = view.findViewById(R.id.imgProfile);
        spNames = view.findViewById(R.id.spNames);
        spinnerManglik = view.findViewById(R.id.spinnerManglik);
        spinnerSkinColor = view.findViewById(R.id.spinnerSkinColor);
        spinnerHandicap = view.findViewById(R.id.spinnerHandicap);
        spinnerActivate = view.findViewById(R.id.spinnerActivate);
        etHeight = view.findViewById(R.id.etHeight);
        etWeight = view.findViewById(R.id.etWeight);
        etHandicapInfo = view.findViewById(R.id.etHandicapInfo);
        etOtherInfo = view.findViewById(R.id.etOtherInfo);
        listView = view.findViewById(R.id.listView);
        btnSave = view.findViewById(R.id.btnSave);
        btnAdd = view.findViewById(R.id.btnAdd);
        btnReset = view.findViewById(R.id.btnReset);

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, SELECT_PICTURE);
            }
        });


        manglikList.clear();
        manglikList.add("Select Manglik");
        manglikList.add("No");
        manglikList.add("Yes");
        new CallRequest(MatrimonialDetailFragment.this).getMatrimonialMembers(user);

        ArrayAdapter aaManglik = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, manglikList);
        aaManglik.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerManglik.setAdapter(aaManglik);
        spinnerManglik.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                System.out.println("**ManglikId::::" + i);

                if (i == 0) {
                    manglikId = "";
                } else if (i == 1) {
                    manglikId = "No";
                } else if (i == 2) {
                    manglikId = "Yes";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                manglikId = "";
            }

        });

        skinColorList.clear();
        skinColorList.add("Select Skin Color*");
        skinColorList.add("Fair");
        skinColorList.add("Moderate");
        skinColorList.add("Brownish");
        skinColorList.add("Blackish");
        skinColorList.add("Dark");

        ArrayAdapter aaSkinColor = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, skinColorList);
        aaSkinColor.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSkinColor.setAdapter(aaSkinColor);
        spinnerSkinColor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                System.out.println("**SkinColor::::" + i);

                if (i == 0) {
                    skinColorId = "";
                } else if (i == 1) {
                    skinColorId = "Fair";
                } else if (i == 2) {
                    skinColorId = "Moderate";
                } else if (i == 3) {
                    skinColorId = "Brownish";
                } else if (i == 4) {
                    skinColorId = "Blackish";
                } else if (i == 5) {
                    skinColorId = "Dark";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                skinColorId = "";
            }

        });

        handicapList.clear();
        handicapList.add("Select Handicap");
        handicapList.add("No");
        handicapList.add("Yes");
        ArrayAdapter aaHandicap = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, handicapList);
        aaHandicap.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerHandicap.setAdapter(aaHandicap);

        spinnerHandicap.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                System.out.println("**handicap id::::" + i);

                if (i == 0) {
                    handicapId = "";
                } else if (i == 1) {
                    handicapId = "No";
                } else if (i == 2) {
                    handicapId = "Yes";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                handicapId = "";
            }

        });

        activateList.clear();
        activateList.add("Select Activation*");
        activateList.add("No");
        activateList.add("Yes");
        ArrayAdapter aaActivate = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, activateList);
        aaActivate.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerActivate.setAdapter(aaActivate);

        spinnerActivate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                System.out.println("**activate id::::" + i);

                if (i == 0) {
                    activateId = "";
                } else if (i == 1) {
                    activateId = "No";
                } else if (i == 2) {
                    activateId = "Yes";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                activateId = "";
            }

        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editMatrimonialDetail(v);
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMatrimonialDetail(v);
            }
        });

        btnSave.setVisibility(View.GONE);
        btnAdd.setVisibility(View.VISIBLE);

        layoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(layoutManager);

        new CallRequest(MatrimonialDetailFragment.this).GetProfile(String.valueOf(user.getFamilyID()));
        new CallRequest(MatrimonialDetailFragment.this).getMatrimonialMembers(user);

        return view;
    }

    private void reset() {
        btnSave.setVisibility(View.GONE);
        btnAdd.setVisibility(View.VISIBLE);

        etHeight.setText("");
        etWeight.setText("");
        etHandicapInfo.setText("");
        etOtherInfo.setText("");

        manglikId = "";
        skinColorId = "";
        handicapId = "";
        activateId = "";
        nameId = "";

        filePath = "";

        imgProfile.setImageResource(R.drawable.avatar);

        spinnerActivate.setSelection(0);
        spinnerHandicap.setSelection(0);
        spinnerSkinColor.setSelection(0);
        spinnerManglik.setSelection(0);
        spNames.setSelection(0);

        new CallRequest(MatrimonialDetailFragment.this).getMatrimonialMembers(user);
    }

    private void addMatrimonialDetail(View v) {
        String height = etHeight.getText().toString().trim();
        String weight = etWeight.getText().toString().trim();
        String otherInfo = etOtherInfo.getText().toString().trim();
        String handicapInfo = etHandicapInfo.getText().toString().trim();

        if (TextUtils.isEmpty(nameId) || nameId.equalsIgnoreCase("0")) {
            Utils.showAlert("Please select member", getActivity());
        } else if (TextUtils.isEmpty(height)) {
            etHeight.requestFocus();
            Utils.showToast("Please enter height", getActivity());
        } else if (TextUtils.isEmpty(weight)) {
            etWeight.requestFocus();
            Utils.showToast("Please enter weight", getActivity());
        } else if (TextUtils.isEmpty(skinColorId) || skinColorId.equalsIgnoreCase("0")) {
            Utils.showAlert("Please select skin color", getActivity());
        } else if (TextUtils.isEmpty(activateId) || activateId.equalsIgnoreCase("0")) {
            Utils.showAlert("Please select activation", getActivity());
        } else if (TextUtils.isEmpty(filePath)) {
            Utils.showAlert("Please select image", getActivity());
        } else {

            BitmapDrawable drawable = (BitmapDrawable) imgProfile.getDrawable();
            Bitmap bitmap = drawable.getBitmap();
            String encodePath = encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG, 100);

            Map<String, String> map = new HashMap<>();
            map.put("url", COMMON_URL + "MatrimonialDetails");
            map.put("FamilyDetailID", nameId);
            map.put("pHeight", height);
            map.put("pWeight", weight);
            map.put("skincolor", skinColorId);
            map.put("Handicap", handicapId);
            map.put("Handicapdetails", handicapInfo);
            map.put("IsManglik", manglikId);
            map.put("otherinfo", otherInfo);
            map.put("Isactive", activateId);
            map.put("photourl", encodePath);
            map.put("filename", fileName);


            try {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void editMatrimonialDetail(View v) {
        String height = etHeight.getText().toString().trim();
        String weight = etWeight.getText().toString().trim();
        String otherInfo = etOtherInfo.getText().toString().trim();
        String handicapInfo = etHandicapInfo.getText().toString().trim();

        if (TextUtils.isEmpty(nameId) || nameId.equalsIgnoreCase("0")) {
            Utils.showAlert("Please select member", getActivity());
        } else if (TextUtils.isEmpty(height)) {
            etHeight.requestFocus();
            Utils.showToast("Please enter height", getActivity());
        } else if (TextUtils.isEmpty(weight)) {
            etWeight.requestFocus();
            Utils.showToast("Please enter weight", getActivity());
        } else if (TextUtils.isEmpty(skinColorId) || skinColorId.equalsIgnoreCase("0")) {
            Utils.showAlert("Please select skin color", getActivity());
        } else if (TextUtils.isEmpty(activateId) || activateId.equalsIgnoreCase("0")) {
            Utils.showAlert("Please select activation", getActivity());
        } else {
            String encodePath = "";
            if (!TextUtils.isEmpty(filePath)) {
                BitmapDrawable drawable = (BitmapDrawable) imgProfile.getDrawable();
                Bitmap bitmap = drawable.getBitmap();
                encodePath = encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG, 100);
            }

            Map<String, String> map = new HashMap<>();
            map.put("url", COMMON_URL + "editmatrimonialdetails");
            map.put("FamilyDetailID", nameId);
            map.put("pHeight", height);
            map.put("pWeight", weight);
            map.put("skincolor", skinColorId);
            map.put("Handicap", handicapId);
            map.put("Handicapdetails", handicapInfo);
            map.put("IsManglik", manglikId);
            map.put("otherinfo", otherInfo);
            map.put("Isactive", activateId);
            map.put("photourl", encodePath);
            map.put("filename", fileName);

            new CallRequest(MatrimonialDetailFragment.this).editMatrimonialDetails(map);

            try {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked

            if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK
                    && null != data) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                int columnIndex = 0;
                if (cursor != null) {
                    cursor.moveToFirst();
                    columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    filePathFirst = cursor.getString(columnIndex);
                    System.out.println(filePathFirst);
                    cursor.close();

                }
                doCropDP(filePathFirst);
            }
            if (requestCode == RESULT_CROP_DP) {
                if (resultCode == Activity.RESULT_OK) {
                    if (!TextUtils.isEmpty(filePath)) {

                        System.out.println("path***" + filePath);
                        Bitmap selectedBitmap = decodeSampledBitmapFromPath(filePath, 100, 100);
                        System.out.println("selectedBitmap:::::" + selectedBitmap);
                        imgProfile.setImageBitmap(selectedBitmap);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doCropDP(String picPath) {
        try {

            Intent cropIntent = new Intent("com.android.camera.action.CROP");

            File f = new File(picPath);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");

            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", false);

            File sdCardDirectory = new File(Environment.getExternalStorageDirectory().getPath() + "/skgjst/profile_pictures");

            if (!sdCardDirectory.exists()) {
                sdCardDirectory.mkdirs();
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                    .format(new Date());

            String nw = "Profile_" + timeStamp + ".jpeg";

            File image = new File(sdCardDirectory, nw);

            //uploadPathDp = sdCardDirectory + "/" + nw;
            filePath = new File(sdCardDirectory, nw).getAbsolutePath();
            fileName = nw;
            System.out.println("UploadPath:::" + filePath);

            try {
                image.createNewFile();
            } catch (IOException ex) {
                Log.e("io", ex.getMessage());
            }

            Uri uri;
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                uri = FileProvider.getUriForFile(getActivity(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        image);
            } else {*/
            uri = Uri.fromFile(image);
            //}

            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, RESULT_CROP_DP);
        } catch (ActivityNotFoundException anfe) {
            String errorMessage = "Your device doesn't support the crop action!";

            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    public void setOnClick(int position) {
        btnSave.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.GONE);

        App.loadMemberArrayList.clear();
        App.loadMemberArrayName.clear();

        MatrimonialMember matrimonialMember = new MatrimonialMember();
    /*    matrimonialMember.FName = App.matrimonialDetailArrayList.get(position).getFName();
        matrimonialMember.FamilyDetailID = App.matrimonialDetailArrayList.get(position).getFamilyDetailID();
        App.loadMemberArrayList.add(matrimonialMember);
        App.loadMemberArrayName.add(App.matrimonialDetailArrayList.get(position).getFName());
*/
        ArrayAdapter aaName = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, App.loadMemberArrayName);
        aaName.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spNames.setAdapter(aaName);
        spNames.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    // relation_id = String.valueOf(relationArrayList.get(i).getRelationID());
                    nameId = String.valueOf(App.loadMemberArrayList.get(i).getFamilyDetailID());
                    /*if (i != 0) {
                        Name = App.loadNameArrayList.get(i).getFName();
                    } else {
                        Name = "";
                    }*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                nameId = "";
            }
        });

        nameId = String.valueOf(App.loadMemberArrayList.get(0).getFamilyDetailID());

        MatrimonialDetail matrimonialDetail = App.matrimonialDetailArrayList.get(position);

        if (matrimonialDetail.getPhotourl().isEmpty()) {
            imgProfile.setImageResource(R.drawable.avatar);
        } else {
            try {
                PicassoTrustAll.getInstance(getActivity())
                        .load(matrimonialDetail.getPhotourl().replace(" ", "%20"))
                        .error(R.drawable.avatar)
                        .resize(256, 256)
                        .into(imgProfile, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

       /* etWeight.setText(matrimonialDetail.getpWeight());
        etHeight.setText(matrimonialDetail.getpHeight());
        etOtherInfo.setText(matrimonialDetail.getOtherinfo());
        etHandicapInfo.setText(matrimonialDetail.getHandicapdetail());

        int manglikId = 0;
        for (int i = 0; i < manglikList.size(); i++) {
            if (String.valueOf(manglikList.get(i)).equalsIgnoreCase(String.valueOf(matrimonialDetail.getIsManglik()))) {
                manglikId = i;
                break;
            }
        }*/
      //  spinnerManglik.setSelection(manglikId);

        int handicapId = 0;
        for (int i = 0; i < handicapList.size(); i++) {
            if (String.valueOf(handicapList.get(i)).equalsIgnoreCase(String.valueOf(matrimonialDetail.getHandicap()))) {
                handicapId = i;
                break;
            }
        }
        spinnerHandicap.setSelection(handicapId);

        int skinColorId = 0;
        for (int i = 0; i < skinColorList.size(); i++) {
            if (String.valueOf(skinColorList.get(i)).equalsIgnoreCase(String.valueOf(matrimonialDetail.getSkincolor()))) {
                skinColorId = i;
                break;
            }
        }
        this.skinColorId = String.valueOf(skinColorId);
        spinnerSkinColor.setSelection(skinColorId);

        int activateId = 0;
        for (int i = 0; i < activateList.size(); i++) {
            if (String.valueOf(activateList.get(i)).equalsIgnoreCase(String.valueOf(matrimonialDetail.getIsactive()))) {
                activateId = i;
                break;
            }
        }
        this.activateId = String.valueOf(activateId);
        spinnerActivate.setSelection(activateId);

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case GetProfile:
                    App.matrimonialDetailArrayList.clear();
                    try {

                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {

                            JSONObject jObjData = jObj.getJSONObject("data");
                            if (jObjData != null) {

                                if (jObjData.getJSONArray("Matrimonial Registered Members") != null && jObjData.getJSONArray("Matrimonial Registered Members").length() > 0) {
                                    JSONArray jBranchArray = jObjData.getJSONArray("Matrimonial Registered Members");

                                    for (int i = 0; i < jBranchArray.length(); i++) {
                                        JSONObject jPackag = jBranchArray.getJSONObject(i);
                                        MatrimonialDetail otherMemberDetail = (MatrimonialDetail) jParser.parseJson(jPackag, new MatrimonialDetail());
                                        App.matrimonialDetailArrayList.add(otherMemberDetail);
                                    }

                                    matrimonialDetailAdapter = new MatrimonialDetailAdapter(MatrimonialDetailFragment.this, App.matrimonialDetailArrayList, getActivity());
                                    listView.setAdapter(matrimonialDetailAdapter);
                                }
                                System.out.println("profile size" + App.matrimonialDetailArrayList.size());
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();

                    break;

                case GetMatrimonialMember:
                    Utils.hideProgressDialog();
                    App.loadMemberArrayList.clear();
                    App.loadMemberArrayName.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jBranchArray = jObj.getJSONArray("data");
                                Utils.addMatrimonialMember();
                                for (int i = 0; i < jBranchArray.length(); i++) {

                                    JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    MatrimonialMember matrimonialMember = (MatrimonialMember) jParser.parseJson(jPackag, new MatrimonialMember());
                                    App.loadMemberArrayList.add(matrimonialMember);
                                    App.loadMemberArrayName.add(matrimonialMember.getFName());

                                }

                                ArrayAdapter aaName = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, App.loadMemberArrayName);
                                aaName.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spNames.setAdapter(aaName);
                                spNames.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        try {
                                            // relation_id = String.valueOf(relationArrayList.get(i).getRelationID());
                                            if (i != 0) {
                                                nameId = String.valueOf(App.loadMemberArrayList.get(i).getFamilyDetailID());
                                            } else {
                                                nameId = "";
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {
                                        nameId = "";
                                    }
                                });
                            } else {
                                Utils.addMatrimonialMember();

                                ArrayAdapter aaName = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, App.loadMemberArrayName);
                                aaName.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spNames.setAdapter(aaName);
                                spNames.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        try {
                                            // relation_id = String.valueOf(relationArrayList.get(i).getRelationID());
                                            if (i != 0) {
                                                nameId = App.loadMemberArrayList.get(i).getFName();
                                            } else {
                                                nameId = "";
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {
                                        nameId = "";
                                    }
                                });
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;

                case AddMatrimonialDetails:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {

                            Toast.makeText(getActivity(), jObj.getString("Message"), Toast.LENGTH_SHORT).show();

                            reset();

                            new CallRequest(MatrimonialDetailFragment.this).GetProfile(App.familyId);

                        } else {
                            Toast.makeText(getActivity(), jObj.getString("Message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case EditMatrimonialDetails:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {

                            Toast.makeText(getActivity(), jObj.getString("Message"), Toast.LENGTH_SHORT).show();

                            reset();

                            new CallRequest(MatrimonialDetailFragment.this).GetProfile(App.familyId);
                        } else {
                            Toast.makeText(getActivity(), jObj.getString("Message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
