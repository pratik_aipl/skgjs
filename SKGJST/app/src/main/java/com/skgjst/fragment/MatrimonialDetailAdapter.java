package com.skgjst.fragment;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.skgjst.model.MatrimonialDetail;
import com.skgjst.model.MemberDetailProfileInterface;
import com.skgjst.R;

import java.util.ArrayList;

/**
 * Created by Krupa Kakkad on 7/9/2018.
 */

public class MatrimonialDetailAdapter extends RecyclerView.Adapter<MatrimonialDetailAdapter.MyViewHolder> {

    public Context context;
    public String emailId, token, userId;
    public ArrayList<MatrimonialDetail> matrimonialDetailArrayList;
    public MemberDetailProfileInterface memberDetailProfileInterface;


    public MatrimonialDetailAdapter(MatrimonialDetailFragment fragment, ArrayList<MatrimonialDetail> matrimonialDetailArrayList, Context context) {
        this.context = context;
        this.matrimonialDetailArrayList = matrimonialDetailArrayList;
        this.memberDetailProfileInterface = (MemberDetailProfileInterface) fragment;
    }


    @Override
    public MatrimonialDetailAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_member_profile_item, parent, false);

        return new MatrimonialDetailAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MatrimonialDetailAdapter.MyViewHolder holder, final int position) {


    //    holder.tvName.setText(matrimonialDetailArrayList.get(position).getFName());

        holder.imgEdit.setTag(position);
        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = (int) view.getTag();
//                matrimonialDetailArrayList.get(position).getFName();
                memberDetailProfileInterface.setOnClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return matrimonialDetailArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvName;
        public ImageView imgEdit;

        public MyViewHolder(View v) {
            super(v);
            tvName = v.findViewById(R.id.tvName);
            imgEdit = v.findViewById(R.id.imgEdit);
        }
    }
}

