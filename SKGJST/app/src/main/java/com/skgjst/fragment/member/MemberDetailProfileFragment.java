package com.skgjst.fragment.member;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.skgjst.App;
import com.skgjst.BaseFragment;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.BloodGroup;
import com.skgjst.model.BusinessNature;
import com.skgjst.model.Gender;
import com.skgjst.model.MaritalStatus;
import com.skgjst.model.OtherMemberDetail;
import com.skgjst.model.Profession;
import com.skgjst.model.Relation;
import com.skgjst.model.Surname;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.PicassoTrustAll;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.app.Activity.RESULT_OK;

/**
 * Created by vidhi-techmishty on 2/4/18.
 */

public class MemberDetailProfileFragment extends BaseFragment implements AsynchTaskListner, MemberDetailProfileInterface {

    private static final int SELECT_PICTURE = 1;
    private static final int RESULT_CROP_DP = 5;
    public LinearLayout llAddMember;
    public View v;
    public RecyclerView listView;
    public JsonParserUniversal jParser;
    public MemberDetailProfileAdapter memberDetailProfileAdapter;
    public TextView tvName;
    public EditText etFname, etMiddleName, etMarriageDate, etMobileno, etEducation, etBusinessDetail,
            etCompanyName, etCompanyAddress, etDob, etOfficeContact, etEmailId, etWebsiteUrl;
    public Button btnSave, btnNext, btnReset, btnAdd;
    public Relation relation;
    public BloodGroup bloodGroup;
    public Surname surname;
    public MaritalStatus maritalStatus;
    public Profession profession;
    public BusinessNature businessNature;
    public String gender_id = "", relation_id = "", marital_status_id = "", bloodGroup_id = "", professionId = "", businessId = "", relationId = "";
    public String surnameId = "";
    //public int relationId = 2;
    public Spinner spSurname, spGeneder, spRelation, spBloodGroup, spProfession, spBusiness, spMaritalStatus;
    public ImageView imgProfile, imgCamera;
    public String filePathFirst, filePath, fileName;
    int checkRelation = 0;
    private RecyclerView.LayoutManager layoutManager;


    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat,
                                        int quality) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    public static Bitmap decodeSampledBitmapFromPath(String path, int reqWidth,
                                                     int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap bmp = BitmapFactory.decodeFile(path, options);
        return bmp;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_member_detail_profile, container, false);

        jParser = new JsonParserUniversal();

        listView = (RecyclerView) v.findViewById(R.id.listView);
        etDob = v.findViewById(R.id.etDob);
        imgProfile = v.findViewById(R.id.imgProfile);
        imgCamera = v.findViewById(R.id.imgCamera);

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, SELECT_PICTURE);
            }
        });
        btnSave = v.findViewById(R.id.btnSave);
        btnNext = v.findViewById(R.id.btnNext);
        btnReset = v.findViewById(R.id.btnReset);
        btnAdd = v.findViewById(R.id.btnAdd);

        spSurname = v.findViewById(R.id.spSurname);
        spRelation = v.findViewById(R.id.spRelation);
        spGeneder = v.findViewById(R.id.spGeneder);
        spBloodGroup = v.findViewById(R.id.spBloodGroup);
        spMaritalStatus = v.findViewById(R.id.spMaritalStatus);

        spProfession = v.findViewById(R.id.spProfessions);
        spBusiness = v.findViewById(R.id.spNatureOfBussiness);

        etFname = v.findViewById(R.id.etFirstName);

        etMiddleName = v.findViewById(R.id.etMiddleName);
        etMarriageDate = v.findViewById(R.id.etMarriageDate);
        etMobileno = v.findViewById(R.id.etMobileNumber);

        etEducation = v.findViewById(R.id.etEducation);
        etBusinessDetail = v.findViewById(R.id.etBJDetil);
        etCompanyName = v.findViewById(R.id.etOfficeCompanyName);

        etCompanyAddress = v.findViewById(R.id.etOfficeCompanyAddress);
        etOfficeContact = v.findViewById(R.id.etOfficeContactNo);
        etEmailId = v.findViewById(R.id.etEmail);
        etWebsiteUrl = v.findViewById(R.id.etWebSiteUrl);

        etDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.generateDatePicker(getActivity(), etDob);
            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // ProfileFragment.setAdapterPos(4);
            }
        });

        etMarriageDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.generateDatePicker(getActivity(), etMarriageDate);
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMemberDetails();
            }
        });

        btnSave.setVisibility(View.GONE);
        btnAdd.setVisibility(View.VISIBLE);

        layoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(layoutManager);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String firstName = etFname.getText().toString().trim();
                String middleName = etMiddleName.getText().toString().trim();
                String marriageDate = etMarriageDate.getText().toString().trim();
                String mobileno = etMobileno.getText().toString().trim();
                String education = etEducation.getText().toString().trim();
                String dob = etDob.getText().toString().trim();

                String businessDetail = etBusinessDetail.getText().toString().trim();
                String companyName = etCompanyName.getText().toString().trim();
                String companyAddress = etCompanyAddress.getText().toString().trim();
                String officeContact = etOfficeContact.getText().toString().trim();
                String emailId = etEmailId.getText().toString().trim();
                String websiteUrl = etWebsiteUrl.getText().toString().trim();
                String genderId = String.valueOf(gender_id.charAt(0));


                final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
                if (etFname.getText().toString().equals("")) {
                    etFname.requestFocus();
                    Utils.showToast("Please enter first name", getActivity());
                } else if (etMiddleName.getText().toString().isEmpty()) {
                    etMiddleName.requestFocus();
                    Utils.showToast("Please enter middle name", getActivity());
                } else if (etDob.getText().toString().isEmpty()) {
                    etDob.requestFocus();
                    Utils.showToast("Please enter Dob", getActivity());
                } else if (TextUtils.isEmpty(relationId)) {
                    Utils.showAlert("Please select relation", getActivity());
                } else if (TextUtils.isEmpty(gender_id)) {
                    Utils.showAlert("Please select gender", getActivity());
                } else if (TextUtils.isEmpty(marital_status_id)) {
                    Utils.showAlert("Please select marital status", getActivity());
                } else if (TextUtils.isEmpty(professionId)) {
                    Utils.showAlert("Please select profession", getActivity());
                } else if (TextUtils.isEmpty(businessId)) {
                    Utils.showAlert("Please select nature of business", getActivity());
                } else {
                    App.memberDetails.put("firstName", firstName);
                    App.memberDetails.put("middleName", middleName);
                    App.memberDetails.put("marriageDate", marriageDate);
                    App.memberDetails.put("mobileNo", mobileno);
                    App.memberDetails.put("education", education);
                    App.memberDetails.put("businessDetail", businessDetail);
                    App.memberDetails.put("companyName", companyName);
                    App.memberDetails.put("companyAddress", companyAddress);
                    App.memberDetails.put("officeContact", officeContact);
                    App.memberDetails.put("emailId", emailId);
                    App.memberDetails.put("websiteUrl", websiteUrl);
                    App.memberDetails.put("relation_id", String.valueOf(relationId));
                    App.memberDetails.put("genderId", genderId);
                    App.memberDetails.put("bloodGroup_id", bloodGroup_id);
                    App.memberDetails.put("surname_id", "");
                    App.memberDetails.put("maritalStatusId", marital_status_id);
                    App.memberDetails.put("profession_id", professionId);
                    App.memberDetails.put("business_nature_id", businessId);
                    App.memberDetails.put("dob", dob);
                    App.memberDetails.put("filename", Utils.getJpegName());

                    BitmapDrawable drawable = (BitmapDrawable) imgProfile.getDrawable();
                    Bitmap bitmap = drawable.getBitmap();
                    String encodePath = encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG, 100);
                    App.memberDetails.put("PhotoUrl", encodePath);


                    new CallRequest(MemberDetailProfileFragment.this).editMember();

                }
            }
        });

        new CallRequest(MemberDetailProfileFragment.this).GetProfile(App.familyId);

        System.out.println("***editMEmberSurname!!!" + App.SurName);

        new CallRequest(MemberDetailProfileFragment.this).Relationformembers();
        new CallRequest(MemberDetailProfileFragment.this).MaritalStatus();
        new CallRequest(MemberDetailProfileFragment.this).Gender_List();
        new CallRequest(MemberDetailProfileFragment.this).BloodGroup();
        new CallRequest(MemberDetailProfileFragment.this).SurNameList();
        new CallRequest(MemberDetailProfileFragment.this).Profession_List();
        new CallRequest(MemberDetailProfileFragment.this).NatureOfBusiness_List();


        return v;
    }

    private void addMemberDetails() {

        spSurname.setEnabled(true);

        String firstName = etFname.getText().toString().trim();
        String middleName = etMiddleName.getText().toString().trim();
        String marriageDate = etMarriageDate.getText().toString().trim();
        String mobileno = etMobileno.getText().toString().trim();
        String education = etEducation.getText().toString().trim();
        String dob = etDob.getText().toString().trim();

        String businessDetail = etBusinessDetail.getText().toString().trim();
        String companyName = etCompanyName.getText().toString().trim();
        String companyAddress = etCompanyAddress.getText().toString().trim();
        String officeContact = etOfficeContact.getText().toString().trim();
        String emailId = etEmailId.getText().toString().trim();
        String websiteUrl = etWebsiteUrl.getText().toString().trim();
        String genderId = String.valueOf(gender_id.charAt(0));


        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (etFname.getText().toString().equals("")) {
            etFname.requestFocus();
            Utils.showToast("Please enter first name", getActivity());
        } else if (etMiddleName.getText().toString().isEmpty()) {
            etMiddleName.requestFocus();
            Utils.showToast("Please enter middle name", getActivity());
        } else if (etDob.getText().toString().isEmpty()) {
            etDob.requestFocus();
            Utils.showToast("Please enter Dob", getActivity());
        } else if (TextUtils.isEmpty(relationId)) {
            Utils.showAlert("Please select relation", getActivity());
        } else if (TextUtils.isEmpty(gender_id)) {
            Utils.showAlert("Please select gender", getActivity());
        } else if (TextUtils.isEmpty(surnameId)) {
            Utils.showAlert("Please select surname", getActivity());
        } else if (TextUtils.isEmpty(marital_status_id)) {
            Utils.showAlert("Please select marital status", getActivity());
        } else if (TextUtils.isEmpty(professionId)) {
            Utils.showAlert("Please select profession", getActivity());
        } else if (TextUtils.isEmpty(businessId)) {
            Utils.showAlert("Please select nature of business", getActivity());
        } else {

            if (TextUtils.isEmpty(education)) {
                education = "";
            }

            App.memberDetails.put("firstName", firstName);
            App.memberDetails.put("middleName", middleName);
            App.memberDetails.put("marriageDate", marriageDate);
            App.memberDetails.put("mobileNo", mobileno);
            App.memberDetails.put("education", education);
            App.memberDetails.put("businessDetail", businessDetail);
            App.memberDetails.put("companyName", companyName);
            App.memberDetails.put("companyAddress", companyAddress);
            App.memberDetails.put("officeContact", officeContact);
            App.memberDetails.put("emailId", emailId);
            App.memberDetails.put("websiteUrl", websiteUrl);
            System.out.println("relationId===" + relationId);
            App.memberDetails.put("relation_id", String.valueOf(relationId));
            App.memberDetails.put("genderId", genderId);
            App.memberDetails.put("bloodGroup_id", bloodGroup_id);
            App.memberDetails.put("surname_id", surnameId);
            App.memberDetails.put("maritalStatusId", marital_status_id);
            App.memberDetails.put("profession_id", professionId);
            App.memberDetails.put("business_nature_id", businessId);
            App.memberDetails.put("dob", dob);
            App.memberDetails.put("filename", filePath);

            BitmapDrawable drawable = (BitmapDrawable) imgProfile.getDrawable();
            Bitmap bitmap = drawable.getBitmap();
            String encodePath = encodeToBase64(bitmap, Bitmap.CompressFormat.JPEG, 100);
            App.memberDetails.put("PhotoUrl", encodePath);

            System.out.println("Member Data::" + App.memberDetails);

          /*  if(!App.familyId.isEmpty())
            {

            }else
            {
                Utils.showToast("Please Fill Further Details of registration",getActivity());
            }*/


            System.out.println("!!!!familyid" + App.familyId);

            if (!App.familyId.isEmpty()) {
                new CallRequest(MemberDetailProfileFragment.this).registerMemberDetails();

            } else {
                Utils.showToast("Please enter further detail of Registation...", getActivity());
            }

            try {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void switchFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, fragment, fragment.getClass().getSimpleName());
        transaction.addToBackStack(fragment.getClass().getSimpleName());
        transaction.commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked

            if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK
                    && null != data) {
                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};
                Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                        filePathColumn, null, null, null);
                int columnIndex = 0;
                if (cursor != null) {
                    cursor.moveToFirst();
                    columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    filePathFirst = cursor.getString(columnIndex);
                    System.out.println(filePathFirst);
                    cursor.close();

                }
                doCropDP(filePathFirst);
            }
            if (requestCode == RESULT_CROP_DP) {
                if (resultCode == Activity.RESULT_OK) {
                    if (!TextUtils.isEmpty(filePath)) {

                        System.out.println("path***" + filePath);
                        Bitmap selectedBitmap = decodeSampledBitmapFromPath(filePath, 100, 100);
                        System.out.println("selectedBitmap:::::" + selectedBitmap);
                        imgProfile.setImageBitmap(selectedBitmap);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doCropDP(String picPath) {
        try {

            Intent cropIntent = new Intent("com.android.camera.action.CROP");

            File f = new File(picPath);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");

            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256);
            cropIntent.putExtra("outputY", 256);
            // retrieve data on return
            cropIntent.putExtra("return-data", false);

            File sdCardDirectory = new File(Environment.getExternalStorageDirectory().getPath() + "/skgjst/profile_pictures");

            if (!sdCardDirectory.exists()) {
                sdCardDirectory.mkdirs();
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                    .format(new Date());

            String nw = "Profile_" + timeStamp + ".jpeg";

            File image = new File(sdCardDirectory, nw);

            //uploadPathDp = sdCardDirectory + "/" + nw;
            filePath = new File(sdCardDirectory, nw).getAbsolutePath();
            fileName = nw;
            System.out.println("UploadPath:::" + filePath);

            try {
                image.createNewFile();
            } catch (IOException ex) {
                Log.e("io", ex.getMessage());
            }

            Uri uri;
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                uri = FileProvider.getUriForFile(getActivity(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        image);
            } else {*/
            uri = Uri.fromFile(image);
            //}

            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, RESULT_CROP_DP);
        } catch (ActivityNotFoundException anfe) {
            String errorMessage = "your device doesn't support the crop action!";

            Toast toast = Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case GetProfile:
                    App.otherMemberDetailArrayList.clear();
                    try {

                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {

                            JSONObject jObjData = jObj.getJSONObject("data");
                            if (jObjData != null) {

                                if (jObjData.getJSONArray("Other Family Member Details") != null && jObjData.getJSONArray("Other Family Member Details").length() > 0) {
                                    JSONArray jBranchArray = jObjData.getJSONArray("Other Family Member Details");

                                    for (int i = 0; i < jBranchArray.length(); i++) {
                                        JSONObject jPackag = jBranchArray.getJSONObject(i);
                                        OtherMemberDetail otherMemberDetail = (OtherMemberDetail) jParser.parseJson(jPackag, new OtherMemberDetail());
                                        App.otherMemberDetailArrayList.add(otherMemberDetail);
                                    }

                                    memberDetailProfileAdapter = new MemberDetailProfileAdapter(MemberDetailProfileFragment.this, App.otherMemberDetailArrayList, getActivity());
                                    listView.setAdapter(memberDetailProfileAdapter);

                                    //setDefaultData();
                                }
                                System.out.println("profile size" + App.otherMemberDetailArrayList.size());
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();

                    break;


                case Relationformembers:
                    Utils.hideProgressDialog();
                    App.relationMembersArrayList.clear();
                    App.relationMembersArrayName.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jBranchArray = jObj.getJSONArray("data");

                                //Utils.addDefaultRelation();
                                Relation relation = new Relation();
                                relation.RelationName = "Select Relation";
                                App.relationMembersArrayList.add(relation);
                                App.relationMembersArrayName.add(relation.getRelationName());

                                for (int i = 0; i < jBranchArray.length(); i++) {
                                    JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    relation = (Relation) jParser.parseJson(jPackag, new Relation());
                                    App.relationMembersArrayList.add(relation);
                                    App.relationMembersArrayName.add(relation.getRelationName());

                                }

                                System.out.println("relation size::::"+App.relationMembersArrayList.size());

                                try {
                                    ArrayAdapter aaCountry = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, App.relationMembersArrayName);
                                    aaCountry.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    spRelation.setAdapter(aaCountry);

                                    spRelation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                            try {
                                                // relation_id = String.valueOf(App.relationMembersArrayList.get(i).getRelationID());
                                                if (i != 0) {
                                                    relationId = String.valueOf(App.relationMembersArrayList.get(i).getRelationID());
                                                    // relationId = relationId + 1;
                                                    System.out.println("RelationId::::" + relationId);
                                                } else {
                                                    relationId = "0";
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> adapterView) {
                                            relationId = "0";
                                        }
                                    });

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;


                case MaritalStatus:
                    Utils.hideProgressDialog();
                    App.maritalStatusArrayList.clear();
                    App.maritalStatusArrayName.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jBranchArray = jObj.getJSONArray("data");

                                Utils.addDefaultMaritalStatus();

                                for (int i = 0; i < jBranchArray.length(); i++) {
                                    JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    maritalStatus = (MaritalStatus) jParser.parseJson(jPackag, new MaritalStatus());
                                    App.maritalStatusArrayList.add(maritalStatus);
                                    App.maritalStatusArrayName.add(maritalStatus.getMaritalstatus());
                                }
                                try {
                                    ArrayAdapter aaState = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, App.maritalStatusArrayName);
                                    aaState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    spMaritalStatus.setAdapter(aaState);
                                    spMaritalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                            try {
                                                if (i != 0) {
                                                    marital_status_id = String.valueOf(App.maritalStatusArrayList.get(i).getMaritalstatusID());
                                                    System.out.println("MaritalId::::" + marital_status_id);
                                                } else {
                                                    marital_status_id = "0";
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> adapterView) {
                                            marital_status_id = "0";
                                        }
                                    });

                                } catch (NullPointerException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;


                case Gender_List:
                    Utils.hideProgressDialog();
                    App.genderArrayList.clear();
                    App.genderArrayName.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jBranchArray = jObj.getJSONArray("data");

                                Utils.addDefaultGender();

                                for (int i = 0; i < jBranchArray.length(); i++) {
                                    JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    //Gender gender = (Gender) jParser.parseJson(jPackag, new Gender());

                                    String gender = jPackag.getString("gender");
                                    String Gendere = jPackag.getString("Gender");

                                    Gender gender1 = new Gender();
                                    gender1.gender = gender;
                                    gender1.Gender = Gendere;

                                    App.genderArrayList.add(gender1);
                                    App.genderArrayName.add(gender);
                                }

                                try {
                                    ArrayAdapter aaCity = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, App.genderArrayName);
                                    aaCity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    spGeneder.setAdapter(aaCity);
                                    spGeneder.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                            try {
                                                if (i != 0) {
                                                    gender_id = App.genderArrayList.get(i).gender;
                                                } else {
                                                    gender_id = "0";
                                                }
                                                System.out.println("GenderId::::" + gender_id);

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> adapterView) {
                                            gender_id = "0";
                                        }
                                    });
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    break;

                case BloodGroup:
                    App.bloodGroupArrayList.clear();
                    App.bloodGroupArrayName.clear();
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jBranchArray = jObj.getJSONArray("data");
                                Utils.addDefaultBloodGroup();

                                for (int i = 0; i < jBranchArray.length(); i++) {
                                    JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    bloodGroup = (BloodGroup) jParser.parseJson(jPackag, new BloodGroup());
                                    App.bloodGroupArrayList.add(bloodGroup);
                                    App.bloodGroupArrayName.add(bloodGroup.getBloodGroupName());
                                }

                                try {
                                    ArrayAdapter aaCity = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, App.bloodGroupArrayName);
                                    aaCity.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    spBloodGroup.setAdapter(aaCity);
                                    spBloodGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                            try {
                                                if (i != 0) {
                                                    bloodGroup_id = String.valueOf(App.bloodGroupArrayList.get(i).getBloodGroupID());
                                                    System.out.println("BloodGroupId::::" + bloodGroup_id);
                                                } else {
                                                    bloodGroup_id = "0";
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> adapterView) {
                                            bloodGroup_id = "0";
                                        }
                                    });

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case SurNameList:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jBranchArray = jObj.getJSONArray("data");

                                Utils.addDefaultSurname();

                                for (int i = 0; i < jBranchArray.length(); i++) {
                                    JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    surname = (Surname) jParser.parseJson(jPackag, new Surname());
                                    App.surnameArrayList.add(surname);
                                    App.surnameArrayName.add(surname.getSurname());

                                }

                                System.out.println("LIst size::::" + App.surnameArrayList.size());

                                ArrayAdapter aaProfession = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, App.surnameArrayName);
                                aaProfession.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spSurname.setAdapter(aaProfession);
                                /*spSurname.setClickable(false);
                                spSurname.setFocusable(false);*/

                                spSurname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        try {

                                            if (i != 0) {
                                                surnameId = String.valueOf(App.surnameArrayList.get(i).getSurname());
                                                System.out.println("SurnameId::::" + surnameId);
                                            } else {
                                                surnameId = "0";
                                            }

                                            System.out.println("SurnameId::::" + surnameId);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {
                                        surnameId = "0";
                                    }
                                });

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;

                case Profession_List:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {

                                App.professionArrayList.clear();
                                App.professionArrayName.clear();

                                JSONArray jBranchArray = jObj.getJSONArray("data");
                                Utils.addDefaultProfession();
                                for (int i = 0; i < jBranchArray.length(); i++) {
                                    JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    profession = (Profession) jParser.parseJson(jPackag, new Profession());
                                    App.professionArrayList.add(profession);
                                    App.professionArrayName.add(profession.getProfessionName());

                                }
                                //Collections.sort( App.professionArrayName, String.CASE_INSENSITIVE_ORDER);
                                System.out.println("LIst size::::" + App.professionArrayList.size());

                                ArrayAdapter aaProfession = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, App.professionArrayName);
                                aaProfession.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spProfession.setAdapter(aaProfession);

                                spProfession.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        try {
                                            if (i != 0) {
                                                professionId = App.professionArrayList.get(i).getProfessionID();
                                                System.out.println("ProfessionId::::" + professionId);
                                            } else {
                                                professionId = "0";
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {
                                        professionId = App.professionArrayList.get(0).getProfessionID();
                                    }
                                });
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    break;

                case NatureOfBusiness_List:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {

                                App.businessNatureArrayList.clear();
                                App.businessArrayName.clear();

                                JSONArray jBranchArray = jObj.getJSONArray("data");

                                Utils.addDefaultNatureOfBusiness();

                                for (int i = 0; i < jBranchArray.length(); i++) {
                                    JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    businessNature = (BusinessNature) jParser.parseJson(jPackag, new BusinessNature());
                                    App.businessNatureArrayList.add(businessNature);
                                    App.businessArrayName.add(businessNature.getIndustryName());
                                }
                                ////Collections.sort(App.businessArrayName, String.CASE_INSENSITIVE_ORDER);
                                System.out.println("List size::::" + App.businessNatureArrayList.size());

                                ArrayAdapter aaBusiness = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, App.businessArrayName);
                                aaBusiness.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spBusiness.setAdapter(aaBusiness);
                                spBusiness.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                        try {
                                            if (i != 0) {
                                                businessId = String.valueOf(App.businessNatureArrayList.get(i).getIndustryID());
                                            } else {
                                                businessId = "0";
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> adapterView) {
                                        businessId = String.valueOf(App.businessNatureArrayList.get(0).getIndustryID());
                                    }
                                });
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    break;

                case editMember:
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            /*            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                             */            //    JSONArray jBranchArray = jObj.getJSONArray("data");
                            Utils.showToast(jObj.getString("Message"), getActivity());

                            reset();
                            new CallRequest(MemberDetailProfileFragment.this).GetProfile(App.familyId);
                            //    }

                        } else {
                            Utils.showToast(jObj.getString("Message"), getActivity());
                            Utils.hideProgressDialog();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;

                case RegisterMember:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            String FamilyDetailID = jObj.getString("FamilyDetailID");

                            App.FamilyDetailID = FamilyDetailID;
                            Toast.makeText(getActivity(), jObj.getString("Message"), Toast.LENGTH_SHORT).show();

                            reset();
                            new CallRequest(MemberDetailProfileFragment.this).GetProfile(App.familyId);

                        } else {
                            Toast.makeText(getActivity(), jObj.getString("Message"), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
            }
        }

    }


    public void reset() {

        btnSave.setVisibility(View.GONE);
        btnAdd.setVisibility(View.VISIBLE);

        etBusinessDetail.setText("");
        etCompanyAddress.setText("");
        etDob.setText("");
        etCompanyName.setText("");
        etEducation.setText("");
        etEmailId.setText("");
        etFname.setText("");
        etMarriageDate.setText("");
        etMiddleName.setText("");
        etMobileno.setText("");
        etOfficeContact.setText("");
        etWebsiteUrl.setText("");

        filePath = "";

        imgProfile.setImageResource(R.drawable.avatar);

        spRelation.setSelection(0);
        spBloodGroup.setSelection(0);
        spMaritalStatus.setSelection(0);
        spBusiness.setSelection(0);
        spGeneder.setSelection(0);
        spProfession.setSelection(0);
        spSurname.setSelection(0);
        spSurname.setEnabled(true);
    }

    @Override
    public void setOnClick(int position) {

        btnSave.setVisibility(View.VISIBLE);
        btnAdd.setVisibility(View.GONE);

        spSurname.setEnabled(false);

        OtherMemberDetail otherMember = App.otherMemberDetailArrayList.get(position);

        etFname.setText(otherMember.getFName());
        etMiddleName.setText(otherMember.getMName());
        etMarriageDate.setText(otherMember.getMarriageDate());
        etMobileno.setText(otherMember.getContactNo());
        etEducation.setText(otherMember.getEducation());
        etBusinessDetail.setText(otherMember.getBusinessDetail());
        etCompanyName.setText(otherMember.getOfficeName());
        etCompanyAddress.setText(otherMember.getOfficeAddress());
        etDob.setText(App.getFormattedTime(otherMember.getDOB()));
        etOfficeContact.setText(otherMember.getOfficeContNo());
        etEmailId.setText(otherMember.getEmailID());
        etWebsiteUrl.setText(otherMember.getWebsiteURL());
        App.FamilyDetailID = String.valueOf(otherMember.getFamilyDetailID());

        if (otherMember.getPhotoURL().isEmpty()) {
            imgProfile.setImageResource(R.drawable.avatar);
        } else {
            try {
                PicassoTrustAll.getInstance(getActivity())
                        .load(otherMember.getPhotoURL().replace(" ", "%20"))
                        .error(R.drawable.avatar)
                        .resize(256, 256)
                        .into(imgProfile, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (otherMember.getGender().equalsIgnoreCase("Male")) {
            spGeneder.setSelection(1);

        } else {
            spGeneder.setSelection(2);
        }

        int maritalStatusId = 0;
        for (int i = 0; i < App.maritalStatusArrayList.size(); i++) {
            if (String.valueOf(App.maritalStatusArrayList.get(i).getMaritalstatusID()).equalsIgnoreCase(String.valueOf(otherMember.getMaritalStatusID()))) {
                maritalStatusId = i;
                break;
            }
        }
        spMaritalStatus.setSelection(maritalStatusId);


        int bloodGroupId = 0;
        for (int i = 0; i < App.bloodGroupArrayList.size(); i++) {
            if (String.valueOf(App.bloodGroupArrayList.get(i).getBloodGroupID()).equalsIgnoreCase(String.valueOf(otherMember.getBloodGroupID()))) {
                bloodGroupId = i;
                break;
            }
        }
        spBloodGroup.setSelection(bloodGroupId);


        int businessId = 0;
        for (int i = 0; i < App.businessNatureArrayList.size(); i++) {
            if (String.valueOf(App.businessNatureArrayList.get(i).getIndustryID()).equalsIgnoreCase(String.valueOf(otherMember.getBusinessType()))) {
                businessId = i;
                break;
            }
        }
        spBusiness.setSelection(businessId);


        int professionId = 0;
        for (int i = 0; i < App.professionArrayList.size(); i++) {
            if (String.valueOf(App.professionArrayList.get(i).getProfessionName()).equalsIgnoreCase(String.valueOf(otherMember.getBusinessType()))) {
                professionId = i;
                break;
            }
        }
        spProfession.setSelection(professionId);


        int relationId = 0;
        for (int i = 0; i < App.relationMembersArrayList.size(); i++) {
            if (String.valueOf(App.relationMembersArrayList.get(i).getRelationID()).equalsIgnoreCase(String.valueOf(otherMember.getRalationID()))) {
                relationId = i;
                break;
            }
        }
        spRelation.setSelection(relationId);

        System.out.println("surname:::" + otherMember.getSurName());
        int surnameId = 0;
        for (int i = 0; i < App.surnameArrayList.size(); i++) {
            if (String.valueOf(App.surnameArrayList.get(i).getSurname()).equalsIgnoreCase(String.valueOf(otherMember.getSurName()))) {
                surnameId = i;
                break;
            }
        }

        spSurname.setSelection(surnameId);
        etFname.requestFocus();
    }
}
