package com.skgjst.fragment.member;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.skgjst.model.OtherMemberDetail;
import com.skgjst.R;
import java.util.ArrayList;

/**
 * Created by empiere-php on 12/26/2017.
 */

public class MemberDetailProfileAdapter extends RecyclerView.Adapter<MemberDetailProfileAdapter.MyViewHolder> {

    //  public ArrayList<EquipmentsModel> list;
    //  public List<EquipmentsModel> searchFilterList = new ArrayList<>();
    public Context context;
    public String emailId, token, userId;
    public ArrayList<OtherMemberDetail> otherMemberDetailArrayList;
    public MemberDetailProfileInterface memberDetailProfileInterface;


    public MemberDetailProfileAdapter(MemberDetailProfileFragment memberDetailProfileFragment, ArrayList<OtherMemberDetail> memberDetailArrayList, Context context) {
        this.context = context;
        this.otherMemberDetailArrayList = memberDetailArrayList;
        this.memberDetailProfileInterface = (MemberDetailProfileInterface) memberDetailProfileFragment;
    }


    @Override
    public MemberDetailProfileAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_member_profile_item, parent, false);

        return new MemberDetailProfileAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MemberDetailProfileAdapter.MyViewHolder holder, final int position) {


        holder.tvName.setText(otherMemberDetailArrayList.get(position).getFName());

        holder.imgEdit.setTag(position);
        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = (int) view.getTag();
                otherMemberDetailArrayList.get(position).getFName();
                memberDetailProfileInterface.setOnClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return otherMemberDetailArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView imgAdd;
        public TextView tvName;
        public EditText etFname, etMiddleName, etMarriageDate, etMobileno, etEducation, etBusinessDetail,
                etCompanyName, etCompanyAddress, etDob, etOfficeContact, etEmailId, etWebsiteUrl;
        public Button btnSave, btnNext;
        public ImageView imgEdit;

        public Spinner spSurname, spGeneder, spRelation, spBloodGroup, spProfession, spBusiness, spMaritalStatus;

        public MyViewHolder(View v) {
            super(v);

            tvName = v.findViewById(R.id.tvName);
            imgEdit=v.findViewById(R.id.imgEdit);

        }
    }
}

