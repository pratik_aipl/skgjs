package com.skgjst.fragment.business;


import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.skgjst.App;
import com.skgjst.BaseFragment;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.BusinessList;
import com.skgjst.model.BusinessNature;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import com.skgjst.model.Profession;
import static com.skgjst.activities.DashBoardActivity.tv_title;

/**
 * Created by vidhi-techmishty on 2/4/18.
 */

public class BusinessSearchFragment extends BaseFragment implements AsynchTaskListner {
    public View v;
    public RecyclerView listView;
    public BusinessSearchAdapter businessSearchAdapter;
    public ImageView imgSearch, imgClose;
    public Spinner spBusiness, spProfession;
    public Profession profession;
    public BusinessNature businessNature;
    public JsonParserUniversal jParser;

    public String professionId = "", businessId = "";
    public LinearLayout llFilter;

    public EditText etName;
    public Dialog dialog;
    public TextView tvMemberCount, tvNoSearchItems;
    private RecyclerView.LayoutManager layoutManager;
    public ArrayList<BusinessList> businessArrayList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_business_search, container, false);

        listView = (RecyclerView) v.findViewById(R.id.listView);

        jParser = new JsonParserUniversal();
        layoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(layoutManager);

        tvMemberCount = v.findViewById(R.id.tvMemberCount);
        tvNoSearchItems = v.findViewById(R.id.tvNoSearchItems);

        imgSearch = v.findViewById(R.id.imgSearch);

        if (businessArrayList.size() > 0) {
            tvMemberCount.setText(String.valueOf(businessArrayList.size()) + " " + "Members");
            layoutManager = new LinearLayoutManager(getActivity());
            listView.setLayoutManager(layoutManager);
            businessSearchAdapter = new BusinessSearchAdapter(BusinessSearchFragment.this, getActivity(), businessArrayList);
            listView.setAdapter(businessSearchAdapter);
            tvNoSearchItems.setVisibility(View.GONE);
        }

        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_business_search);


        //      new CallRequest(BusinessSearchFragment.this).BusinessSearch("", "", "");


        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                spProfession = dialog.findViewById(R.id.spinnerProfession);
              //  spBusiness = dialog.findViewById(R.id.spinnerNatureofBusiness);

                etName = dialog.findViewById(R.id.et_Name);
                llFilter = dialog.findViewById(R.id.ll_Filter);

                professionId = "";
                businessId = "";
                etName.setText("");

                new CallRequest(BusinessSearchFragment.this).Profession_List();
                new CallRequest(BusinessSearchFragment.this).NatureOfBusiness_List();

                /*if (App.professionArrayList.size() == 0) {
                    new CallRequest(BusinessSearchFragment.this).Profession_List();

                } else {
                    ArrayAdapter aaProfession = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, App.professionArrayName);
                    aaProfession.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spProfession.setAdapter(new NothingSelectedSpinnerAdapter(aaProfession, R.layout.contact_spinner_profession_nothing_selected, getContext()));
                }*/

                /*if (App.businessNatureArrayList.size() == 0) {
                    new CallRequest(BusinessSearchFragment.this).NatureOfBusiness_List();
                } else {

                    ArrayAdapter aaBusiness = new ArrayAdapter(getActivity(), R.layout.spinner_text, App.businessArrayName);
                    aaBusiness.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spBusiness.setAdapter(new NothingSelectedSpinnerAdapter(aaBusiness, R.layout.contact_spinner_business_nothing_selected, getContext()));
                }*/

                spProfession.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        try {
                            if (i != 0) {
                                professionId = App.professionArrayList.get(i).getProfessionName();
                                System.out.println("ProfessionId::::" + professionId);
                            } else {
                                professionId = "";
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        professionId = "";
                    }
                });


                spBusiness.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        try {
                            if (i != 0) {
                                businessId = String.valueOf(App.businessNatureArrayList.get(i).getIndustryName());
                            } else {
                                businessId = "";
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                        businessId = "";
                    }
                });


                imgClose = dialog.findViewById(R.id.imgClose);
                imgClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        businessId = "";
                        professionId = "";
                        dialog.dismiss();
                    }
                });

                llFilter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        System.out.println("***businessid" + businessId);
                        System.out.println("***professionid" + professionId);

                        new CallRequest(BusinessSearchFragment.this).BusinessSearch(etName.getText().toString(), professionId, businessId);

                    }
                });
                dialog.show();
            }
        });
        tv_title.setText("BUSINESS SEARCH");

        return v;
    }


    public void switchFragment(Fragment fragment) {

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, fragment, fragment.getClass().getSimpleName());
        transaction.addToBackStack(fragment.getClass().getSimpleName());
        transaction.commit();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case Profession_List:
                    //   Utils.hideProgressDialog();
                    App.professionArrayList.clear();
                    App.professionArrayName.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jBranchArray = jObj.getJSONArray("data");

                                Utils.addDefaultProfession();

                                for (int i = 0; i < jBranchArray.length(); i++) {
                                    JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    profession = (Profession) jParser.parseJson(jPackag, new Profession());
                                    App.professionArrayList.add(profession);
                                    App.professionArrayName.add(profession.getProfessionName());

                                }

                                System.out.println("LIst size::::" + App.professionArrayList.size());

                                ArrayAdapter aaProfession = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, App.professionArrayName);
                                aaProfession.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spProfession.setAdapter(aaProfession);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case NatureOfBusiness_List:
                    Utils.hideProgressDialog();
                    App.businessNatureArrayList.clear();
                    App.businessArrayName.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jBranchArray = jObj.getJSONArray("data");

                                Utils.addDefaultNatureOfBusiness();

                                for (int i = 0; i < jBranchArray.length(); i++) {
                                    JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    businessNature = (BusinessNature) jParser.parseJson(jPackag, new BusinessNature());
                                    App.businessNatureArrayList.add(businessNature);
                                    App.businessArrayName.add(businessNature.getIndustryName());
                                }

                                System.out.println("List size::::" + App.businessNatureArrayList.size());

                                ArrayAdapter aaBusiness = new ArrayAdapter<>(getActivity(), R.layout.spinner_text, App.businessArrayName);
                                aaBusiness.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spBusiness.setAdapter(aaBusiness);

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;

                case BusinessSearch:
                    Utils.hideProgressDialog();
                    tvNoSearchItems.setVisibility(View.GONE);
                    try {
                        JSONObject jObj = new JSONObject(result);

                        businessArrayList.clear();
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jBranchArray = jObj.getJSONArray("data");
                                for (int i = 0; i < jBranchArray.length(); i++) {
                                    JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    BusinessList businessList = (BusinessList) jParser.parseJson(jPackag, new BusinessList());
                                    businessArrayList.add(businessList);
                                }

                                tvMemberCount.setText(String.valueOf(businessArrayList.size()) + " " + "Members");

                                dialog.dismiss();

                                businessSearchAdapter = new BusinessSearchAdapter(BusinessSearchFragment.this, getActivity(), businessArrayList);
                                listView.setAdapter(businessSearchAdapter);
                            } else {
                                dialog.dismiss();
                                Utils.showToast(jObj.getString("Message"), getActivity());
                            }
                            //Utils.showToast(jObj.getString("Message"), getActivity());

                        } else {
                            //Utils.showToast(jObj.getString("Message"), getActivity());
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;
            }
        }
    }
}


