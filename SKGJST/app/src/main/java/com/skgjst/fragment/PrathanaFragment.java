package com.skgjst.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skgjst.BaseFragment;
import com.skgjst.adapter.PrathanaNewAdapter;
import com.skgjst.App;
import com.skgjst.R;
import com.skgjst.utils.JsonParserUniversal;
import static com.skgjst.activities.DashBoardActivity.tv_title;

public class PrathanaFragment extends BaseFragment {

    public JsonParserUniversal jParser;
    public LinearLayout lin_empty;
    public TextView tv_message, tv_count;
    public View view;
    public RecyclerView listView;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_prathana_new, container, false);
        tv_title.setText("DEMISE");
        jParser = new JsonParserUniversal();
        listView = view.findViewById(R.id.listView);
        lin_empty = view.findViewById(R.id.lin_empty);
        tv_message = view.findViewById(R.id.tv_message);
        layoutManager = new LinearLayoutManager(getActivity());
        listView.setLayoutManager(layoutManager);

        if (App.PrathnasArrayList.size() > 0) {
            lin_empty.setVisibility(View.GONE);
            PrathanaNewAdapter listAdapter = new PrathanaNewAdapter(App.PrathnasArrayList, this);
            listView.setAdapter(listAdapter);
        } else {
            lin_empty.setVisibility(View.VISIBLE);
        }

        return view;
    }

}
