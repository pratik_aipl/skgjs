package com.skgjst;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.gson.Gson;
import com.skgjst.activities.DashBoardActivity;
import com.skgjst.activities.LoginActivity;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.MainUser;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.Utils;
import com.skgjst.utils.VersionHelper;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.IOException;

public class SplashActivity extends BaseActivity implements AsynchTaskListner {
    public SplashActivity instance;
    private static int SPLASH_TIME_OUT = 1000;
    public String currentVersion = "";
    public int versionCode;

    @SuppressLint("MissingPermission")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        instance = this;
        PackageInfo pInfo;
        try {
            pInfo = getPackageManager().getPackageInfo(this.getPackageName(), 0);

            currentVersion = pInfo.versionName;
            versionCode = pInfo.versionCode;
            new GetPlayStoreVersion().execute("");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            getCompleted();
        }

    }

    private void getCompleted() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mySharedPref.getIsLoggedIn()) {
                    mySharedPref.setIsAdShow(true);
                    App.isfirst = true;
                    new CallRequest(instance).CheckSecurelogin(user.getMUserID(), mySharedPref.getSecureLoginID(), mySharedPref.getImeiNumber());
                } else {
                    App.isfirst = true;

                    startActivity(new Intent(instance, LoginActivity.class));
                    finish();
                }
            }


        }, SPLASH_TIME_OUT);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();

                switch (request) {


                    case loginStatus:
                        Utils.hideProgressDialog();
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getInt("DStatus") == 1) {
                            startActivity(new Intent(instance, DashBoardActivity.class)
                                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        } else {
                            mySharedPref.clearApp();
                            mySharedPref.setIsAdShow(false);
                            startActivity(new Intent(instance, LoginActivity.class));
                            finish();
                        }


                        break;

                }
            } catch (JSONException e) {
                Utils.hideProgressDialog();
                Utils.showToast("Something getting wrong! Please try again later.", instance);
                e.printStackTrace();
            }
        }
    }

    private class GetPlayStoreVersion extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                return Jsoup.connect("https://play.google.com/store/apps/details?id=" + SplashActivity.this.getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                        .first()
                        .ownText();
            } catch (IOException e) {
                e.printStackTrace();
                return "";
            }

        }

        @Override
        protected void onPostExecute(String result) {

            Log.i("CURRENT VERSION : ", currentVersion);
            Log.i("CURRENT VERSION CODE : ", versionCode + "");
            Log.i("NEW VERSION : ", result);

            try {

                if (VersionHelper.compare(currentVersion, result) == -1) {
                    new AlertDialog.Builder(SplashActivity.this)
                            .setTitle("Update")
                            .setMessage("New version is updated on Playstore. Please update your app to use new features.")
                            .setCancelable(false)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface arg0, int arg1) {
                                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                    try {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                    }
                                }
                            }).create().show();
                } else {
                    getCompleted();
                }

            } catch (NumberFormatException e) {
                e.printStackTrace();
                getCompleted();
            }
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

}
