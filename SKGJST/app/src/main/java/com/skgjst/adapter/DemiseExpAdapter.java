package com.skgjst.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.skgjst.App;
import com.skgjst.R;
import com.skgjst.activities.MyDemiseDetailActivity;
import com.skgjst.activities.demise.DemiseDetailActivity;
import com.skgjst.activities.demise.DemisePrathanaAddActivity;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.callinterface.NotificationListener;
import com.skgjst.model.DemiseDataNew;
import com.skgjst.model.DemiseType;
import com.skgjst.utils.Constant;
import com.skgjst.utils.PicassoTrustAll;
import com.skgjst.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by empiere-vaibhav on 10/9/2018.
 */

public class DemiseExpAdapter extends BaseExpandableListAdapter implements AsynchTaskListner {
    private static final String TAG = "DemiseExpAdapter";
    private Context _context;
    public ArrayList<DemiseType> headerArrayList = new ArrayList<>();

    public ChildHolder childHolder;
    public GroupHolder groupHolder;
    public NotificationListener notificationListener;
    public ExpandableListView expandableListView;


    public DemiseExpAdapter(Fragment context, ArrayList<DemiseType> headerArrayList, ExpandableListView expandableListView) {
        this._context = context.getActivity();
        this.headerArrayList = headerArrayList;
        this.expandableListView = expandableListView;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return headerArrayList.get(groupPosition).demiseDataArray.get(childPosititon);
    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @SuppressLint("NewApi")
    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {


        if (convertView == null) {
            convertView = LayoutInflater.from(_context).inflate(R.layout.custom_demise_raw, null);
            childHolder = new ChildHolder();
            convertView.invalidate();
            convertView.setTag(R.layout.custom_demise_raw, childHolder);


        } else {
            childHolder = (ChildHolder) convertView.getTag(R.layout.custom_demise_raw);
        }
        childHolder.rel_content_main = convertView.findViewById(R.id.rel_content_main);
        childHolder.tv_name = convertView.findViewById(R.id.tv_name);
        childHolder.img_profile = convertView.findViewById(R.id.img_profile);
        childHolder.tv_send = convertView.findViewById(R.id.tv_send);
        childHolder.tv_posted = convertView.findViewById(R.id.tv_posted);
        childHolder.tv_native = convertView.findViewById(R.id.tv_native);

        childHolder.tv_village = convertView.findViewById(R.id.tv_village);
        childHolder.tv_contact_no = convertView.findViewById(R.id.tv_contact_no);
        childHolder.tv_contact = convertView.findViewById(R.id.tv_contact);


        final DemiseDataNew Obj = headerArrayList.get(groupPosition).demiseDataArray.get(childPosition);

        if (Obj != null) {
            childHolder.tv_name.setText(Obj.getName() + " " + Obj.getFatherName() + " " + Obj.getSurname());
            // childHolder.tv_name.setText(Obj.getName());
            childHolder.tv_posted.setText("Posted By : " + Obj.getPostedBy());
            childHolder.tv_native.setText(Obj.getNativePlace() + "-" + Obj.getCurrenPlace());
            childHolder.tv_contact.setText("Contact Person : " + Obj.getContactPerson());
            childHolder.tv_contact_no.setText(Obj.getContactNumber());
            if (!TextUtils.isEmpty(Obj.getImageURL())) {
                PicassoTrustAll.getInstance(_context)
                        .load(Obj.getImageURL())
                        .error(R.drawable.avatar)
                        .resize(256, 256)
                        .into(childHolder.img_profile);

            }
        }


        childHolder.rel_content_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.title = "DEMISE DETAIL";
                if (App.isDemiseType.equalsIgnoreCase("My")) {
                    _context.startActivity(new Intent(_context, MyDemiseDetailActivity.class)
                            .putExtra("DemiseType", "AllDemise")
                            .putExtra("obj", Obj));
                } else {
                    _context.startActivity(new Intent(_context, DemiseDetailActivity.class)
                            .putExtra("DemiseType", "AllDemise")
                            .putExtra("obj", Obj));

                }
            }
        });
        childHolder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.title = "DEMISE DETAIL";
                if (App.isDemiseType.equalsIgnoreCase("My")) {
                    _context.startActivity(new Intent(_context, MyDemiseDetailActivity.class)
                            .putExtra("DemiseType", "AllDemise")
                            .putExtra("obj", Obj));
                } else {
                    _context.startActivity(new Intent(_context, DemiseDetailActivity.class)
                            .putExtra("DemiseType", "AllDemise")
                            .putExtra("obj", Obj));

                }
            }
        });

        childHolder.tv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title = "DEMISE DETAIL";
                _context.startActivity(new Intent(_context, DemisePrathanaAddActivity.class)
                        .putExtra("DemiseType", "AllDemise")
                        .putExtra("obj", Obj));
            }
        });

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int grouppPosition, long id) {
                notifyDataSetChanged();
                Log.i("TAG", "==>" + "IsExapand"); // Do your Staff
                return false;
            }
        });
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return headerArrayList.get(groupPosition).demiseDataArray.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.headerArrayList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.headerArrayList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {

            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
            ExpandableListView mExpandableListView = (ExpandableListView) parent;
            mExpandableListView.expandGroup(groupPosition);
            //view = LayoutInflater.from(mContext).inflate(R.layout.parent_chapter_item, null);
            groupHolder = new GroupHolder();
            groupHolder.lblListHeader = convertView.findViewById(R.id.lblListHeader);
            convertView.setTag(groupHolder);

        } else {
            groupHolder = (GroupHolder) convertView.getTag();
        }
        //  groupHolder.lblListHeader.setTypeface(null, Typeface.BOLD);

        groupHolder.lblListHeader.setText(headerArrayList.get(groupPosition).getNotification_name());


        return convertView;
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            //    Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getWatched:
                    Utils.hideProgressDialog();
                    JSONObject jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            notifyDataSetChanged();
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), _context);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    class GroupHolder {
        TextView lblListHeader;
    }

    class ChildHolder {
        TextView tv_name, tv_posted, tv_village, tv_native, tv_contact, tv_contact_no;
        RelativeLayout rel_content_main;
        CircleImageView img_profile;
        ImageView tv_send;

    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}