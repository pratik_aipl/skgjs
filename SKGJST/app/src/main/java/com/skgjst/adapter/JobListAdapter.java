package com.skgjst.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.skgjst.activities.job.JobDetailActivity;
import com.skgjst.activities.job.MyJobDetailActivity;
import com.skgjst.App;
import com.skgjst.model.JobList;
import com.skgjst.R;
import com.skgjst.model.MainUser;

import java.util.ArrayList;

public class JobListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<JobList> mArrayList;
    public Context context;
    MainUser user;
    public static final int VIEW_TYPE_ITEM = 1;
    public static final int VIEW_TYPE_BLANK = 2;

    public JobListAdapter(MainUser user, ArrayList<JobList> moviesList, Context context) {
        mArrayList = moviesList;
        this.context = context;
        this.user = user;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_job_title, tv_company_name, tv_city_name;
        public ImageButton img_right;

        public MyViewHolder(View view) {
            super(view);
            tv_job_title = view.findViewById(R.id.tv_job_title);
            tv_city_name = view.findViewById(R.id.tv_city_name);
            tv_company_name = view.findViewById(R.id.tv_company_name);
            img_right = view.findViewById(R.id.img_right);

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new MyViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_joblist_raw, parent, false));

    }




    public void bindMyViewHolder(final MyViewHolder holder, final int pos) {
        final JobList Obj = mArrayList.get(pos);
        holder.tv_city_name.setText(Obj.getJoblocation());
        holder.tv_company_name.setText(Obj.getCompanyname());
        holder.tv_job_title.setText(Obj.getJobpost());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Obj.getFamilyDetailid() == user.getFamilyDetailID()) {
                    context.startActivity(new Intent(context, MyJobDetailActivity.class)
                            .putExtra("obj", Obj)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    );
                } else {
                    context.startActivity(new Intent(context, JobDetailActivity.class)
                            .putExtra("obj", Obj)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    );
                }
            }
        });
        holder.img_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Obj.getFamilyDetailid() == user.getFamilyDetailID()) {
                    context.startActivity(new Intent(context, MyJobDetailActivity.class)
                            .putExtra("obj", Obj)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    );
                } else {
                    context.startActivity(new Intent(context, JobDetailActivity.class)
                            .putExtra("obj", Obj)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    );
                }
            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
              MyViewHolder addrHolder = (MyViewHolder) holder;
                bindMyViewHolder(addrHolder, position);



    }



    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


}
