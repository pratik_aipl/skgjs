package com.skgjst.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.skgjst.activities.job.CandidatesDetailActivity;
import com.skgjst.model.Candidates;
import com.skgjst.R;

import java.util.ArrayList;

public class CandidatesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<Candidates> mArrayList;
    public Context context;

    public static final int VIEW_TYPE_ITEM = 1;
    public static final int VIEW_TYPE_BLANK = 2;

    public CandidatesListAdapter(ArrayList<Candidates> moviesList, Context context) {
        mArrayList = moviesList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_candidate_name, tv_expe_year, tv_city_name;

        public MyViewHolder(View view) {
            super(view);
            tv_candidate_name = view.findViewById(R.id.tv_candidate_name);
            tv_expe_year = view.findViewById(R.id.tv_expe_year);

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            return new MyViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_candidates_raw, parent, false));
        } else {
            return new ViewHolderFooter(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_member_raw_footer, parent, false));
        }
    }

    public class ViewHolderFooter extends RecyclerView.ViewHolder {

        public ViewHolderFooter(View view) {
            super(view);

        }
    }

    public void bindFooterHolder(final ViewHolderFooter holder, final int pos) {


    }

    public void bindMyViewHolder(final MyViewHolder holder, final int pos) {
        final Candidates Obj = mArrayList.get(pos);
        holder.tv_candidate_name.setText(Obj.getFirstname() + " " + Obj.getLastname());
        holder.tv_expe_year.setText(Obj.getExperience());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                context.startActivity(new Intent(context, CandidatesDetailActivity.class)
                        .putExtra("obj", Obj)
                );

            }
        });
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 1:
                MyViewHolder addrHolder = (MyViewHolder) holder;
                bindMyViewHolder(addrHolder, position);
                break;
            case 2:
                ViewHolderFooter footerHolder = (ViewHolderFooter) holder;
                bindFooterHolder(footerHolder, position);
                break;
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (position == (mArrayList.size() - 1)) {
            return VIEW_TYPE_BLANK;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


}
