package com.skgjst.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.skgjst.activities.demise.MyPrathnaDetailActivity;
import com.skgjst.activities.demise.DemiseDetailActivity;
import com.skgjst.App;
import com.skgjst.model.DemiseDataNew;
import com.skgjst.R;
import com.skgjst.utils.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by empiere-vaibhav on 9/26/2018.
 */

public class PrathanaNewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {
    private ArrayList<DemiseDataNew> mArrayList;
    public Context context;

    public PrathanaNewAdapter(ArrayList<DemiseDataNew> moviesList, Fragment context) {
        mArrayList = moviesList;
        this.context = context.getActivity();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_name, tv_posted, tv_native, tv_contact, tv_contact_no;
        public LinearLayout lin_info,lin;
        public CircleImageView img_profile;
        public ImageView tv_send;
        RelativeLayout rel_content_main;

        public MyViewHolder(View view) {
            super(view);
            rel_content_main = view.findViewById(R.id.rel_content_main);
            tv_posted = view.findViewById(R.id.tv_posted);
            tv_name = view.findViewById(R.id.tv_name);
            img_profile = view.findViewById(R.id.img_profile);
            tv_send = view.findViewById(R.id.tv_send);
            tv_native = view.findViewById(R.id.tv_native);
            tv_contact_no = view.findViewById(R.id.tv_contact_no);
            tv_contact = view.findViewById(R.id.tv_contact);
            lin = view.findViewById(R.id.lin);
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new MyViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_demise_raw, parent, false));

    }



    public void bindMyViewHolder(final MyViewHolder holder, final int pos) {
        final DemiseDataNew Obj = mArrayList.get(pos);
        holder.tv_name.setText(Obj.getName() + " " + Obj.getFatherName() + " " + Obj.getSurname());
        holder.tv_posted.setText("Posted By : " + Obj.getPostedBy());
        holder.tv_native.setText(Obj.getNativePlace() + "-" + Obj.getCurrenPlace());
        holder.tv_contact.setText("Contact Person : " + Obj.getContactPerson());
        holder.tv_contact_no.setText(Obj.getContactNumber());

        if (!TextUtils.isEmpty(Obj.getImageURL())) {
                PicassoTrustAll.getInstance(context)
                        .load(Obj.getImageURL().replace(" ", "%20"))
                        .error(R.drawable.avatar)
                        .resize(256, 256)
                        .into(holder.img_profile);
        }
        holder.rel_content_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title = "PRTHANA DETAIL";
                if(App.isDemiseType.equalsIgnoreCase("My")){
                    context.startActivity(new Intent(context, MyPrathnaDetailActivity.class)
                            .putExtra("DemiseType", "Prathna")
                           .putExtra("obj", Obj));
                }else{
                    context.startActivity(new Intent(context, DemiseDetailActivity.class)
                            .putExtra("DemiseType", "Prathna")
                            .putExtra("obj", Obj));
                }

            }
        }); holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title = "PRTHANA DETAIL";
                if(App.isDemiseType.equalsIgnoreCase("My")){
                    context.startActivity(new Intent(context, MyPrathnaDetailActivity.class)
                            .putExtra("DemiseType", "Prathna")
                           .putExtra("obj", Obj));
                }else{
                    context.startActivity(new Intent(context, DemiseDetailActivity.class)
                            .putExtra("DemiseType", "Prathna")
                            .putExtra("obj", Obj));
                }

            }
        });

        holder.lin.setVisibility(View.GONE);
        holder.tv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title = "DEMISE DETAIL";
                context.startActivity(new Intent(context, DemiseDetailActivity.class)
                        .putExtra("DemiseType", "AllDemise")
                        .putExtra("obj", Obj));
            }
        });


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                MyViewHolder addrHolder = (MyViewHolder) holder;
                bindMyViewHolder(addrHolder, position);
    }



    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

}

