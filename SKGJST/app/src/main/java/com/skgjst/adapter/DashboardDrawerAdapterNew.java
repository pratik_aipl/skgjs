package com.skgjst.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.skgjst.model.NavDrawerItem;
import com.skgjst.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by empiere-vaibhav on 8/9/2018.
 */

public class DashboardDrawerAdapterNew extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<NavDrawerItem> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;
    public int adCount = 0;
    public Runnable mRunnable;
    public Handler handler;
    public int currentGoldPos;

    public static final int VIEW_TYPE_ITEM = 1;
    public static final int VIEW_TYPE_FOOTER = 2;

    public DashboardDrawerAdapterNew(Context context, List<NavDrawerItem> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 1:
                MyViewHolder addrHolder = (MyViewHolder) holder;
                bindMyViewHolder(addrHolder, position);
                break;
            case 2:
                ViewHolderFooter footerHolder = (ViewHolderFooter) holder;
                bindFooterHolder(footerHolder, position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == (data.size() - 1)) {
            return VIEW_TYPE_FOOTER;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            return new MyViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.nav_drawer_row, parent, false));
        }  else {
            return new ViewHolderFooter(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_member_raw_footer, parent, false));
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView titleIcon;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            titleIcon = (ImageView) view.findViewById(R.id.titleIcon);


        }
    }

    public void bindMyViewHolder(final MyViewHolder holder, final int pos) {
        NavDrawerItem current = data.get(pos);
        holder.title.setText(current.getTitle());
        holder.titleIcon.setImageResource(current.getIcons());


    }

    public class ViewHolderFooter extends RecyclerView.ViewHolder {
        public ImageView imgAdvertise;
        public ProgressBar prgAdvertise;

        public ViewHolderFooter(View view) {
            super(view);
        }
    }

    public void bindFooterHolder(final ViewHolderFooter holder, final int pos) {

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


}

