package com.skgjst.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skgjst.activities.birthday.Anniversary1ProfileActivity;
import com.skgjst.activities.birthday.AnniversaryProfileActivity;
import com.skgjst.App;
import com.skgjst.fragment.AnniversaryFragment;
import com.skgjst.callinterface.AnniversarywishListener;
import com.skgjst.model.Anniversary;
import com.skgjst.R;
import com.skgjst.utils.ImagePopup;
import com.skgjst.utils.PicassoTrustAll;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by empiere-vaibhav on 10/4/2018.
 */

public class AnniversaryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<Anniversary> mArrayList;
    public Context context;
    public static final int VIEW_TYPE_ITEM = 1;
    public static final int VIEW_TYPE_BLANK = 2;
    public AnniversarywishListener birthdaywishListener;

    public AnniversaryAdapter(ArrayList<Anniversary> moviesList, Context context, AnniversaryFragment fragment) {
        mArrayList = moviesList;
        this.context = context;
        this.context = fragment.getActivity();
        this.birthdaywishListener = fragment;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_husbund_name, tv_wife_name, tv_anni_no,tv_native_place,tv_city,tv_count;
        public LinearLayout lin_info;
        public CircleImageView img_profile, img_wife;
        public ImageButton tv_send,tv_bday;

        public MyViewHolder(View view) {
            super(view);
            tv_wife_name = view.findViewById(R.id.tv_wife_name);
            tv_anni_no = view.findViewById(R.id.tv_anni_no);
            tv_husbund_name = view.findViewById(R.id.tv_husbund_name);
            tv_native_place = view.findViewById(R.id.tv_native_place);
            tv_city = view.findViewById(R.id.tv_city);
            img_profile = view.findViewById(R.id.img_profile);
            img_wife = view.findViewById(R.id.img_wife);
            tv_send = view.findViewById(R.id.tv_send);
            tv_bday = view.findViewById(R.id.tv_bday);
            tv_count = view.findViewById(R.id.tv_count);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            return new MyViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_anniversary_row_new, parent, false));
        } else {
            return new ViewHolderFooter(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_member_raw_footer, parent, false));
        }
    }

    public class ViewHolderFooter extends RecyclerView.ViewHolder {


        public ViewHolderFooter(View view) {
            super(view);

        }
    }

    public void bindFooterHolder(final ViewHolderFooter holder, final int pos) {

    }

    public void bindMyViewHolder(final MyViewHolder holder, final int pos) {
        final Anniversary Obj = mArrayList.get(pos);
        holder.tv_husbund_name.setText(Obj.getHusbandName());
        holder.tv_wife_name.setText(Obj.getWifeName());
        holder.tv_native_place.setText("Native Place : "+Obj.getNativePlace());
        holder.tv_city.setText("City : "+Obj.getCurrentCity());
        holder.tv_anni_no.setText(Obj.getYears() + " Anniversary");
        holder.tv_husbund_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, AnniversaryProfileActivity.class)
                        .putExtra("obj", Obj));
            }
        });

        holder.tv_wife_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, Anniversary1ProfileActivity.class)
                        .putExtra("obj", Obj));
            }
        });
        if (Obj.getHPhotoURl() != null && !TextUtils.isEmpty(Obj.getHPhotoURl())) {
            try {
                PicassoTrustAll.getInstance(context)
                        .load(Obj.getHPhotoURl().replace(" ", "%20"))
                        .error(R.drawable.avatar)
                        .resize(256, 256)
                        .into(holder.img_profile, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (Obj.getWPhotoURL() != null && !TextUtils.isEmpty(Obj.getWPhotoURL())) {
            try {
                PicassoTrustAll.getInstance(context)
                        .load(Obj.getWPhotoURL().replace(" ", "%20"))
                        .error(R.drawable.female)
                        .resize(256, 256)
                        .into(holder.img_wife, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (Obj.getIsWish() == 0) {
            holder.tv_send.setVisibility(View.GONE);
        } else {
            holder.tv_send.setVisibility(View.VISIBLE);
        }

        if (Obj.getWishCount() == 0) {
            holder.tv_bday.setVisibility(View.GONE);
            holder.tv_count.setVisibility(View.GONE);
        } else {
            holder.tv_bday.setVisibility(View.VISIBLE);
            holder.tv_count.setText("(" + Obj.getWishCount() + ")");
        }

        holder.tv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title = "Birthday";

                dailogBirthdayWish(Obj);
                // context.startActivity(new Intent(context, CommingSoonActivity.class));
            }
        });
        final ImagePopup imagePopup = new ImagePopup(context);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.BLACK);  // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(true);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional
        imagePopup.initiatePopupWithPicasso(Obj.getHPhotoURl().replace(" ", "%20"));
        holder.img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /** Initiate Popup view **/
                imagePopup.viewPopup();

            }
        });

        final ImagePopup imagePopup2 = new ImagePopup(context);
        imagePopup2.setWindowHeight(800); // Optional
        imagePopup2.setWindowWidth(800); // Optional
        imagePopup2.setBackgroundColor(Color.BLACK);  // Optional
        imagePopup2.setFullScreen(true); // Optional
        imagePopup2.setHideCloseIcon(true);  // Optional
        imagePopup2.setImageOnClickClose(true);  // Optional
        imagePopup2.initiatePopupWithPicasso(Obj.getWPhotoURL().replace(" ", "%20"));
        holder.img_wife.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /** Initiate Popup view **/
                imagePopup2.viewPopup();

            }
        });

    }

    private void dailogBirthdayWish(final Anniversary obj) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dailog_anniversy);
        TextView tv_name = dialog.findViewById(R.id.tv_name);
        TextView tv_wife_name = dialog.findViewById(R.id.tv_wife_name);

        tv_name.setText(obj.getHusbandName());
        tv_wife_name.setText(obj.getWifeName());
        Button btn_send = dialog.findViewById(R.id.btn_send);
        final EditText et_message = dialog.findViewById(R.id.et_message);
        ImageView imgClose = dialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(et_message.getText().toString().isEmpty()){
                    Utils.showToast("Please write some message to wish",context);
                }else{
                    birthdaywishListener.anniversaryWish(et_message.getText().toString(),obj.getHFamilyDetailID(),obj.getWFamilyDetailID());
                    dialog.dismiss();
                }
            }
        });


        dialog.show();

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 1:
                MyViewHolder addrHolder = (MyViewHolder) holder;
                bindMyViewHolder(addrHolder, position);
                break;
            case 2:
                ViewHolderFooter footerHolder = (ViewHolderFooter) holder;
                bindFooterHolder(footerHolder, position);
                break;
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (position == (mArrayList.size() - 1)) {
            return VIEW_TYPE_BLANK;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


}
