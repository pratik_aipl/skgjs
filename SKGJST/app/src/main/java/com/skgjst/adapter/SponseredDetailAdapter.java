package com.skgjst.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.skgjst.activities.SponserImageDisplayActivity;
import com.skgjst.App;
import com.skgjst.model.BusinessAdvertiseMent;
import com.skgjst.R;
import com.skgjst.utils.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 10/16/2018.
 */

public class SponseredDetailAdapter extends PagerAdapter {
    public Context context;
    public ArrayList<BusinessAdvertiseMent> list = new ArrayList<>();
    public LayoutInflater layoutInflater;

    public SponseredDetailAdapter(Context context, ArrayList<BusinessAdvertiseMent> list) {
        this.context = context;
        this.list = list;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.layout_image, container, false);
        ImageView imageView = itemView.findViewById(R.id.imageView);


        // final ProgressBar loader = itemView.findViewById(R.id.loader);
        //String imageURL = "http://ivaccess.blenzabi.com/" + list.get(position).getDocumentPath() + list.get(position).getDocumentName();
        //  loader.setVisibility(View.GONE);
        //  loader.setVisibility(View.VISIBLE);
        if (list.get(position).getFilepath() != null && !TextUtils.isEmpty(list.get(position).getFilepath())) {
            try {
                PicassoTrustAll.getInstance(context)
                        .load(list.get(position).getFilepath().replace(" ", "%20"))
                        .error(R.drawable.avatar)
                        .resize(256, 256)
                        .into(imageView, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, SponserImageDisplayActivity.class)
                        .putExtra("position", position)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                );

                App.businessAdvertiseMents = list;
            }
        });
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }
}