package com.skgjst.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.News;
import com.skgjst.R;
import com.skgjst.utils.Constant;
import com.skgjst.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 10/15/2018.
 */

public class NewsAdapter extends BaseExpandableListAdapter implements AsynchTaskListner {

    private Context _context;
    public ArrayList<News> headerArrayList = new ArrayList<>();

    public ChildHolder childHolder;
    public GroupHolder groupHolder;
     public ExpandableListView expandableListView;


    public NewsAdapter(Context context, ArrayList<News> headerArrayList, ExpandableListView expandableListView) {
        this._context = context;
        this.headerArrayList = headerArrayList;
        this.expandableListView = expandableListView;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return headerArrayList.get(groupPosition).newsSubArray.get(childPosititon);
    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @SuppressLint("NewApi")
    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {


        if (convertView == null) {
            convertView = LayoutInflater.from(_context).inflate(R.layout.list_news_item, null);
            childHolder = new ChildHolder();
            childHolder.tv_mmm = convertView.findViewById(R.id.tv_mmm);
            convertView.invalidate();
            convertView.setTag(R.layout.list_item, childHolder);


        } else {
            childHolder = (ChildHolder) convertView.getTag(R.layout.list_item);
        }
            childHolder.tv_mmm.setText(headerArrayList.get(groupPosition).newsSubArray.get(childPosition).getNewsDesc());


        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return headerArrayList.get(groupPosition).newsSubArray.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.headerArrayList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.headerArrayList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {

            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);

            //view = LayoutInflater.from(mContext).inflate(R.layout.parent_chapter_item, null);
            groupHolder = new GroupHolder();
            groupHolder.lblListHeader = convertView.findViewById(R.id.lblListHeader);
            convertView.setTag(groupHolder);
        } else {
            groupHolder = (GroupHolder) convertView.getTag();
        }
        //  groupHolder.lblListHeader.setTypeface(null, Typeface.BOLD);

        groupHolder.lblListHeader.setText(headerArrayList.get(groupPosition).getNewsTitle());


        return convertView;
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            //    Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getWatched:
                    Utils.hideProgressDialog();
                    JSONObject jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            //   Utils.showToast(jObj.getString("UMessage"), this);
                            notifyDataSetChanged();
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), _context);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    class GroupHolder {
        public TextView lblListHeader;
    }

    class ChildHolder {
        public TextView txtListChild, message, tv_mmm;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
