package com.skgjst.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skgjst.App;
import com.skgjst.R;
import com.skgjst.activities.birthday.BirthdayProfileActivity;
import com.skgjst.callinterface.BirthdaywishListener;
import com.skgjst.fragment.BirthdayFragment;
import com.skgjst.model.Birthday;
import com.skgjst.utils.ImagePopup;
import com.skgjst.utils.PicassoTrustAll;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class BirthdayAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<Birthday> mArrayList;
    public Context context;
    public static final int VIEW_TYPE_ITEM = 1;
    public static final int VIEW_TYPE_BLANK = 2;
    public BirthdaywishListener birthdaywishListener;

    public BirthdayAdapter(ArrayList<Birthday> moviesList, Context context, BirthdayFragment fragment) {
        mArrayList = moviesList;
        this.context = context;
        this.context = fragment.getActivity();
        this.birthdaywishListener = fragment;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name, tv_native_place, tv_city, tv_count;
        public LinearLayout lin_info, lin_main;
        public CircleImageView img_profile;
        public ImageButton tv_send, tv_bday;

        public MyViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.tv_name);
            tv_native_place = view.findViewById(R.id.tv_native_place);
            img_profile = view.findViewById(R.id.img_profile);
            tv_city = view.findViewById(R.id.tv_city);
            tv_send = view.findViewById(R.id.tv_send);
            tv_bday = view.findViewById(R.id.tv_bday);
            tv_count = view.findViewById(R.id.tv_count);
            lin_main = view.findViewById(R.id.lin_main);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            return new MyViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_birthday_row_new, parent, false));
        } else {
            return new ViewHolderFooter(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_member_raw_footer, parent, false));
        }
    }

    public class ViewHolderFooter extends RecyclerView.ViewHolder {


        public ViewHolderFooter(View view) {
            super(view);

        }
    }

    public void bindFooterHolder(final ViewHolderFooter holder, final int pos) {

    }

    public void bindMyViewHolder(final MyViewHolder holder, final int pos) {
        final Birthday Obj = mArrayList.get(pos);
        holder.tv_name.setText(Obj.getFName()
                + " " + Obj.getMName()
                + " " + Obj.getSurName());

        holder.tv_native_place.setText("Native Place : " + Obj.getNativePlace());
        holder.tv_city.setText("City : " + Obj.getCurrentCity());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, BirthdayProfileActivity.class)
                        .putExtra("obj", Obj));
            }
        });

        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, BirthdayProfileActivity.class)
                        .putExtra("obj", Obj));
            }
        });

        if (Obj.getPhotoURL() != null && !TextUtils.isEmpty(Obj.getPhotoURL())) {
            try {
                PicassoTrustAll.getInstance(context)
                        .load(Obj.getPhotoURL().replace(" ", "%20"))
                        .error(R.drawable.avatar)
                        .resize(256, 256)
                        .into(holder.img_profile, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (Obj.getIsWish() == 0) {
            holder.tv_send.setVisibility(View.GONE);
        } else {
            holder.tv_send.setVisibility(View.VISIBLE);
        }

        if (Obj.getWishCount() == 0) {
            holder.tv_bday.setVisibility(View.GONE);
            holder.tv_count.setVisibility(View.GONE);
        } else {
            holder.tv_bday.setVisibility(View.VISIBLE);
            holder.tv_count.setVisibility(View.VISIBLE);
            holder.tv_count.setText("(" + Obj.getWishCount() + ")");
        }

        holder.tv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title = "Birthday";

                dailogBirthdayWish(Obj);
                // context.startActivity(new Intent(context, CommingSoonActivity.class));
            }
        });

        final ImagePopup imagePopup = new ImagePopup(context);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.BLACK);  // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(true);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional
        imagePopup.initiatePopupWithPicasso(Obj.getPhotoURL().replace(" ", "%20"));
        holder.img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /** Initiate Popup view **/
                imagePopup.viewPopup();

            }
        });

    }

    private void dailogBirthdayWish(final Birthday obj) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dailog_birthday_wish);
        TextView tv_name = dialog.findViewById(R.id.tv_name);
        Button btn_send = dialog.findViewById(R.id.btn_send);
        final EditText et_message = dialog.findViewById(R.id.et_message);
        tv_name.setText(obj.getFName()
                + " " + obj.getMName()
                + " " + obj.getSurName());
        ImageView imgClose = dialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_message.getText().toString().isEmpty()) {
                    Utils.showToast("Please write some message to wish", context);
                } else {
                    dialog.dismiss();
                    birthdaywishListener.birthdayWish(et_message.getText().toString(), obj.getSfamilydetailid());

                }
            }
        });


        dialog.show();

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 1:
                MyViewHolder addrHolder = (MyViewHolder) holder;
                bindMyViewHolder(addrHolder, position);
                break;
            case 2:
                ViewHolderFooter footerHolder = (ViewHolderFooter) holder;
                bindFooterHolder(footerHolder, position);
                break;
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (position == (mArrayList.size() - 1)) {
            return VIEW_TYPE_BLANK;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }


}
