package com.skgjst.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.skgjst.App;
import com.skgjst.fragment.ImageDisplayFragment;
import com.skgjst.model.EventImages;
import com.skgjst.R;
import com.skgjst.utils.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.util.ArrayList;

/**
 * Created by User on 17-04-2018.
 */

public class ImageDisplayPagerAdapter extends PagerAdapter {
    public Context context;
    public ArrayList<EventImages> list = new ArrayList<>();
    public LayoutInflater layoutInflater;

    public ImageDisplayPagerAdapter(Context context, ArrayList<EventImages> list) {
        this.context = context;
        this.list = list;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.layout_image, container, false);
        ImageView imageView = itemView.findViewById(R.id.imageView);


       // final ProgressBar loader = itemView.findViewById(R.id.loader);
        //String imageURL = "http://ivaccess.blenzabi.com/" + list.get(position).getDocumentPath() + list.get(position).getDocumentName();
      //  loader.setVisibility(View.GONE);
      //  loader.setVisibility(View.VISIBLE);
        if (list.get(position).getImageURL() != null && !TextUtils.isEmpty(list.get(position).getImageURL()) ) {
            try {
                PicassoTrustAll.getInstance(context)
                        .load(list.get(position).getImageURL())
                        .error(R.drawable.no_image)
                        .into(imageView, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            try {
                PicassoTrustAll.getInstance(context)
                        .load(list.get(position).getCoverImage().replace(" ", "%20"))
                        .error(R.drawable.no_image)
                       .into(imageView, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
           /* final Target target = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    System.out.println("bitmap:::" + bitmap);
                    if (loader != null) {
                        loader.setVisibility(View.GONE);
                    }
                    imageView.setImage(ImageSource.bitmap(bitmap));
                }

                @Override
                public void onBitmapFailed(Drawable errorDrawable) {
                }

                @Override
                public void onPrepareLoad(Drawable placeHolderDrawable) {
                }
            };*/

          /*  Picasso.with(context).load(list.get(position).getImageURL()).resize(512,512).into(target);
            imageView.setTag(target);
        }
*/
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, ImageDisplayFragment.class)
                        .putExtra("position", position)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                );

                App.eventImagesArrayList = list;
            }
        });
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }

    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }
}