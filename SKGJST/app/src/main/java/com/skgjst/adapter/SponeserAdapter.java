package com.skgjst.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.skgjst.activities.SponserDetailActivity;
import com.skgjst.model.SponserAd;
import com.skgjst.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by empiere-vaibhav on 10/16/2018.
 */

public class SponeserAdapter extends RecyclerView.Adapter<SponeserAdapter.MyViewHolder> {
    List<SponserAd> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;

    public SponeserAdapter(Context context, List<SponserAd> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.custom_sponser_ad_raw, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        SponserAd current = data.get(position);
        holder.sponserIcon.setImageResource(current.getImg_sponser());
        holder.titleIcon.setImageResource(current.getImg_logo());

holder.itemView.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        if(position==0){
            context.startActivity(new Intent(context, SponserDetailActivity.class)
                    .putExtra("adId","1")
                    .putExtra("adName","PLATINUM SPONSOR")
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            );
        }else if(position==1){
            context.startActivity(new Intent(context, SponserDetailActivity.class)
                    .putExtra("adId","2")
                    .putExtra("adName","DIAMOND SPONSOR")
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            );
        }else if(position==2){
            context.startActivity(new Intent(context, SponserDetailActivity.class)
                    .putExtra("adId","3")
                    .putExtra("adName","GOLD SPONSOR")
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            );
        }else if(position==3){
            context.startActivity(new Intent(context, SponserDetailActivity.class)
                    .putExtra("adId","4")
                    .putExtra("adName","SILVER SPONSOR")
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            );
        }else if(position==4){
            context.startActivity(new Intent(context, SponserDetailActivity.class)
                    .putExtra("adId","5")
                    .putExtra("adName","WELL WISHER")
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            );
        }

    }
});


    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView titleIcon, sponserIcon;
        View itemView;

        public MyViewHolder(View itemView) {
            super(itemView);
            sponserIcon = (ImageView) itemView.findViewById(R.id.sponserIcon);
            titleIcon = (ImageView) itemView.findViewById(R.id.titleIcon);
            this.itemView = itemView;

        }
    }
}


