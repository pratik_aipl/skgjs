package com.skgjst.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skgjst.activities.business.BusinessSearchActivity;
import com.skgjst.activities.events.EventsActivity;
import com.skgjst.activities.job.JobListActivity;
import com.skgjst.activities.matrimonial.MatrimonialSearchActivity;
import com.skgjst.activities.membersearch.MemberSearchActivity;
import com.skgjst.activities.oraganization.OraganizationListActivity;
import com.skgjst.R;


public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    Context mcontext;
    private String[] web;
    private int[] backgroundColor;
    private int[] Imageid;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;




    public HomeAdapter(FragmentActivity activity, int[] imageId, String[] textId, int[] backgroundColorId) {
        mcontext = activity;
        this.Imageid = imageId;
        this.web = textId;
        this.backgroundColor = backgroundColorId;
        this.mInflater = LayoutInflater.from(mcontext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.adapter_home_item, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.img1.setImageResource(Imageid[position]);
        holder.tv1.setText(web[position]);
        holder.lin_inner.setBackgroundResource(backgroundColor[position]);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(position==4){
                    mcontext.startActivity(new Intent(mcontext, JobListActivity.class));
                }
                if(position==0){
                    mcontext.startActivity(new Intent(mcontext, MemberSearchActivity.class));
                }
                if(position==1){
                 //   bottomBar.setVisibility(View.GONE);
                    mcontext.startActivity(new Intent(mcontext, BusinessSearchActivity.class));
                }
                if(position==2){
                 //   bottomBar.setVisibility(View.GONE);
                    mcontext.startActivity(new Intent(mcontext, MatrimonialSearchActivity.class));
                }
                if(position==3){
                    mcontext.startActivity(new Intent(mcontext, EventsActivity.class));
                }
                if(position==5){
                    mcontext.startActivity(new Intent(mcontext, OraganizationListActivity.class));
                }


            }
        });
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return Imageid.length;
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public CardView card_view;
        LinearLayout lin_inner;
        TextView tv1;
        ImageView img1;

        ViewHolder(View itemView) {
            super(itemView);
            img1 = (ImageView) itemView.findViewById(R.id.imgViewHome);
            tv1 = (TextView) itemView.findViewById(R.id.tv_title);
            card_view = itemView.findViewById(R.id.card_view);
            lin_inner = itemView.findViewById(R.id.lin_inner);


        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) {
                mClickListener.onItemClick(view, getAdapterPosition());
            }
        }
    }

}
