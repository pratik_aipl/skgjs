package com.skgjst.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.callinterface.NotificationListener;
import com.skgjst.model.MainUser;
import com.skgjst.model.NotificationType;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 10/9/2018.
 */

public class NotificationAdapter extends BaseExpandableListAdapter implements AsynchTaskListner {

    private Context _context;
    public ArrayList<NotificationType> headerArrayList = new ArrayList<>();

    public ChildHolder childHolder;
    public GroupHolder groupHolder;
    public NotificationListener notificationListener;
    public ExpandableListView expandableListView;
    public MainUser user;


    public NotificationAdapter(Fragment context, ArrayList<NotificationType> headerArrayList, ExpandableListView expandableListView, MainUser user) {
        this._context = context.getActivity();
        this.headerArrayList = headerArrayList;
        this.notificationListener = (NotificationListener) context;
        this.expandableListView = expandableListView;
        this.user = user;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return headerArrayList.get(groupPosition).notificationsArray.get(childPosititon);
    }


    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @SuppressLint("NewApi")
    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {


        if (convertView == null) {
            convertView = LayoutInflater.from(_context).inflate(R.layout.list_item, null);
            childHolder = new ChildHolder();
            childHolder.txtListChild = convertView.findViewById(R.id.lblListItem);
            childHolder.message = convertView.findViewById(R.id.message);
            childHolder.tv_mmm = convertView.findViewById(R.id.tv_mmm);
            convertView.invalidate();
            convertView.setTag(R.layout.list_item, childHolder);


        } else {
            childHolder = (ChildHolder) convertView.getTag(R.layout.list_item);
        }

        if (headerArrayList.get(groupPosition).notificationsArray.get(childPosition).IsActive) {
            try {
                LinearLayout parentGroup = (LinearLayout) convertView.findViewById(R.id.linearLayout);
                parentGroup.setBackgroundResource(R.color.sky);
               new CallRequest(_context).getWatched(user,String.valueOf(headerArrayList.get(groupPosition).notificationsArray.get(childPosition).getNotification_id()));
               } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                LinearLayout parentGroup = (LinearLayout) convertView.findViewById(R.id.linearLayout);
                parentGroup.setBackgroundResource(R.color.white);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (headerArrayList.get(groupPosition).notificationsArray.get(childPosition).getNotification_type().equals("Birthday")) {
            childHolder.txtListChild.setText(headerArrayList.get(groupPosition).notificationsArray.get(childPosition).getFName());
// " wrote a birthday wish to you"+ "  wrote an aniversary wish to you"
            childHolder.message.setText(headerArrayList.get(groupPosition).notificationsArray.get(childPosition).getMessage());
            childHolder.tv_mmm.setText("wrote a birthday wish to you.");

        } else if (headerArrayList.get(groupPosition).notificationsArray.get(childPosition).getNotification_type().equals("Anniversary")) {
            childHolder.txtListChild.setText(headerArrayList.get(groupPosition).notificationsArray.get(childPosition).getFName());
            childHolder.message.setText(headerArrayList.get(groupPosition).notificationsArray.get(childPosition).getMessage());
            childHolder.tv_mmm.setText("wrote an aniversary wish to you.");


        }
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int grouppPosition, long id) {

               /* if(parent.isGroupExpanded(groupPosition))
                {
               */
                notifyDataSetChanged();

                try {
                    for(int i=0;i<headerArrayList.get(groupPosition).notificationsArray.size();i++){
                        headerArrayList.get(groupPosition).notificationsArray.get(i).setActive(false);

                    }
                  //    new CallRequest(_context).getaaNotifications();
                 //   new CallRequest(_context).getWatched(String.valueOf(headerArrayList.get(groupPosition).notificationsArray.get(grouppPosition).getNotification_id()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.i("TAG", "==>" + "IsExapand"); // Do your Staff
              /*  }
                else{
                    Log.i("TAG","==>"+"collllas"); // Do your Staff
                    try {
                        new CallRequest(_context).getWatched(String.valueOf(headerArrayList.get(groupPosition).notificationsArray.get(childPosition).getNotification_id()));
                    }catch (Exception e){
                        e.printStackTrace();
                    }
              //      notificationListener.notificationWatch(headerArrayList.get(groupPosition).notificationsArray.get(childPosition).getNotification_id()); // Expanded ,Do your Staff

                }*/


                return false;
            }
        });
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return headerArrayList.get(groupPosition).notificationsArray.size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.headerArrayList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.headerArrayList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {

            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);

            //view = LayoutInflater.from(mContext).inflate(R.layout.parent_chapter_item, null);
            groupHolder = new GroupHolder();
            groupHolder.lblListHeader = convertView.findViewById(R.id.lblListHeader);
            convertView.setTag(groupHolder);
        } else {
            groupHolder = (GroupHolder) convertView.getTag();
        }
        //  groupHolder.lblListHeader.setTypeface(null, Typeface.BOLD);

        groupHolder.lblListHeader.setText(headerArrayList.get(groupPosition).getNotification_name());


        return convertView;
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            //    Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getWatched:
                    Utils.hideProgressDialog();
                    JSONObject jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            //   Utils.showToast(jObj.getString("UMessage"), this);
                            notifyDataSetChanged();
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), _context);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    class GroupHolder {
        public TextView lblListHeader;
    }

    class ChildHolder {
        public TextView txtListChild, message, tv_mmm;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}