package com.skgjst;

import android.content.Intent;
import android.os.StrictMode;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;
import com.skgjst.model.BloodGroup;
import com.skgjst.model.BusinessAd;
import com.skgjst.model.BusinessAdvertiseMent;
import com.skgjst.model.BusinessNature;
import com.skgjst.model.City;
import com.skgjst.model.Country;
import com.skgjst.model.DemiseDataNew;
import com.skgjst.model.DemiseType;
import com.skgjst.model.EventImages;
import com.skgjst.model.Gender;
import com.skgjst.model.LoadName;
import com.skgjst.model.MainUser;
import com.skgjst.model.MaritalStatus;
import com.skgjst.model.MatrimonialDetail;
import com.skgjst.model.MatrimonialMember;
import com.skgjst.model.OtherMemberDetail;
import com.skgjst.model.Profession;
import com.skgjst.model.RegisterData;
import com.skgjst.model.Relation;
import com.skgjst.model.State;
import com.skgjst.model.Surname;
import com.skgjst.model.Village;
import com.skgjst.utils.MySharedPref;
import com.skgjst.utils.Utils;

import io.fabric.sdk.android.Fabric;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class App extends MultiDexApplication {

    public static App instance;
  //  public static MySharedPref mySharedPref;
    public static String myPref = "skgjst_pref";
    public static App app;
   // public static MainUser mainUser;
    public static RegisterData registerData;

    public static String userId = "", title = "";
    public static String familyId = "";
    public static String FamilyDetailID = "";
    public static String memberFamilyDetailID = "";
    public static String memberfamilyId = "";
    public static String modified_by = "";
    public static String modified_on = "";
    public static int MemberRelationID=0;

    public static String SurName = "";

    public static int birthdayCount = 0;
    public static int anniCount = 0;
    public static int demiseCount = 0;
    public static int notiCount = 0;

    public static int birthdayID = 0;
    public static int anniID = 0;
    public static int demiseID = 0;
    public static int notiID = 0;
    public static boolean isRemove = false;
    public static boolean isMatriEdit= false,isHome=true;
    public static String MarriedDaughterDetailID = "";
    public static String DikshaDetailID = "";

    public static String DaughterInLawIDWifeID = "";
    public static boolean isAdded = false;
    public static boolean isChageicon = false;
    public static String isDemiseType = "All";

    public static String editProfile = "";
    public static String memberCount = "";
    public static boolean isfirst = false;


    public static ArrayList<BusinessAdvertiseMent> platinumAdvertiseMent = new ArrayList<>();
    public static ArrayList<BusinessAdvertiseMent> goldAdvertiseMent = new ArrayList<>();
    public static ArrayList<BusinessAdvertiseMent> businessAdvertiseMent = new ArrayList<>();
    public static ArrayList<BusinessAdvertiseMent> diamondAdvertiseMent = new ArrayList<>();
    public static ArrayList<BusinessAd> businessAdArrayList = new ArrayList<>();

    public static HashMap<String, String> memberDetails = new HashMap<>();

    public static ArrayList<OtherMemberDetail> otherMemberDetailArrayList = new ArrayList<>();


    public static ArrayList<MatrimonialDetail> matrimonialDetailArrayList = new ArrayList<>();

    public static ArrayList<EventImages> eventImagesArrayList = new ArrayList<>();
    public static ArrayList<BusinessAdvertiseMent> businessAdvertiseMents = new ArrayList<>();

    public static ArrayList<Relation> relationArrayList = new ArrayList<>();
    public static ArrayList<String> relationArrayName = new ArrayList<>();

    public static ArrayList<Relation> relationMembersArrayList = new ArrayList<>();
    public static ArrayList<String> relationMembersArrayName = new ArrayList<>();


    public static ArrayList<Gender> genderArrayList = new ArrayList<>();
    public static ArrayList<String> genderArrayName = new ArrayList<>();

    public static ArrayList<Surname> surnameArrayList = new ArrayList<>();
    public static ArrayList<String> surnameArrayName = new ArrayList<>();

    public static ArrayList<MaritalStatus> maritalStatusArrayList = new ArrayList<>();
    public static ArrayList<String> maritalStatusArrayName = new ArrayList<>();

    public static ArrayList<BloodGroup> bloodGroupArrayList = new ArrayList<>();
    public static ArrayList<String> bloodGroupArrayName = new ArrayList<>();

    public static ArrayList<Village> villageArrayList = new ArrayList<>();
    public static ArrayList<String> villageArrayName = new ArrayList<>();

    public static ArrayList<Profession> professionArrayList = new ArrayList<>();
    public static ArrayList<String> professionArrayName = new ArrayList<>();

    public static ArrayList<BusinessNature> businessNatureArrayList = new ArrayList<>();
    public static ArrayList<String> businessArrayName = new ArrayList<>();

    public static ArrayList<Country> countryArrayList = new ArrayList<>();
    public static ArrayList<String> countryArrayName = new ArrayList<>();

    public static ArrayList<State> stateArrayList = new ArrayList<>();
    public static ArrayList<String> stateArrayName = new ArrayList<>();

    public static ArrayList<City> cityArrayList = new ArrayList<>();
    public static ArrayList<String> cityArrayName = new ArrayList<>();

    public static ArrayList<LoadName> loadNameArrayList = new ArrayList<>();
    public static ArrayList<String> loadNameArrayName = new ArrayList<>();

    public static ArrayList<MatrimonialMember> loadMemberArrayList = new ArrayList<>();
    public static ArrayList<String> loadMemberArrayName = new ArrayList<>();

    public static boolean personalDetail = false, addressDetail = false, bussinesssDetail = false, memberDetail = false, marrideDaughterDetail = false,
            wifeDaughterDetail = false, fatherFamilyDetail = false;

    public static String PleyaerID = "";

    public static ArrayList<DemiseType> headerArrayList = new ArrayList<>();
    public static ArrayList<DemiseDataNew> PrathnasArrayList = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        instance = this;
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        PleyaerID = Utils.OneSignalPlearID();
        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new OneSignal.NotificationOpenedHandler() {
                    @Override
                    public void notificationOpened(OSNotificationOpenResult result) {
                        try {
                            Intent intent = new Intent(instance, SplashActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra("inNoti", "1");
                            instance.startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNotificationReceivedHandler(new OneSignal.NotificationReceivedHandler() {
                    @Override
                    public void notificationReceived(OSNotification notification) {
                    }
                })

                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .init();

        try {
            app = this;
            Class.forName("android.os.AsyncTask");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public App() {
        setInstance(this);
    }

    public static void setInstance(App instance) {
        App.instance = instance;
    }

    public static App getInstance() {
        return instance;
    }

    public static String getFormattedTime(String date) {
        /// android  /// 2018-04-25T08:40:13.16

        try {
            Log.i("DATE", "IN Android FORMAT");
            SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");

            SimpleDateFormat newFormat = new SimpleDateFormat("dd MMM yyyy");
            Date currentDate = new Date();

            currentDate = format.parse(date);

            return newFormat.format(currentDate);

        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

}