package com.skgjst.activities.profile.fragment;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.skgjst.App;
import com.skgjst.BaseFragment;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.Contects;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by empiere-vaibhav on 9/19/2018.
 */

public class ContactFragment extends BaseFragment implements AsynchTaskListner {
    public View v;
    public ContactFragment fragment;
    public Contects obj;
    public JsonParserUniversal jParser;
    public TextView tv_wing, tv_room_number, tv_building_name, tv_road_name, tv_suburb_name,
            tv_city, tv_pincode, tv_contectno, tv_plot_no, tv_district, tv_modified_by, tv_modified_on;;

    public ContactFragment newInstance() {
        fragment = new ContactFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.setHasOptionsMenu(true);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_contect, container, false);
        fragment = this;
        jParser = new JsonParserUniversal();
        tv_modified_by = v.findViewById(R.id.tv_modified_by);
        tv_modified_on = v.findViewById(R.id.tv_modified_on);

        tv_wing = v.findViewById(R.id.tv_wing);
        tv_room_number = v.findViewById(R.id.tv_room_number);
        tv_building_name = v.findViewById(R.id.tv_building_name);
        tv_road_name = v.findViewById(R.id.tv_road_name);
        tv_suburb_name = v.findViewById(R.id.tv_suburb_name);
        tv_city = v.findViewById(R.id.tv_city);
        tv_pincode = v.findViewById(R.id.tv_pincode);
        tv_contectno = v.findViewById(R.id.tv_contectno);
        tv_plot_no = v.findViewById(R.id.tv_plot_no);
        tv_district = v.findViewById(R.id.tv_district);

          return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        new CallRequest(fragment).getContact(user.getFamilyDetailID());
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (getView() != null) {
            new CallRequest(fragment).getContact(user.getFamilyDetailID());
            //  setData();
        } else {

        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            //    Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getContact:

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {

                            if (jObj.getJSONArray("Contacts") != null && jObj.getJSONArray("Contacts").length() > 0) {
                                JSONArray userData = jObj.getJSONArray("Contacts");

                                for (int i = 0; i < userData.length(); i++) {
                                    JSONObject jobj = userData.getJSONObject(i);
                                    obj = (Contects) jParser.parseJson(jobj, new Contects());

                                }
                                setData();
                                Utils.hideProgressDialog();
                            } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("UMessage"), getActivity());
                            }
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), getActivity());
                        }
                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        Utils.showToast("Something getting wrong! Please try again later.", getActivity());

                        e.printStackTrace();
                    }
            }
        }
    }

    public void setData() {
        tv_wing.setText(obj.getWing());
        tv_room_number.setText(obj.getRoomNo());
        tv_building_name.setText(obj.getBuildingName());
        tv_road_name.setText(obj.getRoadName());
        tv_suburb_name.setText(obj.getSuburbName());
        tv_city.setText(obj.getCity());
        tv_pincode.setText(obj.getPincode());
        tv_contectno.setText(obj.getContactNo());
        tv_plot_no.setText(obj.getPlotNo());
        tv_modified_on.setText(obj.getModifiedOn());

        tv_modified_by.setText(obj.getModifiedBy());
        tv_district.setText(obj.getDistrictName());
        tv_contectno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", obj.getContactNo(), null));
                getActivity().startActivity(intent);


              /*  Intent intent = new Intent(Intent.ACTION_CALL);

                intent.setData(Uri.parse("tel:" + obj.getContactNo()));
                startActivity(intent);*/
            }
        });
    }

}
