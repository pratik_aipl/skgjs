package com.skgjst.activities.job;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.adapter.JobListAdapter;
import com.skgjst.fonts.MyCustomTypeface;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.JobList;
import com.skgjst.R;
import com.skgjst.model.MainUser;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class JobListActivity  extends BaseActivity implements AsynchTaskListner {
    public JobListActivity instance;
    public Toolbar toolbar;
    public LinearLayout lin_empty;
    public RecyclerView rv_job_list;
    public JsonParserUniversal jParser;
    public TextView tv_title, tv_message, tv_count;
    public JobList jobObj;
    public ArrayList<JobList> jobArrayList = new ArrayList<>();
    public FloatingActionButton float_add;
    public JobListAdapter adapter;
    public ImageView img_my_job, img_filter;
    public CircleImageView img_profile;
    public Dialog dialog;
    public Spinner sp_exp_to, sp_exp_from, sp_location;
    public ImageView imgClose;
    public String exp_from = "0", exp_to = "0", location = "";
    public Button btn_filter;
    public ArrayList<String> strCityArray = new ArrayList<>();
    public boolean isBack = false;

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_list);


        instance = this;
        jParser = new JsonParserUniversal();
        float_add = findViewById(R.id.float_add);
        rv_job_list = findViewById(R.id.rv_job_list);
        toolbar = findViewById(R.id.toolbar);
        img_profile = toolbar.findViewById(R.id.img_profile);
        Utils.setRoundedImage(instance,user.getPhotoURL(),300,300,img_profile,null);
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });
        tv_title = toolbar.findViewById(R.id.tv_title);
        tv_count = findViewById(R.id.tv_count);
        tv_message = findViewById(R.id.tv_message);
        float_add = findViewById(R.id.float_add);
        lin_empty = findViewById(R.id.lin_empty);
        img_my_job = findViewById(R.id.img_my_job);
        img_filter = findViewById(R.id.img_filter);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("JOB SEARCH");
        new CallRequest(instance).getJoblist(user);
        float_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, CreateJobActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                );
            }
        });

        img_my_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                img_my_job.setVisibility(View.GONE);
                img_filter.setVisibility(View.GONE);
                isBack = true;
                new CallRequest(instance).getMyJoblist(user.getFamilyDetailID());
            }
        });

        img_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog = new Dialog(instance);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setContentView(R.layout.dialog_job_search);
                sp_location = dialog.findViewById(R.id.sp_location);
                sp_exp_from = dialog.findViewById(R.id.sp_exp_from);
                sp_exp_to = dialog.findViewById(R.id.sp_exp_to);
                btn_filter = dialog.findViewById(R.id.btn_filter);
                imgClose = dialog.findViewById(R.id.imgClose);
                new CallRequest(instance).locationsforjob(user);
                imgClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        dialog.dismiss();
                    }
                });


                sp_exp_from.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        try {
                            ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                            ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                            if (position > 0) {
                                ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.color_back));
                                ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                                exp_from = ((TextView) parent.getChildAt(0)).getText().toString();

                            } else {
                                exp_from = "0";
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
                sp_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        try {
                            ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                            ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                            if (position > 0) {
                                ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.color_back));
                                ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                                location = ((TextView) parent.getChildAt(0)).getText().toString();

                            } else {
                                location = "0";
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });

                sp_exp_to.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        try {
                            ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                            ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                            if (position > 0) {
                                ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.color_back));
                                ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                                exp_to = ((TextView) parent.getChildAt(0)).getText().toString();

                            } else {
                                exp_to = "0";
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
                btn_filter.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new CallRequest(instance).jobsearch(user,exp_from, exp_to, location);
                    }
                });
                dialog.show();

                //  new CallRequest(BusinessSearchFragment.this).Profession_List();
                //    new CallRequest(BusinessSearchFragment.this).NatureOfBusiness_List();

            }
        });

    }

    @Override
    public void onBackPressed() {
        if (isBack) {
            new CallRequest(instance).getJoblist(user);
            img_my_job.setVisibility(View.VISIBLE);
            img_filter.setVisibility(View.VISIBLE);
            isBack = false;

        } else {
            super.onBackPressed();

        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();

                switch (request) {
                    case jobsearch:
                        Utils.hideProgressDialog();
                        tv_title.setText("JOB SEARCH");
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            jobArrayList.clear();
                            dialog.dismiss();
                            JSONArray jarray = jObj.getJSONArray("JobData");
                            if (jarray.length() > 0 && jarray != null) {
                                lin_empty.setVisibility(View.GONE);
                                rv_job_list.setVisibility(View.VISIBLE);
                                for (int i = 0; i < jarray.length(); i++) {
                                    JSONObject jData = jarray.getJSONObject(i);
                                    jobObj = (JobList) jParser.parseJson(jData, new JobList());
                                    jobArrayList.add(jobObj);
                                }
                                 tv_count.setText(jobArrayList.size()  + " - Job Found");
                                SetupRecylerView();
                            } else {
                                Utils.hideProgressDialog();
                                tv_count.setText("0 - Job Found");

                                tv_message.setText(jObj.getString("UMessage"));
                                //   Utils.showToast(jObj.getString("UMessage"), this);
                                lin_empty.setVisibility(View.VISIBLE);
                                rv_job_list.setVisibility(View.GONE);
                            }
                        } else {
                            Utils.hideProgressDialog();
                            tv_count.setText("0 - Job Found");

                            tv_message.setText(jObj.getString("UMessage"));
                            //   Utils.showToast(jObj.getString("UMessage"), this);
                            lin_empty.setVisibility(View.VISIBLE);
                            rv_job_list.setVisibility(View.GONE);
                        }
                        break;
                    case getJoblist:
                        Utils.hideProgressDialog();
                        tv_title.setText("JOB SEARCH");
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            jobArrayList.clear();
                            JSONArray jarray = jObj.getJSONArray("JobData");
                            if (jarray.length() > 0 && jarray != null) {
                                lin_empty.setVisibility(View.GONE);
                                rv_job_list.setVisibility(View.VISIBLE);
                                for (int i = 0; i < jarray.length(); i++) {
                                    JSONObject jData = jarray.getJSONObject(i);
                                    jobObj = (JobList) jParser.parseJson(jData, new JobList());
                                    jobArrayList.add(jobObj);
                                }
                                 tv_count.setText(jobArrayList.size() + " - Job Found");
                                SetupRecylerView();
                            } else {
                                Utils.hideProgressDialog();
                                tv_count.setText("0 - Job Found");

                                tv_message.setText(jObj.getString("UMessage"));
                                //   Utils.showToast(jObj.getString("UMessage"), this);
                                lin_empty.setVisibility(View.VISIBLE);
                                rv_job_list.setVisibility(View.GONE);
                            }
                        } else {
                            Utils.hideProgressDialog();
                            tv_message.setText(jObj.getString("UMessage"));
                            //   Utils.showToast(jObj.getString("UMessage"), this);
                            lin_empty.setVisibility(View.VISIBLE);
                            rv_job_list.setVisibility(View.GONE);
                        }
                        break;
                    case locationsforjob:
                        Utils.hideProgressDialog();
                        try

                        {
                            jObj = null;
                            jObj = new JSONObject(result);

                            if (jObj.getBoolean("Success") == true) {
                                strCityArray.clear();
                                if (jObj.getJSONArray("Locations") != null && jObj.getJSONArray("Locations").length() > 0) {

                                    strCityArray.add("Select Location");
                                    JSONArray jDataArray = jObj.getJSONArray("Locations");
                                    for (int i = 0; i < jDataArray.length(); i++) {
                                        strCityArray.add(jDataArray.getJSONObject(i).getString("joblocation"));
                                    }
                                    sp_location.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strCityArray));

                                } else {
                                    Utils.showToast(jObj.getString("UMessage"), instance);
                                }
                            } else {
                                Utils.showToast(jObj.getString("UMessage"), instance);
                            }
                        } catch (
                                JSONException e)

                        {
                            Utils.showToast("Something getting wrong! Please try again later.", instance);
                            e.printStackTrace();
                        }
                        break;
                    case getMyJoblist:
                        Utils.hideProgressDialog();
                        jObj = new JSONObject(result);
                        tv_title.setText("MY JOBS");
                        if (jObj.getBoolean("Success")) {
                            jobArrayList.clear();
                            JSONArray jarray = jObj.getJSONArray("Jobs");
                            if (jarray.length() > 0 && jarray != null) {
                                lin_empty.setVisibility(View.GONE);
                                rv_job_list.setVisibility(View.VISIBLE);
                                for (int i = 0; i < jarray.length(); i++) {
                                    JSONObject jData = jarray.getJSONObject(i);
                                    jobObj = (JobList) jParser.parseJson(jData, new JobList());
                                    jobArrayList.add(jobObj);
                                }
                                jobObj = new JobList();
                                jobArrayList.add(jobObj);
                                tv_count.setText(jobArrayList.size() + " - Job Found");
                                SetupRecylerView();
                            } else {
                                Utils.hideProgressDialog();
                                tv_count.setText("0 - Job Found");

                                tv_message.setText(jObj.getString("UMessage"));
                                //   Utils.showToast(jObj.getString("UMessage"), this);
                                lin_empty.setVisibility(View.VISIBLE);
                                rv_job_list.setVisibility(View.GONE);
                            }
                        } else {
                            Utils.hideProgressDialog();
                            tv_count.setText("0 - Job Found");
                            tv_message.setText(jObj.getString("UMessage"));
                            //   Utils.showToast(jObj.getString("UMessage"), this);
                            lin_empty.setVisibility(View.VISIBLE);
                            rv_job_list.setVisibility(View.GONE);
                        }
                        break;
                }
            } catch (JSONException e) {
                tv_count.setText("0 - Job Found");
                lin_empty.setVisibility(View.VISIBLE);
                rv_job_list.setVisibility(View.GONE);
                Utils.hideProgressDialog();
                Utils.showToast("Server side error...", this);
                e.printStackTrace();
            }
        } else {
            lin_empty.setVisibility(View.VISIBLE);
            rv_job_list.setVisibility(View.GONE);
            Utils.hideProgressDialog();

        }
    }

    public void SetupRecylerView() {

        adapter = new JobListAdapter(user,jobArrayList, instance);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);
        rv_job_list.setLayoutAnimation(controller);
        rv_job_list.scheduleLayoutAnimation();
        rv_job_list.setLayoutManager(new LinearLayoutManager(instance, LinearLayoutManager.VERTICAL, false));
        rv_job_list.setItemAnimator(new DefaultItemAnimator());
        rv_job_list.setAdapter(adapter);
    }
}
