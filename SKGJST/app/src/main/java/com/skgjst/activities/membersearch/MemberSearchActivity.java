package com.skgjst.activities.membersearch;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.R;
import com.skgjst.activities.DashBoardActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.fonts.MyCustomTypeface;
import com.skgjst.fragment.member.MemberSearchAdapter;
import com.skgjst.model.BloodGroup;
import com.skgjst.model.CurrentCity;
import com.skgjst.model.MaritalStatus;
import com.skgjst.model.Surname;
import com.skgjst.model.Village;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MemberSearchActivity extends BaseActivity implements AsynchTaskListner {
    public MemberSearchActivity instance;
    public Toolbar toolbar;
    public LinearLayout lin_empty;
    public RecyclerView rv_job_list;
    public JsonParserUniversal jParser;
    public TextView tv_title, tv_message, tv_count;
    public FloatingActionButton float_add;
    public ImageView img_my_job, img_filter, imgClose, img_my_addprofile_list;
    public CircleImageView img_profile;
    public Dialog dialog;
    public Spinner sp_surname, sp_blood_group, sp_village, sp_mat_status, sp_gender, sp_min_age, sp_max_age, sp_city;
    public Surname surname;
    public ArrayList<Surname> surnameArray = new ArrayList<>();
    public ArrayList<String> strSurnameArray = new ArrayList<>();
    // public String surname_id = "", surname_name = "";
    public BloodGroup bloodGroup;
    public ArrayList<BloodGroup> bloodGroupArray = new ArrayList<>();
    public ArrayList<String> strBloodGroupArray = new ArrayList<>();
    public String blood_group_id = "0";
    public Village village;
    public ArrayList<Village> villageArray = new ArrayList<>();
    public ArrayList<String> strVillageArray = new ArrayList<>();
    public int village_id;
    String village_name = "";
    public CurrentCity city;
    public ArrayList<CurrentCity> cityArray = new ArrayList<>();
    public ArrayList<String> stringCityArray = new ArrayList<>();
    public ArrayList<String> stringSurnameArray = new ArrayList<>();
    public String city_id = "", city_name = "", sur_name = "";
    public int location_id, surnameID;

    public MaritalStatus maritalStatus;
    public ArrayList<MaritalStatus> maritalStatusArray = new ArrayList<>();
    public ArrayList<String> strMaritalStatusArray = new ArrayList<>();
    public String marital_status_id = "0", min_age = "0", max_age = "0", last_select_min_age = "0";
    public String selectGender = "";
    public RecyclerView rv_member_list;
    public EditText et_area, et_first_name;
    public Button btn_filter;
    public MemberSearchAdapter memberSearchAdapter;
    public MemberList memberList;
    public ArrayList<MemberList> memberListArray = new ArrayList<>();
    private ArrayList<String> minAgeList = new ArrayList<>();
    private ArrayList<String> maxAgeList = new ArrayList<>();
    public AutoCompleteTextView et_city, et_surname, et_native;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_search);
        instance = this;
        jParser = new JsonParserUniversal();
        float_add = findViewById(R.id.float_add);
        rv_member_list = findViewById(R.id.rv_member_list);
        toolbar = findViewById(R.id.toolbar);
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_my_addprofile_list = findViewById(R.id.img_my_addprofile_list);

        openFilter();
        Utils.setRoundedImage(instance, user.getPhotoURL(), 300, 300, img_profile, null);

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });
        tv_title = toolbar.findViewById(R.id.tv_title);
        tv_count = findViewById(R.id.tv_count);
        tv_message = findViewById(R.id.tv_message);
        float_add = findViewById(R.id.float_add);
        lin_empty = findViewById(R.id.lin_empty);
        img_my_job = findViewById(R.id.img_my_job);
        img_filter = findViewById(R.id.img_filter);
        img_my_job.setVisibility(View.GONE);
        img_my_addprofile_list.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*startActivity(new Intent(instance, DashBoardActivity.class));
                finish();*/
                onBackPressed();
            }
        });
        tv_title.setText("MEMBER SEARCH");
        float_add.setVisibility(View.GONE);
        float_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, AddMemberActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                );
            }
        });
        img_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFilter();
            }
        });
        //    new CallRequest(instance).getMemberSearch();


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(instance, DashBoardActivity.class));
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void openFilter() {
        dialog = new Dialog(instance);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialog.setContentView(R.layout.dailog_member_filter);
        btn_filter = dialog.findViewById(R.id.btn_filter);
        et_area = dialog.findViewById(R.id.et_area);
        et_first_name = dialog.findViewById(R.id.et_first_name);
        // sp_surname = dialog.findViewById(R.id.sp_surname);
        sp_blood_group = dialog.findViewById(R.id.sp_blood_group);
        // sp_village = dialog.findViewById(R.id.sp_village);
        sp_mat_status = dialog.findViewById(R.id.sp_mat_status);
        sp_gender = dialog.findViewById(R.id.sp_gender);
        sp_max_age = dialog.findViewById(R.id.sp_max_age);
        sp_min_age = dialog.findViewById(R.id.sp_min_age);
        sp_city = dialog.findViewById(R.id.sp_min_age);
        et_city = dialog.findViewById(R.id.et_city);
        et_surname = dialog.findViewById(R.id.et_surname);
        et_native = dialog.findViewById(R.id.et_native);
        surname = new Surname();
        bloodGroup = new BloodGroup();
        village = new Village();
        city = new CurrentCity();
        maritalStatus = new MaritalStatus();
        minAgeList.clear();
        maxAgeList.clear();
        max_age = "0";
        min_age = "0";

        minAgeList.add("From Age");
        maxAgeList.add("To Age");

        for (int i = 0; i < 100; i++) {
            minAgeList.add(String.valueOf(i));
        }
        for (int i = 0; i < 100; i++) {
            maxAgeList.add(String.valueOf(i));
        }
        sp_min_age.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, minAgeList));
        sp_max_age.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, maxAgeList));

        new CallRequest(instance).getSurnameList();
        new CallRequest(instance).getVillageList();
        new CallRequest(instance).getBloodGroup();
        new CallRequest(instance).getMaritalStatus();
        new CallRequest(instance).getCity();

        sp_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        if (position == 1) {
                            selectGender = "M";
                        }
                        if (position == 2) {
                            selectGender = "F";
                        }

                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_mat_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        marital_status_id = String.valueOf(maritalStatusArray.get(position).getMaritalstatusID());

                    } else {
                        marital_status_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
   /*     sp_surname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        surname_id = String.valueOf(surnameArray.get(position).getmSurnameid());
                        surname_name = surnameArray.get(position).getSurname();
                    } else {
                        surname_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/
        sp_blood_group.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        blood_group_id = String.valueOf(bloodGroupArray.get(position).getBloodGroupID());

                    } else {
                        blood_group_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
       /* sp_village.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        village_id = String.valueOf(villageArray.get(position).getVillageID());
                        village_name = villageArray.get(position).getVillagename();

                    } else {
                        village_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/
        sp_min_age.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        min_age = minAgeList.get(position);
                        int minmum_age = Integer.parseInt(minAgeList.get(position));

                        maxAgeList.clear();
                        max_age = "0";
                        maxAgeList.add("To Age");

                        for (int i = (minmum_age + 1); i <= 100; i++) {
                            maxAgeList.add(String.valueOf(i));
                        }
                        sp_max_age.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, maxAgeList));


                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_max_age.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        max_age = String.valueOf(maxAgeList.get(position));
                        int max_age = Integer.parseInt(maxAgeList.get(position));
                        last_select_min_age = min_age;
                       /* if(min_age.equalsIgnoreCase("0")) {
                            minAgeList.clear();
                            minAgeList.add("From Age");

                            for (int i = 1; i < max_age; i++) {
                                minAgeList.add(String.valueOf(i));
                            }
                            sp_min_age.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, minAgeList));
                        }*/
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        imgClose = dialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        et_city.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                city_name = parent.getItemAtPosition(position).toString();
                for (int i = 0; i < cityArray.size(); i++) {
                    if (cityArray.get(i).getCity().equalsIgnoreCase(city_name)) {
                        location_id = (cityArray.get(i).getLocationID());
                    }

                }
                Log.i("City_name", "==>" + city_name);
                Log.i("location_id", "==>" + location_id);
            }
        });
        et_surname.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                sur_name = parent.getItemAtPosition(position).toString();
                for (int i = 0; i < surnameArray.size(); i++) {
                    if (surnameArray.get(i).getSurname().equalsIgnoreCase(sur_name)) {
                        surnameID = (surnameArray.get(i).getmSurnameid());
                    }

                }
                Log.i("City_name", "==>" + city_name);
                Log.i("location_id", "==>" + location_id);
            }
        });
        et_native.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                village_name = parent.getItemAtPosition(position).toString();
                for (int i = 0; i < villageArray.size(); i++) {
                    if (villageArray.get(i).getVillagename().equalsIgnoreCase(village_name)) {
                        village_id = (villageArray.get(i).getVillageID());
                    }

                }
                Log.i("City_name", "==>" + city_name);
                Log.i("location_id", "==>" + location_id);
            }
        });

        btn_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new CallRequest(instance).getMemberSearch(et_first_name.getText().toString(), et_area.getText().toString(), sur_name, marital_status_id,
                        blood_group_id, village_name, selectGender, min_age, max_age, String.valueOf(location_id));
            }
        });

        dialog.show();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getMemberSearch:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            dialog.dismiss();
                            memberListArray.clear();
                            if (jObj.getJSONArray("MemberSearch") != null && jObj.getJSONArray("MemberSearch").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("MemberSearch");

                                if (jSurnameArray.length() > 0) {
                                    lin_empty.setVisibility(View.GONE);
                                    rv_member_list.setVisibility(View.VISIBLE);
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        memberList = (MemberList) jParser.parseJson(jSurnameArray.getJSONObject(i), new MemberList());
                                        memberListArray.add(memberList);
                                    }
                                    tv_count.setText(memberListArray.size() + " - Members Found");

                                    setUpRecycleview();
                                } else {
                                    tv_count.setText("0 - Members Found");

                                    tv_message.setText(jObj.getString("UMessage"));
                                    //   Utils.showToast(jObj.getString("UMessage"), this);
                                    lin_empty.setVisibility(View.VISIBLE);
                                    rv_member_list.setVisibility(View.GONE);

                                    Utils.showToast(jObj.getString("UMessage"), instance);
                                }
                            } else {
                                tv_count.setText("0 - Members Found");

                                tv_message.setText(jObj.getString("UMessage"));
                                //   Utils.showToast(jObj.getString("UMessage"), this);
                                lin_empty.setVisibility(View.VISIBLE);
                                rv_member_list.setVisibility(View.GONE);

                                Utils.showToast(jObj.getString("UMessage"), instance);
                            }
                        } else {
                            tv_count.setText("0 - Members Found");
                            tv_message.setText(jObj.getString("UMessage"));
                            //   Utils.showToast(jObj.getString("UMessage"), this);
                            lin_empty.setVisibility(View.VISIBLE);
                            rv_member_list.setVisibility(View.GONE);
                            Utils.showToast(jObj.getString("UMessage"), instance);
                        }

                    } catch (JSONException e) {
                        tv_count.setText("0 - Members Found");
                        lin_empty.setVisibility(View.VISIBLE);
                        rv_member_list.setVisibility(View.GONE);
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getSurnameList:
                  /*  Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strSurnameArray.clear();
                            surnameArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                surname.setSurname("Surname");
                                surname.setmSurnameid(0);
                                strSurnameArray.add(surname.getSurname());
                                surnameArray.add(surname);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        surname = (Surname) jParser.parseJson(jSurnameArray.getJSONObject(i), new Surname());
                                        surnameArray.add(surname);
                                        strSurnameArray.add(surname.getSurname());
                                    }
                                    sp_surname.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strSurnameArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;*/
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strSurnameArray.clear();
                            surnameArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        surname = (Surname) jParser.parseJson(jSurnameArray.getJSONObject(i), new Surname());
                                        surnameArray.add(surname);
                                        strSurnameArray.add(surname.getSurname());
                                    }
                                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                            (this, android.R.layout.simple_dropdown_item_1line, strSurnameArray);

                                    et_surname.setThreshold(1);//will start working from first character
                                    et_surname.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                                    et_surname.setTextColor(Color.BLACK);

                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getCity:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            stringCityArray.clear();
                            cityArray.clear();
                            if (jObj.getJSONArray("CityList") != null && jObj.getJSONArray("CityList").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("CityList");

                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        city = (CurrentCity) jParser.parseJson(jSurnameArray.getJSONObject(i), new CurrentCity());
                                        cityArray.add(city);
                                        stringCityArray.add(city.getCity());
                                    }
                                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                            (this, android.R.layout.simple_dropdown_item_1line, stringCityArray);

                                    et_city.setThreshold(1);//will start working from first character
                                    et_city.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                                    et_city.setTextColor(Color.BLACK);

                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;

                case getMaritalStatus:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strMaritalStatusArray.clear();
                            maritalStatusArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                maritalStatus.setMaritalstatus("Marital status");
                                maritalStatus.setMaritalstatusID(0);
                                strMaritalStatusArray.add(maritalStatus.getMaritalstatus());
                                maritalStatusArray.add(maritalStatus);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        maritalStatus = (MaritalStatus) jParser.parseJson(jSurnameArray.getJSONObject(i), new MaritalStatus());
                                        maritalStatusArray.add(maritalStatus);
                                        strMaritalStatusArray.add(maritalStatus.getMaritalstatus());
                                    }
                                    sp_mat_status.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strMaritalStatusArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;


                case getBloodGroup:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strBloodGroupArray.clear();
                            bloodGroupArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                bloodGroup.setBloodGroupName("Blood group");
                                bloodGroup.setBloodGroupID(0);
                                strBloodGroupArray.add(bloodGroup.getBloodGroupName());
                                bloodGroupArray.add(bloodGroup);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        bloodGroup = (BloodGroup) jParser.parseJson(jSurnameArray.getJSONObject(i), new BloodGroup());
                                        bloodGroupArray.add(bloodGroup);
                                        strBloodGroupArray.add(bloodGroup.getBloodGroupName());
                                    }
                                    sp_blood_group.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strBloodGroupArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;


                case getVillageList:

                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strVillageArray.clear();
                            villageArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        village = (Village) jParser.parseJson(jSurnameArray.getJSONObject(i), new Village());
                                        villageArray.add(village);
                                        strVillageArray.add(village.getVillagename());
                                    }
                                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                            (this, android.R.layout.simple_dropdown_item_1line, strVillageArray);

                                    et_native.setThreshold(1);//will start working from first character
                                    et_native.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                                    et_native.setTextColor(Color.BLACK);

                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;


                   /* Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strVillageArray.clear();
                            villageArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                village.setVillagename("Native Place");
                                village.setVillageID(0);
                                strVillageArray.add(village.getVillagename());
                                villageArray.add(village);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        village = (Village) jParser.parseJson(jSurnameArray.getJSONObject(i), new Village());
                                        villageArray.add(village);
                                        strVillageArray.add(village.getVillagename());
                                    }
                                    sp_village.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strVillageArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;*/
            }
        }
    }

    private void setUpRecycleview() {
        memberSearchAdapter = new MemberSearchAdapter(memberListArray, instance);
        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);
        rv_member_list.setLayoutAnimation(controller);
        rv_member_list.scheduleLayoutAnimation();
        rv_member_list.setLayoutManager(new LinearLayoutManager(instance, LinearLayoutManager.VERTICAL, false));
        rv_member_list.setItemAnimator(new DefaultItemAnimator());
        rv_member_list.setAdapter(memberSearchAdapter);
    }
}
