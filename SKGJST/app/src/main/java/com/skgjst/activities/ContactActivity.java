package com.skgjst.activities;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.App;
import com.skgjst.R;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class ContactActivity  extends BaseActivity {
    public LinearLayout llBack;
    public TextView tvEmailAddress, tvWebsite, tvHead1, tvHead2;
    public WebView web_map;
    public Toolbar toolbar;
    public TextView tv_title;
    public CircleImageView img_profile, img_profile_local;
    public NewsActivity instance;

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("CONTACT US");
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_profile_local = toolbar.findViewById(R.id.img_profile_local);
        Utils.setRoundedImage(instance,user.getPhotoURL(),300,300,img_profile,null);

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ContactActivity.this, MyProfileActivity.class));
            }
        });

        tvEmailAddress = findViewById(R.id.tvEmailAddress);
        tvWebsite = findViewById(R.id.tvWebsite);
        tvHead1 = findViewById(R.id.tvHead1);
        tvHead2 = findViewById(R.id.tvHead2);
        web_map = findViewById(R.id.web_map);
        web_map.getSettings().setJavaScriptEnabled(true);

        web_map.getSettings().setAllowFileAccess(true);
        web_map.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        web_map.getSettings().setJavaScriptEnabled(true);
        web_map.getSettings().setDomStorageEnabled(true);
        web_map.getSettings().setUseWideViewPort(true);

        web_map.getSettings().setBuiltInZoomControls(false);
        web_map.getSettings().setDisplayZoomControls(false);
        web_map.getSettings().setLoadWithOverviewMode(true);
        web_map.setInitialScale(1);
        web_map.loadUrl("file:///android_asset/map.html");

        tvWebsite.setMovementMethod(LinkMovementMethod.getInstance());
        tvWebsite.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                browserIntent.setData(Uri.parse("http://skgjsedirectory.org/"));
                startActivity(browserIntent);
            }
        });

        tvEmailAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + "helpdesk@skgjsedirectory.org"));
                    intent.putExtra(Intent.EXTRA_SUBJECT, "");
                    intent.putExtra(Intent.EXTRA_TEXT, "");
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    //TODO smth
                    Utils.showToast("There is no app for email", ContactActivity.this);
                }
            }
        });


        tvHead1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "9867059408", null));
                startActivity(intent);
            }
        });

        tvHead2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", "9820375141", null));
                startActivity(intent);
            }
        });

    }
}
