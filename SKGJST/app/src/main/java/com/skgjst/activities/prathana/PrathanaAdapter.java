package com.skgjst.activities.prathana;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.skgjst.App;
import com.skgjst.R;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by empiere-vaibhav on 11/2/2018.
 */

public class PrathanaAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<PrathanaData> mArrayList;
    public Context context;
    public static final int VIEW_TYPE_ITEM = 1;
    public static final int VIEW_TYPE_BLANK = 2;

    public PrathanaAdapter(ArrayList<PrathanaData> moviesList, Context context) {
        mArrayList = moviesList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name;
        public LinearLayout lin_info;
        public CircleImageView img_profile;
        public ImageButton tv_send;

        public MyViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.tv_name);
            tv_send
                    = view.findViewById(R.id.tv_send);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            return new MyViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_prathana_raw, parent, false));
        } else {
            return new ViewHolderFooter(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_member_raw_footer, parent, false));
        }
    }

    public class ViewHolderFooter extends RecyclerView.ViewHolder {


        public ViewHolderFooter(View view) {
            super(view);

        }
    }

    public void bindFooterHolder(final ViewHolderFooter holder, final int pos) {

    }

    public void bindMyViewHolder(final MyViewHolder holder, final int pos) {
        final PrathanaData Obj = mArrayList.get(pos);
        holder.tv_name.setText(Obj.getName());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title = "PRATHANA DETAIL";
                context.startActivity(new Intent(context, PrathanaDetailActivity.class)
                        .putExtra("DemiseType","AllDemise")
                        .putExtra("obj", Obj));
            }
        }); holder.tv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title = "PRATHANA DETAIL";
                context.startActivity(new Intent(context, PrathanaDetailActivity.class)
                        .putExtra("DemiseType","AllDemise")
                        .putExtra("obj", Obj));
            }
        });


    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 1:
                MyViewHolder addrHolder = (MyViewHolder) holder;
                bindMyViewHolder(addrHolder, position);
                break;
            case 2:
                ViewHolderFooter footerHolder = (ViewHolderFooter) holder;
                bindFooterHolder(footerHolder, position);
                break;
        }
    }


    @Override
    public int getItemViewType(int position) {
        if (position == (mArrayList.size() - 1)) {
            return VIEW_TYPE_BLANK;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }



}

