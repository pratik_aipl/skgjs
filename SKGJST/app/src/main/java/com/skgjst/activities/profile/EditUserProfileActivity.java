package com.skgjst.activities.profile;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.skgjst.App;
import com.skgjst.BaseActivity;
import com.skgjst.R;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.fonts.MyCustomTypeface;
import com.skgjst.model.BloodGroup;
import com.skgjst.model.BusinessNature;
import com.skgjst.model.City;
import com.skgjst.model.Country;
import com.skgjst.model.Profession;
import com.skgjst.model.ProfileData;
import com.skgjst.model.State;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditUserProfileActivity extends BaseActivity implements AsynchTaskListner {
    private static final String TAG = "EditUserProfileActivity";
    final Calendar myCalendar = Calendar.getInstance();
    public EditUserProfileActivity instance;
    public Toolbar toolbar;
    public TextView tv_title, tv_dob;
    public CircleImageView img_profile, img_profile_local;
    public ProgressBar pbar;
    public EditText et_education, et_wing, et_flat_no, et_plot_no, et_building_name,
            et_road_name, et_land_mark, et_suburb_name, et_district, et_pincode;
    public ArrayList<BloodGroup> bloodGroupArray = new ArrayList<>();
    public ArrayList<String> strBloodGroupArray = new ArrayList<>();
    public String blood_group_id = "";
    public JsonParserUniversal jParser;
    public Spinner sp_blood_group;
    public Spinner sp_state, sp_city, sp_country;
    public ArrayList<Country> countryArray = new ArrayList<>();
    public ArrayList<String> strCountryArray = new ArrayList<>();
    public String country_id = "";
    public State states;
    public ArrayList<State> statesArray = new ArrayList<>();
    public ArrayList<String> strStatesArray = new ArrayList<>();
    public String states_id = "";
    public City city;
    public ArrayList<City> cityArray = new ArrayList<>();
    public ArrayList<String> strCityArray = new ArrayList<>();
    public String city_id = "";
    public Spinner sp_busi_nature, sp_profession;
    public ArrayList<Profession> professionArray = new ArrayList<>();
    public ArrayList<String> strProfessionArray = new ArrayList<>();
    public String profession_id = "", profession_name = "";
    public ArrayList<BusinessNature> businessNatureArray = new ArrayList<>();
    public ArrayList<String> strBusinessNatureArray = new ArrayList<>();
    public String business_nature_id = "";
    public Button btn_submit, btn_cancel;
    public EditText et_busi_detail, et_company_name, et_url, et_offi_no, et_offi_address, et_email, et_mobile;
    public ProfileData profileData;
    public int selcted_country = 0, selcted_state = 0, selcted_city = 0, selcted_blood_id = 0, selcted_profession = 0, selcted_bustype = 0;
    public String logoPath = "", filename = "", surname_name = "";
    public boolean isFirstTime, isFirstState;
    Uri selectedUri;
    public String FamilyDetailID, FamilyId;
    private RadioGroup radioSexGroup;
    private RadioButton radioSexButton;
    private RadioButton radioMale, radioFemale;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        instance = this;
        jParser = new JsonParserUniversal();

        Toast.makeText(instance, "EditProfile", Toast.LENGTH_SHORT).show();
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        pbar = findViewById(R.id.pbar);
        radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);
        radioMale = (RadioButton) findViewById(R.id.radioMale);
        radioFemale = (RadioButton) findViewById(R.id.radioFemale);
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_profile_local = findViewById(R.id.img_profile_local);
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        img_profile_local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
        et_mobile = findViewById(R.id.et_mobile);
        et_education = findViewById(R.id.et_education);
        tv_dob = findViewById(R.id.tv_dob);
        sp_blood_group = findViewById(R.id.sp_prof_blood);
        et_wing = findViewById(R.id.et_wing);
        et_flat_no = findViewById(R.id.et_flat_no);
        et_plot_no = findViewById(R.id.et_plot_no);
        et_building_name = findViewById(R.id.et_building_name);
        et_road_name = findViewById(R.id.et_road_name);
        et_land_mark = findViewById(R.id.et_land_mark);
        et_suburb_name = findViewById(R.id.et_suburb_name);
        et_district = findViewById(R.id.et_district);
        et_pincode = findViewById(R.id.et_pincode);
        sp_country = findViewById(R.id.sp_country);
        sp_state = findViewById(R.id.sp_state);
        sp_city = findViewById(R.id.sp_city);
        et_email = findViewById(R.id.et_email);
        et_busi_detail = findViewById(R.id.et_busi_detail);
        et_company_name = findViewById(R.id.et_company_name);
        et_url = findViewById(R.id.et_url);
        et_offi_no = findViewById(R.id.et_offi_no);
        et_offi_address = findViewById(R.id.et_offi_address);
        btn_submit = findViewById(R.id.btn_submit);
        btn_cancel = findViewById(R.id.btn_cancel);
        sp_busi_nature = findViewById(R.id.sp_busi_nature);
        sp_profession = findViewById(R.id.sp_profession);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        tv_title.setText(App.title);
        FamilyId = getIntent().getStringExtra("FamilyId");
        FamilyDetailID = getIntent().getStringExtra("FamilyDetailId");
        new CallRequest(instance).GetEditProfileParametersUser(user, FamilyId, FamilyDetailID);
        Utils.setRoundedImage(instance, user.getPhotoURL(), 300, 300, img_profile, null);

        states = new State();
        city = new City();


        new CallRequest(instance).getProfession();
        new CallRequest(instance).getBusinessNature();

        sp_busi_nature.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        business_nature_id = String.valueOf(businessNatureArray.get(position).getIndustryID());

                    } else {
                        business_nature_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_profession.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        profession_id = String.valueOf(professionArray.get(position).getProfessionID());
                        profession_name = String.valueOf(professionArray.get(position).getProfessionID());

                    } else {
                        profession_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!et_mobile.getText().toString().equalsIgnoreCase(profileData.getContactNo()) ||
                        !et_mobile.getText().toString().equalsIgnoreCase(profileData.getContactNo()) ||
                        !tv_dob.getText().toString().equalsIgnoreCase(profileData.getDOB()) ||
                        !et_building_name.getText().toString().equalsIgnoreCase(profileData.getBuildingName()) ||
                        !et_road_name.getText().toString().equalsIgnoreCase(profileData.getRoadname()) ||
                        !et_pincode.getText().toString().equalsIgnoreCase(profileData.getPincode()) ||
                        !et_email.getText().toString().equalsIgnoreCase(profileData.getEmailID())
                ) {
                    new AlertDialog.Builder(instance)
                            .setTitle("Alert?")
                            .setMessage("Are you sure to discard this changes?")
                            .setNegativeButton(android.R.string.no, null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface arg0, int arg1) {

                                    onBackPressed();
                                }
                            }).create().show();
                } else {
                    onBackPressed();
                }

            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int selectedId = radioSexGroup.getCheckedRadioButtonId();
                radioSexButton = findViewById(selectedId);
                String isAlive = radioMale.isChecked() ? "1" : "0";
                if (profileData.isMIsActive()) {
                    /*if (et_mobile.getText().toString().isEmpty()) {
                        et_mobile.setFocusable(true);
                        et_mobile.setError("Please enter mobile number");

                    } else*/
                    if (tv_dob.getText().toString().isEmpty()) {
                        Utils.showToast("Please select date of birth", instance);

                    } else if (et_building_name.getText().toString().isEmpty()) {
                        et_building_name.setFocusable(true);
                        et_building_name.setError("Please enter building name");

                    } else if (et_road_name.getText().toString().isEmpty()) {
                        et_road_name.setFocusable(true);
                        et_road_name.setError("Please enter road name");
                    } else if (country_id.equalsIgnoreCase("") || country_id.equals("0")) {
                        Utils.showToast("Please select country", instance);
                    } else if (states_id.equalsIgnoreCase("") || states_id.equals("0")) {
                        Utils.showToast("Please select state", instance);
                    } else if (city_id.equalsIgnoreCase("") || city_id.equals("0")) {
                        Utils.showToast("Please select city", instance);
                    } else if (et_pincode.getText().toString().isEmpty()) {
                        et_pincode.setFocusable(true);
                        et_pincode.setError("Please enter pincode");
                    } else if (profession_id.equalsIgnoreCase("") || profession_id.equals("0")) {
                        Utils.showToast("Please select profession", instance);
                    } else if (et_email.getText().toString().equals("") || !Utils.isValidEmail(et_email.getText().toString())) {
                        et_email.setError(" Please Enter Valid Email ID");
                        et_email.setFocusable(true);
                    } else {
                        new CallRequest(instance).editProfile(user, tv_dob.getText().toString(),
                                blood_group_id, et_education.getText().toString(), et_wing.getText().toString(),
                                et_flat_no.getText().toString(), et_plot_no.getText().toString(), et_building_name.getText().toString(),
                                et_road_name.getText().toString(), et_land_mark.getText().toString(), et_suburb_name.getText().toString(),
                                "", country_id, states_id, city_id, et_pincode.getText().toString(), profession_name,
                                business_nature_id, et_busi_detail.getText().toString(), et_company_name.getText().toString(), et_url.getText().toString(),
                                et_offi_no.getText().toString(), et_offi_address.getText().toString(), et_email.getText().toString(), logoPath, filename,
                                et_mobile.getText().toString(), FamilyDetailID, isAlive);

                    }
                } else {
                    if (!TextUtils.isEmpty(et_mobile.getText().toString().trim()) && et_mobile.getText().toString().trim().length() < 10) {
                        et_mobile.setError("Please Enter Valid Mobile Number");
                    } else if (et_mobile.getText().toString().trim().length() == 10) {
                        new CallRequest(instance).validatenumber(FamilyDetailID, et_mobile.getText().toString());
                    } else {
                        new CallRequest(instance).editprofileForNonActiveMember(user, tv_dob.getText().toString(),
                                blood_group_id, et_education.getText().toString(), et_wing.getText().toString(),
                                et_flat_no.getText().toString(), et_plot_no.getText().toString(), et_building_name.getText().toString(),
                                et_road_name.getText().toString(), et_land_mark.getText().toString(), et_suburb_name.getText().toString(),
                                "", country_id, states_id, city_id, et_pincode.getText().toString(), profession_name,
                                business_nature_id, et_busi_detail.getText().toString(), et_company_name.getText().toString(), et_url.getText().toString(),
                                et_offi_no.getText().toString(), et_offi_address.getText().toString(), et_email.getText().toString(), logoPath, filename,
                                et_mobile.getText().toString(), FamilyDetailID, isAlive);
                    }

                }

            }
        });
        init();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (!et_mobile.getText().toString().equalsIgnoreCase(profileData.getContactNo()) ||
                    !et_mobile.getText().toString().equalsIgnoreCase(profileData.getContactNo()) ||
                    !tv_dob.getText().toString().equalsIgnoreCase(profileData.getDOB()) ||
                    !et_building_name.getText().toString().equalsIgnoreCase(profileData.getBuildingName()) ||
                    !et_road_name.getText().toString().equalsIgnoreCase(profileData.getRoadname()) ||
                    !et_pincode.getText().toString().equalsIgnoreCase(profileData.getPincode()) ||
                    !et_email.getText().toString().equalsIgnoreCase(profileData.getEmailID())
            ) {
                new AlertDialog.Builder(instance)
                        .setTitle("Alert?")
                        .setMessage("Are you sure to discard this changes?")
                        .setNegativeButton(android.R.string.no, null)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface arg0, int arg1) {

                                onBackPressed();
                            }
                        }).create().show();
            } else {
                onBackPressed();
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private void init() {


        new CallRequest(instance).getBloodGroup();
        new CallRequest(instance).getCountry();
        sp_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        country_id = String.valueOf(countryArray.get(position).getCountryid());
                        new CallRequest(instance).getState(country_id);

                    } else {
                        country_id = "0";
                        states.setState("Select State");
                        states.setStateID(0);
                        strStatesArray.add(states.getState());
                        statesArray.add(states);
                        //   new CallRequest(instance).getState(country_id);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        states_id = String.valueOf(statesArray.get(position).getStateID());
                        //new CallRequest(instance).getCity(states_id);
                        new CallRequest(instance).getCity(states_id);

                    } else {
                        states_id = "0";
                        // new CallRequest(instance).getCity(states_id);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        city_id = String.valueOf(cityArray.get(position).getCityID());

                    } else {
                        city_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        tv_dob.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                    // TODO Auto-generated method stub
                                    myCalendar.set(Calendar.YEAR, year);
                                    myCalendar.set(Calendar.MONTH, monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                    String myFormat = "dd MMM yyyy"; //In which you need put here
                                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                    tv_dob.setText(sdf.format(myCalendar.getTime()));

                                }
                            };
                            DatePickerDialog datePickerDialog = new DatePickerDialog(instance, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                            myCalendar.set(Calendar.HOUR_OF_DAY, myCalendar.getMinimum(Calendar.HOUR_OF_DAY));
                            myCalendar.set(Calendar.MINUTE, myCalendar.getMinimum(Calendar.MINUTE));
                            myCalendar.set(Calendar.SECOND, myCalendar.getMinimum(Calendar.SECOND));
                            myCalendar.set(Calendar.MILLISECOND, myCalendar.getMinimum(Calendar.MILLISECOND));
                            Calendar temp = Calendar.getInstance();
                            datePickerDialog.getDatePicker().setMaxDate(temp.getTimeInMillis());
                            datePickerDialog.show();

                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return true;
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case validatenumber:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            if (jObj.getString("ValidationInfo").isEmpty() && jObj.getString("ValidationInfo").equalsIgnoreCase("")) {
                                Utils.showToast(jObj.getString("UMessage"), this);
                            } else {
                                String isAlive = radioMale.isChecked() ? "1" : "0";
                                new CallRequest(instance).editprofileForNonActiveMember(user, tv_dob.getText().toString(),
                                        blood_group_id, et_education.getText().toString(), et_wing.getText().toString(),
                                        et_flat_no.getText().toString(), et_plot_no.getText().toString(), et_building_name.getText().toString(),
                                        et_road_name.getText().toString(), et_land_mark.getText().toString(), et_suburb_name.getText().toString(),
                                        "", country_id, states_id, city_id, et_pincode.getText().toString(), profession_name,
                                        business_nature_id, et_busi_detail.getText().toString(), et_company_name.getText().toString(), et_url.getText().toString(),
                                        et_offi_no.getText().toString(), et_offi_address.getText().toString(), et_email.getText().toString(), logoPath, filename,
                                        et_mobile.getText().toString(), FamilyDetailID, isAlive);
                            }
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;
                case getBloodGroup:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strBloodGroupArray.clear();
                            bloodGroupArray.clear();
                            if (jObj.has("data") && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");
                                BloodGroup bloodGroupDef = new BloodGroup();
                                bloodGroupDef.setBloodGroupName(getResources().getString(R.string.bloodgroup));
                                bloodGroupDef.setBloodGroupID(0);
                                strBloodGroupArray.add(bloodGroupDef.getBloodGroupName());
                                bloodGroupArray.add(bloodGroupDef);

                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        BloodGroup bloodGroup = (BloodGroup) jParser.parseJson(jSurnameArray.getJSONObject(i), new BloodGroup());
                                        bloodGroupArray.add(bloodGroup);
                                        strBloodGroupArray.add(bloodGroup.getBloodGroupName());
                                    }

                                    for (int i = 0; i < bloodGroupArray.size(); i++) {
                                        if (profileData.getBloodgroupid() == (bloodGroupArray.get(i).getBloodGroupID()))
                                            selcted_blood_id = i;
                                    }

                                    sp_blood_group.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strBloodGroupArray));
                                    sp_blood_group.setSelection(selcted_blood_id);

                                    sp_blood_group.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                        @Override
                                        public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                                            if (bloodGroupArray.size() > 0) {
                                                ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                                                ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                                                ((TextView) parent.getChildAt(0)).setTextSize(14);
                                                if (position > 0) {
                                                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                                                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                                                    blood_group_id = String.valueOf(bloodGroupArray.get(position).getBloodGroupID());

                                                } else {
                                                    blood_group_id = "0";
                                                }
                                            }
                                        }

                                        @Override
                                        public void onNothingSelected(AdapterView<?> adapterView) {

                                        }
                                    });

                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }

                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getProfession:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strProfessionArray.clear();
                            professionArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                Profession professionDef = new Profession();
                                professionDef.setProfessionName(getResources().getString(R.string.profession));
                                professionDef.setProfessionID("0");
                                strProfessionArray.add(professionDef.getProfessionName());
                                professionArray.add(professionDef);

                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        Profession profession = (Profession) jParser.parseJson(jSurnameArray.getJSONObject(i), new Profession());
                                        professionArray.add(profession);
                                        strProfessionArray.add(profession.getProfessionName());
                                    }

                                    for (int i = 0; i < professionArray.size(); i++) {
                                        if (profileData.getWorkProfession().equalsIgnoreCase(professionArray.get(i).getProfessionID()))
                                            selcted_profession = i;
                                    }


                                    sp_profession.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strProfessionArray));
                                    sp_profession.setSelection(selcted_profession);
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getBusinessNature:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strBusinessNatureArray.clear();
                            businessNatureArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                BusinessNature businessNatureDEf = new BusinessNature();
                                businessNatureDEf.setIndustryName(getResources().getString(R.string.business));
                                businessNatureDEf.setIndustryID(0);
                                strBusinessNatureArray.add(businessNatureDEf.getIndustryName());
                                businessNatureArray.add(businessNatureDEf);

                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        BusinessNature businessNature = (BusinessNature) jParser.parseJson(jSurnameArray.getJSONObject(i), new BusinessNature());
                                        businessNatureArray.add(businessNature);
                                        strBusinessNatureArray.add(businessNature.getIndustryName());
                                    }

                                    for (int i = 0; i < businessNatureArray.size(); i++) {
                                        if (profileData.getBusinessType().equalsIgnoreCase(String.valueOf(businessNatureArray.get(i).getIndustryID())))
                                            selcted_bustype = i;
                                    }
                                    sp_busi_nature.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strBusinessNatureArray));
                                    sp_busi_nature.setSelection(selcted_bustype);
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case GetEditProfileParameters:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        profileData = (ProfileData) jParser.parseJson(jSurnameArray.getJSONObject(i), new ProfileData());
                                    }
                                    setProfileData(profileData);
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case getCountry:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strCountryArray.clear();
                            countryArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                Country countryDef = new Country();
                                countryDef.setLocationName(getResources().getString(R.string.country));
                                countryDef.setCountryid(0);
                                strCountryArray.add(countryDef.getLocationName());
                                countryArray.add(countryDef);

                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        Country country = (Country) jParser.parseJson(jSurnameArray.getJSONObject(i), new Country());
                                        countryArray.add(country);
                                        strCountryArray.add(country.getLocationName());
                                    }

                                    for (int i = 0; i < countryArray.size(); i++) {
                                        if (profileData.getCountry().equalsIgnoreCase(String.valueOf(countryArray.get(i).getCountryid()))) {
                                            selcted_country = i;
                                        }
                                    }

                                    sp_country.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strCountryArray));
                                    sp_country.setSelection(selcted_country);
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getState:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strStatesArray.clear();
                            statesArray.clear();
                            if (jObj.getJSONArray("StateList") != null && jObj.getJSONArray("StateList").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("StateList");

                                states.setState(getResources().getString(R.string.state));
                                states.setStateID(0);
                                strStatesArray.add(states.getState());
                                statesArray.add(states);

                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        states = (State) jParser.parseJson(jSurnameArray.getJSONObject(i), new State());
                                        statesArray.add(states);
                                        strStatesArray.add(states.getState());
                                    }

                                    for (int i = 0; i < strStatesArray.size(); i++) {
                                        if (profileData.getState().equalsIgnoreCase(String.valueOf(statesArray.get(i).getStateID())))
                                            selcted_state = i;
                                    }
                                    sp_state.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strStatesArray));
                                    // if (isFirstState) {
                                    sp_state.setSelection(selcted_state);
                                    //   isFirstState = false;
                                    //   }
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getCity:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strCityArray.clear();
                            cityArray.clear();
                            if (jObj.getJSONArray("citylist") != null && jObj.getJSONArray("citylist").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("citylist");

                                city.setCity(getResources().getString(R.string.city));
                                city.setCityID(0);
                                strCityArray.add(city.getCity());
                                cityArray.add(city);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        city = (City) jParser.parseJson(jSurnameArray.getJSONObject(i), new City());
                                        cityArray.add(city);
                                        strCityArray.add(city.getCity());
                                    }

                                    for (int i = 0; i < cityArray.size(); i++) {
                                        if (profileData.getCity().equalsIgnoreCase(String.valueOf(cityArray.get(i).getCityID())))
                                            selcted_city = i;
                                    }
                                    sp_city.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strCityArray));
                                    sp_city.setSelection(selcted_city);

                                    ///  if (isFirstTime) {
                                    //      isFirstTime = false;
                                    //   }

                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case editProfile:
                    Utils.hideProgressDialog();
                    JSONObject jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            Utils.showToast(jObj.getString("UMessage"), this);

                         /*   Intent i = new Intent(EditUserProfileActivity.this, FamilyProfileActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);*/
                            Intent i = new Intent(instance, FamilyProfileActivity.class);
                            i.putExtra("fmlid",FamilyDetailID);
                            i.putExtra("fmlID",FamilyId);
                            startActivity(i);
                            finish();
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;


            }
        }
    }

    private void setProfileData(ProfileData profileData) {
        tv_dob.setText(profileData.getDOB());
        et_mobile.setText(profileData.getContactNo());
        et_education.setText(profileData.getEducation());
        et_wing.setText(profileData.getWing());
        et_flat_no.setText(profileData.getRoomNo());
        et_plot_no.setText(profileData.getPlotno());
        et_building_name.setText(profileData.getBuildingName());
        et_road_name.setText(profileData.getRoadname());
        et_land_mark.setText(profileData.getLandmark());
        et_suburb_name.setText(profileData.getSubrubName());
        et_district.setText(profileData.getDistrictName());
        et_pincode.setText(profileData.getPincode());
        et_busi_detail.setText(profileData.getBusinessDetail());
        et_company_name.setText(profileData.getOfficeName());
        et_url.setText(profileData.getWebsiteURL());
        et_offi_no.setText(profileData.getOfficeContNo());
        et_offi_address.setText(profileData.getOfficeAddress());
        et_email.setText(profileData.getEmailID());

        if (profileData.isAlive())
            radioMale.setChecked(true);
        else
            radioFemale.setChecked(true);

        if (profileData.getMemberRelationID() == 1) {
            et_mobile.setEnabled(true);
        } else {
            et_mobile.setEnabled(!profileData.MIsActive);
        }
        Utils.setRoundedImage(instance, profileData.getPhotoURL(), 300, 300, img_profile_local, pbar);
    }

    public void selectImage() {
        CropImage.startPickImageActivity(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //      super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                selectedUri = imageUri;
                logoPath = selectedUri.getPath().toString();
                filename = (logoPath.substring(logoPath.lastIndexOf('/') + 1));

                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    img_profile_local.setImageURI(result.getUri());
                    selectedUri = result.getUri();
                    logoPath = selectedUri.getPath().toString();
                    filename = (logoPath.substring(logoPath.lastIndexOf('/') + 1));
                    Log.i("TAG", "logoPath :-> " + logoPath);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(true)
                .setMultiTouchEnabled(true)
                .start(this);
    }

}
