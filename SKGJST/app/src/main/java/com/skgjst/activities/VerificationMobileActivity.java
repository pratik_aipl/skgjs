package com.skgjst.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.skgjst.App;
import com.skgjst.BaseActivity;
import com.skgjst.BuildConfig;
import com.skgjst.R;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.callinterface.OTPListener;
import com.skgjst.model.MainUser;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.MySMSBroadCastReceiver;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class VerificationMobileActivity extends BaseActivity implements AsynchTaskListner, OTPListener {
    public Toolbar toolbar;
    public TextView tv_title;
    public VerificationMobileActivity instance;
    public String OTP = "", StaticOTP = "", FamilyID = "", FamilyDetailID = "", mobile = "";
    public Button btn_submit;
    public EditText et_otp;
    public JsonParserUniversal jParser;
    public CircleImageView img_profile;
    public String PleyaerID = "";
    public TextView tv_mobile;
    TelephonyManager telephonyManager;
    public String imeiNumber = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_mobile);
        instance = VerificationMobileActivity.this;
        jParser = new JsonParserUniversal();
        tv_mobile = findViewById(R.id.tv_mobile);
        toolbar = findViewById(R.id.toolbar);
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_profile.setVisibility(View.GONE);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("VERIFICATION");
        btn_submit = findViewById(R.id.btn_submit);
        et_otp = findViewById(R.id.et_otp);

        App.PleyaerID = getIntent().getStringExtra("PleyaerID");
        OTP = getIntent().getStringExtra("OTP");
        StaticOTP = getIntent().getStringExtra("StaticOTP");
        FamilyID = getIntent().getStringExtra("FamilyID");
        FamilyDetailID = getIntent().getStringExtra("FamilyDetailID");
        mobile = getIntent().getStringExtra("Mobile");

        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        imeiNumber = telephonyManager.getDeviceId();


        if (BuildConfig.DEBUG)
            et_otp.setText("4402");
        MySMSBroadCastReceiver.bind(instance, "SKJST");

        tv_mobile.setText(mobile);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_otp.getText().toString().isEmpty()) {
                    et_otp.setError("Please enter OTP");
                    et_otp.setFocusable(true);
                } else if (et_otp.getText().toString().equalsIgnoreCase(OTP) ||
                        et_otp.getText().toString().equalsIgnoreCase(StaticOTP)) {
                    if (Utils.isNetworkAvailable(instance)) {
                      /*  if (App.PleyaerID!= null && App.PleyaerID.equalsIgnoreCase("")) {
                            App.PleyaerID = Utils.OneSignalPlearID();
                            Utils.showProgressDialog(instance);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Utils.removeWorkingDialog(instance);
                                }
                            }, 5000);
                        } else {*/
                        new CallRequest(VerificationMobileActivity.this).verifyOtp(FamilyID, FamilyDetailID, App.PleyaerID);
                        //  }
                    }
                } else {
                    Utils.showToast("Invalid OTP", instance);

                }
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void otpReceived(String messageText) {
        et_otp.setText(messageText.substring(0, 4));
        if (!this.isDestroyed()) {
            if (Utils.isNetworkAvailable(instance)) {
                if (et_otp.getText().toString().isEmpty()) {
                    et_otp.setError("Please enter OTP");
                    et_otp.setFocusable(true);
                } else if (et_otp.getText().toString().equalsIgnoreCase(OTP) ||
                        et_otp.getText().toString().equalsIgnoreCase(StaticOTP)) {
                    if (Utils.isNetworkAvailable(instance)) {
                      /*  if (PleyaerID.equalsIgnoreCase("")) {
                            PleyaerID = Utils.OneSignalPlearID();
                            Utils.showProgressDialog(instance);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Utils.removeWorkingDialog(instance);
                                }
                            }, 5000);
                        } else {*/
                        new CallRequest(VerificationMobileActivity.this).verifyOtp(FamilyID, FamilyDetailID, "");
                        //   }
                    }
                } else {
                    Utils.showToast("Invalid OTP", instance);

                }
            } else {
                Utils.showToast("Please connect internet to Verify OTP", instance);
            }
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();

                switch (request) {

                    case verifyOtp:
                        Utils.hideProgressDialog();
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("Details") != null && jObj.getJSONArray("Details").length() > 0) {
                                Utils.showToast(jObj.getString("UMessage"), this);
                                JSONArray jData = jObj.getJSONArray("Details");
                                for (int i = 0; i < jData.length(); i++) {
                                    JSONObject jUser = jData.getJSONObject(i);
                                    user = (MainUser) jParser.parseJson(jUser, new MainUser());
                                    mySharedPref.setUserModel(gson.toJson(user));
                                    mySharedPref.setIsLoggedIn(true);
                                    mySharedPref.setIsAdShow(true);
                                }
                                new CallRequest(instance).AddDeviceLogin(user.getMUserID(), imeiNumber);
                            }


                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }

                        break;

                    case addDeviceLogin:
                        Utils.hideProgressDialog();
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            mySharedPref.setSecureLoginID(jObj.getString("SecureLoginID"));

                            Intent intent = new Intent(instance, DashBoardActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();

                        } else {
                            mySharedPref.clearApp();
                            mySharedPref.setIsAdShow(false);

                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }

                        break;

                }
            } catch (JSONException e) {
                Utils.hideProgressDialog();
                Utils.showToast("Something getting wrong! Please try again later.", instance);
                e.printStackTrace();
            }
        }
    }
}
