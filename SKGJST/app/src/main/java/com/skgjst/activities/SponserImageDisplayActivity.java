package com.skgjst.activities;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.skgjst.BaseActivity;
import com.skgjst.adapter.SponseredDetailAdapter;
import com.skgjst.App;
import com.skgjst.R;

public class SponserImageDisplayActivity  extends BaseActivity {
    public ViewPager mViewPager;
    public ImageView ivClose, ivLeft, ivRight;
    public int imagePosition;

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_image_display);

        imagePosition = getIntent().getExtras().getInt("position");


        System.out.println("position::::" + imagePosition);
        System.out.println("image list size:::" + App.eventImagesArrayList.size());
        System.out.println("list::::::::::::" + App.eventImagesArrayList);

        mViewPager = findViewById(R.id.pager);
        ivClose = findViewById(R.id.ivClose);
        ivLeft = findViewById(R.id.ivLeft);
        ivRight = findViewById(R.id.ivRight);

        if (imagePosition == 0) {
            ivLeft.setVisibility(View.GONE);
            ivRight.setVisibility(View.VISIBLE);
        } else if (imagePosition == App.businessAdvertiseMents.size() - 1) {
            ivLeft.setVisibility(View.VISIBLE);
            ivRight.setVisibility(View.GONE);
        } else {
            ivLeft.setVisibility(View.VISIBLE);
            ivRight.setVisibility(View.VISIBLE);
        }

        SponseredDetailAdapter photoViewAdapter = new SponseredDetailAdapter(SponserImageDisplayActivity.this, App.businessAdvertiseMents);
        mViewPager.setAdapter(photoViewAdapter);
        mViewPager.setCurrentItem(imagePosition);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                imagePosition = position;
                if (imagePosition == 0) {
                    ivLeft.setVisibility(View.GONE);
                    ivRight.setVisibility(View.VISIBLE);
                } else if (imagePosition == App.businessAdvertiseMents.size() - 1) {
                    ivLeft.setVisibility(View.VISIBLE);
                    ivRight.setVisibility(View.GONE);
                } else {
                    ivLeft.setVisibility(View.VISIBLE);
                    ivRight.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageSelected(int position) {
                imagePosition = position;
                if (imagePosition == 0) {
                    ivLeft.setVisibility(View.GONE);
                    ivRight.setVisibility(View.VISIBLE);
                } else if (imagePosition == App.businessAdvertiseMents.size() - 1) {
                    ivLeft.setVisibility(View.VISIBLE);
                    ivRight.setVisibility(View.GONE);
                } else {
                    ivLeft.setVisibility(View.VISIBLE);
                    ivRight.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        ivRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(getItem(+1), true);
            }
        });

        ivLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(getItem(-1), true);
            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SponserImageDisplayActivity.this.onBackPressed();
            }
        });


    }

    public int getItem(int i) {
        return mViewPager.getCurrentItem() + i;
    }

}
