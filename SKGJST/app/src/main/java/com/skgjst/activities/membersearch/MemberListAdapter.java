package com.skgjst.activities.membersearch;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.skgjst.activities.profile.FamilyProfileActivity;
import com.skgjst.model.FamilyMembers;
import com.skgjst.R;
import com.skgjst.utils.ImagePopup;
import com.skgjst.utils.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MemberListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int VIEW_TYPE_ITEM = 1;
    public static final int VIEW_TYPE_BLANK = 2;
    public Context context;
    private ArrayList<FamilyMembers> mArrayList;

    public MemberListAdapter(ArrayList<FamilyMembers> moviesList, Context context) {
        mArrayList = moviesList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            return new MyViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_member_raw, parent, false));
        } else {
            return new ViewHolderFooter(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_member_raw_footer, parent, false));
        }
    }

    public void bindFooterHolder(final ViewHolderFooter holder, final int pos) {


    }

    public void bindMyViewHolder(final MyViewHolder holder, final int pos) {
        final FamilyMembers Obj = mArrayList.get(pos);
        holder.tv_name.setText(Obj.getFName()
                + " " + Obj.getMName()
                + " " + Obj.getSurName());
        holder.tv_relation.setText(Obj.getRelationName());
        if (Obj.getPhotoURL() != null && !TextUtils.isEmpty(Obj.getPhotoURL())) {
            try {
                PicassoTrustAll.getInstance(context)
                        .load(Obj.getPhotoURL().replace(" ", "%20"))
                        .error(R.drawable.avatar)
                        .resize(256, 256)
                        .into(holder.img_profile, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        holder.img_active.setVisibility(View.VISIBLE);
        if (Obj.isAlive()) {
            if (Obj.isMIsActive() && !Obj.getPlayerID().equalsIgnoreCase(""))
                holder.img_active.setImageResource(R.drawable.ic_active);
            else
                holder.img_active.setVisibility(View.GONE);
        } else {
            holder.img_active.setImageResource(R.drawable.ic_dis_active);
        }
        final ImagePopup imagePopup = new ImagePopup(context);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.BLACK);  // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(true);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional
        imagePopup.initiatePopupWithPicasso(Obj.getPhotoURL().replace(" ", "%20"));
        holder.img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /** Initiate Popup view **/
                imagePopup.viewPopup();

            }
        });

        holder.relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, FamilyProfileActivity.class)
                        .putExtra("obj", Obj)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                );
            }
        });



    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 1:
                MyViewHolder addrHolder = (MyViewHolder) holder;
                bindMyViewHolder(addrHolder, position);
                break;
            case 2:
                ViewHolderFooter footerHolder = (ViewHolderFooter) holder;
                bindFooterHolder(footerHolder, position);
                break;
        }
    }

    public void openImage() {


    }

    @Override
    public int getItemViewType(int position) {
        if (position == (mArrayList.size() - 1)) {
            return VIEW_TYPE_BLANK;
        } else {
            return VIEW_TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name, tv_relation;
        public CircleImageView img_profile;
        public ImageView img_active;
        public RelativeLayout relative;

        public MyViewHolder(View view) {
            super(view);
           tv_name = view.findViewById(R.id.tv_name);
            tv_relation = view.findViewById(R.id.tv_relation);
            img_profile = view.findViewById(R.id.img_profile);
            img_active = view.findViewById(R.id.img_active);
            relative = view.findViewById(R.id.relative);
        }
    }

    public class ViewHolderFooter extends RecyclerView.ViewHolder {

        public ViewHolderFooter(View view) {
            super(view);

        }
    }


}
