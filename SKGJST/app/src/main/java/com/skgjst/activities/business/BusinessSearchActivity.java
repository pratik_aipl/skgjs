package com.skgjst.activities.business;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.activities.DashBoardActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.App;
import com.skgjst.fonts.MyCustomTypeface;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.BusinessList;
import com.skgjst.model.BusinessNature;
import com.skgjst.model.Profession;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class BusinessSearchActivity  extends BaseActivity implements AsynchTaskListner {
    public Toolbar toolbar;
    public TextView tv_title, tv_message, tv_count;
    public BusinessSearchActivity instance;
    public JsonParserUniversal jParser;
    public Dialog dialog;
    public Spinner sp_profession, sp_busi_nature;
    public CircleImageView img_profile;
    public Profession profession;
    public ArrayList<Profession> professionArray = new ArrayList<>();
    public ArrayList<String> strProfessionArray = new ArrayList<>();
    public String profession_id = "", profession_name = "";
    public BusinessNature businessNature;
    public ArrayList<BusinessNature> businessNatureArray = new ArrayList<>();
    public ArrayList<String> strBusinessNatureArray = new ArrayList<>();
    public String business_nature_id = "";
    public ImageView img_my_job, img_filter;
    public Button btn_filter;
    public LinearLayout lin_empty;
    public RecyclerView rv_member_list;
    public BusinessList businessList;
    public ArrayList<BusinessList> businessLists = new ArrayList<>();


    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_search);
        instance = BusinessSearchActivity.this;
        jParser = new JsonParserUniversal();
        toolbar = findViewById(R.id.toolbar);
        tv_message = findViewById(R.id.tv_message);
        tv_count = findViewById(R.id.tv_count);
        img_profile = findViewById(R.id.img_profile);
        img_filter = findViewById(R.id.img_filter);
        tv_title = toolbar.findViewById(R.id.tv_title);
        lin_empty = findViewById(R.id.lin_empty);
        rv_member_list = findViewById(R.id.rv_member_list);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*startActivity(new Intent(instance, DashBoardActivity.class));
                finish();*/
                onBackPressed();
            }
        });
        tv_title.setText("BUSINESS SEARCH");
        Utils.setRoundedImage(instance,user.getPhotoURL(),300,300,img_profile,null);
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });

        openFilter();
        img_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFilter();
            }
        });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(instance, DashBoardActivity.class));
            finish();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void openFilter() {
        dialog = new Dialog(instance);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialog.setContentView(R.layout.dialog_business_search);
        final EditText et_first_name = dialog.findViewById(R.id.et_first_name);
        sp_profession = dialog.findViewById(R.id.sp_profession);
        sp_busi_nature = dialog.findViewById(R.id.sp_busi_nature);
        btn_filter = dialog.findViewById(R.id.btn_filter);
        profession = new Profession();
        businessNature = new BusinessNature();
        new CallRequest(instance).getProfession();
        new CallRequest(instance).getBusinessNature();
        sp_busi_nature.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        business_nature_id = String.valueOf(businessNatureArray.get(position).getIndustryID());

                    } else {
                        business_nature_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_profession.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        profession_id = String.valueOf(professionArray.get(position).getProfessionID());
                        profession_name = String.valueOf(professionArray.get(position).getProfessionID());

                    } else {
                        profession_id = "";
                        profession_name = "";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });

        ImageView imgClose = dialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });

        btn_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CallRequest(instance).getBusinesssearch(user,et_first_name.getText().toString(), business_nature_id, profession_id);

            }
        });
        dialog.show();

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case getProfession:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strProfessionArray.clear();
                            professionArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                profession.setProfessionName("Profession");
                                profession.setProfessionID("");
                                strProfessionArray.add(profession.getProfessionName());
                                professionArray.add(profession);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        profession = (Profession) jParser.parseJson(jSurnameArray.getJSONObject(i), new Profession());
                                        professionArray.add(profession);
                                        strProfessionArray.add(profession.getProfessionName());
                                    }
                                    sp_profession.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strProfessionArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getBusinessNature:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strBusinessNatureArray.clear();
                            businessNatureArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                businessNature.setIndustryName(getResources().getString(R.string.business));
                                businessNature.setIndustryID(0);
                                strBusinessNatureArray.add(businessNature.getIndustryName());
                                businessNatureArray.add(businessNature);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        businessNature = (BusinessNature) jParser.parseJson(jSurnameArray.getJSONObject(i), new BusinessNature());
                                        businessNatureArray.add(businessNature);
                                        strBusinessNatureArray.add(businessNature.getIndustryName());
                                    }
                                    sp_busi_nature.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strBusinessNatureArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getBusinesssearch:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            dialog.dismiss();
                            businessLists.clear();
                            if (jObj.getJSONArray("Businessdata") != null && jObj.getJSONArray("Businessdata").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("Businessdata");

                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    lin_empty.setVisibility(View.GONE);
                                    rv_member_list.setVisibility(View.VISIBLE);
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        businessList = (BusinessList) jParser.parseJson(jSurnameArray.getJSONObject(i), new BusinessList());
                                        businessLists.add(businessList);
                                    }
                                    tv_count.setText(businessLists.size() + " - Businesses Found");

                                    setUpRecycleview();
                                } else {
                                    tv_count.setText("0 - Businesses Found");

                                    tv_message.setText(jObj.getString("UMessage"));
                                    //   Utils.showToast(jObj.getString("UMessage"), this);
                                    lin_empty.setVisibility(View.VISIBLE);
                                    rv_member_list.setVisibility(View.GONE);

                                    Utils.showToast(jObj.getString("UMessage"), instance);
                                }
                            }
                        } else {
                            tv_count.setText("0 - Businesses Found");
                            tv_message.setText(jObj.getString("UMessage"));
                            //   Utils.showToast(jObj.getString("UMessage"), this);
                            lin_empty.setVisibility(View.VISIBLE);
                            rv_member_list.setVisibility(View.GONE);
                            Utils.showToast(jObj.getString("UMessage"), instance);
                        }

                    } catch (JSONException e) {
                        tv_count.setText("0 - Businesses Found");
                        lin_empty.setVisibility(View.VISIBLE);
                        rv_member_list.setVisibility(View.GONE);
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

    private void setUpRecycleview() {
        BusinessAdapter memberSearchAdapter = new BusinessAdapter(businessLists, instance);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);
        rv_member_list.setLayoutAnimation(controller);
        rv_member_list.scheduleLayoutAnimation();
        rv_member_list.setLayoutManager(new LinearLayoutManager(instance, LinearLayoutManager.VERTICAL, false));
        rv_member_list.setItemAnimator(new DefaultItemAnimator());
        rv_member_list.setAdapter(memberSearchAdapter);
    }
}