package com.skgjst.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.skgjst.App;
import com.skgjst.BaseActivity;
import com.skgjst.BuildConfig;
import com.skgjst.R;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.MainUser;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends BaseActivity implements AsynchTaskListner {

    public LinearLayout lin_otp, lin_userid;
    public TextView tvSignUp, tvForgotPassword, tv_userid, tv_otp;
    public RelativeLayout rlLogin;
    public FloatingActionButton btn_login;
    public EditText et_email, et_password, et_mobile;
    public JsonParserUniversal jParser;
    public MainUser mainUser;
    public LoginActivity instance;
    public boolean isOtp = false;
    public String PleyaerID = "";
    String[] PERMISSIONS = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    int PERMISSION_ALL = 1;
    TelephonyManager telephonyManager;
    public String imeiNumber = "";

    @SuppressLint("MissingPermission")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        jParser = new JsonParserUniversal();
        instance = LoginActivity.this;
        if (!Utils.hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

        lin_otp = findViewById(R.id.lin_otp);
        lin_userid = findViewById(R.id.lin_userid);
        tv_userid = findViewById(R.id.tv_userid);
        tv_otp = findViewById(R.id.tv_otp);
        tvSignUp = findViewById(R.id.tvSignUp);
        tvForgotPassword = findViewById(R.id.tv_Forgot_Password);
        et_email = findViewById(R.id.et_email);
        et_password = findViewById(R.id.et_password);
        et_mobile = findViewById(R.id.et_mobile);
        btn_login = findViewById(R.id.btn_login);
        tv_otp.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
                lin_otp.setVisibility(View.VISIBLE);
                lin_userid.setVisibility(View.GONE);
                tv_otp.setBackground(getResources().getDrawable(R.drawable.background_red));
                tv_userid.setBackground(getResources().getDrawable(R.drawable.background_white));
                tv_otp.setTextColor(getResources().getColor(R.color.white));
                tv_userid.setTextColor(getResources().getColor(R.color.colorPrimary));
                tv_otp.setTag("OTP");
                tvForgotPassword.setVisibility(View.INVISIBLE);
                isOtp = true;
            }
        });
        tv_userid.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
                lin_otp.setVisibility(View.GONE);
                lin_userid.setVisibility(View.VISIBLE);
                tv_userid.setBackground(getResources().getDrawable(R.drawable.background_red));
                tv_otp.setBackground(getResources().getDrawable(R.drawable.background_white));
                tv_userid.setTextColor(getResources().getColor(R.color.white));
                tv_otp.setTextColor(getResources().getColor(R.color.colorPrimary));
                tvForgotPassword.setVisibility(View.VISIBLE);
                isOtp = false;
            }
        });
        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(instance, ForgotPasswordActivity.class);
                startActivity(i);
            }
        });
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        imeiNumber = telephonyManager.getDeviceId();

//        btn_login.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (isOtp) {
//                    getOtpValidation();
//                } else {
//                    getLoginValidation();
//                }
//
//            }
//        });
       /* tvSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);

            }
        });*/

        if (BuildConfig.DEBUG)
            et_mobile.setText("7208893941");

    }

    public void clickLogin(View v) {
        if (isOtp) {
            getOtpValidation();
        } else {
            getLoginValidation();
        }
    }

    public void clickSignup(View v) {
        Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(i);

    }

    @Override
    protected void onResume() {
        super.onResume();
        App.PleyaerID = Utils.OneSignalPlearID();


    }

    @Override
    protected void onStart() {
        super.onStart();
        App.PleyaerID = Utils.OneSignalPlearID();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        finishAffinity();
    }

    private void getLoginValidation() {
        App.PleyaerID = Utils.OneSignalPlearID();
        if (et_email.getText().toString().isEmpty()) {
            et_email.setFocusable(true);
            et_email.setError("Please enter user id");
        } else if (et_password.getText().toString().isEmpty()) {
            et_password.setFocusable(true);
            et_password.setError("Please enter password");
        } else if (et_password.getText().toString().length() < 6) {
            et_password.setFocusable(true);
            et_password.setError("Please enter the password of atlest 6 characters");
        } else {
            if (Utils.isNetworkAvailable(instance)) {
              /*  if (App.PleyaerID.equalsIgnoreCase("")) {
                    App.PleyaerID = Utils.OneSignalPlearID();
                    Utils.showProgressDialog(instance);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Utils.removeWorkingDialog(instance);
                        }
                    }, 8000);
                } else {*/
                new CallRequest(LoginActivity.this).getLogin(et_email.getText().toString(), et_password.getText().toString(), App.PleyaerID);
                //     }
            }
        }
    }

    private void getOtpValidation() {
        App.PleyaerID = Utils.OneSignalPlearID();
        if (et_mobile.getText().toString().isEmpty()) {
            et_mobile.setError("Please enter your registed mobile number");
            et_mobile.setFocusable(true);
        } else {
              /*  if (App.PleyaerID.equalsIgnoreCase("")) {
                    Utils.showProgressDialog(instance);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Utils.removeWorkingDialog(instance);
                            App.PleyaerID = Utils.OneSignalPlearID();

                        }
                    }, 8000);
                    return;
                } else {*/
            new CallRequest(LoginActivity.this).generateOtp(et_mobile.getText().toString());
            // }

        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();

                switch (request) {

                    case getLogin:
                        Utils.hideProgressDialog();
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("Details") != null && jObj.getJSONArray("Details").length() > 0) {
                                Utils.showToast(jObj.getString("UMessage"), this);
                                JSONArray jData = jObj.getJSONArray("Details");
                                for (int i = 0; i < jData.length(); i++) {
                                    JSONObject jUser = jData.getJSONObject(i);
                                    mainUser = (MainUser) jParser.parseJson(jUser, new MainUser());
                                }
                                if (telephonyManager != null) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                        mySharedPref.setImeiNumber(telephonyManager.getImei());
                                    } else {
                                        mySharedPref.setImeiNumber(telephonyManager.getDeviceId());
                                    }
                                }


                                if (TextUtils.isEmpty(mainUser.getContactNo())) {
                                    Intent intent1 = new Intent(instance, EnterMobileActivity.class);
                                    startActivity(intent1);
                                    finish();

                                } else {
                                    if (Utils.isNetworkAvailable(instance)) {
                                        //  if (App.PleyaerID.equalsIgnoreCase("")) {
                                        App.PleyaerID = Utils.OneSignalPlearID();
                                        //  Utils.showProgressDialog(instance);
                                          /*  new Handler().postDelayed(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Utils.removeWorkingDialog(instance);
                                                }
                                            }, 8000);
                                        } else {*/
                                        new CallRequest(LoginActivity.this).generateOtp(user.getContactNo());
                                        //    }
                                    } else {
                                        Utils.showAlert("No Internet, Please try again later", LoginActivity.this);

                                    }
                                }
                                //

                            } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("UMessage"), this);
                            }


                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }

                        break;


                    case generateOtp:
                        Utils.hideProgressDialog();
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {

                            if (jObj.getString("OTP").isEmpty() && jObj.getString("OTP").equalsIgnoreCase("")) {
                                Utils.showToast(jObj.getString("UMessage"), this);

                            } else {
                                if (isOtp) {
                                    if (telephonyManager != null) {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                            mySharedPref.setImeiNumber(telephonyManager.getImei());
                                        } else {
                                            mySharedPref.setImeiNumber(telephonyManager.getDeviceId());
                                        }
                                    }

                                    Intent intent1 = new Intent(instance, VerificationMobileActivity.class);
                                    intent1.putExtra("PleyaerID", App.PleyaerID);
                                    intent1.putExtra("OTP", jObj.getString("OTP"));
                                    intent1.putExtra("Mobile", et_mobile.getText().toString());
                                    intent1.putExtra("StaticOTP", jObj.getString("StaticOTP"));
                                    intent1.putExtra("FamilyID", jObj.getString("FamilyID"));
                                    intent1.putExtra("FamilyDetailID", jObj.getString("FamilyDetailID"));
                                    startActivity(intent1);
                                    finish();

                                } else {
                                    Intent intent1 = new Intent(instance, VerifyOtpActivity.class);
                                    intent1.putExtra("PleyaerID", App.PleyaerID);
                                    intent1.putExtra("OTP", jObj.getString("OTP"));
                                    intent1.putExtra("Mobile", user.getContactNo());
                                    intent1.putExtra("StaticOTP", jObj.getString("StaticOTP"));
                                    intent1.putExtra("FamilyID", jObj.getString("FamilyID"));
                                    intent1.putExtra("FamilyDetailID", jObj.getString("FamilyDetailID"));
                                    startActivity(intent1);

                                }
                            }

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }

                        break;
                }
            } catch (JSONException e) {
                Utils.hideProgressDialog();
                Utils.showToast("Something getting wrong! Please try again later.", instance);
                e.printStackTrace();
            }
        }
    }
}
