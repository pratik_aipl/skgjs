package com.skgjst.activities.demise;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.R;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.fonts.MyCustomTypeface;
import com.skgjst.model.DemiseDataNew;
import com.skgjst.model.JobList;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.PicassoTrustAll;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class DemiseEditActivity extends BaseActivity implements AsynchTaskListner {
    final Calendar myCalendar = Calendar.getInstance();
    public DemiseEditActivity instance;
    public Toolbar toolbar;
    public JsonParserUniversal jParser;
    public TextView tv_title, et_demise_date, et_pd_date;
    public JobList obj;
    public Button btn_browse, btn_submit;
    public String SelectedResume = "", Filename = "";
    public EditText et_full_name, et_fathe_name, et_g_fathe_name, et_surname, et_native_place, et_current_place, et_prathna_time,
            et_contact_person, et_contact_number, et_info, et_pd_place, et_age;
    public int REQUEST_CODE_OPEN = 111;
    public ArrayList<String> strCityArray = new ArrayList<>();
    public String LocationName = "";
    public Spinner sp_pd_type;
    public CircleImageView img_profile, img_profile_local;
    public String logoPath = "", filename = "", surname_name = "", Selected_pd_type = "D";
    Uri selectedUri;
    public DemiseDataNew demiseObj;
    Uri uri;
    public TextView tv_fun_time, tv_fun_place, tv_fun_date, tv_posted, tv_age;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demise_edit);

        instance = this;
        jParser = new JsonParserUniversal();
        toolbar = findViewById(R.id.toolbar);
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_profile_local = findViewById(R.id.img_profile_local);
        et_full_name = findViewById(R.id.et_full_name);
        et_fathe_name = findViewById(R.id.et_fathe_name);
        et_g_fathe_name = findViewById(R.id.et_g_fathe_name);
        et_surname = findViewById(R.id.et_surname);
        et_native_place = findViewById(R.id.et_native_place);
        et_current_place = findViewById(R.id.et_current_place);
        et_contact_person = findViewById(R.id.et_contact_person);
        et_contact_number = findViewById(R.id.et_contact_number);
        et_info = findViewById(R.id.et_info);
        et_pd_date = findViewById(R.id.et_pd_date);
        et_pd_place = findViewById(R.id.et_pd_place);
        // et_pd_time = findViewById(R.id.et_pd_time);
        et_age = findViewById(R.id.et_age);
        et_demise_date = findViewById(R.id.et_demise_date);
        sp_pd_type = findViewById(R.id.sp_pd_type);
        tv_fun_time = findViewById(R.id.tv_fun_time);
        tv_fun_place = findViewById(R.id.tv_fun_place);
        tv_fun_date = findViewById(R.id.tv_fun_date);
        et_prathna_time = findViewById(R.id.et_prathna_time);

        tv_fun_date.setText("Funeral Date : ");
        tv_fun_place.setText("Funeral Place : ");
        tv_fun_time.setText("Funeral Time : ");

        img_profile_local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });

        btn_submit = findViewById(R.id.btn_submit);
        tv_title = toolbar.findViewById(R.id.tv_title);
        tv_title.setText("EDIT DEMISE");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        Utils.setRoundedImage(instance, user.getPhotoURL(), 300, 300, img_profile, null);
        Intent intent = getIntent();


        if (intent.hasExtra("obj")) {
            demiseObj = (DemiseDataNew) getIntent().getExtras().getSerializable("obj");
            //  btn_submit.setText("ADD");
            SetData();
        }

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });

        et_pd_date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                    // TODO Auto-generated method stub
                                    myCalendar.set(Calendar.YEAR, year);
                                    myCalendar.set(Calendar.MONTH, monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                    String myFormat = "dd MMM yyyy"; //In which you need put here
                                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                    et_pd_date.setText(sdf.format(myCalendar.getTime()));

                                }
                            };
                            DatePickerDialog datePickerDialog = new DatePickerDialog(instance, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                            myCalendar.set(Calendar.HOUR_OF_DAY, myCalendar.getMinimum(Calendar.HOUR_OF_DAY));
                            myCalendar.set(Calendar.MINUTE, myCalendar.getMinimum(Calendar.MINUTE));
                            myCalendar.set(Calendar.SECOND, myCalendar.getMinimum(Calendar.SECOND));
                            myCalendar.set(Calendar.MILLISECOND, myCalendar.getMinimum(Calendar.MILLISECOND));
                            Calendar temp = Calendar.getInstance();
                            datePickerDialog.getDatePicker().setMinDate(temp.getTimeInMillis());
                            datePickerDialog.show();

                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return true;
            }
        });
        sp_pd_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        et_pd_date.setHint(R.string.funeral_date);
                        et_pd_place.setHint(R.string.funeral_place);
                        et_prathna_time.setHint(R.string.funeral_time);
                        Selected_pd_type = "D";


                    } else {
                        // new CallRequest(instance).getCity(states_id);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        et_demise_date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                    // TODO Auto-generated method stub
                                    myCalendar.set(Calendar.YEAR, year);
                                    myCalendar.set(Calendar.MONTH, monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                    String myFormat = "dd MMM yyyy"; //In which you need put here
                                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                    et_demise_date.setText(sdf.format(myCalendar.getTime()));

                                }
                            };
                            DatePickerDialog datePickerDialog = new DatePickerDialog(instance, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                            myCalendar.set(Calendar.HOUR_OF_DAY, myCalendar.getMinimum(Calendar.HOUR_OF_DAY));
                            myCalendar.set(Calendar.MINUTE, myCalendar.getMinimum(Calendar.MINUTE));
                            myCalendar.set(Calendar.SECOND, myCalendar.getMinimum(Calendar.SECOND));
                            myCalendar.set(Calendar.MILLISECOND, myCalendar.getMinimum(Calendar.MILLISECOND));
                            Calendar temp = Calendar.getInstance();
                            datePickerDialog.getDatePicker().setMaxDate(temp.getTimeInMillis());
                            datePickerDialog.show();

                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return true;
            }
        });
    /*    et_pd_time.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            final Calendar c = Calendar.getInstance();
                            int mHour = c.get(Calendar.HOUR_OF_DAY);
                            int mMinute = c.get(Calendar.MINUTE);

                            // Launch Time Picker Dialog
                            TimePickerDialog timePickerDialog = new TimePickerDialog(instance,
                                    new TimePickerDialog.OnTimeSetListener() {

                                        @Override
                                        public void onTimeSet(TimePicker view, int hourOfDay,
                                                              int minute) {
                                            int hour = hourOfDay;
                                            int minutes = minute;
                                            String timeSet = "";
                                            if (hour > 12) {
                                                hour -= 12;
                                                timeSet = "PM";
                                            } else if (hour == 0) {
                                                hour += 12;
                                                timeSet = "AM";
                                            } else if (hour == 12) {
                                                timeSet = "PM";
                                            } else {
                                                timeSet = "AM";
                                            }

                                            String min = "";
                                            if (minutes < 10)
                                                min = "0" + minutes;
                                            else
                                                min = String.valueOf(minutes);

                                            // Append in a StringBuilder
                                            String aTime = new StringBuilder().append(hour).append(':')
                                                    .append(min).append(" ").append(timeSet).toString();
                                            et_pd_time.setText(aTime);
                                            //   et_pd_time.setText(hourOfDay + ":" + minute);
                                        }
                                    }, mHour, mMinute, false);
                            timePickerDialog.show();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return true;
            }
        });*/

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_full_name.getText().toString().equals("")) {
                    et_full_name.setError("Please enter full name");
                    et_full_name.setFocusable(true);
                } else if (et_fathe_name.getText().toString().equals("")) {
                    et_fathe_name.setError("Please enter father name");
                    et_fathe_name.setFocusable(true);
                } else if (et_surname.getText().toString().equals("")) {
                    et_surname.setError("Please enter surname");
                    et_surname.setFocusable(true);
                } else if (et_native_place.getText().toString().equals("")) {
                    et_native_place.setError("Please enter native place");
                    et_native_place.setFocusable(true);
                } else if (et_current_place.getText().toString().equals("")) {
                    et_current_place.setError("Please enter current place");
                    et_current_place.setFocusable(true);
                } else if (et_demise_date.getText().toString().equals("")) {
                    et_demise_date.setError("Please enter demise date ");
                    et_demise_date.setFocusable(true);
                } else if (Selected_pd_type.toString().equals("")) {
                    Utils.showToast("Please Select Demise or Prathna", instance);
                } else if (et_pd_date.toString().equals("")) {
                    et_pd_date.setError("Please enter funeral date");
                    et_pd_date.setFocusable(true);

                } else if (et_pd_place.toString().equals("")) {
                    et_pd_place.setError("Please enter funeral place");
                    et_pd_place.setFocusable(true);


                } else if (et_prathna_time.toString().equals("")) {
                    et_prathna_time.setError("Please enter funeral time");
                    et_prathna_time.setFocusable(true);

                } else {


                    new CallRequest(instance).EditPrathnaDemise(user, demiseObj.getDemisePrathnaID().toString(), et_full_name.getText().toString(),
                            et_fathe_name.getText().toString(), et_g_fathe_name.getText().toString(),
                            et_surname.getText().toString(), et_native_place.getText().toString(),
                            et_current_place.getText().toString(), et_demise_date.getText().toString(),
                            logoPath, filename, et_contact_person.getText().toString(), et_contact_number.getText().toString(),
                            et_info.getText().toString(), Selected_pd_type, et_pd_date.getText().toString(),
                            et_pd_place.getText().toString(), et_prathna_time.getText().toString(), String.valueOf(user.getFamilyDetailID()), et_age.getText().toString());
                }
            }
        });
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {

                case EditPrathnaDemise:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success") == true) {
                            Utils.showToast(jObj.getString("UMessage"), instance);
                            onBackPressed();
                        } else {
                            Utils.showToast(jObj.getString("UMessage"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

    public void selectImage() {
        CropImage.startPickImageActivity(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                selectedUri = imageUri;
                logoPath = selectedUri.getPath().toString();
                filename = (logoPath.substring(logoPath.lastIndexOf('/') + 1));

                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    img_profile_local.setImageURI(result.getUri());
                    selectedUri = result.getUri();
                    final InputStream imageStream = getContentResolver().openInputStream(selectedUri);
                    logoPath = selectedUri.getPath().toString();
                    filename = (logoPath.substring(logoPath.lastIndexOf('/') + 1));
                    Log.i("TAG", "logoPath :-> " + logoPath);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }

    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(true)

                .setMultiTouchEnabled(true)
                .start(this);
    }

    public void SetData() {
        et_full_name.setText(demiseObj.getName());
        et_fathe_name.setText(demiseObj.getFatherName());
        et_g_fathe_name.setText(demiseObj.getGrandFatherName());
        et_surname.setText(demiseObj.getSurname());
        et_native_place.setText(demiseObj.getNativePlace());
        et_current_place.setText(demiseObj.getCurrenPlace());
        et_demise_date.setText(demiseObj.getDemiseDate());
        et_contact_person.setText(demiseObj.getContactPerson());
        et_contact_number.setText(demiseObj.getContactNumber());
        et_info.setText(demiseObj.getOtherInfo());
        et_age.setText(demiseObj.getAge());

        Selected_pd_type = "D";
        sp_pd_type.setSelection(1);
        sp_pd_type.setEnabled(false);
        sp_pd_type.setClickable(false);


        et_pd_date.setText(demiseObj.getPDDate());
        et_pd_place.setText(demiseObj.getPDPlace());
        et_prathna_time.setText(demiseObj.getPDTime());

        if (demiseObj.getImageURL() != null) {
            try {
                PicassoTrustAll.getInstance(instance)
                        .load(demiseObj.getImageURL().replace(" ", "%20"))
                        .error(R.drawable.avatar)
                        .resize(256, 256)
                        .into(img_profile_local, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
