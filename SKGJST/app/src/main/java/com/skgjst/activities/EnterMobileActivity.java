package com.skgjst.activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.skgjst.App;
import com.skgjst.BaseActivity;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.MainUser;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class EnterMobileActivity  extends BaseActivity implements AsynchTaskListner {
    public Toolbar toolbar;
    public TextView tv_title;
    public EnterMobileActivity instance;
    public Button btn_submit;
    public EditText et_mobile;
    public JsonParserUniversal jParser;
    public MainUser mainUser;
    public CircleImageView img_profile;
    public String PleyaerID = "";

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_mobile);
        instance = EnterMobileActivity.this;
        jParser = new JsonParserUniversal();
        toolbar = findViewById(R.id.toolbar);
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_profile.setVisibility(View.GONE);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("2 STEP VERIFICATION");
        btn_submit = findViewById(R.id.btn_submit);
        et_mobile = findViewById(R.id.et_mobile);

        App.PleyaerID = getIntent().getStringExtra("PleyaerID");

        //   et_otp.setText(OTP);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_mobile.getText().toString().isEmpty()) {
                    et_mobile.setError("Please enter your mobile number");
                    et_mobile.setFocusable(true);
                } else {
                    if (Utils.isNetworkAvailable(instance)) {
                        new CallRequest(instance).Validatenumber(et_mobile.getText().toString(), String.valueOf(user.getFamilyDetailID()));

                    } else {
                        Utils.showAlert("No Internet, Please try again later", instance);

                    }
                }
            }
        });
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent1 = new Intent(instance, LoginActivity.class);
        startActivity(intent1);
        finish();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case Validatenumber:
                    Utils.hideProgressDialog();
                    JSONObject jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            if (jObj.getString("Number") != null && !jObj.getString("Number").equals("") ) {
                                new CallRequest(instance).generateOtp(et_mobile.getText().toString());
                            } else {
                                Utils.showToast(jObj.getString("UMessage"), this);

                            }

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case generateOtp:
                    Utils.hideProgressDialog();
                    jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {

                            if (jObj.getString("OTP").isEmpty() && jObj.getString("OTP").equalsIgnoreCase("")) {
                                Utils.showToast(jObj.getString("UMessage"), this);

                            } else {
                                Intent intent1 = new Intent(instance, VerifyOtpActivity.class);
                                intent1.putExtra("PleyaerID", App.PleyaerID);
                                intent1.putExtra("OTP", jObj.getString("OTP"));
                                intent1.putExtra("StaticOTP", jObj.getString("StaticOTP"));
                                intent1.putExtra("FamilyID", jObj.getString("FamilyID"));
                                intent1.putExtra("FamilyDetailID", jObj.getString("FamilyDetailID"));
                                intent1.putExtra("Mobile", et_mobile.getText().toString());
                                startActivity(intent1);

                            }

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }
}

