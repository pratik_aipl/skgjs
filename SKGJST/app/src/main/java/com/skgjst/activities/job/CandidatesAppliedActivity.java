package com.skgjst.activities.job;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.adapter.CandidatesListAdapter;
import com.skgjst.App;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.Candidates;
import com.skgjst.model.JobList;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class CandidatesAppliedActivity  extends BaseActivity implements AsynchTaskListner {
    public CandidatesAppliedActivity instance;
    public Toolbar toolbar;
    public LinearLayout lin_empty;
    public RecyclerView rv_job_list;
    public JsonParserUniversal jParser;
    public TextView tv_title, tv_message, tv_count;
    public Candidates candidatesObj;
    public ArrayList<Candidates> jobArrayList = new ArrayList<>();
    public FloatingActionButton float_add;
    public CandidatesListAdapter adapter;
    public ImageView img_my_job, img_filter;
    public JobList obj;
    public CircleImageView img_profile;

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_list);

        instance = this;
        jParser = new JsonParserUniversal();
        Intent intent = getIntent();

        float_add = findViewById(R.id.float_add);
        rv_job_list = findViewById(R.id.rv_job_list);
        toolbar = findViewById(R.id.toolbar);
        img_profile = toolbar.findViewById(R.id.img_profile);


        tv_title = toolbar.findViewById(R.id.tv_title);
        tv_count = findViewById(R.id.tv_count);
        tv_message = findViewById(R.id.tv_message);
        float_add = findViewById(R.id.float_add);
        lin_empty = findViewById(R.id.lin_empty);
        img_my_job = findViewById(R.id.img_my_job);
        img_filter = findViewById(R.id.img_filter);
        img_my_job.setVisibility(View.GONE);
        img_filter.setVisibility(View.GONE);
        float_add.setVisibility(View.GONE);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("CANDIDATES LIST");
        if (intent.hasExtra("obj")) {
            obj = (JobList) getIntent().getExtras().getSerializable("obj");

        }
        Utils.setRoundedImage(instance,user.getPhotoURL(),300,300,img_profile,null);

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });


        new CallRequest(instance).getCandidateslist(obj.getJobid());

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();

                switch (request) {

                    case getCandidateslist:
                        Utils.hideProgressDialog();
                        tv_title.setText("CANDIDATES LIST");
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            JSONArray jarray = jObj.getJSONArray("Candidates");
                            tv_title.setText("CANDIDATES LIST");
                            if (jarray.length() > 0 && jarray != null) {
                                lin_empty.setVisibility(View.GONE);
                                rv_job_list.setVisibility(View.VISIBLE);
                                for (int i = 0; i < jarray.length(); i++) {
                                    JSONObject jData = jarray.getJSONObject(i);
                                    candidatesObj = (Candidates) jParser.parseJson(jData, new Candidates());
                                    jobArrayList.add(candidatesObj);
                                }
                                candidatesObj = new Candidates();
                                jobArrayList.add(candidatesObj);
                                tv_count.setText(jobArrayList.size() - 1 + " - Candidates Applied");
                                SetupRecylerView();
                            } else {
                                tv_count.setText("0 - Candidates Applied");

                            }
                        } else {
                            tv_count.setText("0 - Candidates Applied");
                            Utils.hideProgressDialog();
                            tv_message.setText(jObj.getString("UMessage"));
                            //   Utils.showToast(jObj.getString("UMessage"), this);
                            lin_empty.setVisibility(View.VISIBLE);
                            rv_job_list.setVisibility(View.GONE);
                        }
                        break;
                }
            } catch (JSONException e) {
                tv_count.setText("0 - Candidates Applied");
                tv_title.setText("CANDIDATES LIST");
                lin_empty.setVisibility(View.VISIBLE);
                rv_job_list.setVisibility(View.GONE);
                Utils.hideProgressDialog();
                Utils.showToast("Server side error...", this);
                e.printStackTrace();
            }
        } else {
            tv_count.setText("0 - Candidates Applied");
            tv_title.setText("CANDIDATES LIST");
            lin_empty.setVisibility(View.VISIBLE);
            rv_job_list.setVisibility(View.GONE);
            Utils.hideProgressDialog();

        }
    }

    public void SetupRecylerView() {

        adapter = new CandidatesListAdapter(jobArrayList, instance);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);
        rv_job_list.setLayoutAnimation(controller);
        rv_job_list.scheduleLayoutAnimation();
        rv_job_list.setLayoutManager(new LinearLayoutManager(instance, LinearLayoutManager.VERTICAL, false));
        rv_job_list.setItemAnimator(new DefaultItemAnimator());
        rv_job_list.setAdapter(adapter);
    }
}

