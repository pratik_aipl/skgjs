package com.skgjst.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.skgjst.App;
import com.skgjst.BaseActivity;
import com.skgjst.fonts.MyCustomTypeface;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.BusinessNature;
import com.skgjst.model.Profession;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class RegisterThreeActivity  extends BaseActivity implements AsynchTaskListner {
    public Toolbar toolbar;
    public TextView tv_title;
    public RegisterThreeActivity instance;
    public Spinner sp_busi_nature, sp_profession;
    public Profession profession;
    public ArrayList<Profession> professionArray = new ArrayList<>();
    public ArrayList<String> strProfessionArray = new ArrayList<>();
    public String profession_id = "", profession_name = "";
    public BusinessNature businessNature;
    public ArrayList<BusinessNature> businessNatureArray = new ArrayList<>();
    public ArrayList<String> strBusinessNatureArray = new ArrayList<>();
    public String business_nature_id = "";
    public JsonParserUniversal jParser;
    public Button btn_submit;
    public EditText et_busi_detail, et_company_name, et_url, et_offi_no, et_offi_address, et_email;
    public CircleImageView img_profile;

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_three);
        instance = RegisterThreeActivity.this;
        jParser = new JsonParserUniversal();
        toolbar = findViewById(R.id.toolbar);
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_profile.setVisibility(View.GONE);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("REGISTER");
        Validation();
        profession = new Profession();
        businessNature = new BusinessNature();
    }

    private void Validation() {
        et_email = findViewById(R.id.et_email);
        et_busi_detail = findViewById(R.id.et_busi_detail);
        et_company_name = findViewById(R.id.et_company_name);
        et_url = findViewById(R.id.et_url);
        et_offi_no = findViewById(R.id.et_offi_no);
        et_offi_address = findViewById(R.id.et_offi_address);
        btn_submit = findViewById(R.id.btn_submit);
        sp_busi_nature = findViewById(R.id.sp_busi_nature);
        sp_profession = findViewById(R.id.sp_profession);
        new CallRequest(instance).getProfession();
        new CallRequest(instance).getBusinessNature();
        sp_busi_nature.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        business_nature_id = String.valueOf(businessNatureArray.get(position).getIndustryID());

                    } else {
                        business_nature_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_profession.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        profession_id = String.valueOf(professionArray.get(position).getProfessionID());
                        profession_name = String.valueOf(professionArray.get(position).getProfessionID());

                    } else {
                        profession_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (profession_id.equalsIgnoreCase("") || profession_id.equals("0")) {
                    Utils.showToast("Please select profession", instance);
                } else if (et_email.getText().toString().equals("") || !Utils.isValidEmail(et_email.getText().toString())) {
                    et_email.setError(" Please Enter Valid Email ID");
                    et_email.setFocusable(true);
                } else {
                    App.registerData.setEmailID(et_email.getText().toString());
                    App.registerData.setWorkprofession(profession_id);
                    App.registerData.setBusstype(business_nature_id);
                    App.registerData.setBussdetails(et_busi_detail.getText().toString());
                    App.registerData.setOfficename(et_company_name.getText().toString());
                    App.registerData.setWebsiteurl(et_url.getText().toString());
                    App.registerData.setOfficecontactNo(et_offi_no.getText().toString());
                    App.registerData.setOffaddress(et_offi_address.getText().toString());
                    new CallRequest(instance).getRegister(
                            App.registerData.getSurname(),
                            App.registerData.getFirstname(),
                            App.registerData.getMiddlename(),
                            App.registerData.getNativePlace(),
                            App.registerData.getPhotoUrl(),
                            App.registerData.getFilename(),
                            App.registerData.getGender(),
                            App.registerData.getDob(),
                            App.registerData.getBloodgroupid(),
                            App.registerData.getMaritalstatusid(),
                            App.registerData.getMarriagedate(),
                            App.registerData.getContactno(),
                            App.registerData.getEducation(),
                            App.registerData.getWorkprofession(),
                            App.registerData.getBusstype(),
                            App.registerData.getBussdetails(),
                            App.registerData.getOfficename(),
                            App.registerData.getOffaddress(),
                            App.registerData.getOfficecontactNo(),
                            App.registerData.getEmailID(),
                            App.registerData.getWebsiteurl(),
                            App.registerData.getWing(),
                            App.registerData.getRoomno(),
                            App.registerData.getPlotNo(),
                            App.registerData.getBuildingname(),
                            App.registerData.getRoadname(),
                            App.registerData.getLandmark(),
                            App.registerData.getSuburbname(),
                            App.registerData.getHomecontactno(),
                            App.registerData.getDistrictname(),
                            App.registerData.getCountryid(),
                            App.registerData.getStateid(),
                            App.registerData.getCityid(),
                            App.registerData.getPincode(),
                            App.registerData.getInternational(),
                            App.registerData.getCountrycode(),
                            App.registerData.getMothername(),
                            App.registerData.getGrandfathername(),
                            App.registerData.getGrandmothername(),
                            App.registerData.getMfathername(),
                            App.registerData.getMmothername(),
                            App.registerData.getMfathersurname(),
                            App.registerData.getMfathervillage(),
                            App.registerData.getFathersurname(),
                            App.registerData.getFathervillage(),
                            App.registerData.getSpousename(),
                            App.registerData.getSpousefathername(),
                            App.registerData.getSpousemothername(),
                            App.registerData.getWifefathersurname(),
                            App.registerData.getWifefathervillage(),
                            App.registerData.getFathername());
                }
            }
        });

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case getProfession:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strProfessionArray.clear();
                            professionArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                profession.setProfessionName(getResources().getString(R.string.profession));
                                profession.setProfessionID("0");
                                strProfessionArray.add(profession.getProfessionName());
                                professionArray.add(profession);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        profession = (Profession) jParser.parseJson(jSurnameArray.getJSONObject(i), new Profession());
                                        professionArray.add(profession);
                                        strProfessionArray.add(profession.getProfessionName());
                                    }
                                    sp_profession.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strProfessionArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getBusinessNature:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strBusinessNatureArray.clear();
                            businessNatureArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                businessNature.setIndustryName(getResources().getString(R.string.business));
                                businessNature.setIndustryID(0);
                                strBusinessNatureArray.add(businessNature.getIndustryName());
                                businessNatureArray.add(businessNature);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        businessNature = (BusinessNature) jParser.parseJson(jSurnameArray.getJSONObject(i), new BusinessNature());
                                        businessNatureArray.add(businessNature);
                                        strBusinessNatureArray.add(businessNature.getIndustryName());
                                    }
                                    sp_busi_nature.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strBusinessNatureArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getRegister:
                    Utils.hideProgressDialog();
                    try {
                        final JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            final AlertDialog.Builder builder1 = new AlertDialog.Builder(instance);
                            builder1.setMessage(jObj.getString("UMessage"));
                            builder1.setCancelable(true);

                            builder1.setPositiveButton(
                                    "Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            try {
                                                if (jObj.getString("FamilyDetailID") != null && !jObj.getString("FamilyDetailID").equals("")) {
                                                    startActivity(new Intent(instance, LoginActivity.class));
                                                    finish();
                                                    dialog.cancel();
                                                }else{
                                                    dialog.cancel();
                                                }
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                            Utils.showAlert(jObj.getString("UMessage"), this);

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }
}
