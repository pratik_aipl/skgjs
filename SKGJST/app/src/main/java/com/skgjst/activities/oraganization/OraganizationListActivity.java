package com.skgjst.activities.oraganization;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skgjst.App;
import com.skgjst.BaseActivity;
import com.skgjst.R;
import com.skgjst.activities.DashBoardActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.fragment.OrganizationList;
import com.skgjst.fragment.OrganizationsAdapter;
import com.skgjst.fragment.TrusteeDetails;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class OraganizationListActivity  extends BaseActivity implements AsynchTaskListner {
    public Toolbar toolbar;
    public TextView tv_title, tv_message;
    public CircleImageView img_profile, img_profile_local;
    public OraganizationListActivity instance;
    public JsonParserUniversal jParser;
    public OrganizationList organizationList;
    public ArrayList<OrganizationList> organizationLists = new ArrayList<>();
    public RecyclerView rv_org;
    public LinearLayout lin_empty;

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oraganization_list);
        instance = OraganizationListActivity.this;
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        jParser = new JsonParserUniversal();
        rv_org = findViewById(R.id.rv_org);
        lin_empty = findViewById(R.id.lin_empty);
        tv_message = findViewById(R.id.tv_message);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startActivity(new Intent(instance, DashBoardActivity.class));
                onBackPressed();
            }
        });
        tv_title.setText("ORAGANIZATION");
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_profile_local = toolbar.findViewById(R.id.img_profile_local);

        Utils.setRoundedImage(instance,user.getPhotoURL(),300,300,img_profile,null);

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });
        new CallRequest(instance).getOraganization();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(instance, DashBoardActivity.class));
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getOraganization:
                    Utils.hideProgressDialog();

                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            rv_org.setVisibility(View.VISIBLE);
                            lin_empty.setVisibility(View.GONE);
                            if (jObj.getJSONObject("data") != null && jObj.getJSONObject("data").length() > 0) {
                                JSONObject object = jObj.getJSONObject("data");
                                JSONObject jsonObject = object.getJSONObject("Organizations");
                                JSONArray array = jsonObject.getJSONArray("Organisation");

                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object1 = array.getJSONObject(i);
                                    OrganizationList organizationList = new OrganizationList();
                                    organizationList.setOrgName(object1.getString("OrgName"));
                                    organizationList.setOrgAddress(object1.getString("OrgAddress"));
                                    organizationList.setOrgCntPersonName(object1.getString("OrgCntPersonName"));
                                    organizationList.setOrgContctNo(object1.getString("OrgContctNo"));
                                    organizationList.setOrgContEmail(object1.getString("OrgContEmail"));
                                    organizationList.setOrgLogo(object1.getString("OrgLogo"));
                                    organizationList.setOrgDesc(object1.getString("OrgDesc"));
                                    organizationList.setWebsiteUrl(object1.getString("WebsiteUrl"));
                                    organizationList.setPlace(object1.getString("Place"));
                                    organizationList.setOrgContctNo2(object1.getString("OrgContctNo2"));

                                    ArrayList<TrusteeDetails> list = new ArrayList<>();
                                    if (object1.has("Trustee_Details")) {
                                        JSONArray jsonArray = object1.getJSONArray("Trustee_Details");
                                        for (int j = 0; j < jsonArray.length(); j++) {
                                            JSONObject object2 = jsonArray.getJSONObject(j);
                                            if (object2.length() > 0) {
                                                TrusteeDetails trusteeDetails = (TrusteeDetails) jParser.parseJson(object2, new TrusteeDetails());
                                                list.add(trusteeDetails);
                                            }
                                        }
                                    }

                                    organizationList.setTrustee_Details(list);

                                    organizationLists.add(organizationList);
                                }

                                setUpRecycleview();
                            } else {
                                tv_message.setText(jObj.getString("UMessage"));
                                //   Utils.showToast(jObj.getString("UMessage"), this);
                                lin_empty.setVisibility(View.VISIBLE);
                                rv_org.setVisibility(View.GONE);
                                Utils.showToast(jObj.getString("UMessage"), instance);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

    private void setUpRecycleview() {
        OrganizationsAdapter memberSearchAdapter = new OrganizationsAdapter(organizationLists, instance);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);
        rv_org.setLayoutAnimation(controller);
        rv_org.scheduleLayoutAnimation();
        rv_org.setLayoutManager(new LinearLayoutManager(instance, LinearLayoutManager.VERTICAL, false));
        rv_org.setItemAnimator(new DefaultItemAnimator());
        rv_org.setAdapter(memberSearchAdapter);
    }
}
