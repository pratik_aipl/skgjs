package com.skgjst.activities.matrimonial;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skgjst.model.MatrimonialDetail;
import com.skgjst.R;
import com.skgjst.utils.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by empiere-vaibhav on 10/5/2018.
 */


public class MyMatrimonialAdapter extends RecyclerView.Adapter<MyMatrimonialAdapter.MyViewHolder> {
    public static final int VIEW_TYPE_ITEM = 1;
    public static final int VIEW_TYPE_BLANK = 2;
    public Context context;
    private ArrayList<MatrimonialDetail> mArrayList;

    public MyMatrimonialAdapter(ArrayList<MatrimonialDetail> moviesList, Context context) {
        mArrayList = moviesList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_member_profile_item, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final MatrimonialDetail Obj = mArrayList.get(position);
        holder.tv_name.setText(Obj.getFullName());
        // holder.tvAge.setText(Obj.getDateOfBirth());
        //holder.tv_Marital_Status.setText(Obj.getMaritalStatus());
        if(Obj.getNativePlace().equalsIgnoreCase("")){
            holder.tvAge.setText("Native Place : Not available"+Obj.getNativePlace());

        }else{
            holder.tvAge.setText("Native Place : "+Obj.getNativePlace());

        }
        if(Obj.getNativePlace().equalsIgnoreCase("")) {

            holder.tv_Marital_Status.setText("N/A");
        }
        else{
            holder.tv_Marital_Status.setText(Obj.getCity());

        }

        holder.tv_status.setText(Obj.getMaritalStatus());

        if (Obj.getPhotourl() != null && !TextUtils.isEmpty(Obj.getPhotourl())) {
            try {
                PicassoTrustAll.getInstance(context)
                        .load(Obj.getPhotourl().replace(" ", "%20"))
                        .error(R.drawable.avatar)
                        .resize(256, 256)
                        .into(holder.img_profile, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        holder.imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, MatrimonialEditActivity.class)
                        .putExtra("obj", Obj)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                );
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, MatrimonialEditActivity.class)
                        .putExtra("obj", Obj)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                );
            }
        });

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name, tv_Marital_Status, tvAge,tv_status;
        public LinearLayout lin_info;
        public CircleImageView img_profile;
        public ImageButton tv_send;
        public ImageView imgEdit;

        public MyViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.tv_name);
            tv_Marital_Status = view.findViewById(R.id.tv_Marital_Status);
            tv_status = view.findViewById(R.id.tv_status);
            tvAge = view.findViewById(R.id.tvAge);
            img_profile = view.findViewById(R.id.img_profile);
            imgEdit = view.findViewById(R.id.imgEdit);
            tv_send
                    = view.findViewById(R.id.tv_send);
        }
    }

}







