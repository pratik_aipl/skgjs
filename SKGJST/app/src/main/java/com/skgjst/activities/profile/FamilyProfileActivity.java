package com.skgjst.activities.profile;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.skgjst.App;
import com.skgjst.BaseActivity;
import com.skgjst.R;
import com.skgjst.activities.matrimonial.MatriProfileList;
import com.skgjst.activities.membersearch.fragment.MemberContactFragment;
import com.skgjst.activities.membersearch.fragment.MemberFamilyFragment;
import com.skgjst.activities.membersearch.fragment.MemberProfileFragment;
import com.skgjst.activities.profile.fragment.AddFatherMotherActivity;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.fonts.LatoRegularTextView;
import com.skgjst.model.FamilyMembers;
import com.skgjst.model.Profile;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.CustPagerTransformer;
import com.skgjst.utils.ImagePopup;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class FamilyProfileActivity extends BaseActivity implements AsynchTaskListner {
    private static final String TAG = "FamilyProfileActivity";
    public static final String FRAGMENT_FIRST = "Family Member";
    public static final String FRAGMENT_SECOND = "Profile";
    public static final String FRAGMENT_THIRD = "Contact";
    public static FloatingActionButton float_add, float_tree;
    public static FamilyMembers memberData;
    //    public static FamilyMembers memberList;
    public Toolbar toolbar;
    public TextView tv_title, tv_name, tv_city;
    public FamilyProfileActivity instance;
    public ViewPager viewPager;
    public TabLayout tabLayout;
    public ProgressBar pbar;
    public CircleImageView img_profile, img_profile_local;
    public Profile obj;
    public JsonParserUniversal jParser;
    public MatriProfileList matriProfileList;
    ViewPagerAdapter adapter;
    public Dialog dialog;
    int MemberRelationID = 0;
    public ImageView img_active, img_tree;

    public LinearLayout lin_tree;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        jParser = new JsonParserUniversal();
        Intent intent = getIntent();


        if (intent.hasExtra("obj")) {
            memberData = (FamilyMembers) getIntent().getExtras().getSerializable("obj");
            App.memberFamilyDetailID = String.valueOf(memberData.getFamilyDetailID());
            App.memberfamilyId = String.valueOf(memberData.getFamilyID());
        } else if (intent.hasExtra("fmlid")) {
            //  memberData.setFamilyDetailID(String.valueOf(getIntent().getStringExtra("fmlid");
            App.memberFamilyDetailID = String.valueOf(getIntent().getStringExtra("fmlid"));
            App.memberfamilyId = String.valueOf(getIntent().getStringExtra("fmlID"));
        }

        instance = this;
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        pbar = findViewById(R.id.pbar);
        lin_tree = findViewById(R.id.lin_tree);
        img_profile_local = findViewById(R.id.img_profile_local);
        img_profile = findViewById(R.id.img_profile);
        img_profile.setVisibility(View.GONE);
        img_active = findViewById(R.id.img_active);
        tv_name = findViewById(R.id.tv_name);
        tv_city = findViewById(R.id.tv_city);
        img_tree = findViewById(R.id.img_tree);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("MEMBER PROFILE");
        viewPager = findViewById(R.id.viewPager);
        float_add = findViewById(R.id.float_add);
        // float_add.setVisibility(View.GONE);
        float_tree = findViewById(R.id.float_tree);
        tabLayout = findViewById(R.id.tabs);
        setupViewPager(viewPager);
        tv_name.setText(memberData.getFName() + " " + memberData.getMName() + " " + memberData.getSurName());
        tv_city.setText(memberData.getNativePlace());
        Log.i("FamilyDetailId", "==>" + memberData.getFamilyDetailID());
        Utils.setRoundedImage(instance, memberData.getPhotoURL(), 300, 300, img_profile_local, null);

        lin_tree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, FamilyTreeActivity.class).putExtra("FamilyDetailID",
                        App.memberFamilyDetailID));
            }
        });
        float_tree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title = "EditProfile";
                // startActivity(new Intent(instance, EditProfileActivity.class));
                startActivity(new Intent(instance, EditUserProfileActivity.class).
                        setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).
                        setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).
                        setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra("FamilyId", App.memberfamilyId + "")
                        .putExtra("FamilyDetailId", App.memberFamilyDetailID + ""));


            }
        });
        img_profile_local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openImage();
            }
        });
        float_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addMemberDailog();

            }
        });

        if (intent.hasExtra("obj")) {
            new CallRequest(instance).getMyprofile(user, memberData.getFamilyDetailID());

        } else if (intent.hasExtra("fmlid")) {
            new CallRequest(instance).getMyprofile(user, Integer.parseInt(App.memberFamilyDetailID));
        }


        // new CallRequest(instance).getMyprofilesMember(user, memberData.getFamilyDetailID());

        if (memberData.isAlive()) {
            if (memberData.isMIsActive() && !TextUtils.isEmpty(memberData.getPlayerID()))
                img_active.setImageResource(R.drawable.ic_active);
            else
                img_active.setVisibility(View.VISIBLE);
        } else {
            img_active.setImageResource(R.drawable.ic_dis_active);
        }
    }

    public void openImage() {
        final ImagePopup imagePopup = new ImagePopup(FamilyProfileActivity.this);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.BLACK);  // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(true);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional
        imagePopup.initiatePopupWithPicasso(memberData.getPhotoURL().replace(" ", "%20"));
        img_profile_local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /** Initiate Popup view **/
                if (imagePopup != null)
                    imagePopup.viewPopup();

            }
        });


    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new MemberFamilyFragment().newInstance(), "Family Member");
        adapter.addFragment(new MemberProfileFragment().newInstance(), "Profile");
        adapter.addFragment(new MemberContactFragment().newInstance(), "Contact");
        viewPager.setAdapter(adapter);
        viewPager.setPageTransformer(false, new CustPagerTransformer(this));

        tabLayout.setupWithViewPager(viewPager);
        setupTabFont();


    }

    public void PagechangListner() {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

            @Override
            public void onPageSelected(int position) {
                if (MemberRelationID == 0) {
                    float_add.setVisibility(View.GONE);
                    float_tree.setVisibility(View.GONE);
                } else {
                    if (position == 0) {
                        float_add.show();
                        float_tree.hide();
                    } else if (position == 1) {
                        float_add.hide();
                        float_tree.show();
                    } else if (position == 2) {
                        float_add.hide();
                        float_tree.hide();
                    }
                }
            }
        });
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Utils.hideProgressDialog();
            //    Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getMyprofiles:
                    obj = new Profile();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {

                            PagechangListner();
                            if (jObj.getJSONArray("Profile") != null && jObj.getJSONArray("Profile").length() > 0) {
                                JSONArray userData = jObj.getJSONArray("Profile");
                                for (int i = 0; i < userData.length(); i++) {
                                    JSONObject jobj = userData.getJSONObject(i);
                                    memberData = (FamilyMembers) jParser.parseJson(jobj, new FamilyMembers());
                                    obj = (Profile) jParser.parseJson(jobj, new Profile());
                                }
                                MemberRelationID = (int) jObj.get("MemberRelationID");
                                if (MemberRelationID == 0) {
                                    float_add.setVisibility(View.GONE);
                                } else {
                                    float_add.setVisibility(View.VISIBLE);
                                }

                                if (memberData.isAlive()) {
                                    if (memberData.isMIsActive() && !TextUtils.isEmpty(memberData.getPlayerID()))
                                        img_active.setImageResource(R.drawable.ic_active);
                                    else
                                        img_active.setVisibility(View.VISIBLE);
                                } else {
                                    img_active.setImageResource(R.drawable.ic_dis_active);
                                }
                                Utils.hideProgressDialog();
                            } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("UMessage"), instance);
                            }


                        } else {
                            PagechangListner();
                            Utils.hideProgressDialog();
                        }
                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        Utils.showToast("Something getting wrong! Please try again later.", instance);

                        e.printStackTrace();
                    }
            }
        }
    }

    private void setupTabFont() {
        LatoRegularTextView firsttab = (LatoRegularTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        firsttab.setText(FRAGMENT_FIRST);
        tabLayout.getTabAt(0).setCustomView(firsttab);

        LatoRegularTextView secondtab = (LatoRegularTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        secondtab.setText(FRAGMENT_SECOND);
        tabLayout.getTabAt(1).setCustomView(secondtab);

        LatoRegularTextView thirdtab = (LatoRegularTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        thirdtab.setText(FRAGMENT_THIRD);
        tabLayout.getTabAt(2).setCustomView(thirdtab);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // adapter.notifyDataSetChanged();
        //   new CallRequest(instance).getMyprofilesActiviti(memberData.getFamilyDetailID());
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    public void addMemberDailog() {
        //  new CallRequest(instance).getMyprofilesMember((memberList.getFamilyDetailID()));
        dialog = new Dialog(instance);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dailog_choise_member);
        dialog.show();
        TextView tv_name = dialog.findViewById(R.id.tv_name);
        Button btn_brother = dialog.findViewById(R.id.btn_brother);
        Button btn_sister = dialog.findViewById(R.id.btn_sister);
        Button btn_spouse = dialog.findViewById(R.id.btn_spouse);
        Button btn_son = dialog.findViewById(R.id.btn_son);
        Button btn_daughter = dialog.findViewById(R.id.btn_daughter);
        Button btn_addfm = dialog.findViewById(R.id.btn_addfm);
        tv_name.setText(memberData.getFName() + " " + memberData.getMName() + " " + memberData.getSurName());

        Log.d(TAG, "addMemberDailog: " + gson.toJson(obj));
        if (obj.getIsFatherProfile() > 0){
            if (obj.getIsFatherProfile() == 1 && obj.getIsMotherProfile() == 1) {
                btn_addfm.setVisibility(View.GONE);
            } else {
                btn_addfm.setVisibility(View.VISIBLE);
            }
        }


        if (!obj.getMaritalstatus().equalsIgnoreCase("UnMarried") && obj.getIsSpouseProfile() == 1) {
            btn_spouse.setVisibility(View.GONE);
            btn_son.setVisibility(View.VISIBLE);
            btn_daughter.setVisibility(View.VISIBLE);
        } else if (!obj.getMaritalstatus().equalsIgnoreCase("UnMarried") && obj.getIsSpouseProfile() == 0) {
            btn_spouse.setVisibility(View.VISIBLE);
            btn_son.setVisibility(View.VISIBLE);
            btn_daughter.setVisibility(View.VISIBLE);

        } else {
            btn_spouse.setVisibility(View.VISIBLE);
            btn_son.setVisibility(View.GONE);
            btn_daughter.setVisibility(View.GONE);
        }

        btn_addfm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(FamilyProfileActivity.this, "CLick......", Toast.LENGTH_SHORT).show();
                App.title = "ADD Father Mother";
                dialog.dismiss();
                startActivity(new Intent(instance, AddFatherMotherActivity.class).
                        putExtra("FamilyDetailId", String.valueOf(user.getFamilyDetailID()))
                        .putExtra("Name", obj.getFName())
                        .putExtra("FatherName", obj.getFatherName())
                        .putExtra("Gender", obj.getGender())
                        .putExtra("MotherName", obj.getMotherName())
                        .putExtra("FathersVillage", obj.getFathersVillage())
                        .putExtra("FatherSurname", obj.getFathersSurname()));
            }
        });

        btn_brother.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title = "ADD BROTHER";
                dialog.dismiss();
                startActivity(new Intent(instance, AddBrotherActivity.class).
                        putExtra("FamilyDetailId", App.memberFamilyDetailID + "")
                        .putExtra("Name", memberData.getFName())
                        .putExtra("FatherName", memberData.getFatherName())
                        .putExtra("Gender", memberData.getGender())
                        .putExtra("FathersVillage", memberData.getFathersVillage())
                        .putExtra("FatherSurname", memberData.getFathersSurname()));
            }
        });
        btn_sister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title = "ADD SISTER";
                dialog.dismiss();
                startActivity(new Intent(instance, AddSisterActivity.class).
                        putExtra("FamilyDetailId", App.memberFamilyDetailID + "")
                        .putExtra("Name", memberData.getFName())
                        .putExtra("FatherName", memberData.getFatherName())
                        .putExtra("Gender", memberData.getGender())
                        .putExtra("FathersVillage", memberData.getFathersVillage())
                        .putExtra("FatherSurname", memberData.getFathersSurname()));
            }
        });
        btn_spouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title = "ADD SPOUSE";
                dialog.dismiss();
                startActivity(new Intent(instance, AddSpouseActivity.class).
                        putExtra("Gender", memberData.getGender())
                        .putExtra("FamilyDetailId", App.memberFamilyDetailID + "")
                        .putExtra("FathersVillage", memberData.getFathersVillage())
                        .putExtra("FatherSurname", memberData.getFathersSurname()));
            }
        });

        btn_son.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title = "ADD SON";
                dialog.dismiss();
                startActivity(new Intent(instance, AddSonActivity.class).
                        putExtra("FamilyDetailId", App.memberFamilyDetailID + "")
                        .putExtra("Name", memberData.getFName())
                        .putExtra("FatherName", memberData.getFatherName())
                        .putExtra("Maritalstatus", memberData.getMaritalstatus())
                        .putExtra("Gender", memberData.getGender())
                        .putExtra("FathersVillage", memberData.getFathersVillage())
                        .putExtra("FatherSurname", memberData.getFathersSurname()));
            }
        });

        btn_daughter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title = "ADD DAUGHTER";
                dialog.dismiss();
                startActivity(new Intent(instance, AddDaughterActivity.class).
                        putExtra("FamilyDetailId", App.memberFamilyDetailID + "")
                        .putExtra("Name", memberData.getFName())
                        .putExtra("FatherName", memberData.getFatherName())
                        .putExtra("Maritalstatus", memberData.getMaritalstatus())
                        .putExtra("Gender", memberData.getGender())
                        .putExtra("FathersVillage", memberData.getFathersVillage())
                        .putExtra("FatherSurname", memberData.getFathersSurname()));
            }
        });
        ImageView imgClose = dialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });
    }

}

