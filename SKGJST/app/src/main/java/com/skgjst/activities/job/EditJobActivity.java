package com.skgjst.activities.job;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.App;
import com.skgjst.fonts.MyCustomTypeface;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.JobList;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditJobActivity  extends BaseActivity implements AsynchTaskListner {
    final Calendar myCalendar = Calendar.getInstance();
    public EditJobActivity instance;
    public JsonParserUniversal jParser;
    public Toolbar toolbar;
    public TextView tv_title, tv_exp_date;
    public EditText et_company_name, et_job_potion, et_exp_requierd, et_job_disc;
    public Spinner sp_job_location;
    public Button btn_submit, btn_cancel;
    public ArrayList<String> strCityArray = new ArrayList<>();
    public String LocationName = "";
    public int SelectedId = 0;
    public JobList obj;
    public CircleImageView img_profile;

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_job);

        instance = this;
        jParser = new JsonParserUniversal();
        Intent intent = getIntent();

        new CallRequest(instance).GetCityList();
        toolbar = findViewById(R.id.toolbar);
        img_profile = toolbar.findViewById(R.id.img_profile);

        tv_title = toolbar.findViewById(R.id.tv_title);

        tv_exp_date = findViewById(R.id.tv_exp_date);
        et_company_name = findViewById(R.id.et_company_name);
        et_job_potion = findViewById(R.id.et_job_potion);
        et_exp_requierd = findViewById(R.id.et_exp_requierd);
        et_job_disc = findViewById(R.id.et_job_disc);
        sp_job_location = findViewById(R.id.sp_job_location);
        btn_submit = findViewById(R.id.btn_submit);
        btn_cancel = findViewById(R.id.btn_cancel);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("EDIT JOB");
        if (intent.hasExtra("obj")) {
            obj = (JobList) getIntent().getExtras().getSerializable("obj");
            setData();
        }
        new CallRequest(instance).GetCityList();
        sp_job_location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.color_back));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        LocationName = ((TextView) parent.getChildAt(0)).getText().toString();

                    } else {
                        LocationName = "";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        Utils.setRoundedImage(instance,user.getPhotoURL(),300,300,img_profile,null);
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });

        tv_exp_date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    DatePicker(event);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Validation();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    public void setData() {
        tv_exp_date.setText(obj.getExpiryDate());
        et_company_name.setText(obj.getCompanyname());
        et_job_potion.setText(obj.getJobpost());
        et_exp_requierd.setText(obj.getExperience());
        et_job_disc.setText(obj.getJobdescription());
    }

    public void Validation() {
        if (et_company_name.getText().toString().equals("")) {
            et_company_name.setError("Please enter company name");
            et_company_name.setFocusable(true);
        } else if (et_job_potion.getText().toString().equals("")) {
            et_job_potion.setError("Please enter job position");
            et_job_potion.setFocusable(true);
        } else if (LocationName.equals("")) {
            Utils.showToast("Please select job location", instance);
        } else if (et_exp_requierd.getText().toString().equals("")) {
            et_exp_requierd.setError("Please enter experience");
            et_exp_requierd.setFocusable(true);
        } else if (et_job_disc.getText().toString().equals("")) {
            et_job_disc.setError("Please enter description");
            et_job_disc.setFocusable(true);
        } else if (tv_exp_date.getText().toString().equals("")) {
            tv_exp_date.setError("Please select date");
            tv_exp_date.setFocusable(true);
        } else {

            new CallRequest(instance).editJob(obj.getJobid(), tv_exp_date.getText().toString(),
                    et_company_name.getText().toString(), et_job_potion.getText().toString(), LocationName,
                    et_job_disc.getText().toString(), et_exp_requierd.getText().toString());
        }
    }

    public void DatePicker(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        String myFormat = "dd MMM yyyy"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        tv_exp_date.setText(sdf.format(myCalendar.getTime()));

                    }
                };
                DatePickerDialog datePickerDialog = new DatePickerDialog(instance, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                myCalendar.set(Calendar.HOUR_OF_DAY, myCalendar.getMinimum(Calendar.HOUR_OF_DAY));
                myCalendar.set(Calendar.MINUTE, myCalendar.getMinimum(Calendar.MINUTE));
                myCalendar.set(Calendar.SECOND, myCalendar.getMinimum(Calendar.SECOND));
                myCalendar.set(Calendar.MILLISECOND, myCalendar.getMinimum(Calendar.MILLISECOND));
                Calendar temp = Calendar.getInstance();
                datePickerDialog.getDatePicker().setMinDate(temp.getTimeInMillis());
                datePickerDialog.show();

                break;
            case MotionEvent.ACTION_UP:
                break;
            default:
                break;
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case editJob:
                    Utils.hideProgressDialog();
                    try

                    {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success") == true) {
                            Utils.showToast(jObj.getString("UMessage"), instance);
                            startActivity(new Intent(instance, JobListActivity.class));
                        }
                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;


                case GetCityList:
                    Utils.hideProgressDialog();
                    try

                    {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success") == true) {
                            strCityArray.clear();
                            if (jObj.getJSONArray("CityList") != null && jObj.getJSONArray("CityList").length() > 0) {
                                String city = "";
                                strCityArray.add("Select city");
                                JSONArray jDataArray = jObj.getJSONArray("CityList");
                                for (int i = 0; i < jDataArray.length(); i++) {
                                    city = jDataArray.getJSONObject(i).getString("city");
                                    strCityArray.add(city);
                                    if (obj.getJoblocation().equals(city)) {
                                        LocationName = city;
                                        SelectedId = i;
                                    }
                                }
                                sp_job_location.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strCityArray));
                                sp_job_location.setSelection(SelectedId + 1);
                            } else {
                                Utils.showToast(jObj.getString("UMessage"), instance);
                            }
                        } else {
                            Utils.showToast(jObj.getString("UMessage"), instance);
                        }
                    } catch (
                            JSONException e)

                    {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }
}
