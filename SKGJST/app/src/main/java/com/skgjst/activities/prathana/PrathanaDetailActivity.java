package com.skgjst.activities.prathana;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.R;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class PrathanaDetailActivity extends BaseActivity implements AsynchTaskListner {
    public PrathanaDetailActivity instance;
    public Toolbar toolbar;
    public JsonParserUniversal jParser;
    public TextView tv_title, tv_prathna_info, tv_prathna_place, tv_prathna_date, tv_person_info, tv_death_date, tv_age, tv_fulll_name;
    public CircleImageView img_profile, img_profile_local;
    public PrathanaData obj;
    public Button btn_delete;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prathana_detail);
        instance = this;
        jParser = new JsonParserUniversal();
        toolbar = findViewById(R.id.toolbar);
        img_profile = toolbar.findViewById(R.id.img_profile);

        img_profile_local = findViewById(R.id.img_profile_local);
        tv_title = toolbar.findViewById(R.id.tv_title);
        tv_prathna_info = findViewById(R.id.tv_prathna_info);
        tv_prathna_place = findViewById(R.id.tv_prathna_place);
        tv_prathna_date = findViewById(R.id.tv_prathna_date);
        tv_person_info = findViewById(R.id.tv_person_info);
        tv_death_date = findViewById(R.id.tv_death_date);
        tv_age = findViewById(R.id.tv_age);
        tv_fulll_name = findViewById(R.id.tv_fulll_name);
        btn_delete = findViewById(R.id.btn_delete);
        tv_title.setText("PRATHNA DETAIL");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        if (user.getPhotoURL() != null)
            Utils.setRoundedImage(instance, user.getPhotoURL(), 300, 300, img_profile, null);

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });
        Intent intent = getIntent();

        if (intent.getStringExtra("DemiseType").equalsIgnoreCase("MyDemise")) {
            btn_delete.setVisibility(View.VISIBLE);
        }

        if (intent.hasExtra("obj")) {
            obj = (PrathanaData) getIntent().getExtras().getSerializable("obj");
            setData();
        }

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CallRequest(instance).deletePrathna(user, String.valueOf(obj.getPrathnaID()));
            }
        });

    }

    private void setData() {
        tv_fulll_name.setText(obj.getName());
        tv_prathna_place.setText(obj.getPrathnaPlace());
        tv_prathna_date.setText(obj.getPrathnaDate());
        tv_prathna_info.setText(obj.getPrathnaInfo());

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {

                case deletePrathna:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success") == true) {
                            Utils.showToast(jObj.getString("UMessage"), instance);
                            onBackPressed();
                        } else {
                            Utils.showToast(jObj.getString("UMessage"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }
}
