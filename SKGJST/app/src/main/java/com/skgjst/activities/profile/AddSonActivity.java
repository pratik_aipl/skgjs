package com.skgjst.activities.profile;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.R;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddSonActivity extends BaseActivity implements AsynchTaskListner {
    public Toolbar toolbar;
    public AddSonActivity instance;
    public TextView tv_title, tv_father_village, tv_father_surname, tv_father_name;
    public ProgressBar pbar;
    public CircleImageView img_profile, img_profile_local;
    public EditText et_father_village, et_father_surname, et_father_name, et_son_name,
            et_son_father_name, et_son_father_surname, et_son_father_village;
    public Button btn_submit;
    public LinearLayout linear, linear_divorced;
    public String FamilyDetailID, Name, Gender, FatherSurname, FathersVillage, FatherName, Maritalstatus;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_son);
        instance = AddSonActivity.this;
        FamilyDetailID = getIntent().getStringExtra("FamilyDetailId");
        Name = getIntent().getStringExtra("Name");
        FatherName = getIntent().getStringExtra("FatherName");
        Gender = getIntent().getStringExtra("Gender");
        FatherSurname = getIntent().getStringExtra("FatherSurname");
        FathersVillage = getIntent().getStringExtra("FathersVillage");
        Maritalstatus = getIntent().getStringExtra("Maritalstatus");

        pbar = findViewById(R.id.pbar);
        et_father_village = findViewById(R.id.et_father_village);
        et_father_surname = findViewById(R.id.et_father_surname);
        et_father_name = findViewById(R.id.et_father_name);
        et_son_name = findViewById(R.id.et_son_name);
        et_son_father_name = findViewById(R.id.et_son_father_name);
        et_son_father_surname = findViewById(R.id.et_son_father_surname);
        et_son_father_village = findViewById(R.id.et_son_father_village);
        btn_submit = findViewById(R.id.btn_submit);
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("ADD SON");
        img_profile = toolbar.findViewById(R.id.img_profile);
        linear = findViewById(R.id.linear);
        tv_father_village = findViewById(R.id.tv_father_village);
        tv_father_surname = findViewById(R.id.tv_father_surname);
        tv_father_name = findViewById(R.id.tv_father_name);
        linear_divorced = findViewById(R.id.linear_divorced);
        if (user.getPhotoURL() != null)
            Utils.setRoundedImage(instance, user.getPhotoURL(), 300, 300, img_profile, pbar);

        tv_father_name.setText(Name + " Father Name");
        tv_father_surname.setText(Name + " Father Surname");
        tv_father_village.setText(Name + " Father Village");

        if (Gender.equalsIgnoreCase("Male")) {
            et_father_surname.setVisibility(View.GONE);
            et_father_village.setVisibility(View.GONE);
            tv_father_surname.setVisibility(View.GONE);
            tv_father_village.setVisibility(View.GONE);
            linear.setVisibility(View.GONE);
        } else {
            linear.setVisibility(View.VISIBLE);
            et_father_surname.setVisibility(View.VISIBLE);
            et_father_village.setVisibility(View.VISIBLE);
            tv_father_surname.setVisibility(View.VISIBLE);
            tv_father_village.setVisibility(View.VISIBLE);

        }

        if (Gender.equalsIgnoreCase("Female") && Maritalstatus.equalsIgnoreCase("Divorced")) {
            linear_divorced.setVisibility(View.VISIBLE);
        } else {
            linear_divorced.setVisibility(View.GONE);

        }
        if (!FatherName.equalsIgnoreCase("") && FatherName != null) {
            et_father_name.setText(FatherName);
            et_father_name.setClickable(false);
            et_father_name.setFocusable(false);
            et_father_name.setFocusableInTouchMode(false);
        }

        if (!FatherSurname.equalsIgnoreCase("") && FatherSurname != null) {
            et_father_surname.setText(FatherSurname);
            et_father_surname.setClickable(false);
            et_father_surname.setFocusable(false);
            et_father_surname.setFocusableInTouchMode(false);
        }
        if (!FathersVillage.equalsIgnoreCase("") && FathersVillage != null) {
            et_father_village.setText(FathersVillage);
            et_father_village.setClickable(false);
            et_father_village.setFocusable(false);
            et_father_village.setFocusableInTouchMode(false);
        }

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(et_son_name.getText().toString().trim())) {
                    et_son_name.requestFocus();
                    et_son_name.setError("Please enter son name");

                } else if (et_father_name.getText().toString().isEmpty()) {
                    et_father_name.requestFocus();
                    et_father_name.setError("Please enter father name");
                    return;

                } else if (et_father_surname.getText().toString().isEmpty()) {
                    et_father_surname.requestFocus();
                    et_father_surname.setError("Please enter father surname");
                    return;

                } else if (et_father_village.getText().toString().isEmpty()) {
                    et_father_village.requestFocus();
                    et_father_village.setError("Please enter father village");
                    return;

                } else {
                    if (Gender.equalsIgnoreCase("Female") && Maritalstatus.equalsIgnoreCase("Divorced")) {
                        if (et_son_father_name.getText().toString().isEmpty()) {
                            et_son_father_name.requestFocus();
                            et_son_father_name.setError("Please enter daughter's father name");
                            return;

                        } else if (et_son_father_village.getText().toString().isEmpty()) {
                            et_son_father_village.requestFocus();
                            et_son_father_village.setError("Please enter daughter's father village");
                            return;

                        } else if (et_son_father_surname.getText().toString().isEmpty()) {
                            et_son_father_surname.requestFocus();
                            et_son_father_surname.setError("Please enter daughter's father surname");
                            return;

                        } else {
                            new CallRequest(instance).addSonNew(user, et_son_name.getText().toString(), et_father_name.getText().toString(),
                                    et_father_surname.getText().toString(), et_father_village.getText().toString(), FamilyDetailID,
                                    et_son_father_name.getText().toString(), et_son_father_village.getText().toString(), et_son_father_surname.getText().toString());

                        }
                    } else {
                        new CallRequest(instance).addSonNew(user, et_son_name.getText().toString(), et_father_name.getText().toString(),
                                et_father_surname.getText().toString(), et_father_village.getText().toString(), FamilyDetailID,
                                et_son_father_name.getText().toString(), et_son_father_village.getText().toString(), et_son_father_surname.getText().toString());

                    }
                }
            }
        });


    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {

                case addSon:
                    Utils.hideProgressDialog();
                    JSONObject jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            Utils.showToast(jObj.getString("UMessage"), this);
                            onBackPressed();

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;


            }
        }
    }
}
