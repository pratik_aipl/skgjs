package com.skgjst.activities.profile;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.skgjst.App;
import com.skgjst.BaseActivity;
import com.skgjst.R;
import com.skgjst.utils.Utils;

import de.hdodenhof.circleimageview.CircleImageView;

public class FamilyDetailsTreeActivity extends BaseActivity {
    private static final String TAG = "FamilyTreeActivity";
    public Toolbar toolbar;
    public TextView tv_title;
    public CircleImageView img_profile, img_profile_local;
    public FamilyDetailsTreeActivity instance;
    ProgressBar mProgress;
    private WebView webView;
    public String family_id = "";
    String newUA = "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0";

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family_tree);
        family_id = getIntent().getStringExtra("FamilyDetailID");
        instance = FamilyDetailsTreeActivity.this;
        toolbar = findViewById(R.id.toolbar);
        mProgress = findViewById(R.id.mProgress);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("MY RELATION TREE");
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_profile_local = toolbar.findViewById(R.id.img_profile_local);

        if (!TextUtils.isEmpty(user.getPhotoURL())) {
            Utils.setRoundedImage(instance, user.getPhotoURL(), 300, 300, img_profile, null);
        }
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });
        webView = findViewById(R.id.webView);

        /*https://skgjsedirectory.org/ConnectionTree.aspx?SID=1&DID=89958


Here SID and DID are SourceFamilyDetailID and DestinationFamilyDetailID respectively.
*/

        //renderWebPage(webView,"https://skgjsedirectory.org/MyFamilyTree.aspx?FDID=" + family_id);
        renderWebPage(webView,"https://skgjsedirectory.org/ConnectionTree.aspx?SID="+user.getFamilyDetailID()+"&DID="+family_id);
    }

    protected void renderWebPage(WebView mWebView, String urlToRender) {
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                mProgress.setVisibility(View.VISIBLE);
                // Do something on page loading started
            }

            @Override
            public void onPageFinished(WebView view, String url) {
           //     webView.loadUrl("javascript:MyApp.resize(document.body.getBoundingClientRect().height)");
                mProgress.setVisibility(View.GONE);
//                webView.scrollTo(getResources().getDisplayMetrics().widthPixels/2,getResources().getDisplayMetrics().heightPixels/4);
//                view.scrollBy(getResources().getDisplayMetrics().widthPixels/2,20);
                // Do something when page loading finished
            }
        });

/*        mWebView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int newProgress) {
                view.scrollBy(100,20);
            }
        });*/

        // Enable the javascript
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDisplayZoomControls(true);

    /*    final float mDensity = getResources().getDisplayMetrics().density;
        if (mDensity == 240) {
            mWebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        } else if (mDensity == 160) {
            mWebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        } else if(mDensity == 120) {
            mWebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.CLOSE);
        }else if(mDensity == DisplayMetrics.DENSITY_XHIGH){
            mWebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        }else if (mDensity == DisplayMetrics.DENSITY_TV){
            mWebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        }else {
            mWebView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.MEDIUM);
        }
*/
        mWebView.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        mWebView.getSettings().setSupportZoom(true);
       // mWebView.setInitialScale(200);
        Log.d(TAG, "renderWebPage: width--> "+getResources().getDisplayMetrics().widthPixels);
        Log.d(TAG, "renderWebPage: height--> "+getResources().getDisplayMetrics().heightPixels);
        setDesktopMode(mWebView, true);
        mWebView.loadUrl(urlToRender);
    }

//    @JavascriptInterface
//    public void resize(final float height) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                webView.setLayoutParams(new LinearLayout.LayoutParams(getResources().getDisplayMetrics().widthPixels, (int) (height * getResources().getDisplayMetrics().density)));
//            }
//        });
//    }

    public void setDesktopMode(WebView webView, boolean enabled) {
        String newUserAgent = webView.getSettings().getUserAgentString();
        if (enabled) {
            try {
                String ua = webView.getSettings().getUserAgentString();
                String androidOSString = webView.getSettings().getUserAgentString().substring(ua.indexOf("("), ua.indexOf(")") + 1);
                newUserAgent = webView.getSettings().getUserAgentString().replace(androidOSString, "(X11; Linux x86_64)");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            newUserAgent = null;
        }

        webView.getSettings().setUserAgentString(newUserAgent);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.reload();
    }

    // To handle &quot;Back&quot; key press event for WebView to go back to previous screen.
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        {
            if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
                webView.goBack();
                return true;
            }
            return super.onKeyDown(keyCode, event);
        }

    }
}


