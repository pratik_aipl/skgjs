package com.skgjst.activities.membersearch.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.skgjst.App;
import com.skgjst.BaseFragment;
import com.skgjst.R;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.Profile;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 10/2/2018.
 */

public class MemberProfileFragment extends BaseFragment implements AsynchTaskListner {
    public View v;
    public MemberProfileFragment fragment;
    public TextView tv_first_name, tv_middle_name, tv_surname, tv_head_of_family,
            tv_education, tv_native_place, tv_age, tv_gender, tv_blood_group, tv_matrimonal, tv_modified_by, tv_modified_on;
    public JsonParserUniversal jParser;
    public ArrayList<Profile> profilesArrayList = new ArrayList<>();
    public Profile obj;

    public MemberProfileFragment newInstance() {
        fragment = new MemberProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        fragment.setHasOptionsMenu(true);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_profile, container, false);
        fragment = this;
        jParser = new JsonParserUniversal();
        tv_education = v.findViewById(R.id.tv_education);
        tv_modified_by = v.findViewById(R.id.tv_modified_by);
        tv_modified_on = v.findViewById(R.id.tv_modified_on);
        tv_first_name = v.findViewById(R.id.tv_first_name);
        tv_middle_name = v.findViewById(R.id.tv_middle_name);
        tv_surname = v.findViewById(R.id.tv_surname);
        tv_head_of_family = v.findViewById(R.id.tv_head_of_family);
        tv_head_of_family.setVisibility(View.GONE);
        tv_native_place = v.findViewById(R.id.tv_native_place);
        tv_age = v.findViewById(R.id.tv_age);
        tv_gender = v.findViewById(R.id.tv_gender);
        tv_blood_group = v.findViewById(R.id.tv_blood_group);
        tv_matrimonal = v.findViewById(R.id.tv_matrimonal);

       /* if (!TextUtils.isEmpty(App.memberFamilyDetailID))
           */ new CallRequest(fragment).getMyprofiles(user,Integer.parseInt((App.memberFamilyDetailID)));
        return v;
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (getView() != null) {
            //   new CallRequest(fragment).getMyprofiles(Integer.parseInt(App.memberFamilyDetailID));
            //  setData();
        } else {

        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            //    Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getMyprofiles:
                    obj = new Profile();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {

                            if (jObj.getJSONArray("Profile") != null && jObj.getJSONArray("Profile").length() > 0) {
                                JSONArray userData = jObj.getJSONArray("Profile");

                                for (int i = 0; i < userData.length(); i++) {
                                    JSONObject jobj = userData.getJSONObject(i);
                                    obj = (Profile) jParser.parseJson(jobj, new Profile());
                                    //    user.setPhotoURL(obj.getPhotoURL());
                                }


                                Utils.hideProgressDialog();
                                setData();
                            } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("UMessage"), getActivity());
                            }
                        } else {
                            Utils.hideProgressDialog();
                            //  Utils.showToast(jObj.getString("UMessage"), getActivity());
                        }
                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        Utils.showToast("Something getting wrong! Please try again later.", getActivity());

                        e.printStackTrace();
                    }
            }
        }
    }

    public void setData() {
        tv_first_name.setText(obj.getFName());
        tv_middle_name.setText(obj.getMName());
        tv_surname.setText(obj.getSurName());
        tv_native_place.setText(obj.getNativePlace());
        tv_age.setText(obj.getDOB());
        tv_gender.setText(obj.getGender());
        tv_education.setText(obj.getEducation());
        tv_matrimonal.setText(obj.getMaritalstatus());
        tv_blood_group.setText(obj.getBloodGroupName());
        tv_modified_by.setText(obj.getModifiedBy());
        tv_modified_on.setText(obj.getModifiedOn());

    }
}

