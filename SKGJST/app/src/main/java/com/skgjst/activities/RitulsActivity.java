package com.skgjst.activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.App;
import com.skgjst.fonts.MyCustomTypeface;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.Surname;
import com.skgjst.model.Village;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class RitulsActivity  extends BaseActivity implements AsynchTaskListner {
    public Toolbar toolbar;
    public TextView tv_title;
    public CircleImageView img_profile, img_profile_local;
    public RitulsActivity instance;
    public LinearLayout ll_Download_LagnaVidhi, ll_Download_Chandlavidhi, llDownload;
    public String name;
    public Surname surname;
    public ArrayList<Surname> surnameArray = new ArrayList<>();
    public ArrayList<String> strSurnameArray = new ArrayList<>();
    public String surname_id = "", surname_name = "";
    public Spinner spSurname, spVillage;
    public JsonParserUniversal jParser;
    public Village village;
    public ArrayList<Village> villageArray = new ArrayList<>();
    public ArrayList<String> strVillageArray = new ArrayList<>();
    public String village_id = "", village_name = "", filePath = "";

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_jar_vidhi);
        instance = RitulsActivity.this;
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        jParser = new JsonParserUniversal();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("RITULS");
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_profile_local = toolbar.findViewById(R.id.img_profile_local);
        Utils.setRoundedImage(instance,user.getPhotoURL(),300,300,img_profile,null);

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });

        ll_Download_LagnaVidhi = findViewById(R.id.ll_Download_LagnaVidhi);
        ll_Download_Chandlavidhi = findViewById(R.id.ll_Download_Chandlavidhi);
        new CallRequest(instance).getSurnameList();
        new CallRequest(instance).getVillageList();

        ll_Download_LagnaVidhi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = "lagnavidhi";
//                new DownloadFile().execute("http://skgjsedirectory.org/Upload/JarVidhiFiles/lagnavidhi.pdf");
//                Utils.showToast("File Downloaded SuccessFully",getActivity());

                String url = "http://skgjsedirectory.org/Upload/JarVidhiFiles/lagnavidhi.pdf";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });

        ll_Download_Chandlavidhi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = "Chandlavidhi";
                //   new DownloadFile().execute("http://skgjsedirectory.org/Upload/JarVidhiFiles/Chandlavidhi.pdf");
                String url = "http://skgjsedirectory.org/Upload/JarVidhiFiles/Chandlavidhi.pdf";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);


            }
        });

        llDownload = findViewById(R.id.ll_Download_Pdf);
        surname = new Surname();
        village = new Village();
        spSurname = findViewById(R.id.spinnerSurname);
        spVillage = findViewById(R.id.spinnerVillage);

        spSurname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        surname_id = String.valueOf(surnameArray.get(position).getmSurnameid());
                        surname_name = surnameArray.get(position).getSurname();
                    } else {
                        surname_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        spVillage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        village_id = String.valueOf(villageArray.get(position).getVillageID());
                        village_name = villageArray.get(position).getVillagename();

                    } else {
                        village_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        llDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CallRequest(instance).JarVidhi(village_id, surname_id);
            }
        });
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case getSurnameList:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strSurnameArray.clear();
                            surnameArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                surname.setSurname(getResources().getString(R.string.surname));
                                surname.setmSurnameid(0);
                                strSurnameArray.add(surname.getSurname());
                                surnameArray.add(surname);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        surname = (Surname) jParser.parseJson(jSurnameArray.getJSONObject(i), new Surname());
                                        surnameArray.add(surname);
                                        strSurnameArray.add(surname.getSurname());
                                    }
                                    spSurname.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strSurnameArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getVillageList:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strVillageArray.clear();
                            villageArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                village.setVillagename(getResources().getString(R.string.village));
                                village.setVillageID(0);
                                strVillageArray.add(village.getVillagename());
                                villageArray.add(village);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        village = (Village) jParser.parseJson(jSurnameArray.getJSONObject(i), new Village());
                                        villageArray.add(village);
                                        strVillageArray.add(village.getVillagename());
                                    }
                                    spVillage.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strVillageArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case JarVidhi:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {

                                JSONArray jBranchArray = jObj.getJSONArray("data");

                                for (int i = 0; i < jBranchArray.length(); i++) {
                                    JSONObject jPackag = jBranchArray.getJSONObject(i);
                                    filePath = jPackag.getString("filepath").replace(" ", "%20");

                                }
                                if (!filePath.isEmpty()) {
                                    name = "";
                                    String url = filePath;
                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                    i.setData(Uri.parse(url));
                                    startActivity(i);

                                    //new DownloadFile().execute(filePath);
                                    //Utils.showToast("File Downloaded SuccessFully", getActivity());
                                }
                            } else {
                                Utils.showToast(jObj.getString("Message"), this);
                            }
                        } else {
                            Utils.showAlert(jObj.getString("Message"), this);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
