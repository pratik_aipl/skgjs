package com.skgjst.activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.adapter.NewsAdapter;
import com.skgjst.App;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.News;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class NewsActivity  extends BaseActivity implements AsynchTaskListner {
    public Toolbar toolbar;
    public TextView tv_title;
    public CircleImageView img_profile, img_profile_local;
    public NewsActivity instance;
    public JsonParserUniversal jParser;
    public ArrayList<News> newsArrayList = new ArrayList<>();
    ExpandableListView expListView;

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        instance = NewsActivity.this;
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        jParser = new JsonParserUniversal();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("NEWS");
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_profile_local = toolbar.findViewById(R.id.img_profile_local);
        Utils.setRoundedImage(instance,user.getPhotoURL(),300,300,img_profile,null);

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });

        expListView = (ExpandableListView) findViewById(R.id.lvExp);
        new CallRequest(this).getNews();

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            //    Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getNews:
                    newsArrayList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("News") != null && jObj.getJSONArray("News").length() > 0) {
//                                lin_empty.setVisibility(View.GONE);
                                //   rv_product_list.setVisibility(View.VISIBLE);
                                JSONArray userData = jObj.getJSONArray("News");
                                for (int i = 0; i < userData.length(); i++) {
                                    JSONObject obj = userData.getJSONObject(i);
                                    News notiObj = (News) jParser.parseJson(obj, new News());
                                    notiObj.newsSubArray.add(notiObj);
                                    newsArrayList.add(notiObj);

                                }
                                NewsAdapter listAdapter = new NewsAdapter(NewsActivity.this, newsArrayList, expListView);
                                expListView.setAdapter(listAdapter);

                            } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("UMessage"), instance);
                                //   lin_empty.setVisibility(View.VISIBLE);
                                //    rv_product_list.setVisibility(View.GONE);
                            }


                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), instance);
                            // lin_empty.setVisibility(View.VISIBLE);
                            //  rv_product_list.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {

                        Utils.hideProgressDialog();
                        Utils.showToast("UMessage", instance);
                        //   lin_empty.setVisibility(View.VISIBLE);
                        //   rv_product_list.setVisibility(View.GONE);
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;


            }
        }
    }
}
