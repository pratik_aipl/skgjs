package com.skgjst.activities.matrimonial;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.App;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.MatrimonialList;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.ImagePopup;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class MatriAddProfileActivity  extends BaseActivity implements AsynchTaskListner {
    public Toolbar toolbar;
    public TextView tv_title;
    public MatriAddProfileActivity instance;
    public JsonParserUniversal jParser;
    public CircleImageView img_profile, img_profile_local;
    public MatrimonialList matrimonialList;
    //  public MatriProfileList matriProfileList;
    public TextView tv_first_name, tv_middle_name, tv_surname, tv_native_place, tv_dob, tv_gender, tv_height, tv_contectno, tv_busi_type, tv_profession, tv_blood_group,
            tv_education, tv_Marital_Status, tv_other_info, tv_manglik, tv_handicap, tv_skin_color, tv_weight;
    public ProgressBar pbar;
    public Button btn_view_profile, btn_add;

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matri_add_profile);
        instance = MatriAddProfileActivity.this;
        Intent intent = getIntent();

        jParser = new JsonParserUniversal();
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("MATRIMONIAL DETAIL");
        img_profile = toolbar.findViewById(R.id.img_profile);
        btn_view_profile = findViewById(R.id.btn_view_profile);
        btn_add = findViewById(R.id.btn_add);
        Utils.setRoundedImage(instance,user.getPhotoURL(),300,300,img_profile,null);

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });

        Validation();
        if (intent.hasExtra("obj")) {
            matrimonialList = (MatrimonialList) getIntent().getExtras().getSerializable("obj");
            setData(matrimonialList);
            //   new CallRequest(instance).fillMatrimonial(String.valueOf(matriProfileList.getMatrimonialid()));

        }
        btn_view_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MatrimonialProfileActivity.class)
                        .putExtra("obj", matrimonialList));
            }
        });
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CallRequest(instance).removeProfile(user,matrimonialList.getMatrimonialid(), matrimonialList.getSelectionid());
            }
        });
    }

    private void Validation() {
        pbar = findViewById(R.id.pbar);
        img_profile_local = findViewById(R.id.img_profile_local);
        tv_first_name = findViewById(R.id.tv_first_name);
        tv_middle_name = findViewById(R.id.tv_middle_name);
        tv_surname = findViewById(R.id.tv_surname);
        tv_native_place = findViewById(R.id.tv_native_place);
        tv_dob = findViewById(R.id.tv_dob);
        tv_gender = findViewById(R.id.tv_gender);
        tv_height = findViewById(R.id.tv_height);
        tv_contectno = findViewById(R.id.tv_contectno);
        tv_busi_type = findViewById(R.id.tv_busi_type);
        tv_profession = findViewById(R.id.tv_profession);
        tv_blood_group = findViewById(R.id.tv_blood_group);
        tv_education = findViewById(R.id.tv_education);
        tv_Marital_Status = findViewById(R.id.tv_Marital_Status);
        tv_other_info = findViewById(R.id.tv_other_info);
        tv_manglik = findViewById(R.id.tv_manglik);
        tv_handicap = findViewById(R.id.tv_handicap);
        tv_skin_color = findViewById(R.id.tv_skin_color);
        tv_weight = findViewById(R.id.tv_weight);

    }

    private void setData(MatrimonialList matriProfileList) {
        tv_first_name.setText(matriProfileList.getFName().toString());
        tv_middle_name.setText(matriProfileList.getMName().toString());
        tv_surname.setText(matriProfileList.getSurName().toString());
        tv_native_place.setText(matriProfileList.getNativePlace().toString());
        tv_dob.setText(matriProfileList.getDOB().toString());
        tv_gender.setText(matriProfileList.getGender().toString());
        tv_height.setText(matriProfileList.getHeight().toString());
        tv_contectno.setText(matriProfileList.getContactNo().toString());
        tv_busi_type.setText(matriProfileList.getIndustryName().toString());
        tv_profession.setText(matriProfileList.getWorkProfession().toString());
        tv_blood_group.setText(matriProfileList.getBloodGroupName().toString());
        tv_education.setText(matriProfileList.getEducation().toString());
        tv_Marital_Status.setText(matriProfileList.getMaritalStatus().toString());
        tv_other_info.setText(matriProfileList.getOtherinfo().toString());
        tv_manglik.setText(matriProfileList.getManglik().toString());
        tv_handicap.setText(matriProfileList.getHandicap().toString());
        tv_skin_color.setText(matriProfileList.getSkincolor().toString());
        tv_weight.setText(matriProfileList.getWeight().toString());
        Utils.setRoundedImage(instance,matriProfileList.getPhotoURL(),300,300,img_profile_local,pbar);
       img_profile_local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openImage();
            }
        });

    }

    public void openImage() {
        final ImagePopup imagePopup = new ImagePopup(MatriAddProfileActivity.this);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.BLACK);  // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(true);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional
        imagePopup.initiatePopupWithPicasso(matrimonialList.getPhotoURL().replace(" ", "%20"));
        img_profile_local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /** Initiate Popup view **/
                imagePopup.viewPopup();

            }
        });


    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {


                case removeProfile:
                    Utils.hideProgressDialog();
                    JSONObject jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            Utils.showToast(jObj.getString("UMessage"), this);
                           // startActivity(new Intent(instance, MatrimonialSearchActivity.class));
                            App.isRemove = true;
                            onBackPressed();

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

            /*    case fillMatrimonial:
                    Utils.hideProgressDialog();
                    jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("Matrimonialinfo") != null && jObj.getJSONArray("Matrimonialinfo").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("Matrimonialinfo");

                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        matrimonialList = (MatrimonialList) jParser.parseJson(jSurnameArray.getJSONObject(i), new MatrimonialList());

                                    setData(matrimonialList);}
                                } else {
                                    Utils.showToast(jObj.getString("UMessage"), instance);
                                }
                            }
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;*/
            }
        }
    }
}

