package com.skgjst.activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.activities.viewpager.VerticalInfiniteCycleViewPager;
import com.skgjst.adapter.SponseredDetailAdapter;
import com.skgjst.App;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.BusinessAdvertiseMent;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SponserDetailActivity  extends BaseActivity implements AsynchTaskListner {
    public Toolbar toolbar;
    public TextView tv_title, tv_ad_name;
    public CircleImageView img_profile, img_profile_local;
    public SponserDetailActivity instance;
    public VerticalInfiniteCycleViewPager mViewPager;
    public JsonParserUniversal jParser;
    public ArrayList<BusinessAdvertiseMent> businessAdvertiseMentArray = new ArrayList<>();

    public String adId = "", adName = "";
    public LinearLayout lin_empty;

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sponser_detail);
        jParser = new JsonParserUniversal();
        instance = SponserDetailActivity.this;
        toolbar = findViewById(R.id.toolbar);
        lin_empty = findViewById(R.id.lin_empty);
        tv_ad_name = findViewById(R.id.tv_ad_name);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_profile_local = toolbar.findViewById(R.id.img_profile_local);

        Utils.setRoundedImage(instance,user.getPhotoURL(),300,300,img_profile,null);

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });
        adName = getIntent().getStringExtra("adName");
        adId = getIntent().getStringExtra("adId");
        mViewPager = findViewById(R.id.hicvp);
        tv_ad_name.setText(adName);
        tv_title.setText(adName);
        new CallRequest(instance).getDimondAd(adId);

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            Utils.hideProgressDialog();
            switch (request) {
                case getDimondAd:
                    businessAdvertiseMentArray.clear();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj != null) {
                                if (jObj.getJSONArray("SponsoredAdvertisements") != null && jObj.getJSONArray("SponsoredAdvertisements").length() > 0) {

                                    JSONArray DiamondAds = jObj.getJSONArray("SponsoredAdvertisements");
                                    for (int i = 0; i < DiamondAds.length(); i++) {
                                        JSONObject obj = DiamondAds.getJSONObject(i);
                                        BusinessAdvertiseMent businessAdvertiseMent = (BusinessAdvertiseMent) jParser.parseJson(obj, new BusinessAdvertiseMent());
                                        businessAdvertiseMentArray.add(businessAdvertiseMent);
                                    }
                                }


                                System.out.println("size" + businessAdvertiseMentArray.size());

                                if (businessAdvertiseMentArray.size() > 0) {
                                    SponseredDetailAdapter photoViewAdapter = new SponseredDetailAdapter(instance, businessAdvertiseMentArray);
                                    mViewPager.setAdapter(photoViewAdapter);
                                    lin_empty.setVisibility(View.GONE);
                                } else {
                                    lin_empty.setVisibility(View.VISIBLE);
                                }


                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        lin_empty.setVisibility(View.VISIBLE);
                    }
                    break;
            }
        }
    }
}
