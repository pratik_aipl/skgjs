package com.skgjst.activities.business;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.R;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.model.BusinessList;
import com.skgjst.utils.ImagePopup;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.PicassoTrustAll;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;

import de.hdodenhof.circleimageview.CircleImageView;

public class BusinessDeatilActivity extends BaseActivity {
    public Toolbar toolbar;
    public TextView tv_title, tv_dob, tv_first_name, tv_middle_name, tv_surname, tv_busi_type, tv_office_address, tv_office_name,
            tv_office_contact, tv_email, tv_mobile;
    public BusinessDeatilActivity instance;
    public JsonParserUniversal jParser;
    public CircleImageView img_profile, img_profile_local;
    public BusinessList businessList;

    public Button btn_view_profile;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_deatil);
        instance = BusinessDeatilActivity.this;
        jParser = new JsonParserUniversal();
        Intent intent = getIntent();
        toolbar = findViewById(R.id.toolbar);
        btn_view_profile = findViewById(R.id.btn_view_profile);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("BUSINESS DETAIL");
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_profile_local = findViewById(R.id.img_profile_local);

        Utils.setRoundedImage(instance, user.getPhotoURL(), 300, 300, img_profile, null);

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });

        tv_first_name = findViewById(R.id.tv_first_name);
        tv_middle_name = findViewById(R.id.tv_middle_name);
        tv_surname = findViewById(R.id.tv_surname);
        tv_busi_type = findViewById(R.id.tv_busi_type);
        tv_office_address = findViewById(R.id.tv_office_address);
        tv_office_name = findViewById(R.id.tv_office_name);
        tv_office_contact = findViewById(R.id.tv_office_contact);
        tv_email = findViewById(R.id.tv_email);
        tv_mobile = findViewById(R.id.tv_mobile);
        if (intent.hasExtra("obj")) {
            businessList = (BusinessList) getIntent().getExtras().getSerializable("obj");
            setData();
        }

        btn_view_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, BusinessProfileActivity.class)
                        .putExtra("obj", businessList));
            }
        });
    }

    private void setData() {
        tv_first_name.setText(businessList.getFName());
        tv_middle_name.setText(businessList.getMName());
        tv_surname.setText(businessList.getSurName());
        tv_busi_type.setText(businessList.getIndustryName());
        tv_office_address.setText(businessList.getOfficeAddress());
        tv_office_name.setText(businessList.getOfficeName());
        tv_office_contact.setText(businessList.getOfficeContNo());
        tv_email.setText(businessList.getEmailID());
        tv_mobile.setText(businessList.getContactNo());

        tv_mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", businessList.getContactNo(), null));
                startActivity(intent);
            }
        });
        if (businessList.getPhotoURL() != null && !TextUtils.isEmpty(businessList.getPhotoURL())) {
            try {
                PicassoTrustAll.getInstance(BusinessDeatilActivity.this)
                        .load(businessList.getPhotoURL().replace(" ", "%20"))
                        .error(R.drawable.avatar)
                        .resize(256, 256)
                        .into(img_profile_local, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        img_profile_local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openImage();
            }
        });

    }

    public void openImage() {
        final ImagePopup imagePopup = new ImagePopup(BusinessDeatilActivity.this);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.BLACK);  // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(true);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional
        imagePopup.initiatePopupWithPicasso(businessList.getPhotoURL().replace(" ", "%20"));
        img_profile_local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /** Initiate Popup view **/
                imagePopup.viewPopup();

            }
        });


    }
}
