package com.skgjst.activities;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.skgjst.App;
import com.skgjst.BaseActivity;
import com.skgjst.R;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.fonts.MyCustomTypeface;
import com.skgjst.model.BloodGroup;
import com.skgjst.model.RegisterData;
import com.skgjst.model.Surname;
import com.skgjst.model.Village;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class RegisterActivity extends BaseActivity implements AsynchTaskListner {
    final Calendar myCalendar = Calendar.getInstance();
    public Toolbar toolbar;
    public TextView tv_title, tv_dob;
    public RegisterActivity instance;
    public FloatingActionButton float_right_button;
    public CircleImageView img_profile_local, img_profile;
    public String logoPath = "", filename = "", surname_name = "";
    public Surname surname;
    public ArrayList<Surname> surnameArray = new ArrayList<>();
    public ArrayList<String> strSurnameArray = new ArrayList<>();
    public String surname_id = "";
    public Village village;
    public ArrayList<Village> villageArray = new ArrayList<>();
    public ArrayList<String> strVillageArray = new ArrayList<>();
    public String village_id = "", village_name = "";
    public BloodGroup bloodGroup;
    public ArrayList<BloodGroup> bloodGroupArray = new ArrayList<>();
    public ArrayList<String> strBloodGroupArray = new ArrayList<>();
    public String blood_group_id = "";
    public Spinner sp_surname, sp_village, sp_blood_group;
    public JsonParserUniversal jParser;
    public EditText et_mf_village, et_mf_surname, et_mmn, et_mfn, et_gmn, et_gfn, et_mn, et_fn, et_education, et_mobile, et_middle_name, et_first_name;
    Uri selectedUri;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_one);
        instance = RegisterActivity.this;
        jParser = new JsonParserUniversal();
        toolbar = findViewById(R.id.toolbar);
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_profile.setVisibility(View.GONE);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("REGISTER");
        Validation();
        surname = new Surname();
        village = new Village();
        bloodGroup = new BloodGroup();
        openDailog();
    }

    private void openDailog() {
        final Dialog dialog = new Dialog(instance);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.alert_registration);
        ImageView imgClose = dialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });
        dialog.show();
    }

    private void Validation() {
        et_mf_village = findViewById(R.id.et_mf_village);
        et_mf_surname = findViewById(R.id.et_mf_surname);
        et_mmn = findViewById(R.id.et_mmn);
        et_mfn = findViewById(R.id.et_mfn);
        et_gmn = findViewById(R.id.et_gmn);
        et_gfn = findViewById(R.id.et_gfn);
        et_mn = findViewById(R.id.et_mn);
        et_fn = findViewById(R.id.et_fn);
        et_education = findViewById(R.id.et_education);
        et_mobile = findViewById(R.id.et_mobile);
        et_middle_name = findViewById(R.id.et_middle_name);
        et_first_name = findViewById(R.id.et_first_name);
        img_profile_local = findViewById(R.id.img_profile_local);
        sp_surname = findViewById(R.id.sp_surname);
        sp_village = findViewById(R.id.sp_village);
        sp_blood_group = findViewById(R.id.sp_blood_group);
        tv_dob = findViewById(R.id.tv_dob);
        float_right_button = findViewById(R.id.float_right_button);

        img_profile_local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });

        new CallRequest(instance).getSurnameList();
        new CallRequest(instance).getVillageList();
        new CallRequest(instance).getBloodGroup();

        sp_surname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        surname_id = String.valueOf(surnameArray.get(position).getmSurnameid());
                        surname_name = surnameArray.get(position).getSurname();
                    } else {
                        surname_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sp_village.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        village_id = String.valueOf(villageArray.get(position).getVillageID());
                        village_name = villageArray.get(position).getVillagename();

                    } else {
                        village_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        sp_blood_group.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        blood_group_id = String.valueOf(bloodGroupArray.get(position).getBloodGroupID());

                    } else {
                        blood_group_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        tv_dob.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                    // TODO Auto-generated method stub
                                    myCalendar.set(Calendar.YEAR, year);
                                    myCalendar.set(Calendar.MONTH, monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                    String myFormat = "dd/MM/yyyy"; //In which you need put here
                                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                    tv_dob.setText(sdf.format(myCalendar.getTime()));

                                }
                            };
                            DatePickerDialog datePickerDialog = new DatePickerDialog(instance, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                            myCalendar.set(Calendar.HOUR_OF_DAY, myCalendar.getMinimum(Calendar.HOUR_OF_DAY));
                            myCalendar.set(Calendar.MINUTE, myCalendar.getMinimum(Calendar.MINUTE));
                            myCalendar.set(Calendar.SECOND, myCalendar.getMinimum(Calendar.SECOND));
                            myCalendar.set(Calendar.MILLISECOND, myCalendar.getMinimum(Calendar.MILLISECOND));
                            Calendar temp = Calendar.getInstance();
                            datePickerDialog.getDatePicker().setMaxDate(temp.getTimeInMillis());
                            datePickerDialog.show();

                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return true;
            }
        });


        float_right_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.registerData = new RegisterData();

                if (et_first_name.getText().toString().isEmpty()) {
                    et_first_name.setFocusable(true);
                    et_first_name.setError("Please enter first name");

                }/* else if (et_fn.getText().toString().isEmpty()) {
                    et_fn.setFocusable(true);
                    et_fn.setError("Please enter Middle name");

                }*/ else if (et_middle_name.getText().toString().isEmpty()) {
                    et_middle_name.setFocusable(true);
                    et_middle_name.setError("Please enter Middle name");

                } else if (surname_id.equalsIgnoreCase("") || surname_id.equals("0")) {
                    Utils.showToast("Please select surname", instance);
                } else if (village_id.equalsIgnoreCase("") || village_id.equals("0")) {
                    Utils.showToast("Please select village", instance);
                } else if (tv_dob.getText().toString().isEmpty()) {
                    Utils.showToast("Please select date of birth", instance);

                } else if (et_mobile.getText().toString().isEmpty()) {
                    et_mobile.setFocusable(true);
                    et_mobile.setError("Please enter mobile number");

                } else if (et_mn.getText().toString().isEmpty()) {
                    et_mn.setFocusable(true);
                    et_mn.setError("Please enter mother name");

                } else if (et_gfn.getText().toString().isEmpty()) {
                    et_gfn.setFocusable(true);
                    et_gfn.setError("Please enter Grand father name");

                } else if (et_gmn.getText().toString().isEmpty()) {
                    et_gmn.setFocusable(true);
                    et_gmn.setError("Please enter Grand mother name ");

                } else if (et_mfn.getText().toString().isEmpty()) {
                    et_mfn.setFocusable(true);
                    et_mfn.setError("Please enter mother's father name");

                } else if (et_mmn.getText().toString().isEmpty()) {
                    et_mmn.setFocusable(true);
                    et_mmn.setError("Please enter mother's mother name");

                } else if (TextUtils.isEmpty(et_mf_surname.getText().toString().trim())) {
                    et_mf_surname.setFocusable(true);
                    et_mf_surname.setError("Please enter mother's father surname");

                } else if (et_mf_village.getText().toString().isEmpty()) {
                    et_mf_village.setFocusable(true);
                    et_mf_village.setError("Please enter mother's father village");

                } else if (logoPath.equalsIgnoreCase("")) {
                    Utils.showToast("Please choose image", instance);

                } else {
                    new CallRequest(instance).Validatenumber(et_mobile.getText().toString());
                }
                // startActivity(new Intent(instance, RegisterOneActivity.class));
            }
        });


    }

    public void selectImage() {
        CropImage.startPickImageActivity(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                selectedUri = imageUri;
                logoPath = selectedUri.getPath();
                filename = (logoPath.substring(logoPath.lastIndexOf('/') + 1));

                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    img_profile_local.setImageURI(result.getUri());
                    selectedUri = result.getUri();
                    logoPath = selectedUri.getPath();
                    filename = (logoPath.substring(logoPath.lastIndexOf('/') + 1));
                    Log.i("TAG", "logoPath :-> " + logoPath);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(true)

                .setMultiTouchEnabled(true)
                .start(this);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case getSurnameList:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strSurnameArray.clear();
                            surnameArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                surname.setSurname(getResources().getString(R.string.surname));
                                surname.setmSurnameid(0);
                                strSurnameArray.add(surname.getSurname());
                                surnameArray.add(surname);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        surname = (Surname) jParser.parseJson(jSurnameArray.getJSONObject(i), new Surname());
                                        surnameArray.add(surname);
                                        strSurnameArray.add(surname.getSurname());
                                    }
                                    sp_surname.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strSurnameArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getVillageList:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strVillageArray.clear();
                            villageArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                village.setVillagename(getResources().getString(R.string.village));
                                village.setVillageID(0);
                                strVillageArray.add(village.getVillagename());
                                villageArray.add(village);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        village = (Village) jParser.parseJson(jSurnameArray.getJSONObject(i), new Village());
                                        villageArray.add(village);
                                        strVillageArray.add(village.getVillagename());
                                    }
                                    sp_village.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strVillageArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getBloodGroup:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strBloodGroupArray.clear();
                            bloodGroupArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                bloodGroup.setBloodGroupName(getResources().getString(R.string.bloodgroup));
                                bloodGroup.setBloodGroupID(0);
                                strBloodGroupArray.add(bloodGroup.getBloodGroupName());
                                bloodGroupArray.add(bloodGroup);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        bloodGroup = (BloodGroup) jParser.parseJson(jSurnameArray.getJSONObject(i), new BloodGroup());
                                        bloodGroupArray.add(bloodGroup);
                                        strBloodGroupArray.add(bloodGroup.getBloodGroupName());
                                    }
                                    sp_blood_group.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strBloodGroupArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case Validatenumber:
                    Utils.hideProgressDialog();
                    JSONObject jObj;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            App.registerData.setFirstname(et_first_name.getText().toString());
                            App.registerData.setMiddlename(et_middle_name.getText().toString());
                            App.registerData.setSurname(surname_name);
                            App.registerData.setNativePlace(village_name);
                            App.registerData.setPhotoUrl(logoPath);
                            App.registerData.setFilename(filename);
                            App.registerData.setDob(tv_dob.getText().toString());
                            App.registerData.setBloodgroupid(blood_group_id);
                            App.registerData.setContactno(et_mobile.getText().toString());
                            App.registerData.setEducation(et_education.getText().toString());
                            App.registerData.setCountrycode("+91");
                            //App.registerData.setFathername(et_fn.getText().toString());
                            App.registerData.setMothername(et_mn.getText().toString());
                            App.registerData.setMmothername(et_mmn.getText().toString());
                            App.registerData.setGrandfathername(et_gfn.getText().toString());
                            App.registerData.setGrandmothername(et_gmn.getText().toString());
                            App.registerData.setMfathername(et_mfn.getText().toString().trim());
                            App.registerData.setMfathersurname(et_mf_surname.getText().toString().trim());
                            App.registerData.setMfathervillage(et_mf_village.getText().toString());
                            startActivity(new Intent(instance, RegisterOneActivity.class));

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }
}
