package com.skgjst.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.BottomBarTab;
import com.roughike.bottombar.OnTabSelectListener;
import com.skgjst.App;
import com.skgjst.BaseActivity;
import com.skgjst.R;
import com.skgjst.activities.business.BusinessSearchActivity;
import com.skgjst.activities.events.EventsActivity;
import com.skgjst.activities.job.JobListActivity;
import com.skgjst.activities.matrimonial.MatrimonialSearchActivity;
import com.skgjst.activities.membersearch.MemberSearchActivity;
import com.skgjst.activities.oraganization.OraganizationListActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.fragment.AnniversaryFragment;
import com.skgjst.fragment.BirthdayFragment;
import com.skgjst.fragment.DemiseFragment;
import com.skgjst.fragment.HomeFragment;
import com.skgjst.fragment.NotificationFragment;
import com.skgjst.model.BusinessAd;
import com.skgjst.model.BusinessAdvertiseMent;
import com.skgjst.model.NotiCount;
import com.skgjst.model.Profile;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.FragmentDrawer;
import com.skgjst.utils.ImagePopup;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class DashBoardActivity extends BaseActivity implements FragmentDrawer.FragmentDrawerListener, AsynchTaskListner {
    private static final String TAG = "DashBoardActivity";
    public static BottomBar bottomBar;
    public static TextView tv_title;
    public Toolbar toolbar;
    public DashBoardActivity instance;
    public RelativeLayout mainLay;
    public ImageView drawerButton;
    public JsonParserUniversal jParser;
    public CircleImageView img_profile_local;
    public ImageView img_logout;
    public AppBarLayout rel_header;
    public CircleImageView nav_img_profile;
    public TextView nav_username, nav_email;
    public ProgressBar prgAdvertise1;
    public NotiCount notiObj;
    public BottomBarTab tab_birthday, tab_anniversary, tab_demise, tab_notification;
    private FragmentDrawer drawerFragment;
    public ImageView imgAdvertise;
    public Handler handler;
    public int currentGoldPos;
    public Runnable mRunnable;
    TelephonyManager telephonyManager;
    public String imeiNumber = "";
    public static FrameLayout frame_slecte;
    public static ImageView tv_mydemise;

    public void changeFragment(Fragment fragment) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, fragment);
        // transaction.commit();
        transaction.commitAllowingStateLoss();
    }

    public void changeFragment(Fragment fragment, boolean doAddToBackStack) {


        // FragmentTransaction transaction = manager.beginTransaction();

//        transaction.add(R.id.frame, fragment);
//        transaction.addToBackStack(null);
//        //setBackButton();


    }

    @SuppressLint("MissingPermission")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        instance = this;
        jParser = new JsonParserUniversal();
        App.isAdded = false;

        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

    /*    imeiNumber = Settings.Secure.getString(this.getContentResolver(),
                InstanceID.getInstance(this).getId());*/
        if (telephonyManager != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                // mySharedPref.setImeiNumber(telephonyManager.getImei());
                imeiNumber = telephonyManager.getImei();
            } else {
                imeiNumber = telephonyManager.getDeviceId();
            }
        }
//        imeiNumber = telephonyManager.getDeviceId();
       /* try {
            PackageInfo pInfo = instance.getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            int verCode = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }*/

        Toolbar toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        img_profile_local = toolbar.findViewById(R.id.img_profile);


        nav_img_profile = findViewById(R.id.nav_img_profile);
        nav_email = findViewById(R.id.nav_email);
        frame_slecte = findViewById(R.id.frame_slecte);
        tv_mydemise = findViewById(R.id.tv_mydemise);
        nav_username = findViewById(R.id.nav_username);
        imgAdvertise = findViewById(R.id.imgAdvertise);
        prgAdvertise1 = (ProgressBar) findViewById(R.id.prgAdvertise);

        img_logout = findViewById(R.id.img_logout);
        rel_header = findViewById(R.id.rel_header);

        setSupportActionBar(toolbar);
        mainLay = findViewById(R.id.mainLay);

        if (user != null) {
            nav_username.setText(user.getFName() + " " + user.getMName() + " " + user.getSurName());
            nav_email.setText(user.getEmailID());
        }

        rel_header.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });
        img_profile_local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(instance, MyProfileActivity.class);
                //    startActivity(new Intent(instance, MyProfileActivity.class));
                startActivity(i);

            }
        });

        drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerButton = findViewById(R.id.drawerButton);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), drawerButton, mainLay);
        drawerFragment.setDrawerListener(this);

        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), drawerButton, mainLay);
        drawerFragment.setDrawerListener(this);
        bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        bottomBar.setVisibility(View.VISIBLE);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                if (tabId == R.id.tab_home) {
                    tv_mydemise.setVisibility(View.GONE);
                    frame_slecte.setVisibility(View.VISIBLE);
                    App.isHome = true;

                    changeFragment(new HomeFragment());
                }
                if (tabId == R.id.tab_birthday) {

                    tv_mydemise.setVisibility(View.GONE);
                    frame_slecte.setVisibility(View.VISIBLE);
                    changeFragment(new BirthdayFragment());
                    App.birthdayCount = 0;
                    tab_birthday.setBadgeCount(0);
                    App.isHome = false;
                    new CallRequest(instance).updateNotifications(user, String.valueOf(App.birthdayID), "Birthday");
                }
                if (tabId == R.id.tab_anniversary) {

                    tv_mydemise.setVisibility(View.GONE);
                    frame_slecte.setVisibility(View.VISIBLE);
                    changeFragment(new AnniversaryFragment());
                    App.anniCount = 0;
                    tab_anniversary.setBadgeCount(0);
                    App.isHome = false;
                    new CallRequest(instance).updateNotifications(user, String.valueOf(App.anniID), "Anniversary");
                }

                if (tabId == R.id.tab_demise) {

                    tv_mydemise.setVisibility(View.VISIBLE);
                    frame_slecte.setVisibility(View.GONE);
                    changeFragment(new DemiseFragment());
                    App.demiseCount = 0;
                    tab_demise.setBadgeCount(0);
                    App.isHome = false;
                    new CallRequest(instance).updateNotifications(user, String.valueOf(App.demiseID), "Demise");
                }
                if (tabId == R.id.tab_notification) {

                    tv_mydemise.setVisibility(View.GONE);
                    frame_slecte.setVisibility(View.VISIBLE);
                    changeFragment(new NotificationFragment());
                    App.notiCount = 0;
                    App.isHome = false;
                    tab_notification.setBadgeCount(0);
                    new CallRequest(instance).updateNotifications(user, String.valueOf(App.notiID), "General");
                }

            }
        });
        tab_birthday = bottomBar.getTabWithId(R.id.tab_birthday);
        tab_anniversary = bottomBar.getTabWithId(R.id.tab_anniversary);
        tab_demise = bottomBar.getTabWithId(R.id.tab_demise);
        tab_notification = bottomBar.getTabWithId(R.id.tab_notification);

        img_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doLogout();
            }
        });

        if (user.getFamilyDetailID() != 0) {
            new CallRequest(instance).getcount(user.getFamilyDetailID());
            new CallRequest(instance).getMyprofilesActiviti(user.getFamilyDetailID());
        }


        new CallRequest(instance).getBusinessAd();
        if (App.isHome)
            new CallRequest(instance).Business_Advertisements();
        Utils.setRoundedImage(instance, user.getPhotoURL(), 300, 300, nav_img_profile, null);
        Utils.setRoundedImage(instance, user.getPhotoURL(), 300, 300, img_profile_local, null);
    }

    public void adSet() {
        handler = new Handler();
        mRunnable = new Runnable() {
            public void run() {
                try {
                    if (App.goldAdvertiseMent.size() > 0) {
                        if (currentGoldPos >= App.goldAdvertiseMent.size() - 1) {
                            currentGoldPos = 0;
                        } else {
                            currentGoldPos++;
                        }


                        Animation fadeIn = AnimationUtils.loadAnimation(instance, R.anim.fed_in);
                        prgAdvertise1.startAnimation(fadeIn);
                        if (App.platinumAdvertiseMent.size() > 0) {
                            prgAdvertise1.setVisibility(View.VISIBLE);
                            Utils.setRoundedImage(instance, App.platinumAdvertiseMent.get(currentGoldPos).getFilepath(), 572, 572, imgAdvertise, prgAdvertise1);
                            final ImagePopup imagePopup = new ImagePopup(instance);
                            imagePopup.setWindowHeight(800); // Optional
                            imagePopup.setWindowWidth(800); // Optional
                            imagePopup.setBackgroundColor(Color.BLACK);  // Optional
                            imagePopup.setFullScreen(true); // Optional
                            imagePopup.setHideCloseIcon(true);  // Optional
                            imagePopup.setImageOnClickClose(true);  // Optional


                            imagePopup.initiatePopupWithPicasso(App.platinumAdvertiseMent.get(currentGoldPos).getFilepath());

                            imgAdvertise.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    /** Initiate Popup view **/
                                    imagePopup.viewPopup();

                                }
                            });
                        }
                        fadeIn.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                handler.postDelayed(mRunnable, 5000);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        handler.postDelayed(mRunnable, 10);
    }

    @Override
    protected void onResume() {
        super.onResume();

      /*  new CallRequest(instance).getBusinessAd();
        if (App.isHome)
            new CallRequest(instance).Business_Advertisements();
        Utils.setRoundedImage(instance, user.getPhotoURL(), 300, 300, nav_img_profile, null);
        Utils.setRoundedImage(instance, user.getPhotoURL(), 300, 300, img_profile_local, null);*/


    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    public void displayView(int position) {

        switch (position) {
            case 0:
                bottomBar.selectTabAtPosition(0);
                bottomBar.setVisibility(View.VISIBLE);
                App.isHome = true;
                if (!new HomeFragment().isVisible())
                    changeFragment(new HomeFragment());
                break;
            case 1:
                startActivity(new Intent(instance, AboutusActivity.class));
                break;

            case 2:
                startActivity(new Intent(instance, MemberSearchActivity.class));
                /*.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)*/
                break;
            case 3:
                startActivity(new Intent(instance, BusinessSearchActivity.class));
                break;
            case 4:
                startActivity(new Intent(instance, MatrimonialSearchActivity.class));
                break;
            case 5:
                startActivity(new Intent(instance, EventsActivity.class));
                break;
            case 6:
                startActivity(new Intent(instance, JobListActivity.class));
                break;
            case 7:
                // startActivity(new Intent(instance, PrathanaListActivity.class));
                bottomBar.setVisibility(View.GONE);
                changeFragment(new DemiseFragment());
                break;
            case 8:
                startActivity(new Intent(instance, OraganizationListActivity.class));
                break;
            case 9:
                startActivity(new Intent(instance, NewsActivity.class));
                break;
            case 10:
                startActivity(new Intent(instance, SponeserAdActivity.class));
                break;
            case 11:
                startActivity(new Intent(instance, KutchGurjariActivity.class));
                break;

            case 12:
                startActivity(new Intent(instance, RitulsActivity.class));
                break;
            case 13:
                startActivity(new Intent(instance, ChangePassActivity.class));

                break;
            case 14:
                startActivity(new Intent(instance, ContactActivity.class));

                break;
          /*  case 14:
                bottomBar.setVisibility(View.GONE);
                changeFragment(new LanguageSettingFragment());
                break;*/

            default:
                break;
        }

    }

    @Override
    public void onBackPressed() {
        if (drawerFragment.mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerFragment.mDrawerLayout.closeDrawer(Gravity.LEFT);
        }
        FragmentManager manager = getSupportFragmentManager();

        if (manager.getBackStackEntryCount() == 1) {

            manager.popBackStack();
        } else if (bottomBar.getCurrentTabPosition() != 0) {
            bottomBar.selectTabAtPosition(0);
        } else {
            new AlertDialog.Builder(this)
                    .setTitle("Really Exit?")
                    .setMessage("Are you sure you want to exit?")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {
                            App.isfirst = false;
                            finish();
                            finishAffinity();
                            moveTaskToBack(true);
                            mySharedPref.setIsAdShow(false);

                        }
                    }).create().show();
        }


    }

    public void doLogout() {
        if (drawerFragment.mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            drawerFragment.mDrawerLayout.closeDrawer(Gravity.LEFT);
        }

        new AlertDialog.Builder(this)
                .setTitle("Logout?")
                .setMessage("Are you sure for logout?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        new CallRequest(instance).LogOutStatus(user.getMUserID(), mySharedPref.getSecureLoginID(), imeiNumber);

                    }
                }).create().show();

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            //    Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case logOutStatus:
                    Utils.hideProgressDialog();
                    JSONObject jObj = null;
                    try {
                        jObj = new JSONObject(result);


                        if (jObj.getInt("LogOutStatus") == 1) {
                            mySharedPref.clearApp();
                            mySharedPref.setIsAdShow(false);
                            Intent i = new Intent(instance, LoginActivity.class);
                            startActivity(i);
                            finish();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case Business_Advertisements:
                    try {
                        Log.i("TAG", "TAG Result : " + result);
                        jObj = new JSONObject(result);
                        App.platinumAdvertiseMent.clear();
                        App.goldAdvertiseMent.clear();
                        App.businessAdvertiseMent.clear();
                        App.diamondAdvertiseMent.clear();
                        if (jObj.getBoolean("Success")) {
                            JSONObject jsonData = jObj.getJSONObject("data");

                            JSONArray PlatinumAds = jsonData.getJSONArray("PlatinumAds");
                            for (int i = 0; i < PlatinumAds.length(); i++) {
                                JSONObject obj = PlatinumAds.getJSONObject(i);
                                BusinessAdvertiseMent businessAdvertiseMent = (BusinessAdvertiseMent) jParser.parseJson(obj, new BusinessAdvertiseMent());
                                App.platinumAdvertiseMent.add(businessAdvertiseMent);
                            }

                            JSONArray DiamondAds = jsonData.getJSONArray("DiamondAds");
                            for (int i = 0; i < DiamondAds.length(); i++) {
                                JSONObject obj = DiamondAds.getJSONObject(i);
                                BusinessAdvertiseMent businessAdvertiseMent = (BusinessAdvertiseMent) jParser.parseJson(obj, new BusinessAdvertiseMent());
                                App.diamondAdvertiseMent.add(businessAdvertiseMent);
                            }
                            //  startDiamondAdAnimation();

                            JSONArray GoldAds = jsonData.getJSONArray("GoldAds");
                            for (int i = 0; i < GoldAds.length(); i++) {
                                JSONObject obj = GoldAds.getJSONObject(i);
                                BusinessAdvertiseMent businessAdvertiseMent = (BusinessAdvertiseMent) jParser.parseJson(obj, new BusinessAdvertiseMent());
                                App.goldAdvertiseMent.add(businessAdvertiseMent);
                            }
                            // startGoldAdAnimation();

                            JSONArray BusinessAds = jsonData.getJSONArray("BusinessAds");
                            for (int i = 0; i < BusinessAds.length(); i++) {
                                JSONObject obj = BusinessAds.getJSONObject(i);
                                BusinessAdvertiseMent businessAdvertiseMent = (BusinessAdvertiseMent) jParser.parseJson(obj, new BusinessAdvertiseMent());
                                App.businessAdvertiseMent.add(businessAdvertiseMent);
                            }

                            //openBusinessDialog();

                            /*DialogFragmentWindow drg = new DialogFragmentWindow();
                            drg.setFragmentManager(getActivity().gets);
                            drg.show(getActivity().getFragmentManager(),"");*/

                        }
                        if (drawerFragment != null && drawerFragment.adapter != null) {
                            drawerFragment.adapter.notifyDataSetChanged();
                        }
                        if (App.isHome && !new HomeFragment().isVisible()) {
                            changeFragment(new HomeFragment());
                        }

                        adSet();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case getBirthdayWishes:
                    Utils.hideProgressDialog();
                    jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            Utils.showToast(jObj.getString("UMessage"), this);
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case getcount:
                    Utils.hideProgressDialog();
                    jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("Count") != null && jObj.getJSONArray("Count").length() > 0) {
                                JSONArray userData = jObj.getJSONArray("Count");
                                for (int i = 0; i < userData.length(); i++) {
                                    JSONObject obj = userData.getJSONObject(i);
                                    notiObj = (NotiCount) jParser.parseJson(obj, new NotiCount());

                                    if (notiObj.getNotification_type().equalsIgnoreCase("Birthday")) {
                                        tab_birthday.setBadgeCount(notiObj.getCountvalue());
                                        App.birthdayCount = notiObj.getCountvalue();
                                        App.birthdayID = notiObj.getNotid();

                                    }
                                    if (notiObj.getNotification_type().equalsIgnoreCase("Anniversary")) {
                                        tab_anniversary.setBadgeCount(notiObj.getCountvalue());
                                        App.anniCount = notiObj.getCountvalue();

                                        App.anniID = notiObj.getNotid();
                                    }
                                    if (notiObj.getNotification_type().equalsIgnoreCase("General")) {
                                        tab_notification.setBadgeCount(notiObj.getCountvalue());
                                        App.notiCount = notiObj.getCountvalue();
                                        App.notiID = notiObj.getNotid();
                                    }
                                    if (notiObj.getNotification_type().equalsIgnoreCase("Demise")) {
                                        tab_demise.setBadgeCount(notiObj.getCountvalue());
                                        App.demiseID = notiObj.getNotid();
                                        App.demiseCount = notiObj.getCountvalue();
                                    }
                                }


                             /*   BottomBarTab tab_anniversary = bottomBar.getTabWithId(R.id.tab_anniversary);
                                tab_anniversary.setBadgeCount(5);
                                BottomBarTab tab_demise = bottomBar.getTabWithId(R.id.tab_demise);
                                tab_demise.setBadgeCount(5);
                                BottomBarTab tab_notification = bottomBar.getTabWithId(R.id.tab_notification);
                                tab_notification.setBadgeCount(0);

*/
                            } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("UMessage"), instance);
                                //   lin_empty.setVisibility(View.VISIBLE);
                                //    rv_product_list.setVisibility(View.GONE);
                            }

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case getDimondAd:
                    Utils.hideProgressDialog();
                    jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            App.businessAdArrayList.clear();
                            if (jObj.getJSONArray("SponsoredAdvertisements") != null && jObj.getJSONArray("SponsoredAdvertisements").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("SponsoredAdvertisements");

                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        BusinessAdvertiseMent businessAdvertiseMent = (BusinessAdvertiseMent) jParser.parseJson(jSurnameArray.getJSONObject(i), new BusinessAdvertiseMent());
                                        App.diamondAdvertiseMent.add(businessAdvertiseMent);

                                    }
                                    //  changeFragment(new HomeFragment());
                                } else {

                                    Utils.showToast(jObj.getString("UMessage"), instance);
                                }
                            }
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case getAnniversarywishes:
                    Utils.hideProgressDialog();
                    jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            Utils.showToast(jObj.getString("UMessage"), this);

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case updateNotifications:
                    Utils.hideProgressDialog();
                    jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            //  Utils.showToast(jObj.getString("UMessage"), this);

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case getWatched:
                    Utils.hideProgressDialog();
                    jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            //   Utils.showToast(jObj.getString("UMessage"), this);

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case getBusinessAd:
                    Utils.hideProgressDialog();
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            App.businessAdArrayList.clear();
                            if (jObj.getJSONArray("BusinessAds") != null && jObj.getJSONArray("BusinessAds").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("BusinessAds");

                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        BusinessAd businessList = (BusinessAd) jParser.parseJson(jSurnameArray.getJSONObject(i), new BusinessAd());
                                        App.businessAdArrayList.add(businessList);
                                    }
                                    // changeFragment(new HomeFragment());
                                } else {

                                    Utils.showToast(jObj.getString("UMessage"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("UMessage"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;

                case getMyprofiles:
                    try {
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {

                            if (jObj.getJSONArray("Profile") != null && jObj.getJSONArray("Profile").length() > 0) {
                                JSONArray userData = jObj.getJSONArray("Profile");
                                Log.d(TAG, "onTaskCompleted: " + userData.toString());
                                for (int i = 0; i < userData.length(); i++) {
                                    JSONObject jobj = userData.getJSONObject(i);
                                    Profile obj = (Profile) jParser.parseJson(jobj, new Profile());

                                    user.setPhotoURL(obj.getPhotoURL());
                                }
                                Utils.setRoundedImage(instance, user.getPhotoURL(), 300, 300, nav_img_profile, null);
                                Utils.setRoundedImage(instance, user.getPhotoURL(), 300, 300, img_profile_local, null);

                                Utils.hideProgressDialog();
                            } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("UMessage"), instance);
                            }
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), instance);
                        }
                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        Utils.showToast("Something getting wrong! Please try again later.", instance);

                        e.printStackTrace();
                    }
            }
        }


    }

   /* @Override
    public void birthdayWish(String messageText, int familydetailid) {
        new CallRequest(this).getBirthdayWishes(messageText, familydetailid);

    }*/

   /* @Override
    public void anniversaryWish(String messageText, int HFamilydetailID, int WFamilyDetailID) {
        new CallRequest(this).getAnniversarywishes(messageText, HFamilydetailID, WFamilyDetailID);

    }
*/
}