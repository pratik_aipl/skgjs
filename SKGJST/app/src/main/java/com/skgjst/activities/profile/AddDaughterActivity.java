package com.skgjst.activities.profile;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.R;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.fonts.MyCustomTypeface;
import com.skgjst.model.MaritalStatus;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddDaughterActivity extends BaseActivity implements AsynchTaskListner {
    public Spinner sp_mat_status;
    public Toolbar toolbar;
    public AddDaughterActivity instance;
    public JsonParserUniversal jParser;
    public TextView tv_title, tv_father_village, tv_father_surname, tv_father_name;
    public MaritalStatus maritalStatus;
    public ArrayList<MaritalStatus> maritalStatusArray = new ArrayList<>();
    public ArrayList<String> strMaritalStatusArray = new ArrayList<>();
    public String marital_status_id = "";
    public ProgressBar pbar;
    public CircleImageView img_profile, img_profile_local;
    public EditText et_father_village, et_father_surname, et_father_name, et_daughter_name,
            et_husbund_name, et_husbund_surname, et_husbund_father_name,
            et_mother_name, et_husbund_village, et_son_father_name, et_son_father_surname, et_son_father_village;
    ;
    public Button btn_submit;
    public LinearLayout lin_married, linear, linear_divorced;
    public String FamilyDetailID, Name, Gender, FatherSurname, FathersVillage, FatherName, Maritalstatus;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_daughter);
        instance = AddDaughterActivity.this;
        jParser = new JsonParserUniversal();
        pbar = findViewById(R.id.pbar);
        FamilyDetailID = getIntent().getStringExtra("FamilyDetailId");
        Name = getIntent().getStringExtra("Name");
        FatherName = getIntent().getStringExtra("FatherName");
        Gender = getIntent().getStringExtra("Gender");
        FatherSurname = getIntent().getStringExtra("FatherSurname");
        FathersVillage = getIntent().getStringExtra("FathersVillage");
        Maritalstatus = getIntent().getStringExtra("Maritalstatus");
        lin_married = findViewById(R.id.lin_married);
        et_father_village = findViewById(R.id.et_father_village);
        et_father_surname = findViewById(R.id.et_father_surname);
        et_father_name = findViewById(R.id.et_father_name);
        et_daughter_name = findViewById(R.id.et_daughter_name);
        et_husbund_name = findViewById(R.id.et_husbund_name);
        et_husbund_surname = findViewById(R.id.et_husbund_surname);
        et_husbund_father_name = findViewById(R.id.et_husbund_father_name);
        et_mother_name = findViewById(R.id.et_mother_name);
        et_husbund_village = findViewById(R.id.et_husbund_village);
        et_son_father_name = findViewById(R.id.et_son_father_name);
        linear_divorced = findViewById(R.id.linear_divorced);
        et_son_father_village = findViewById(R.id.et_son_father_village);
        et_son_father_village = findViewById(R.id.et_son_father_village);
        et_son_father_surname = findViewById(R.id.et_son_father_surname);
        sp_mat_status = findViewById(R.id.sp_mat_status);
        btn_submit = findViewById(R.id.btn_submit);
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("ADD DAUGHTER");
        img_profile = toolbar.findViewById(R.id.img_profile);
        linear = findViewById(R.id.linear);
        tv_father_village = findViewById(R.id.tv_father_village);
        tv_father_surname = findViewById(R.id.tv_father_surname);
        tv_father_name = findViewById(R.id.tv_father_name);

        Utils.setRoundedImage(instance, user.getPhotoURL(), 300, 300, img_profile, pbar);
        new CallRequest(instance).getMaritalStatus();
        maritalStatus = new MaritalStatus();

        tv_father_name.setText(Name + " Father Name");
        tv_father_surname.setText(Name + " Father Surname");
        tv_father_village.setText(Name + " Father Village");

        if (Gender.equalsIgnoreCase("Male")) {
            et_father_surname.setVisibility(View.GONE);
            et_father_village.setVisibility(View.GONE);
            tv_father_surname.setVisibility(View.GONE);
            tv_father_village.setVisibility(View.GONE);
            linear.setVisibility(View.GONE);
        } else {
            linear.setVisibility(View.VISIBLE);
            et_father_surname.setVisibility(View.VISIBLE);
            et_father_village.setVisibility(View.VISIBLE);
            tv_father_surname.setVisibility(View.VISIBLE);
            tv_father_village.setVisibility(View.VISIBLE);

        }
        if (!FatherName.equalsIgnoreCase("") && FatherName != null) {
            et_father_name.setText(FatherName);
            et_father_name.setClickable(false);
            et_father_name.setFocusable(false);
            et_father_name.setFocusableInTouchMode(false);
        }

        if (!FatherSurname.equalsIgnoreCase("") && FatherSurname != null) {
            et_father_surname.setText(FatherSurname);
            et_father_surname.setClickable(false);
            et_father_surname.setFocusable(false);
            et_father_surname.setFocusableInTouchMode(false);
        }
        if (!FathersVillage.equalsIgnoreCase("") && FathersVillage != null) {
            et_father_village.setText(FathersVillage);
            et_father_village.setClickable(false);
            et_father_village.setFocusable(false);
            et_father_village.setFocusableInTouchMode(false);
        }
        if (Gender.equalsIgnoreCase("Female") && Maritalstatus.equalsIgnoreCase("Divorced")) {
            linear_divorced.setVisibility(View.VISIBLE);
        } else {
            linear_divorced.setVisibility(View.GONE);

        }
        sp_mat_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        marital_status_id = String.valueOf(maritalStatusArray.get(position).getMaritalstatusID());
                        if (marital_status_id.equalsIgnoreCase("5") || marital_status_id.equalsIgnoreCase("4")) {
                            lin_married.setVisibility(View.VISIBLE);
                        } else {
                            lin_married.setVisibility(View.GONE);
                        }
                    } else {
                        marital_status_id = "0";
                        lin_married.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_daughter_name.getText().toString().isEmpty()) {
                    et_daughter_name.requestFocus();
                    et_daughter_name.setError("Please enter daughter name");

                } else if (et_father_name.getText().toString().isEmpty()) {
                    et_father_name.requestFocus();
                    et_father_name.setError("Please enter father name");

                } else if (et_father_surname.getText().toString().isEmpty()) {
                    et_father_surname.requestFocus();
                    et_father_surname.setError("Please enter father surname");

                } else if (et_father_village.getText().toString().isEmpty()) {
                    et_father_village.requestFocus();
                    et_father_village.setError("Please enter father village");

                } else if (marital_status_id.equalsIgnoreCase("") || marital_status_id.equals("0")) {
                    Utils.showToast("Please select marital status", instance);
                } else {
                    if (Gender.equalsIgnoreCase("Female") && Maritalstatus.equalsIgnoreCase("Divorced")) {
                        if (et_son_father_name.getText().toString().isEmpty()) {
                            et_son_father_name.requestFocus();
                            et_son_father_name.setError("Please enter daughter's father name");
                            return;

                        } else if (et_son_father_village.getText().toString().isEmpty()) {
                            et_son_father_village.requestFocus();
                            et_son_father_village.setError("Please enter daughter's father village");
                            return;

                        } else if (et_son_father_surname.getText().toString().isEmpty()) {
                            et_son_father_surname.requestFocus();
                            et_son_father_surname.setError("Please enter daughter's father surname");
                            return;

                        } else {
                            new CallRequest(instance).addDaughterNew(user, et_daughter_name.getText().toString(), marital_status_id, et_father_name.getText().toString(),
                                    et_father_surname.getText().toString(), et_father_village.getText().toString(), et_husbund_name.getText().toString(), et_husbund_surname.getText().toString(),
                                    et_husbund_father_name.getText().toString(), et_mother_name.getText().toString(), et_husbund_village.getText().toString(), FamilyDetailID,
                                    et_son_father_name.getText().toString(), et_son_father_village.getText().toString(), et_son_father_surname.getText().toString());

                        }
                    } new CallRequest(instance).addDaughterNew(user, et_daughter_name.getText().toString(), marital_status_id, et_father_name.getText().toString(),
                            et_father_surname.getText().toString(), et_father_village.getText().toString(), et_husbund_name.getText().toString(), et_husbund_surname.getText().toString(),
                            et_husbund_father_name.getText().toString(), et_mother_name.getText().toString(), et_husbund_village.getText().toString(), FamilyDetailID,
                            et_son_father_name.getText().toString(), et_son_father_village.getText().toString(), et_son_father_surname.getText().toString());
            }
        }
    });


}

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case getMaritalStatus:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strMaritalStatusArray.clear();
                            maritalStatusArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                maritalStatus.setMaritalstatus(getResources().getString(R.string.MaritalStatus));
                                maritalStatus.setMaritalstatusID(0);
                                strMaritalStatusArray.add(maritalStatus.getMaritalstatus());
                                maritalStatusArray.add(maritalStatus);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        maritalStatus = (MaritalStatus) jParser.parseJson(jSurnameArray.getJSONObject(i), new MaritalStatus());
                                        maritalStatusArray.add(maritalStatus);
                                        strMaritalStatusArray.add(maritalStatus.getMaritalstatus());
                                    }
                                    sp_mat_status.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strMaritalStatusArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;


                case addDaughter:
                    Utils.hideProgressDialog();
                    JSONObject jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            Utils.showToast(jObj.getString("UMessage"), this);
                            onBackPressed();

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;


            }
        }
    }
}
