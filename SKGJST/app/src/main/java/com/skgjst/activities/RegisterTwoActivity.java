package com.skgjst.activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.skgjst.App;
import com.skgjst.BaseActivity;
import com.skgjst.fonts.MyCustomTypeface;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.City;
import com.skgjst.model.Country;
import com.skgjst.model.State;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class RegisterTwoActivity  extends BaseActivity implements AsynchTaskListner {
    public Toolbar toolbar;
    public TextView tv_title;
    public RegisterTwoActivity instance;
    public FloatingActionButton float_previous, float_next;
    public Spinner sp_state, sp_city, sp_country;
    public Country country;
    public ArrayList<Country> countryArray = new ArrayList<>();
    public ArrayList<String> strCountryArray = new ArrayList<>();
    public String country_id = "";
    public State states;
    public ArrayList<State> statesArray = new ArrayList<>();
    public ArrayList<String> strStatesArray = new ArrayList<>();
    public String states_id = "";
    public City city;
    public ArrayList<City> cityArray = new ArrayList<>();
    public ArrayList<String> strCityArray = new ArrayList<>();
    public String city_id = "";
    public JsonParserUniversal jParser;
    public EditText et_wing, et_flat_no, et_plot_no, et_building_name, et_road_name, et_land_mark,
            et_suburb_name, et_home_number, et_district, et_pincode;
    public CircleImageView img_profile;

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_two);
        instance = RegisterTwoActivity.this;
        jParser = new JsonParserUniversal();
        toolbar = findViewById(R.id.toolbar);
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_profile.setVisibility(View.GONE);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("REGISTER");
        float_previous = findViewById(R.id.float_previous);
        float_next = findViewById(R.id.float_next);

        float_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        country = new Country();
        states = new State();
        city = new City();
        Validation();
    }

    private void Validation() {
        et_wing = findViewById(R.id.et_wing);
        et_flat_no = findViewById(R.id.et_flat_no);
        et_plot_no = findViewById(R.id.et_plot_no);
        et_building_name = findViewById(R.id.et_building_name);
        et_road_name = findViewById(R.id.et_road_name);
        et_land_mark = findViewById(R.id.et_land_mark);
        et_suburb_name = findViewById(R.id.et_suburb_name);
        et_home_number = findViewById(R.id.et_home_number);
        et_district = findViewById(R.id.et_district);
        et_pincode = findViewById(R.id.et_pincode);
        sp_country = findViewById(R.id.sp_country);
        sp_state = findViewById(R.id.sp_state);
        sp_city = findViewById(R.id.sp_city);
        new CallRequest(instance).getCountry();
        sp_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        country_id = String.valueOf(countryArray.get(position).getCountryid());
                        new CallRequest(instance).getState(country_id);

                    } else {
                        country_id = "0";
                        states.setState("Select State");
                        states.setStateID(0);
                        strStatesArray.add(states.getState());
                        statesArray.add(states);
                        //   new CallRequest(instance).getState(country_id);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_state.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        states_id = String.valueOf(statesArray.get(position).getStateID());
                        new CallRequest(instance).getCity(states_id);

                    } else {
                        states_id = "0";
                        // new CallRequest(instance).getCity(states_id);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        city_id = String.valueOf(cityArray.get(position).getCityID());

                    } else {
                        city_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        float_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_building_name.getText().toString().isEmpty()) {
                    et_building_name.setFocusable(true);
                    et_building_name.setError("Please enter building name");

                } else if (et_road_name.getText().toString().isEmpty()) {
                    et_road_name.setFocusable(true);
                    et_road_name.setError("Please enter road name");
                } else if (country_id.equalsIgnoreCase("") || country_id.equals("0")) {
                    Utils.showToast("Please select country", instance);
                } else if (states_id.equalsIgnoreCase("") || states_id.equals("0")) {
                    Utils.showToast("Please select state", instance);
                } else if (city_id.equalsIgnoreCase("") || city_id.equals("0")) {
                    Utils.showToast("Please select city", instance);
                } else if (et_pincode.getText().toString().isEmpty()) {
                    et_pincode.setFocusable(true);
                    et_pincode.setError("Please enter pincode");
                } else {

                    App.registerData.setWing(et_wing.getText().toString());
                    App.registerData.setRoomno(et_flat_no.getText().toString());
                    App.registerData.setPlotNo(et_plot_no.getText().toString());
                    App.registerData.setBuildingname(et_building_name.getText().toString());
                    App.registerData.setRoadname(et_road_name.getText().toString());
                    App.registerData.setLandmark(et_land_mark.getText().toString());
                    App.registerData.setSuburbname(et_suburb_name.getText().toString());
                    App.registerData.setHomecontactno(et_home_number.getText().toString());
                    App.registerData.setCountryid(country_id);
                    App.registerData.setStateid(states_id);
                    App.registerData.setCityid(city_id);
                    App.registerData.setDistrictname(et_district.getText().toString());
                    App.registerData.setPincode(et_pincode.getText().toString());
                    startActivity(new Intent(instance, RegisterThreeActivity.class));
                }
            }
        });
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case getCountry:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strCountryArray.clear();
                            countryArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                country.setLocationName(getResources().getString(R.string.country));
                                country.setCountryid(0);
                                strCountryArray.add(country.getLocationName());
                                countryArray.add(country);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        country = (Country) jParser.parseJson(jSurnameArray.getJSONObject(i), new Country());
                                        countryArray.add(country);
                                        strCountryArray.add(country.getLocationName());
                                    }
                                    sp_country.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strCountryArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getState:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strStatesArray.clear();
                            statesArray.clear();
                            if (jObj.getJSONArray("StateList") != null && jObj.getJSONArray("StateList").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("StateList");

                                states.setState(getResources().getString(R.string.state));
                                states.setStateID(0);
                                strStatesArray.add(states.getState());
                                statesArray.add(states);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        states = (State) jParser.parseJson(jSurnameArray.getJSONObject(i), new State());
                                        statesArray.add(states);
                                        strStatesArray.add(states.getState());
                                    }
                                    sp_state.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strStatesArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getCity:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strCityArray.clear();
                            cityArray.clear();
                            if (jObj.getJSONArray("citylist") != null && jObj.getJSONArray("citylist").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("citylist");

                                city.setCity(getResources().getString(R.string.city));
                                city.setCityID(0);
                                strCityArray.add(city.getCity());
                                cityArray.add(city);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        city = (City) jParser.parseJson(jSurnameArray.getJSONObject(i), new City());
                                        cityArray.add(city);
                                        strCityArray.add(city.getCity());
                                    }
                                    sp_city.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strCityArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

}
