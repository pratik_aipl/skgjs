package com.skgjst.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.skgjst.App;
import com.skgjst.BaseActivity;
import com.skgjst.R;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.fonts.MyCustomTypeface;
import com.skgjst.model.MaritalStatus;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

public class RegisterOneActivity extends BaseActivity implements AsynchTaskListner {
    final Calendar myCalendar = Calendar.getInstance();
    public Toolbar toolbar;
    public TextView tv_title, et_mrg_date, et_male_mrg_date;
    public RegisterOneActivity instance;
    public JsonParserUniversal jParser;
    public Spinner sp_mat_status, sp_gender;
    public FloatingActionButton float_previous, float_next;
    public MaritalStatus maritalStatus;
    public ArrayList<MaritalStatus> maritalStatusArray = new ArrayList<>();
    public ArrayList<String> strMaritalStatusArray = new ArrayList<>();
    public String marital_status_id = "";
    public String selectGender = "", mrg_date = "";
    public LinearLayout lin_fm, lin_mm;
    public EditText et_wfvillage, et_wfsurname, et_smn, et_sfn, et_sn,
            et_fvillage,
            et_fsurname, et_fsmn, et_fsfn, et_fsn, etfn;
    public CircleImageView img_profile;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_one2);
        instance = RegisterOneActivity.this;
        jParser = new JsonParserUniversal();
        toolbar = findViewById(R.id.toolbar);
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_profile.setVisibility(View.GONE);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("REGISTER");
        Validation();
        maritalStatus = new MaritalStatus();
    }

    private void Validation() {
        et_fsmn = findViewById(R.id.et_fsmn);
        et_fsfn = findViewById(R.id.et_fsfn);
        et_fsn = findViewById(R.id.et_fsn);
        etfn = findViewById(R.id.et_fname);
        et_fsurname = findViewById(R.id.et_fsurname);
        et_fvillage = findViewById(R.id.et_fvillage);
        et_mrg_date = findViewById(R.id.et_mrg_date);
        et_sn = findViewById(R.id.et_sn);
        et_sfn = findViewById(R.id.et_sfn);
        et_smn = findViewById(R.id.et_smn);
        et_wfsurname = findViewById(R.id.et_wfsurname);
        et_wfvillage = findViewById(R.id.et_wfvillage);
        et_male_mrg_date = findViewById(R.id.et_male_mrg_date);
        sp_mat_status = findViewById(R.id.sp_mat_status);
        sp_gender = findViewById(R.id.sp_gender);
        lin_fm = findViewById(R.id.lin_fm);
        lin_mm = findViewById(R.id.lin_mm);
        float_previous = findViewById(R.id.float_previous);
        float_next = findViewById(R.id.float_next);

        float_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        et_mrg_date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                    // TODO Auto-generated method stub
                                    myCalendar.set(Calendar.YEAR, year);
                                    myCalendar.set(Calendar.MONTH, monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                    String myFormat = "dd/MM/yyyy"; //In which you need put here
                                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                    et_mrg_date.setText(sdf.format(myCalendar.getTime()));

                                }
                            };
                            DatePickerDialog datePickerDialog = new DatePickerDialog(instance, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                            myCalendar.set(Calendar.HOUR_OF_DAY, myCalendar.getMinimum(Calendar.HOUR_OF_DAY));
                            myCalendar.set(Calendar.MINUTE, myCalendar.getMinimum(Calendar.MINUTE));
                            myCalendar.set(Calendar.SECOND, myCalendar.getMinimum(Calendar.SECOND));
                            myCalendar.set(Calendar.MILLISECOND, myCalendar.getMinimum(Calendar.MILLISECOND));
                            Calendar temp = Calendar.getInstance();
                            datePickerDialog.getDatePicker().setMaxDate(temp.getTimeInMillis());
                            datePickerDialog.show();

                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return true;
            }
        });
        et_male_mrg_date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                      int dayOfMonth) {
                                    // TODO Auto-generated method stub
                                    myCalendar.set(Calendar.YEAR, year);
                                    myCalendar.set(Calendar.MONTH, monthOfYear);
                                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                                    String myFormat = "dd/MM/yyyy"; //In which you need put here
                                    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                                    et_male_mrg_date.setText(sdf.format(myCalendar.getTime()));

                                }
                            };
                            DatePickerDialog datePickerDialog = new DatePickerDialog(instance, date, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                            myCalendar.set(Calendar.HOUR_OF_DAY, myCalendar.getMinimum(Calendar.HOUR_OF_DAY));
                            myCalendar.set(Calendar.MINUTE, myCalendar.getMinimum(Calendar.MINUTE));
                            myCalendar.set(Calendar.SECOND, myCalendar.getMinimum(Calendar.SECOND));
                            myCalendar.set(Calendar.MILLISECOND, myCalendar.getMinimum(Calendar.MILLISECOND));
                            Calendar temp = Calendar.getInstance();
                            datePickerDialog.getDatePicker().setMaxDate(temp.getTimeInMillis());
                            datePickerDialog.show();

                            break;
                        case MotionEvent.ACTION_UP:
                            break;
                        default:
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return true;
            }
        });
        new CallRequest(instance).getMaritalStatus();

        sp_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        if (position == 1) {
                            selectGender = "M";
                        }
                        if (position == 2) {
                            selectGender = "F";
                        }
                        if (selectGender.equalsIgnoreCase("F") && marital_status_id.equalsIgnoreCase("5")) {
                            lin_fm.setVisibility(View.VISIBLE);
                            lin_mm.setVisibility(View.GONE);
                        } else if (selectGender.equalsIgnoreCase("F") && marital_status_id.equalsIgnoreCase("4")) {
                            lin_fm.setVisibility(View.VISIBLE);
                            lin_mm.setVisibility(View.GONE);
                        } else if (selectGender.equalsIgnoreCase("M") && marital_status_id.equalsIgnoreCase("5")) {
                            lin_mm.setVisibility(View.VISIBLE);
                            lin_fm.setVisibility(View.GONE);
                        } else if (selectGender.equalsIgnoreCase("M") && marital_status_id.equalsIgnoreCase("4")) {
                            lin_mm.setVisibility(View.VISIBLE);
                            lin_fm.setVisibility(View.GONE);
                        } else {
                            lin_fm.setVisibility(View.GONE);
                            lin_mm.setVisibility(View.GONE);

                        }
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_mat_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        marital_status_id = String.valueOf(maritalStatusArray.get(position).getMaritalstatusID());
                        if (selectGender.equalsIgnoreCase("F") && marital_status_id.equalsIgnoreCase("5")) {
                            lin_fm.setVisibility(View.VISIBLE);
                            lin_mm.setVisibility(View.GONE);
                        } else if (selectGender.equalsIgnoreCase("F") && marital_status_id.equalsIgnoreCase("4")) {
                            lin_fm.setVisibility(View.VISIBLE);
                            lin_mm.setVisibility(View.GONE);
                        } else if (selectGender.equalsIgnoreCase("M") && marital_status_id.equalsIgnoreCase("5")) {
                            lin_mm.setVisibility(View.VISIBLE);
                            lin_fm.setVisibility(View.GONE);
                        } else if (selectGender.equalsIgnoreCase("M") && marital_status_id.equalsIgnoreCase("4")) {
                            lin_mm.setVisibility(View.VISIBLE);
                            lin_fm.setVisibility(View.GONE);
                        } else {
                            lin_fm.setVisibility(View.GONE);
                            lin_mm.setVisibility(View.GONE);

                        }
                    } else {
                        marital_status_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        float_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (selectGender.equalsIgnoreCase("") || selectGender.equals("0")) {
                    Utils.showToast("Please select gender", instance);
                } else if (marital_status_id.equalsIgnoreCase("") || marital_status_id.equals("0")) {
                    Utils.showToast("Please select marital status", instance);
                } else if (selectGender.equalsIgnoreCase("F") && marital_status_id.equalsIgnoreCase("5")) {
                    mrg_date = et_mrg_date.getText().toString();
                  /*  if (et_fsn.getText().toString().isEmpty()) {
                        et_fsn.setFocusable(true);
                        et_fsn.setError("Please enter spouse name");

                    } else*/
                    if (et_fsfn.getText().toString().isEmpty()) {
                        et_fsfn.setFocusable(true);
                        et_fsfn.setError("Please enter spouse father name");

                    } else if (et_fsmn.getText().toString().isEmpty()) {
                        et_fsmn.setFocusable(true);
                        et_fsmn.setError("Please enter spouse mother name");

                    } else if (etfn.getText().toString().isEmpty()) {
                        etfn.setFocusable(true);
                        etfn.setError("Please enter your father name");
                    } else if (et_fsurname.getText().toString().isEmpty()) {
                        et_fsurname.setFocusable(true);
                        et_fsurname.setError("Please enter your father surname");

                    } else if (et_fvillage.getText().toString().isEmpty()) {
                        et_fvillage.setFocusable(true);
                        et_fvillage.setError("Please enter your father village");

                    } else if (et_mrg_date.getText().toString().isEmpty()) {
                        Utils.showToast("Please enter marriage date", instance);

                    } else {
                        App.registerData.setGender("F");
                        App.registerData.setMaritalstatusid(marital_status_id);
                        //App.registerData.setSpousename(et_fsn.getText().toString());
                        App.registerData.setSpousefathername(et_fsfn.getText().toString().trim());
                        App.registerData.setSpousemothername(et_fsmn.getText().toString().trim());
                        App.registerData.setWifefathersurname(et_fsurname.getText().toString());
                        App.registerData.setWifefathervillage(et_fvillage.getText().toString());
                        App.registerData.setFathername(etfn.getText().toString());
                        App.registerData.setFathersurname(et_fsurname.getText().toString());
                        App.registerData.setFathervillage(et_fvillage.getText().toString());
                        App.registerData.setMarriagedate(et_mrg_date.getText().toString());
                        startActivity(new Intent(instance, RegisterTwoActivity.class));

                    }
                } else if (selectGender.equalsIgnoreCase("F") && marital_status_id.equalsIgnoreCase("4")) {
                    mrg_date = et_mrg_date.getText().toString();
                /*    if (et_fsn.getText().toString().isEmpty()) {
                        et_fsn.setFocusable(true);
                        et_fsn.setError("Please enter spouse name");

                    } else*/
                    if (et_fsfn.getText().toString().isEmpty()) {
                        et_fsfn.setFocusable(true);
                        et_fsfn.setError("Please enter spouse father name");

                    } else if (et_fsmn.getText().toString().isEmpty()) {
                        et_fsmn.setFocusable(true);
                        et_fsmn.setError("Please enter spouse mother name");

                    } else if (etfn.getText().toString().isEmpty()) {
                        etfn.setFocusable(true);
                        etfn.setError("Please enter your father name");
                    } else if (et_fsurname.getText().toString().isEmpty()) {
                        et_fsurname.setFocusable(true);
                        et_fsurname.setError("Please enter your father surname");

                    } else if (et_fvillage.getText().toString().isEmpty()) {
                        et_fvillage.setFocusable(true);
                        et_fvillage.setError("Please enter your father village");

                    } else if (et_mrg_date.getText().toString().isEmpty()) {
                        Utils.showToast("Please enter marriage date", instance);

                    } else {
                        App.registerData.setGender("F");
                        App.registerData.setMaritalstatusid(marital_status_id);
                        //App.registerData.setSpousename(et_fsn.getText().toString());
                        App.registerData.setSpousefathername(et_fsfn.getText().toString());
                        App.registerData.setSpousemothername(et_fsmn.getText().toString());
                        App.registerData.setWifefathersurname(et_fsurname.getText().toString());
                        App.registerData.setWifefathervillage(et_fvillage.getText().toString());
                        App.registerData.setFathername(etfn.getText().toString());
                        App.registerData.setFathersurname(et_fsurname.getText().toString());
                        App.registerData.setFathervillage(et_fvillage.getText().toString());
                        App.registerData.setMarriagedate(et_mrg_date.getText().toString());
                        startActivity(new Intent(instance, RegisterTwoActivity.class));

                    }
                } else if (selectGender.equalsIgnoreCase("M") && marital_status_id.equalsIgnoreCase("5")) {
                    mrg_date = et_male_mrg_date.getText().toString();
                    if (et_sn.getText().toString().isEmpty()) {
                        et_sn.setFocusable(true);
                        et_sn.setError("Please enter spouse name");

                    } else if (et_sfn.getText().toString().isEmpty()) {
                        et_sfn.setFocusable(true);
                        et_sfn.setError("Please enter spouse father name");

                    } else if (et_smn.getText().toString().isEmpty()) {
                        et_smn.setFocusable(true);
                        et_smn.setError("Please enter spouse mother name");

                    } else if (et_wfsurname.getText().toString().isEmpty()) {
                        et_wfsurname.setFocusable(true);
                        et_wfsurname.setError("Please enter wife's father surname");

                    } else if (et_wfvillage.getText().toString().isEmpty()) {
                        et_wfvillage.setFocusable(true);
                        et_wfvillage.setError("Please enter wife's father village");

                    } else if (et_male_mrg_date.getText().toString().isEmpty()) {
                        Utils.showToast("Please enter marriage date", instance);
                    } else {
                        App.registerData.setGender("M");
                        App.registerData.setMaritalstatusid(marital_status_id);
                        App.registerData.setSpousename(et_sn.getText().toString());
                        App.registerData.setSpousefathername(et_sfn.getText().toString());
                        App.registerData.setSpousemothername(et_smn.getText().toString());
                        App.registerData.setWifefathersurname(et_wfsurname.getText().toString());
                        App.registerData.setWifefathervillage(et_wfvillage.getText().toString());
                        App.registerData.setFathersurname(et_fsurname.getText().toString());
                        App.registerData.setFathervillage(et_fvillage.getText().toString());
                        App.registerData.setMarriagedate(et_male_mrg_date.getText().toString());
                        startActivity(new Intent(instance, RegisterTwoActivity.class));

                    }
                } else if (selectGender.equalsIgnoreCase("M") && marital_status_id.equalsIgnoreCase("4")) {
                    mrg_date = et_male_mrg_date.getText().toString();
                    if (et_sn.getText().toString().isEmpty()) {
                        et_sn.setFocusable(true);
                        et_sn.setError("Please enter spouse name");

                    } else if (et_sfn.getText().toString().isEmpty()) {
                        et_sfn.setFocusable(true);
                        et_sfn.setError("Please enter spouse father name");

                    } else if (et_smn.getText().toString().isEmpty()) {
                        et_smn.setFocusable(true);
                        et_smn.setError("Please enter spouse mother name");

                    } else if (et_wfsurname.getText().toString().isEmpty()) {
                        et_wfsurname.setFocusable(true);
                        et_wfsurname.setError("Please enter wife's father surname");

                    } else if (et_wfvillage.getText().toString().isEmpty()) {
                        et_wfvillage.setFocusable(true);
                        et_wfvillage.setError("Please enter wife's father village");

                    } else if (et_male_mrg_date.getText().toString().isEmpty()) {
                        Utils.showToast("Please enter marriage date", instance);
                    } else {
                        App.registerData.setGender("M");
                        App.registerData.setMaritalstatusid(marital_status_id);
                        App.registerData.setSpousename(et_sn.getText().toString());
                        App.registerData.setSpousefathername(et_sfn.getText().toString());
                        App.registerData.setSpousemothername(et_smn.getText().toString());
                        App.registerData.setWifefathersurname(et_wfsurname.getText().toString());
                        App.registerData.setWifefathervillage(et_wfvillage.getText().toString());
                        App.registerData.setFathersurname(et_fsurname.getText().toString());
                        App.registerData.setFathervillage(et_fvillage.getText().toString());
                        App.registerData.setMarriagedate(et_male_mrg_date.getText().toString());
                        startActivity(new Intent(instance, RegisterTwoActivity.class));

                    }
                } else {
                    App.registerData.setGender("M");
                    App.registerData.setMaritalstatusid(marital_status_id);
                    App.registerData.setSpousename(et_sn.getText().toString());
                    App.registerData.setSpousefathername(et_sfn.getText().toString());
                    App.registerData.setSpousemothername(et_smn.getText().toString());
                    App.registerData.setWifefathersurname(et_wfsurname.getText().toString());
                    App.registerData.setWifefathervillage(et_wfvillage.getText().toString());
                    App.registerData.setFathersurname(et_fsurname.getText().toString());
                    App.registerData.setFathervillage(et_fvillage.getText().toString());
                    App.registerData.setMarriagedate(et_male_mrg_date.getText().toString());
                    startActivity(new Intent(instance, RegisterTwoActivity.class));

                }
                //startActivity(new Intent(instance, RegisterTwoActivity.class));

            }
        });
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case getMaritalStatus:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strMaritalStatusArray.clear();
                            maritalStatusArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                maritalStatus.setMaritalstatus(getResources().getString(R.string.MaritalStatus));
                                maritalStatus.setMaritalstatusID(0);
                                strMaritalStatusArray.add(maritalStatus.getMaritalstatus());
                                maritalStatusArray.add(maritalStatus);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        maritalStatus = (MaritalStatus) jParser.parseJson(jSurnameArray.getJSONObject(i), new MaritalStatus());
                                        maritalStatusArray.add(maritalStatus);
                                        strMaritalStatusArray.add(maritalStatus.getMaritalstatus());
                                    }
                                    sp_mat_status.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strMaritalStatusArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }
}
