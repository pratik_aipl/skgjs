package com.skgjst.activities.prathana;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.App;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class PrathanaListActivity  extends BaseActivity implements AsynchTaskListner{
    public TextView tv_title;
    public CircleImageView img_profile, img_profile_local;
    public PrathanaListActivity instance;
    public Toolbar toolbar;
    public JsonParserUniversal jParser;
    public FloatingActionButton float_add;
    public ImageView img_my_job;
    public ArrayList<PrathanaData> birthdayArrayList = new ArrayList<>();
    public RecyclerView rv_product_list;
    public LinearLayout lin_empty;
    public TextView tv_message,tv_count;

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prathana_list);
        instance = PrathanaListActivity.this;
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("PRATHNA");
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_profile_local = toolbar.findViewById(R.id.img_profile_local);
        Utils.setRoundedImage(instance,user.getPhotoURL(),300,300,img_profile,null);

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });
        jParser = new JsonParserUniversal();

        float_add = findViewById(R.id.float_add);
        img_my_job = findViewById(R.id.img_my_job);
        tv_count = findViewById(R.id.tv_count);

        rv_product_list = findViewById(R.id.rcyclerView);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);
        rv_product_list.setLayoutAnimation(controller);
        rv_product_list.scheduleLayoutAnimation();
        rv_product_list.setLayoutManager(new LinearLayoutManager(instance, LinearLayoutManager.VERTICAL, false));
        rv_product_list.setHasFixedSize(true);
        rv_product_list.setItemViewCacheSize(20);
        rv_product_list.setDrawingCacheEnabled(true);
        rv_product_list.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        lin_empty = findViewById(R.id.lin_empty);
        tv_message = findViewById(R.id.tv_message);
        new CallRequest(this).getPrathanaList();


        float_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title="ADD PRATHNA";
                startActivity(new Intent(instance, PrathanaAddActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                );
            }
        });

        img_my_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CallRequest(instance).getMyPrathanaList(user);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        new CallRequest(this).getPrathanaList();
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            //    Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getPrathanaList:
                    birthdayArrayList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("PrathnaData") != null && jObj.getJSONArray("PrathnaData").length() > 0) {
                                lin_empty.setVisibility(View.GONE);
                                rv_product_list.setVisibility(View.VISIBLE);
                                JSONArray userData = jObj.getJSONArray("PrathnaData");
                                for (int i = 0; i < userData.length(); i++) {
                                    JSONObject obj = userData.getJSONObject(i);
                                    PrathanaData birthday = (PrathanaData) jParser.parseJson(obj, new PrathanaData());
                                    birthdayArrayList.add(birthday);
                                }
                                PrathanaData birthday = new PrathanaData();
                                birthday.setName("blank");
                                birthdayArrayList.add(birthday);
                                System.out.println("birthday list::::" + birthdayArrayList.size());
                                PrathanaAdapter birthdayAdapter = new PrathanaAdapter(birthdayArrayList, instance);
                                rv_product_list.setAdapter(birthdayAdapter);
                                tv_count.setText(birthdayArrayList.size() - 1 + " - Prathna");


                            } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("UMessage"), instance);
                                lin_empty.setVisibility(View.VISIBLE);
                                rv_product_list.setVisibility(View.GONE);
                                tv_count.setText("0 - Prathna");
                            }


                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), instance);
                            lin_empty.setVisibility(View.VISIBLE);
                            rv_product_list.setVisibility(View.GONE);
                            tv_count.setText("0 - Prathna");
                        }
                    } catch (JSONException e) {

                        Utils.hideProgressDialog();
                        Utils.showToast("UMessage", instance);
                        lin_empty.setVisibility(View.VISIBLE);
                        tv_count.setText("0 - Prathna");
                        rv_product_list.setVisibility(View.GONE);
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
                case getMyPrathanaList:
                    birthdayArrayList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("PrathnaInfo") != null && jObj.getJSONArray("PrathnaInfo").length() > 0) {
                                lin_empty.setVisibility(View.GONE);
                                rv_product_list.setVisibility(View.VISIBLE);
                                JSONArray userData = jObj.getJSONArray("PrathnaInfo");
                                for (int i = 0; i < userData.length(); i++) {
                                    JSONObject obj = userData.getJSONObject(i);
                                    PrathanaData birthday = (PrathanaData) jParser.parseJson(obj, new PrathanaData());
                                    birthdayArrayList.add(birthday);
                                }
                                PrathanaData birthday = new PrathanaData();
                                birthday.setName("blank");
                                birthdayArrayList.add(birthday);

                                MyPrathanaAdapter birthdayAdapter = new MyPrathanaAdapter(birthdayArrayList, instance);
                                rv_product_list.setAdapter(birthdayAdapter);
                                tv_count.setText(birthdayArrayList.size() - 1 + " -My Prathna");


                            } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("UMessage"), instance);
                                lin_empty.setVisibility(View.VISIBLE);
                                tv_count.setText("0 -My Prathna");
                                rv_product_list.setVisibility(View.GONE);
                            }


                        } else {
                            Utils.hideProgressDialog();
                            tv_count.setText("0 -My Prathna");
                            Utils.showToast(jObj.getString("UMessage"), instance);
                            lin_empty.setVisibility(View.VISIBLE);
                            rv_product_list.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {

                        Utils.hideProgressDialog();
                        Utils.showToast("UMessage", instance);
                        lin_empty.setVisibility(View.VISIBLE);
                        rv_product_list.setVisibility(View.GONE);
                        tv_count.setText("0 -My Prathna");
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
            }
        }
    }

}
