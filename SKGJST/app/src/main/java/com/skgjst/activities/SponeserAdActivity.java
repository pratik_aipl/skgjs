package com.skgjst.activities;

import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.adapter.SponeserAdapter;
import com.skgjst.App;
import com.skgjst.model.SponserAd;
import com.skgjst.R;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class SponeserAdActivity  extends BaseActivity {
    private static TypedArray icons;
    private static String[] titles = null;
    public Toolbar toolbar;
    public TextView tv_title;
    public CircleImageView img_profile, img_profile_local;
    public SponeserAdActivity instance;
    public Integer[] titleIcon = {R.drawable.img_platinum, R.drawable.img_daimond, R.drawable.img_gold, R.drawable.img_silver, R.drawable.img_wish};
    public Integer[] sponserIcon = {R.drawable.platinum_sponser, R.drawable.diamond_sponser,
            R.drawable.gold_sponser, R.drawable.silver_sponser, R.drawable.wish_sponser};
    public SponserAd sponserAd;
    private List<SponserAd> sponserAdList = new ArrayList<>();
    private RecyclerView recyclerView;
    private SponeserAdapter mAdapter;

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sponeser_ad);
        instance = SponeserAdActivity.this;
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("SPONSORER AD");
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_profile_local = toolbar.findViewById(R.id.img_profile_local);
        Utils.setRoundedImage(instance,user.getPhotoURL(),300,300,img_profile,null);
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });
        for (int i = 0; i < titleIcon.length; i++) {
            sponserAdList.add(new SponserAd(titleIcon[i], sponserIcon[i]));
        }
        recyclerView = findViewById(R.id.recyclerView);
        setUpRecycleview();

    }

    private void setUpRecycleview() {
        mAdapter = new SponeserAdapter(instance, sponserAdList);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);
        recyclerView.setLayoutAnimation(controller);
        recyclerView.scheduleLayoutAnimation();
        recyclerView.setLayoutManager(new LinearLayoutManager(instance, LinearLayoutManager.VERTICAL, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
    }

}
