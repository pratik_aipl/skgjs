package com.skgjst.activities.profile.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skgjst.BaseFragment;
import com.skgjst.activities.membersearch.MemberListAdapter;
import com.skgjst.adapter.JobListAdapter;
import com.skgjst.App;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.FamilyMembers;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by empiere-vaibhav on 9/19/2018.
 */

public class FamilyMemberFragment extends  BaseFragment implements AsynchTaskListner {
    public View v;
    public FamilyMemberFragment fragment;
    public RecyclerView rv_member_list;
    public JsonParserUniversal jParser;
    public ArrayList<FamilyMembers> memberArrayList = new ArrayList<>();
    public FamilyMembers obj;
    public JobListAdapter adapter;
    public LinearLayout lin_empty,lin_top;
    public TextView tv_message;
    public MemberListAdapter birthdayAdapter;

    public FamilyMemberFragment newInstance() {
        fragment = new FamilyMemberFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        fragment.setHasOptionsMenu(true);
        return fragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_family_member, container, false);
        jParser = new JsonParserUniversal();
        fragment = this;
        rv_member_list = v.findViewById(R.id.rv_member_list);
        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);
        rv_member_list.setLayoutAnimation(controller);
        rv_member_list.scheduleLayoutAnimation();
        rv_member_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rv_member_list.setHasFixedSize(true);
        rv_member_list.setItemViewCacheSize(20);
        rv_member_list.setDrawingCacheEnabled(true);
        rv_member_list.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        lin_empty = v.findViewById(R.id.lin_empty);
        lin_top = v.findViewById(R.id.lin_top);
        lin_top.setVisibility(View.GONE);
        tv_message = v.findViewById(R.id.tv_message);
        new CallRequest(this).getFamilymembers(user.getFamilyDetailID());

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        new CallRequest(this).getFamilymembers(user.getFamilyDetailID());
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (getView() != null) {
            //  setData();
        } else {

        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            //    Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getFamilymembers:
                    memberArrayList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("FamilyMembers") != null && jObj.getJSONArray("FamilyMembers").length() > 0) {
                                lin_empty.setVisibility(View.GONE);
                                rv_member_list.setVisibility(View.VISIBLE);
                                JSONArray userData = jObj.getJSONArray("FamilyMembers");
                                for (int i = 0; i < userData.length(); i++) {
                                    JSONObject jobj = userData.getJSONObject(i);
                                    obj = (FamilyMembers) jParser.parseJson(jobj, new FamilyMembers());
                                    memberArrayList.add(obj);
                                }
                                 System.out.println("birthday list::::" + memberArrayList.size());
                               // Collections.reverse(memberArrayList);
                                FamilyMembers Tobj = new FamilyMembers();
                                Tobj.setFName("blank");
                                memberArrayList.add(Tobj);
                                birthdayAdapter = new MemberListAdapter(memberArrayList, getActivity());
                                rv_member_list.setAdapter(birthdayAdapter);


                            } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("UMessage"), getActivity());
                                lin_empty.setVisibility(View.VISIBLE);
                                rv_member_list.setVisibility(View.GONE);
                            }


                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), getActivity());
                            lin_empty.setVisibility(View.VISIBLE);
                            rv_member_list.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {

                        Utils.hideProgressDialog();
                        Utils.showToast("Something getting wrong! Please try again later.", getActivity());
                        lin_empty.setVisibility(View.VISIBLE);
                        rv_member_list.setVisibility(View.GONE);
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
            }
        }
    }
}

