package com.skgjst.activities.events;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.activities.DashBoardActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.App;
import com.skgjst.fragment.event.PastEventsFragment;
import com.skgjst.fragment.event.UpcomingEventsFragment;
import com.skgjst.R;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class EventsActivity  extends BaseActivity {
        public ViewPager viewPager;
        public ViewPagerAdapter adapter;
        public Toolbar toolbar;
    public TextView tv_title;
    public CircleImageView img_profile, img_profile_local;
    public EventsActivity instance;
    private TabLayout tabLayout;

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        instance = EventsActivity.this;
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // startActivity(new Intent(instance, DashBoardActivity.class));
            onBackPressed();
            }
        });
        tv_title.setText("EVENTS");
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_profile_local = toolbar.findViewById(R.id.img_profile_local);
        Utils.setRoundedImage(instance,user.getPhotoURL(),300,300,img_profile,null);
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        changeTabsFont(tabLayout);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode==KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(instance, DashBoardActivity.class));
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new UpcomingEventsFragment(), "UPCOMING EVENTS");
        adapter.addFragment(new PastEventsFragment(), "PAST EVENTS");


        viewPager.setAdapter(adapter);
    }

    protected void changeTabsFont(TabLayout tabLayout) {
        Log.i("In change tab font", "");
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        Log.i("Tab count--->", String.valueOf(tabsCount));
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof AppCompatTextView) {
                    Log.i("In font", "");

                 /*   LatoRegularTextView firsttab = (LatoRegularTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
                  //  firsttab.setText(FRAGMENT_FIRST);
                    tabLayout.getTabAt(0).setCustomView(firsttab);

                    Typeface type = Typeface.createFromAsset(getActivity().getAssets(), "Lato-Regular_0.ttf");
                    TextView viewChild = (TextView) tabViewChild;
                    viewChild.setTypeface(type);
                    viewChild.setAllCaps(false);
                    viewChild.setTextSize(10);*/
                }
            }
        }
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
