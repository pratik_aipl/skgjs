package com.skgjst.activities.membersearch.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skgjst.App;
import com.skgjst.BaseFragment;
import com.skgjst.R;
import com.skgjst.activities.membersearch.MemberListAdapter;
import com.skgjst.activities.profile.FamilyDetailsTreeActivity;
import com.skgjst.activities.profile.FamilyTreeActivity;
import com.skgjst.adapter.JobListAdapter;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.FamilyMembers;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class MemberFamilyFragment extends BaseFragment implements AsynchTaskListner {
    private static final String TAG = "MemberFamilyFragment";
    public View v;
    public MemberFamilyFragment fragment;
    public RecyclerView rv_member_list;
    public JsonParserUniversal jParser;
    public ArrayList<FamilyMembers> memberArrayList = new ArrayList<>();
    public FamilyMembers obj;
    public JobListAdapter adapter;
    public LinearLayout lin_empty,lin_top;
    public TextView tv_message,tv_count,tv_nameone,tv_nametwo;
    public MemberListAdapter birthdayAdapter;
    public CircleImageView img_profileone, img_profiletwo;

    public MemberFamilyFragment newInstance() {
        fragment = new MemberFamilyFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        fragment.setHasOptionsMenu(true);
        return fragment;

    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_family_member, container, false);
        jParser = new JsonParserUniversal();
        fragment = this;

        rv_member_list = v.findViewById(R.id.rv_member_list);
        LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_fall_down);
        rv_member_list.setLayoutAnimation(controller);
        rv_member_list.scheduleLayoutAnimation();
        rv_member_list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rv_member_list.setHasFixedSize(true);
        rv_member_list.setItemViewCacheSize(20);
        rv_member_list.setDrawingCacheEnabled(true);
        rv_member_list.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);

        lin_empty = v.findViewById(R.id.lin_empty);
        lin_top = v.findViewById(R.id.lin_top);
        tv_message = v.findViewById(R.id.tv_message);
        tv_count = v.findViewById(R.id.tv_count);
        tv_nameone = v.findViewById(R.id.tv_nameone);
        tv_nametwo = v.findViewById(R.id.tv_nametwo);
        img_profileone = v.findViewById(R.id.img_profileone);
        img_profiletwo = v.findViewById(R.id.img_profiletwo);

        lin_top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), FamilyDetailsTreeActivity.class).putExtra("FamilyDetailID",
                        App.memberFamilyDetailID));
            }
        });

        Log.d(TAG, "memberFamilyDetailID: "+App.memberFamilyDetailID);
        Log.d(TAG, "getUserID: "+user.getFamilyDetailID());
      /*  if(App.memberFamilyDetailID.equalsIgnoreCase(String.valueOf(user.getFamilyDetailID()))){
            lin_top.setVisibility(View.GONE);
        }else{
            lin_top.setVisibility(View.VISIBLE);

        }*/
        new CallRequest(this).familymembers(Integer.parseInt(App.memberfamilyId),
                Integer.parseInt(App.memberFamilyDetailID));
        new CallRequest(this).ConnectionTreeCount(user.getFamilyDetailID(),
                Integer.parseInt(App.memberFamilyDetailID));

        return v;
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            //    Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getFamilymembers:
                    memberArrayList.clear();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("FamilyMembers") != null && jObj.getJSONArray("FamilyMembers").length() > 0) {
                                lin_empty.setVisibility(View.GONE);
                                rv_member_list.setVisibility(View.VISIBLE);
                                JSONArray userData = jObj.getJSONArray("FamilyMembers");
                                for (int i = 0; i < userData.length(); i++) {
                                    JSONObject jobj = userData.getJSONObject(i);
                                    obj = (FamilyMembers) jParser.parseJson(jobj, new FamilyMembers());
                                    memberArrayList.add(obj);
                                }

                                FamilyMembers Tobj = new FamilyMembers();
                                Tobj.setFName("blank");
                                memberArrayList.add(Tobj);
                                System.out.println("birthday list::::" + memberArrayList.size());
                                birthdayAdapter = new MemberListAdapter(memberArrayList, getActivity());
                                rv_member_list.setAdapter(birthdayAdapter);


                            } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("UMessage"), getActivity());
                                lin_empty.setVisibility(View.VISIBLE);
                                rv_member_list.setVisibility(View.GONE);
                            }


                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), getActivity());
                            lin_empty.setVisibility(View.VISIBLE);
                            rv_member_list.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {

                        Utils.hideProgressDialog();
                        Utils.showToast("Something getting wrong! Please try again later.", getActivity());
                        lin_empty.setVisibility(View.VISIBLE);
                        rv_member_list.setVisibility(View.GONE);
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;

                    case getFamilymembersCount:
                    try {
//                        Utils.showProgressDialog(getActivity());
                        JSONObject jObj = new JSONObject(result);
                        Log.d(TAG, "getFamilymembersCount: "+result);
                        if (jObj.getBoolean("Success")) {

                            if(jObj.getJSONArray("data").getJSONObject(0).getString("dcount").equalsIgnoreCase("null")){

                                lin_top.setVisibility(View.GONE);
                            }else{
                                lin_top.setVisibility(View.VISIBLE);
                                tv_count.setText(""+(Integer.parseInt(jObj.getJSONArray("data").getJSONObject(0).getString("dcount"))+1));
                                tv_nameone.setText(jObj.getJSONArray("data").getJSONObject(0).getString("srcName"));
                                tv_nametwo.setText(jObj.getJSONArray("data").getJSONObject(0).getString("dstName"));
                                Picasso.with(getActivity()).load(jObj.getJSONArray("data").getJSONObject(0).getString("srcPhotoURL").replace(" ", "%20")).resize(300, 300).into(img_profileone);
                                Picasso.with(getActivity()).load(jObj.getJSONArray("data").getJSONObject(0).getString("dstPhotoURL").replace(" ", "%20")).resize(300, 300).into(img_profiletwo);
                            }

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("Message"), getActivity());
                        }
                    } catch (JSONException e) {

                        Utils.hideProgressDialog();
                        Utils.showToast("Something getting wrong! Please try again later.", getActivity());
                        e.printStackTrace();
                    }
                    Utils.hideProgressDialog();
                    break;
            }
        }
    }
}

