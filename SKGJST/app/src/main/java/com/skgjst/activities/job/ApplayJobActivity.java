package com.skgjst.activities.job;

import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.App;
import com.skgjst.fonts.MyCustomTypeface;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.JobList;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ApplayJobActivity  extends BaseActivity implements AsynchTaskListner {
    public ApplayJobActivity instance;
    public Toolbar toolbar;
    public JsonParserUniversal jParser;
    public TextView tv_title, tv_file_name;
    public JobList obj;
    public Button btn_browse, btn_submit;
    public String SelectedResume = "", Filename = "";
    public EditText et_firstname, et_lastname, et_experiance, et_notice_perioad, et_currant_ctc, et_expected_ctc;
    public int REQUEST_CODE_OPEN = 111;
    public ArrayList<String> strCityArray = new ArrayList<>();
    public String LocationName = "";
    public Spinner sp_city;
    public CircleImageView img_profile;
    Uri selectedUri;
    Uri uri;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getPath(Context context, Uri uri) {
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_applay_job);
        instance = this;
        jParser = new JsonParserUniversal();
        Intent intent = getIntent();
        if (intent.hasExtra("obj")) {
            obj = (JobList) getIntent().getExtras().getSerializable("obj");

        }
        new CallRequest(instance).GetCityList();
        et_firstname = findViewById(R.id.et_firstname);
        et_lastname = findViewById(R.id.et_lastname);
        et_experiance = findViewById(R.id.et_experiance);
        et_notice_perioad = findViewById(R.id.et_notice_perioad);
        et_currant_ctc = findViewById(R.id.et_currant_ctc);
        et_expected_ctc = findViewById(R.id.et_expected_ctc);
        sp_city = findViewById(R.id.sp_city);
        btn_submit = findViewById(R.id.btn_submit);
        btn_browse = findViewById(R.id.btn_browse);
        tv_file_name = findViewById(R.id.tv_file_name);
        btn_browse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("*/*");
                String[] mimetypes = {"application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword", "application/pdf"};
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
                startActivityForResult(intent, REQUEST_CODE_OPEN);
            }
        });


        sp_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.color_back));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        LocationName = ((TextView) parent.getChildAt(0)).getText().toString();

                    } else {
                        LocationName = "";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });


        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Validation();
            }
        });
        toolbar = findViewById(R.id.toolbar);
        img_profile = toolbar.findViewById(R.id.img_profile);


        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!et_firstname.getText().toString().isEmpty() ||
                        !et_lastname.getText().toString().isEmpty() ||
                        !et_experiance.getText().toString().isEmpty() ||
                        !et_notice_perioad.getText().toString().isEmpty() ||
                        !et_currant_ctc.getText().toString().isEmpty() ||
                        !et_expected_ctc.getText().toString().isEmpty() ||
                        !LocationName.isEmpty()) {
                    new AlertDialog.Builder(instance)
                            .setTitle("Alert?")
                            .setMessage("Are you sure to discard this changes?")
                            .setNegativeButton(android.R.string.no, null)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface arg0, int arg1) {

                                    onBackPressed();
                                }
                            }).create().show();
                } else {
                    onBackPressed();

                }

            }
        });
        tv_title.setText("APPLY FOR" + obj.getJobpost());
        Utils.setRoundedImage(instance,user.getPhotoURL(),300,300,img_profile,null);

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (!et_firstname.getText().toString().isEmpty() ||
                    !et_lastname.getText().toString().isEmpty() ||
                    !et_experiance.getText().toString().isEmpty() ||
                    !et_notice_perioad.getText().toString().isEmpty() ||
                    !et_currant_ctc.getText().toString().isEmpty() ||
                    !et_expected_ctc.getText().toString().isEmpty() ||
                    !LocationName.isEmpty()) {
                new AlertDialog.Builder(instance)
                        .setTitle("Alert?")
                        .setMessage("Are you sure to discard this changes?")
                        .setNegativeButton(android.R.string.no, null)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface arg0, int arg1) {

                                onBackPressed();
                            }
                        }).create().show();
            } else {
                onBackPressed();

            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public void Validation() {
        if (et_firstname.getText().toString().equals("")) {
            et_firstname.setError("Please enter first name");
            et_firstname.setFocusable(true);
        } else if (et_firstname.getText().length() < 4) {
            et_firstname.setError("Please enter minimum four character first name");
            et_firstname.setFocusable(true);
        } else if (et_lastname.getText().toString().equals("")) {
            et_lastname.setError("Please enter last name");
            et_lastname.setFocusable(true);
        } else if (et_lastname.getText().length() < 4) {
            et_lastname.setError("Please enter minimum four character last name");
            et_lastname.setFocusable(true);
        } else if (et_experiance.getText().toString().equals("")) {
            et_experiance.setError("Please enter experiance");
            et_experiance.setFocusable(true);
        } else if (et_notice_perioad.getText().toString().equals("")) {
            et_notice_perioad.setError("Please enter notice period");
            et_notice_perioad.setFocusable(true);
        } else if (LocationName.equals("")) {
            Utils.showToast("Please select city", instance);
        } else if (et_currant_ctc.getText().toString().equals("")) {
            et_currant_ctc.setError("Please enter currant ctc");
            et_currant_ctc.setFocusable(true);
        } else if (et_expected_ctc.getText().toString().equals("")) {
            et_expected_ctc.setError("Please enter expected ctc");
            et_expected_ctc.setFocusable(true);
        } /*else if (Integer.parseInt(et_currant_ctc.getText().toString()) < Integer.parseInt(et_expected_ctc.getText().toString())) {
            et_expected_ctc.setError("Please enter expected ctc");
            et_expected_ctc.setFocusable(true);
        }*/ else if (SelectedResume.equals("")) {
            Utils.showToast("Please select Resume", instance);
        } else {
            new CallRequest(instance).jobapply(obj.getFamilyid(), obj.getFamilyDetailid(), obj.getJobid(), et_firstname.getText().toString(), et_lastname.getText().toString(),
                    et_experiance.getText().toString(), LocationName, et_notice_perioad.getText().toString(), et_currant_ctc.getText().toString(), et_expected_ctc.getText().toString(), SelectedResume, Filename);

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == REQUEST_CODE_OPEN) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    selectedUri = data.getData();
                    uri = data.getData();
                    SelectedResume = getPath(instance, uri);
                    Filename = (SelectedResume.substring(SelectedResume.lastIndexOf('/') + 1));
                    tv_file_name.setText(Filename);

                    Log.i("TAG", "SelectedResume :-> " + SelectedResume);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {

                case jobapply:
                    Utils.hideProgressDialog();
                    try

                    {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success") == true) {
                            Utils.showToast(jObj.getString("UMessage"), instance);
                            startActivity(new Intent(instance, JobListActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                            finish();
                        } else {
                            Utils.showToast(jObj.getString("UMessage"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case GetCityList:
                    Utils.hideProgressDialog();
                    try

                    {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success") == true) {
                            strCityArray.clear();
                            if (jObj.getJSONArray("CityList") != null && jObj.getJSONArray("CityList").length() > 0) {

                                strCityArray.add("Select city*");
                                JSONArray jDataArray = jObj.getJSONArray("CityList");
                                for (int i = 0; i < jDataArray.length(); i++) {
                                    strCityArray.add(jDataArray.getJSONObject(i).getString("city"));
                                }
                                sp_city.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strCityArray));

                            } else {
                                Utils.showToast(jObj.getString("UMessage"), instance);
                            }
                        } else {
                            Utils.showToast(jObj.getString("UMessage"), instance);
                        }
                    } catch (
                            JSONException e)

                    {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }
}