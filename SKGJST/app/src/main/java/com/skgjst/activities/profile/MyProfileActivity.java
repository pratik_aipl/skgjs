package com.skgjst.activities.profile;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.skgjst.App;
import com.skgjst.BaseActivity;
import com.skgjst.R;
import com.skgjst.activities.DashBoardActivity;
import com.skgjst.activities.profile.fragment.AddFatherMotherActivity;
import com.skgjst.activities.profile.fragment.ContactFragment;
import com.skgjst.activities.profile.fragment.FamilyMemberFragment;
import com.skgjst.activities.profile.fragment.ProfileFragment;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.fonts.LatoRegularTextView;
import com.skgjst.model.Profile;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.CustPagerTransformer;
import com.skgjst.utils.ImagePopup;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyProfileActivity extends BaseActivity implements AsynchTaskListner {
    private static final String TAG = "MyProfileActivity";
    public static final String FRAGMENT_FIRST = "Family Member";
    public static final String FRAGMENT_SECOND = "Profile";
    public static final String FRAGMENT_THIRD = "Contact";
    public FloatingActionButton float_add, float_tree;
    public Toolbar toolbar;
    public TextView tv_title, tv_name, tv_city;
    public MyProfileActivity instance;
    public ViewPager viewPager;
    public TabLayout tabLayout;
    public ProgressBar pbar;
    public CircleImageView img_profile, img_profile_local, img_tree;
    public Profile obj;
    public JsonParserUniversal jParser;
    //    public Dialog dialog;
    ViewPagerAdapter adapter;
    public ImageView img_active;

    public LinearLayout lin_tree;
    int MemberRelationID = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        jParser = new JsonParserUniversal();
        instance = this;

        //Toast.makeText(instance, "MyProfile", Toast.LENGTH_SHORT).show();

        lin_tree = findViewById(R.id.lin_tree);
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        img_profile = findViewById(R.id.img_profile);
        img_profile.setVisibility(View.GONE);
        pbar = findViewById(R.id.pbar);
        img_profile_local = findViewById(R.id.img_profile_local);
        img_tree = findViewById(R.id.img_tree);
        tv_name = findViewById(R.id.tv_name);
        tv_city = findViewById(R.id.tv_city);
        img_active = findViewById(R.id.img_active);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, DashBoardActivity.class));

            }
        });
        tv_title.setText("MY PROFILE");
        viewPager = findViewById(R.id.viewPager);
        float_add = findViewById(R.id.float_add);
        float_tree = findViewById(R.id.float_tree);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        setupViewPager(viewPager);
        float_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addMemberDailog();
            }
        });
        Utils.setRoundedImage(instance, user.getPhotoURL(), 300, 300, img_profile, null);
        float_tree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (float_add.getVisibility() == View.GONE) {
                    App.title = "EditProfile";
                    // startActivity(new Intent(instance, EditProfileActivity.class));
                    startActivity(new Intent(instance, EditProfileActivity.class).putExtra("FamilyDetailId", String.valueOf(user.getFamilyDetailID())));

                } else {
                    App.title = "Family Tree";
                    startActivity(new Intent(instance, CommingSoonActivity.class));
                }


            }
        });

        lin_tree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, FamilyTreeActivity.class)
                        .putExtra("FamilyDetailID", user.getFamilyDetailID() + ""));

            }
        });

        new CallRequest(instance).getMyprofile(user, user.getFamilyDetailID());

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(instance, DashBoardActivity.class));
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void openImage() {
        final ImagePopup imagePopup = new ImagePopup(MyProfileActivity.this);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.BLACK);  // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(true);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional
        imagePopup.initiatePopupWithPicasso(user.getPhotoURL().replace(" ", "%20"));
        img_profile_local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /** Initiate Popup view **/
                imagePopup.viewPopup();

            }
        });


    }

    public void addMemberDailog() {
        final Dialog dialog = new Dialog(instance);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dailog_choise_member);
        dialog.show();
        TextView tv_name = dialog.findViewById(R.id.tv_name);
        Button btn_brother = dialog.findViewById(R.id.btn_brother);
        Button btn_sister = dialog.findViewById(R.id.btn_sister);
        Button btn_spouse = dialog.findViewById(R.id.btn_spouse);
        Button btn_son = dialog.findViewById(R.id.btn_son);
        Button btn_addfm = dialog.findViewById(R.id.btn_addfm);
        Button btn_daughter = dialog.findViewById(R.id.btn_daughter);
        tv_name.setText(obj.getFName() + " " + obj.getMName() + " " + obj.getSurName());

        Log.d(TAG, "addMemberDailog: " + gson.toJson(obj));
        if (obj.getIsFatherProfile() == 1 && obj.getIsMotherProfile() == 1) {
            btn_addfm.setVisibility(View.GONE);
        } else {
            btn_addfm.setVisibility(View.VISIBLE);
        }

        if (!obj.getMaritalstatus().equalsIgnoreCase("UnMarried") && obj.getIsSpouseProfile() == 1) {
            btn_spouse.setVisibility(View.GONE);
            btn_son.setVisibility(View.VISIBLE);
            btn_daughter.setVisibility(View.VISIBLE);

        } else if (!obj.getMaritalstatus().equalsIgnoreCase("UnMarried") && obj.getIsSpouseProfile() == 0) {
            btn_spouse.setVisibility(View.VISIBLE);
            btn_son.setVisibility(View.VISIBLE);
            btn_daughter.setVisibility(View.VISIBLE);

        } else {
            btn_spouse.setVisibility(View.GONE);
            btn_son.setVisibility(View.GONE);
            btn_daughter.setVisibility(View.GONE);
        }


        btn_brother.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title = "ADD BROTHER";
                dialog.dismiss();
                startActivity(new Intent(instance, AddBrotherActivity.class).
                        putExtra("FamilyDetailId", String.valueOf(user.getFamilyDetailID()))
                        .putExtra("Name", obj.getFName())
                        .putExtra("FatherName", obj.getFatherName())
                        .putExtra("Gender", user.getGender())
                        .putExtra("FathersVillage", user.getFathersVillage())
                        .putExtra("FatherSurname", user.getFathersSurname()));
            }
        });


        btn_addfm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Toast.makeText(MyProfileActivity.this, "CLick......", Toast.LENGTH_SHORT).show();
                App.title = "ADD Father Mother";
                dialog.dismiss();
                startActivity(new Intent(instance, AddFatherMotherActivity.class).
                        putExtra("FamilyDetailId", String.valueOf(user.getFamilyDetailID()))
                        .putExtra("Name", obj.getFName())
                        .putExtra("FatherName", obj.getFatherName())
                        .putExtra("Gender", obj.getGender())
                        .putExtra("FathersVillage", obj.getFathersVillage())
                        .putExtra("MotherName", obj.getMotherName())
                        .putExtra("FatherSurname", obj.getFathersSurname()));
            }
        });
        btn_sister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title = "ADD SISTER";
                dialog.dismiss();
                startActivity(new Intent(instance, AddSisterActivity.class).
                        putExtra("FamilyDetailId", String.valueOf(user.getFamilyDetailID()))
                        .putExtra("Name", obj.getFName())
                        .putExtra("FatherName", obj.getFatherName())
                        .putExtra("Gender", user.getGender())
                        .putExtra("FathersVillage", user.getFathersVillage())
                        .putExtra("FatherSurname", user.getFathersSurname()));
            }
        });
        btn_spouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title = "ADD SPOUSE";
                dialog.dismiss();
                startActivity(new Intent(instance, AddSpouseActivity.class).
                        putExtra("FamilyDetailId", String.valueOf(user.getFamilyDetailID()))
                        .putExtra("Gender", user.getGender()));
            }
        });

        btn_son.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title = "ADD SON";
                dialog.dismiss();
                startActivity(new Intent(instance, AddSonActivity.class).
                        putExtra("FamilyDetailId", String.valueOf(user.getFamilyDetailID()))
                        .putExtra("Name", obj.getFName())
                        .putExtra("FatherName", obj.getFatherName())
                        .putExtra("Gender", user.getGender())
                        .putExtra("Maritalstatus", obj.getMaritalstatus())
                        .putExtra("FathersVillage", user.getFathersVillage())
                        .putExtra("FatherSurname", user.getFathersSurname()));
            }
        });

        btn_daughter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title = "ADD DAUGHTER";
                dialog.dismiss();
                startActivity(new Intent(instance, AddDaughterActivity.class).
                        putExtra("FamilyDetailId", String.valueOf(user.getFamilyDetailID()))
                        .putExtra("Name", obj.getFName())
                        .putExtra("FatherName", obj.getFatherName())
                        .putExtra("Maritalstatus", obj.getMaritalstatus())
                        .putExtra("Gender", user.getGender())
                        .putExtra("FathersVillage", user.getFathersVillage())
                        .putExtra("FatherSurname", user.getFathersSurname()));
            }
        });
        ImageView imgClose = dialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {

        if (viewPager != null) {
            adapter = new ViewPagerAdapter(getSupportFragmentManager());
            adapter.addFragment(new FamilyMemberFragment().newInstance(), "Family Member");
            adapter.addFragment(new ProfileFragment().newInstance(), "Profile");
            adapter.addFragment(new ContactFragment().newInstance(), "Contact");
            viewPager.setAdapter(adapter);
            viewPager.setPageTransformer(false, new CustPagerTransformer(this));

            tabLayout.setupWithViewPager(viewPager);
            setupTabFont();
        }


        if (viewPager != null) {
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }

                @Override
                public void onPageSelected(int position) {

                    if (MemberRelationID == 0) {
                        float_add.setVisibility(View.GONE);
                        float_tree.setVisibility(View.GONE);
                    } else {
                        if (position == 0) {
                            float_add.show();
                            float_tree.hide();
                        } else if (position == 1) {
                            float_add.hide();
                            float_tree.show();
                        } else if (position == 2) {
                            float_add.hide();
                            float_tree.hide();
                        }
                    }


                }
            });
        }

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            //    Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getMyprofiles:
                    obj = new Profile();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {

                            if (jObj.getJSONArray("Profile") != null && jObj.getJSONArray("Profile").length() > 0) {
                                JSONArray userData = jObj.getJSONArray("Profile");

                                for (int i = 0; i < userData.length(); i++) {
                                    JSONObject jobj = userData.getJSONObject(i);
                                    obj = (Profile) jParser.parseJson(jobj, new Profile());
                                    user.setPhotoURL(obj.getPhotoURL());
                                    tv_name.setText(obj.getFName() + " " + obj.getMName() + " " + obj.getSurName());
                                    tv_city.setText(obj.getNativePlace());

                                }

                                MemberRelationID = (int) jObj.get("MemberRelationID");
                                Log.d(TAG, "Member ID>>>>>: " + MemberRelationID);
                                if (MemberRelationID == 0) {
                                    float_add.setVisibility(View.GONE);
                                } else {
                                    float_add.setVisibility(View.VISIBLE);
                                }

                                App.modified_by = obj.getModifiedBy();
                                App.modified_on = obj.getModifiedOn();
                                if (!user.getPhotoURL().equals("")) {

                                    Picasso.with(instance).load(user.getPhotoURL().replace(" ", "%20")).resize(300, 300).into(img_profile_local, new Callback() {
                                        @Override
                                        public void onSuccess() {
                                            pbar.setVisibility(View.GONE);
                                        }

                                        @Override
                                        public void onError() {

                                        }

                                    });
                                }
                                img_profile_local.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        openImage();
                                    }
                                });
                                if (obj.isAlive()) {
                                    if (obj.isMIsActive() && !obj.getPlayerID().equalsIgnoreCase(""))
                                        img_active.setImageResource(R.drawable.ic_active);
                                    else
                                        img_active.setVisibility(View.VISIBLE);
                                } else {
                                    img_active.setImageResource(R.drawable.ic_dis_active);
                                }

                                Utils.hideProgressDialog();
                            } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("UMessage"), instance);
                            }
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), instance);
                        }
                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        Utils.showToast("Something getting wrong! Please try again later.", instance);

                        e.printStackTrace();
                    }
            }
        }
    }

    private void setupTabFont() {
        LatoRegularTextView firsttab = (LatoRegularTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        firsttab.setText(FRAGMENT_FIRST);
        tabLayout.getTabAt(0).setCustomView(firsttab);

        LatoRegularTextView secondtab = (LatoRegularTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        secondtab.setText(FRAGMENT_SECOND);
        tabLayout.getTabAt(1).setCustomView(secondtab);

        LatoRegularTextView thirdtab = (LatoRegularTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        thirdtab.setText(FRAGMENT_THIRD);
        tabLayout.getTabAt(2).setCustomView(thirdtab);
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
//        new CallRequest(instance).getMyprofilesActiviti(user.getFamilyDetailID());

    }


    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
