package com.skgjst.activities.business;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skgjst.model.BusinessList;
import com.skgjst.R;
import com.skgjst.utils.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by empiere-vaibhav on 10/9/2018.
 */

public class BusinessAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int VIEW_TYPE_ITEM = 1;
    public static final int VIEW_TYPE_BLANK = 2;
    public Context context;
    private ArrayList<BusinessList> mArrayList;

    public BusinessAdapter(ArrayList<BusinessList> moviesList, Context context) {
        mArrayList = moviesList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new MyViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.custom_business_raw, parent, false));

    }



    public void bindMyViewHolder(final MyViewHolder holder, final int pos) {
        final BusinessList Obj = mArrayList.get(pos);
        holder.tv_name.setText(Obj.getFName()+" "+Obj.getMName()+" "+Obj.getSurName());
        holder.tv_busi_type.setText(Obj.getIndustryName());
        holder.tv_profession.setText(Obj.getWorkProfession()+" - ");
        holder.tv_number.setText(Obj.getContactNo());

        if (Obj.getPhotoURL() != null && !TextUtils.isEmpty(Obj.getPhotoURL())) {
            try {
                PicassoTrustAll.getInstance(context)
                        .load(Obj.getPhotoURL().replace(" ", "%20"))
                        .error(R.drawable.avatar)
                        .resize(256, 256)
                        .into(holder.img_profile, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        holder.tv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, BusinessDeatilActivity.class)
                        .putExtra("obj", Obj)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                );
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, BusinessDeatilActivity.class)
                        .putExtra("obj", Obj)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                );
            }
        });

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                MyViewHolder addrHolder = (MyViewHolder) holder;
                bindMyViewHolder(addrHolder, position);

    }



    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_name, tv_busi_type,tv_profession,tv_number;
        public LinearLayout lin_info;
        public CircleImageView img_profile;
        public ImageButton tv_send;

        public MyViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.tv_name);
            tv_busi_type = view.findViewById(R.id.tv_busi_type);
            tv_number = view.findViewById(R.id.tv_number);
            tv_profession = view.findViewById(R.id.tv_profession);
            img_profile = view.findViewById(R.id.img_profile);
            tv_send
                    = view.findViewById(R.id.tv_send);
        }
    }

    public class ViewHolderFooter extends RecyclerView.ViewHolder {


        public ViewHolderFooter(View view) {
            super(view);

        }
    }


}


