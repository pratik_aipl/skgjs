package com.skgjst.activities.job;

import android.app.DownloadManager;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.App;
import com.skgjst.model.Candidates;
import com.skgjst.R;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;

import de.hdodenhof.circleimageview.CircleImageView;

public class CandidatesDetailActivity  extends BaseActivity {
    public TextView tv_first_name, tv_title, tv_last_name, tv_experience, tv_notice_period,
            tv_present_location, tv_current_ctc, tv_expected_ctc;
    public Button btn_goto_list;
    public CandidatesDetailActivity instance;
    public Candidates obj;
    public Toolbar toolbar;
    public FloatingActionButton float_download;
    public CircleImageView img_profile;
    String[] PERMISSIONS = {android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE};
    int PERMISSION_ALL = 1;

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidates_detail);

        instance = this;
        Intent intent = getIntent();

        tv_first_name = findViewById(R.id.tv_first_name);
        tv_last_name = findViewById(R.id.tv_last_name);
        tv_notice_period = findViewById(R.id.tv_notice_period);
        tv_experience = findViewById(R.id.tv_experience);
        tv_present_location = findViewById(R.id.tv_present_location);
        tv_current_ctc = findViewById(R.id.tv_current_ctc);
        tv_expected_ctc = findViewById(R.id.tv_expected_ctc);
        btn_goto_list = findViewById(R.id.btn_goto_list);
        float_download = findViewById(R.id.float_download);
        toolbar = findViewById(R.id.toolbar);
        img_profile = toolbar.findViewById(R.id.img_profile);

        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (intent.hasExtra("obj")) {
            obj = (Candidates) getIntent().getExtras().getSerializable("obj");
            setData();
        }
        tv_title.setText("CANDIDATES DETAIL");
        btn_goto_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        Utils.setRoundedImage(instance,user.getPhotoURL(),300,300,img_profile,null);
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });

        float_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (!Utils.hasPermissions(instance, PERMISSIONS)) {
                    ActivityCompat.requestPermissions(instance, PERMISSIONS, PERMISSION_ALL);

                } else {*/
                Uri URL = Uri.parse(obj.getResume().replaceAll(" ", "%20"));
                String convert = obj.getResume();
                String extantion = convert.substring(convert.lastIndexOf("."));

                String fileName = (convert.substring(convert.lastIndexOf('/') + 1));
                File pdf = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + fileName + extantion);
                Utils.showToast("Downloading", instance);
                DownloadManager.Request request = new DownloadManager.Request(URL);
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName + extantion);
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED); // to notify when download is complete
                request.allowScanningByMediaScanner();// if you want to be available from media players
                DownloadManager manager = (DownloadManager) instance.getSystemService(DOWNLOAD_SERVICE);
                manager.enqueue(request);
            }
            //}
        });


    }

    public void setData() {

        tv_first_name.setText(obj.getFirstname());
        tv_last_name.setText(obj.getLastname());
        tv_experience.setText(obj.getExperience());
        tv_current_ctc.setText(obj.getCurrentctc());
        tv_expected_ctc.setText(obj.getExpectedctc());
        tv_notice_period.setText(obj.getNoticeperiod());
        tv_present_location.setText(obj.getPresentlocation());


    }
}
