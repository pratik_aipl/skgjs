package com.skgjst.activities.matrimonial;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.App;
import com.skgjst.fonts.MyCustomTypeface;
import com.skgjst.fragment.MatrimonialSearchAdapter;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.CurrentCity;
import com.skgjst.model.MaritalStatus;
import com.skgjst.model.MatrimonialDetail;
import com.skgjst.model.MatrimonialList;
import com.skgjst.model.Village;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class MatrimonialSearchActivity  extends BaseActivity implements AsynchTaskListner {
    public Toolbar toolbar;
    public TextView tv_title, tv_dob, tv_count, tv_message;
    public MatrimonialSearchActivity instance;
    public JsonParserUniversal jParser;
    public FloatingActionButton float_add;
    public ImageView img_my_job, img_filter, imgClose, img_my_addprofile_list;
    public Spinner sp_gender, sp_min_age, sp_max_age, sp_handicap, sp_mat_status, sp_manglik, sp_village, sp_skin_color;
    public String selectGender = "", handicapId = "", manglikId = "", skinColorId = "", min_age = "0", max_age = "0", marital_status_id = "0";
    public ArrayList<String> handicapList = new ArrayList<>();
    public ArrayList<String> manglikList = new ArrayList<>();
    public Village village;
    public ArrayList<Village> villageArray = new ArrayList<>();
    public ArrayList<String> strVillageArray = new ArrayList<>();
    public int village_id ;
    String  village_name = "";
    public ArrayList<String> skinColorList = new ArrayList<>();
    public RecyclerView rv_member_list;
    public LinearLayout lin_empty;
    public MatrimonialList matrimonialList;
    public ArrayList<MatrimonialList> matrimonialLists = new ArrayList<>();
    public MatrimonialDetail matrimonialDetail;
    public ArrayList<MatrimonialDetail> matrimonialDetails = new ArrayList<>();
    public boolean isBack = false;
    public CircleImageView img_profile;
    private ArrayList<String> minAgeList = new ArrayList<>();
    private ArrayList<String> maxAgeList = new ArrayList<>();
    public AutoCompleteTextView et_city,et_native;
    public CurrentCity city;
    public ArrayList<CurrentCity> cityArray = new ArrayList<>();
    public ArrayList<String> stringCityArray = new ArrayList<>();
    public String city_id = "", city_name = "";
    public int location_id;
    public String strGen = "", strFromAge = "", strToAge = "", strVillage = "", strSkinColor = "", strMaglik = "", strHandicap = "", strCity = "";
    public int selected_village, sel_min_age, sel_max_age, selected_skin;
    public MaritalStatus maritalStatus;
    public ArrayList<MaritalStatus> maritalStatusArray = new ArrayList<>();
    public ArrayList<String> strMaritalStatusArray = new ArrayList<>();

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_search);
        instance = MatrimonialSearchActivity.this;
        jParser = new JsonParserUniversal();
        tv_count = findViewById(R.id.tv_count);
        tv_message = findViewById(R.id.tv_message);
        toolbar = findViewById(R.id.toolbar);
        lin_empty = findViewById(R.id.lin_empty);
        img_my_job = findViewById(R.id.img_my_job);
        img_profile = findViewById(R.id.img_profile);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("MATRIMONIAL SEARCH");

        float_add = findViewById(R.id.float_add);

        //float_add.setVisibility(View.GONE);
        float_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, AddMatrimonialActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                );
            }
        });

        img_filter = findViewById(R.id.img_filter);
        img_my_addprofile_list = findViewById(R.id.img_my_addprofile_list);
        img_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFilter();
            }
        });
        rv_member_list = findViewById(R.id.rv_member_list);
        //new CallRequest(instance).MatrimonialListActivity();
        img_my_addprofile_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                img_my_job.setVisibility(View.GONE);
                img_filter.setVisibility(View.GONE);
                float_add.hide();
                isBack = true;
                tv_title.setText("MY MATRIMONIAL LIST");
                img_my_addprofile_list.setVisibility(View.GONE);
                new CallRequest(instance).myprofiles(user);

            }
        });

        img_my_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                img_my_job.setVisibility(View.GONE);
                img_filter.setVisibility(View.GONE);
                img_my_addprofile_list.setVisibility(View.GONE);
                isBack = true;
                tv_title.setText("ADDED FAMILY MEMBERS");
                new CallRequest(instance).myMatrimonial(user);

            }
        });
        new CallRequest(instance).MatrimonialListActivity(user);

      /*  if (App.isRemove) {
            img_my_job.setVisibility(View.GONE);
            img_filter.setVisibility(View.GONE);
            float_add.hide();
            isBack = true;
            tv_title.setText("MY MATRIMONIAL LIST");
            img_my_addprofile_list.setVisibility(View.GONE);
            new CallRequest(instance).myprofiles();
        } else {
             App.isRemove = false;

        }*/
        Utils.setRoundedImage(instance,user.getPhotoURL(),300,300,img_profile,null);

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (App.isRemove) {
            img_my_job.setVisibility(View.GONE);
            img_filter.setVisibility(View.GONE);
            float_add.hide();
            isBack = true;
            tv_title.setText("MY MATRIMONIAL LIST");
            img_my_addprofile_list.setVisibility(View.GONE);
            new CallRequest(instance).myprofiles(user);
            App.isRemove = false;

        }else if(App.isMatriEdit){
            img_my_job.setVisibility(View.GONE);
            img_filter.setVisibility(View.GONE);
            img_my_addprofile_list.setVisibility(View.GONE);
            isBack = true;
            tv_title.setText("ADDED FAMILY MEMBERS");
            new CallRequest(instance).myMatrimonial(user);

        }else{
            new CallRequest(instance).MatrimonialListActivity(user);
        }
        ///    new CallRequest(instance).MatrimonialListActivity();
        //new CallRequest(instance).matriSearch(village_name, min_age, max_age, skinColorId, handicapId, selectGender, manglikId);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void openFilter() {
        final Dialog dialog = new Dialog(instance);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_matrimonial_search);
        Button btn_filter = dialog.findViewById(R.id.btn_filter);
        sp_gender = dialog.findViewById(R.id.sp_gender);
        sp_max_age = dialog.findViewById(R.id.sp_max_age);
        sp_min_age = dialog.findViewById(R.id.sp_min_age);
        sp_handicap = dialog.findViewById(R.id.sp_handicap);
        sp_manglik = dialog.findViewById(R.id.sp_manglik);
       // sp_village = dialog.findViewById(R.id.sp_village);
        sp_skin_color = dialog.findViewById(R.id.sp_skin_color);
        sp_mat_status = dialog.findViewById(R.id.sp_mat_status);
        et_city = dialog.findViewById(R.id.et_city);
        et_native = dialog.findViewById(R.id.et_native);

        village = new Village();
        new CallRequest(instance).getVillageList();
        new CallRequest(instance).getCity();
        new CallRequest(instance).getMaritalStatus();
        maritalStatus = new MaritalStatus();
        minAgeList.clear();
        maxAgeList.clear();
        max_age = "0";
        min_age = "0";
        city = new CurrentCity();

        minAgeList.add("From Age");
        maxAgeList.add("To Age");

        for (int i = 18; i < 100; i++) {
            minAgeList.add(String.valueOf(i));

        }
        for (int i = 0; i < minAgeList.size(); i++) {
            if (min_age.equalsIgnoreCase(minAgeList.get(i))) {
                sel_min_age = i + 1;
            }
        }

        for (int i = 19; i <= 100; i++) {
            maxAgeList.add(String.valueOf(i));


        }

       /* if(strGen.equalsIgnoreCase("Male")){
            sp_gender.setSelection(1);
        }else if(strGen.equalsIgnoreCase("Female")){
            sp_gender.setSelection(2);
        }
*/
        sp_gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        if (position == 1) {
                            selectGender = "M";
                            strGen = "Male";
                        }
                        if (position == 2) {
                            selectGender = "F";
                            strGen = "Female";
                        }

                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_min_age.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, minAgeList));
        sp_min_age.setSelection(sel_min_age);
        sp_max_age.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, maxAgeList));

        handicapList.clear();
        handicapList.add("Select Handicap");
        handicapList.add("No");
        handicapList.add("Yes");
        sp_handicap.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, handicapList));
        sp_handicap.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (i > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        if (i == 0) {
                            handicapId = "";
                        } else if (i == 1) {
                            handicapId = "No";
                        } else if (i == 2) {
                            handicapId = "Yes";
                        }
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        manglikList.clear();
        manglikList.add("Select Manglik");
        manglikList.add("No");
        manglikList.add("Yes");

        sp_manglik.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, manglikList));

        sp_manglik.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (i > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        if (i == 0) {
                            manglikId = "";
                        } else if (i == 1) {
                            manglikId = "No";
                        } else if (i == 2) {
                            manglikId = "Yes";
                        }
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_mat_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        marital_status_id = String.valueOf(maritalStatusArray.get(position).getMaritalstatusID());

                    } else {
                        marital_status_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
     /*   sp_village.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        village_id = String.valueOf(villageArray.get(position).getVillageID());
                        village_name = villageArray.get(position).getVillagename();

                    } else {
                        village_id = "0";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        skinColorList.clear();
        skinColorList.add("Select Skin Color");
        skinColorList.add("Fair");
        skinColorList.add("Moderate");
        skinColorList.add("Brownish");
        skinColorList.add("Blackish");
        skinColorList.add("Dark");
        if (skinColorId.equalsIgnoreCase("Fair")) {
            selected_skin = 1;
        } else if (skinColorId.equalsIgnoreCase("Moderate")) {
            selected_skin = 2;
        } else if (skinColorId.equalsIgnoreCase("Brownish")) {
            selected_skin = 3;
        } else if (skinColorId.equalsIgnoreCase("Blackish")) {
            selected_skin = 4;
        } else if (skinColorId.equalsIgnoreCase("Dark")) {
            selected_skin = 5;
        }

        sp_skin_color.setSelection(selected_skin);
        sp_skin_color.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, skinColorList));
        sp_skin_color.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));

                        if (position == 0) {
                            skinColorId = "";
                        } else if (position == 1) {
                            skinColorId = "Fair";
                        } else if (position == 2) {
                            skinColorId = "Moderate";
                        } else if (position == 3) {
                            skinColorId = "Brownish";
                        } else if (position == 4) {
                            skinColorId = "Blackish";
                        } else if (position == 5) {
                            skinColorId = "Dark";
                        }
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_min_age.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        min_age = minAgeList.get(position);
                        int minmum_age = Integer.parseInt(minAgeList.get(position));

                        maxAgeList.clear();
                        max_age = "0";
                        maxAgeList.add("To Age");

                        for (int i = (minmum_age + 1); i <= 100; i++) {
                            maxAgeList.add(String.valueOf(i));
                        }
                        sp_max_age.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, maxAgeList));
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        sp_max_age.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        max_age = maxAgeList.get(position);

                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        imgClose = dialog.findViewById(R.id.imgClose);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

            }
        });
        et_city.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                city_name = parent.getItemAtPosition(position).toString();
                for (int i = 0; i < cityArray.size(); i++) {
                    if (cityArray.get(i).getCity().equalsIgnoreCase(city_name)) {
                        location_id = (cityArray.get(i).getLocationID());
                    }

                }
                Log.i("City_name", "==>" + city_name);
                Log.i("location_id", "==>" + location_id);
            }
        });
        et_native.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                village_name = parent.getItemAtPosition(position).toString();
                for (int i = 0; i < villageArray.size(); i++) {
                    if (villageArray.get(i).getVillagename().equalsIgnoreCase(village_name)) {
                        village_id = (villageArray.get(i).getVillageID());
                    }

                }
                Log.i("City_name", "==>" + city_name);
                Log.i("location_id", "==>" + location_id);
            }
        });
        btn_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                isBack = true;

                new CallRequest(instance).matriSearch(user,village_name, min_age, max_age, skinColorId, handicapId, selectGender, manglikId, String.valueOf(location_id), marital_status_id);
            }
        });
        dialog.show();

    }

    @Override
    public void onBackPressed() {
        if (isBack) {
            new CallRequest(instance).MatrimonialListActivity(user);
            img_my_job.setVisibility(View.VISIBLE);
            img_filter.setVisibility(View.VISIBLE);
            float_add.show();
            isBack = false;
            tv_title.setText("MATRIMONIAL SEARCH");


        } else {
            super.onBackPressed();

        }

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {

        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case MatrimonialList:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            matrimonialDetails.clear();
                            matrimonialLists.clear();
                            float_add.setVisibility(View.VISIBLE);
                            img_my_job.setVisibility(View.VISIBLE);
                            img_filter.setVisibility(View.VISIBLE);
                            img_my_addprofile_list.setVisibility(View.VISIBLE);

                            if (jObj.getJSONArray("MatrimonialData") != null && jObj.getJSONArray("MatrimonialData").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("MatrimonialData");

                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    lin_empty.setVisibility(View.GONE);
                                    rv_member_list.setVisibility(View.VISIBLE);
                                    matrimonialLists.clear();
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        matrimonialList = (MatrimonialList) jParser.parseJson(jSurnameArray.getJSONObject(i), new MatrimonialList());
                                        matrimonialLists.add(matrimonialList);
                                    }
                                    tv_count.setText(matrimonialLists.size() + " - Profile Found");

                                    setUpRecycleview();
                                } else {
                                    tv_count.setText("0 - Profile Found");

                                    tv_message.setText(jObj.getString("UMessage"));
                                    //   Utils.showToast(jObj.getString("UMessage"), this);
                                    lin_empty.setVisibility(View.VISIBLE);
                                    rv_member_list.setVisibility(View.GONE);

                                    Utils.showToast(jObj.getString("UMessage"), instance);
                                }
                            } else {
                                tv_count.setText("0 - Profile Found");
                                tv_message.setText(jObj.getString("UMessage"));
                                //   Utils.showToast(jObj.getString("UMessage"), this);
                                lin_empty.setVisibility(View.VISIBLE);
                                rv_member_list.setVisibility(View.GONE);
                                Utils.showToast(jObj.getString("UMessage"), instance);
                            }
                        } else {
                            tv_count.setText("0 - Profile Found");
                            tv_message.setText(jObj.getString("UMessage"));
                            //   Utils.showToast(jObj.getString("UMessage"), this);
                            lin_empty.setVisibility(View.VISIBLE);
                            rv_member_list.setVisibility(View.GONE);
                            Utils.showToast(jObj.getString("UMessage"), instance);
                        }

                    } catch (JSONException e) {
                        tv_count.setText("0 - Profile Found");
                        lin_empty.setVisibility(View.VISIBLE);
                        rv_member_list.setVisibility(View.GONE);
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getMaritalStatus:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strMaritalStatusArray.clear();
                            maritalStatusArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                maritalStatus.setMaritalstatus("Marital status");
                                maritalStatus.setMaritalstatusID(0);
                                strMaritalStatusArray.add(maritalStatus.getMaritalstatus());
                                maritalStatusArray.add(maritalStatus);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        maritalStatus = (MaritalStatus) jParser.parseJson(jSurnameArray.getJSONObject(i), new MaritalStatus());
                                        if (!maritalStatus.getMaritalstatus().equalsIgnoreCase("Married")) {
                                            maritalStatusArray.add(maritalStatus);
                                            strMaritalStatusArray.add(maritalStatus.getMaritalstatus());

                                        }
                                    }
                                    sp_mat_status.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strMaritalStatusArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getCity:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            stringCityArray.clear();
                            cityArray.clear();
                            if (jObj.getJSONArray("CityList") != null && jObj.getJSONArray("CityList").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("CityList");

                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        city = (CurrentCity) jParser.parseJson(jSurnameArray.getJSONObject(i), new CurrentCity());
                                        cityArray.add(city);
                                        stringCityArray.add(city.getCity());
                                    }
                                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                            (this, android.R.layout.simple_dropdown_item_1line, stringCityArray);

                                    et_city.setThreshold(1);//will start working from first character
                                    et_city.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                                    et_city.setTextColor(Color.BLACK);

                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case myMatrimonial:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            matrimonialDetails.clear();
                            matrimonialLists.clear();
                            float_add.setVisibility(View.GONE);
                            if (jObj.getJSONArray("MatrimonialData") != null && jObj.getJSONArray("MatrimonialData").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("MatrimonialData");

                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    lin_empty.setVisibility(View.GONE);
                                    rv_member_list.setVisibility(View.VISIBLE);
                                    matrimonialDetails.clear();
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        matrimonialDetail = (MatrimonialDetail) jParser.parseJson(jSurnameArray.getJSONObject(i), new MatrimonialDetail());
                                        matrimonialDetails.add(matrimonialDetail);
                                    }
                                    tv_count.setText(matrimonialDetails.size() + " - Profile Found");

                                    setUpRecycleview2();
                                } else {
                                    tv_count.setText("0 - Profile Found");

                                    tv_message.setText(jObj.getString("UMessage"));
                                    //   Utils.showToast(jObj.getString("UMessage"), this);
                                    lin_empty.setVisibility(View.VISIBLE);
                                    rv_member_list.setVisibility(View.GONE);

                                    Utils.showToast(jObj.getString("UMessage"), instance);
                                }
                            } else {
                                tv_count.setText("0 - Profile Found");
                                tv_message.setText(jObj.getString("UMessage"));
                                //   Utils.showToast(jObj.getString("UMessage"), this);
                                lin_empty.setVisibility(View.VISIBLE);
                                rv_member_list.setVisibility(View.GONE);
                                Utils.showToast(jObj.getString("UMessage"), instance);
                            }
                        } else {
                            tv_count.setText("0 - Profile Found");
                            tv_message.setText(jObj.getString("UMessage"));
                            //   Utils.showToast(jObj.getString("UMessage"), this);
                            lin_empty.setVisibility(View.VISIBLE);
                            rv_member_list.setVisibility(View.GONE);
                            Utils.showToast(jObj.getString("UMessage"), instance);
                        }

                    } catch (JSONException e) {
                        tv_count.setText("0 - Profile Found");
                        lin_empty.setVisibility(View.VISIBLE);
                        rv_member_list.setVisibility(View.GONE);
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
                case getVillageList:

                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strVillageArray.clear();
                            villageArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        village = (Village) jParser.parseJson(jSurnameArray.getJSONObject(i), new Village());
                                        villageArray.add(village);
                                        strVillageArray.add(village.getVillagename());
                                    }
                                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                            (this, android.R.layout.simple_dropdown_item_1line, strVillageArray);

                                    et_native.setThreshold(1);//will start working from first character
                                    et_native.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView
                                    et_native.setTextColor(Color.BLACK);

                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;

                   /* Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strVillageArray.clear();
                            villageArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                village.setVillagename("Native Place");
                                village.setVillageID(0);
                                strVillageArray.add(village.getVillagename());
                                villageArray.add(village);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        village = (Village) jParser.parseJson(jSurnameArray.getJSONObject(i), new Village());
                                        villageArray.add(village);
                                        strVillageArray.add(village.getVillagename());

                                        if (village.getVillagename().equalsIgnoreCase(village_name)) {
                                            selected_village = i + 1;
                                        }
                                    }
                                    sp_village.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strVillageArray));
                                    //   sp_village.setSelection(selected_village);
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;*/

                case myprofiles:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            matrimonialDetails.clear();
                            matrimonialLists.clear();
                            float_add.setVisibility(View.GONE);
                            if (jObj.getJSONArray("Profiledata") != null && jObj.getJSONArray("Profiledata").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("Profiledata");

                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    lin_empty.setVisibility(View.GONE);
                                    rv_member_list.setVisibility(View.VISIBLE);
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        matrimonialList = (MatrimonialList) jParser.parseJson(jSurnameArray.getJSONObject(i), new MatrimonialList());
                                        matrimonialLists.add(matrimonialList);
                                    }
                                    tv_count.setText(matrimonialLists.size() + " - Profile Found");

                                    setUpRecycleview3();
                                } else {
                                    tv_count.setText("0 - Profile Found");

                                    tv_message.setText(jObj.getString("UMessage"));
                                    //   Utils.showToast(jObj.getString("UMessage"), this);
                                    lin_empty.setVisibility(View.VISIBLE);
                                    rv_member_list.setVisibility(View.GONE);

                                    Utils.showToast(jObj.getString("UMessage"), instance);
                                }
                            } else {
                                tv_count.setText("0 - Profile Found");
                                tv_message.setText(jObj.getString("UMessage"));
                                //   Utils.showToast(jObj.getString("UMessage"), this);
                                lin_empty.setVisibility(View.VISIBLE);
                                rv_member_list.setVisibility(View.GONE);
                                Utils.showToast(jObj.getString("UMessage"), instance);
                            }

                        } else {
                            tv_count.setText("0 - Profile Found");
                            tv_message.setText(jObj.getString("UMessage"));
                            //   Utils.showToast(jObj.getString("UMessage"), this);
                            lin_empty.setVisibility(View.VISIBLE);
                            rv_member_list.setVisibility(View.GONE);
                            Utils.showToast(jObj.getString("UMessage"), instance);
                        }

                    } catch (JSONException e) {
                        tv_count.setText("0 - Profile Found");
                        lin_empty.setVisibility(View.VISIBLE);
                        rv_member_list.setVisibility(View.GONE);
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }

    private void setUpRecycleview() {
        MatrimonialSearchAdapter memberSearchAdapter = new MatrimonialSearchAdapter(matrimonialLists, instance);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);
        rv_member_list.setLayoutAnimation(controller);
        rv_member_list.scheduleLayoutAnimation();
        rv_member_list.setLayoutManager(new LinearLayoutManager(instance, LinearLayoutManager.VERTICAL, false));
        rv_member_list.setItemAnimator(new DefaultItemAnimator());
        rv_member_list.setAdapter(memberSearchAdapter);
    }

    private void setUpRecycleview2() {
        MyMatrimonialAdapter memberSearchAdapter = new MyMatrimonialAdapter(matrimonialDetails, instance);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);
        rv_member_list.setLayoutAnimation(controller);
        rv_member_list.scheduleLayoutAnimation();
        rv_member_list.setLayoutManager(new LinearLayoutManager(instance, LinearLayoutManager.VERTICAL, false));
        rv_member_list.setItemAnimator(new DefaultItemAnimator());
        rv_member_list.setAdapter(memberSearchAdapter);
    }

    private void setUpRecycleview3() {
        MyProfileMatriAdapter memberSearchAdapter = new MyProfileMatriAdapter(matrimonialLists, instance);
        final LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(instance, R.anim.layout_animation_fall_down);
        rv_member_list.setLayoutAnimation(controller);
        rv_member_list.scheduleLayoutAnimation();
        rv_member_list.setLayoutManager(new LinearLayoutManager(instance, LinearLayoutManager.VERTICAL, false));
        rv_member_list.setItemAnimator(new DefaultItemAnimator());
        rv_member_list.setAdapter(memberSearchAdapter);
    }

}
