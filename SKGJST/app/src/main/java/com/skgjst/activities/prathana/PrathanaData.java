package com.skgjst.activities.prathana;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 11/2/2018.
 */

public class PrathanaData implements Serializable{
    public int PrathnaID;
    public String Name = "";
    public String PrathnaDate = "";
    public String PrathnaPlace = "";

    public int getPrathnaID() {
        return PrathnaID;
    }

    public void setPrathnaID(int prathnaID) {
        PrathnaID = prathnaID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPrathnaDate() {
        return PrathnaDate;
    }

    public void setPrathnaDate(String prathnaDate) {
        PrathnaDate = prathnaDate;
    }

    public String getPrathnaPlace() {
        return PrathnaPlace;
    }

    public void setPrathnaPlace(String prathnaPlace) {
        PrathnaPlace = prathnaPlace;
    }

    public String getPrathnaInfo() {
        return PrathnaInfo;
    }

    public void setPrathnaInfo(String prathnaInfo) {
        PrathnaInfo = prathnaInfo;
    }

    public String getCreatedBy() {
        return CreatedBy;
    }

    public void setCreatedBy(String createdBy) {
        CreatedBy = createdBy;
    }

    public String PrathnaInfo = "";
    public String CreatedBy = "";
}
