package com.skgjst.activities.demise;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.skgjst.App;
import com.skgjst.BaseActivity;
import com.skgjst.R;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.DemiseDataNew;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.PicassoTrustAll;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyPrathnaDetailActivity extends BaseActivity implements AsynchTaskListner {
    private static final String TAG = "MyPrathnaDetailActivity";
    public MyPrathnaDetailActivity instance;
    public Toolbar toolbar;
    public JsonParserUniversal jParser;
    public TextView tv_title, tv_name, tv_father_name, tv_g_father_name, tv_surname, tv_native_place, tv_current_place, tv_demise_date, tv_contact_person, tv_contact_number, tv_other_info, tv_pd_type, tv_pd_date, tv_pd_place, tv_pd_time;
    public CircleImageView img_profile, img_profile_local;
    public DemiseDataNew obj;
    public Button btn_delete, btn_edit;
    public TextView tv_fun_time, tv_fun_place, tv_fun_date, tv_posted, tv_age, tv_type, type_info;
    ImageView img_back;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demise_detail);

        instance = this;
        jParser = new JsonParserUniversal();
        toolbar = findViewById(R.id.toolbar);
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_profile_local = findViewById(R.id.img_profile_local);
        tv_title = toolbar.findViewById(R.id.tv_title);
        tv_age = findViewById(R.id.tv_age);
        tv_name = findViewById(R.id.tv_name);
        img_back = findViewById(R.id.img_back);
        tv_fun_time = findViewById(R.id.tv_fun_time);
        tv_fun_place = findViewById(R.id.tv_fun_place);
        tv_fun_date = findViewById(R.id.tv_fun_date);
        tv_type = findViewById(R.id.tv_type);
        type_info = findViewById(R.id.typeinfo);

        //  tv_father_name = findViewById(R.id.tv_father_name);
        tv_g_father_name = findViewById(R.id.tv_g_father_name);
        // tv_surname = findViewById(R.id.tv_surname);
        tv_native_place = findViewById(R.id.tv_native_place);
        //  tv_current_place = findViewById(R.id.tv_current_place);
        tv_demise_date = findViewById(R.id.tv_demise_date);
        tv_contact_person = findViewById(R.id.tv_contact_person);
        tv_posted = findViewById(R.id.tv_posted);

        tv_contact_number = findViewById(R.id.tv_contact_number);
        tv_other_info = findViewById(R.id.tv_other_info);
        tv_pd_type = findViewById(R.id.tv_pd_type);
        tv_pd_date = findViewById(R.id.tv_pd_date);
        tv_pd_place = findViewById(R.id.tv_pd_place);
        tv_pd_time = findViewById(R.id.tv_pd_time);

        btn_delete = findViewById(R.id.btn_delete);
        btn_edit = findViewById(R.id.btn_edit);
        btn_delete.setVisibility(View.VISIBLE);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                App.isChageicon = true;
            }
        });
        if (user.getPhotoURL() != null)
            Utils.setRoundedImage(instance, user.getPhotoURL(), 300, 300, img_profile, null);

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });

        Intent intent = getIntent();

        tv_title.setText("DEMISE DETAIL");
        btn_edit.setVisibility(View.VISIBLE);
        if (intent.getStringExtra("DemiseType").equalsIgnoreCase("Prathna")) {
            tv_title.setText("PRATHNA DETAIL");
         /*   tv_fun_date.setText("Date : ");
            tv_fun_place.setText("Place : ");
            tv_fun_time.setText("Time : ");*/
            tv_type.setText("Prathna");
            tv_type.setCompoundDrawablesWithIntrinsicBounds(R.drawable.prathana_logo, 0, R.drawable.prathana_logo, 0);

            type_info.setText("Prathna Info");

        }

        if (intent.hasExtra("obj")) {
            obj = (DemiseDataNew) getIntent().getExtras().getSerializable("obj");
            setData();

        }

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new CallRequest(instance).deleteDemise(user, String.valueOf(obj.getDemisePrathnaID()));
            }
        });

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, PrathnaEditActivity.class)
                        .putExtra("DemiseType", obj.getPDType())
                        .putExtra("obj", obj));

            }
        });
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        App.isChageicon = true;
    }

    private void setData() {
        tv_name.setText(obj.getName() + "  " + obj.getFatherName() + "  " + obj.getSurname());
        tv_g_father_name.setText(obj.getGrandFatherName());
        // tv_surname.setText(obj.getSurname());
        tv_native_place.setText(obj.getNativePlace());
        // tv_current_place.setText(obj.getCurrenPlace());
        tv_demise_date.setText(obj.getDemiseDate());
        tv_contact_person.setText(obj.getContactPerson());
        tv_contact_number.setText(obj.getContactNumber());
        tv_other_info.setText(obj.getOtherInfo());
        Log.d(TAG, "setData: " + gson.toJson(obj));
        Log.d(TAG, "setData: " + obj.getOtherInfo());
        if (obj.getAge() != null && !obj.getAge().equalsIgnoreCase("0")) {
            tv_age.setText("(" + "Age" + obj.getAge() + ")");
        } else {
            tv_age.setVisibility(View.GONE);
        }
        if (obj.getPDType().equals("D")) {
            tv_pd_type.setText("Demise");
        } else {
            tv_pd_type.setText("Prathana");
        }
        tv_posted.setText(obj.getPostedBy());

        tv_pd_date.setText(obj.getPDDate());
        tv_pd_place.setText(obj.getPDPlace());
        tv_pd_time.setText(obj.getPDTime());

        if (obj.getIseditable().equalsIgnoreCase("1")) {
            btn_edit.setVisibility(View.VISIBLE);
        }

        if (obj.getImageURL() != null) {
            try {
                PicassoTrustAll.getInstance(instance)
                        .load(obj.getImageURL().replace(" ", "%20"))
                        .error(R.drawable.avatar)
                        .resize(256, 256)
                        .into(img_profile_local, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {

                case deleteDemise:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success") == true) {
                            Utils.showToast(jObj.getString("UMessage"), instance);
                            onBackPressed();
                            App.isChageicon = true;
                        } else {
                            Utils.showToast(jObj.getString("UMessage"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }
}
