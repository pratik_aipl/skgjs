package com.skgjst.activities.matrimonial;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.App;
import com.skgjst.fonts.MyCustomTypeface;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.MatrimonialMember;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddMatrimonialActivity  extends BaseActivity implements AsynchTaskListner {
    public Toolbar toolbar;
    public TextView tv_title, tv_dob;
    public AddMatrimonialActivity instance;
    public JsonParserUniversal jParser;
    public Spinner sp_skin_color, sp_handicap, sp_manglik, sp_names, sp_active;
    public ArrayList<String> skinColorList = new ArrayList<>();
    public ArrayList<String> handicapList = new ArrayList<>();
    public ArrayList<String> manglikList = new ArrayList<>();
    public CircleImageView img_profile_local, img_profile;
    public String logoPath = "", filename = "", FfamilyDetailId = "", skinColorId = "", handicapId = "", manglikId = "", isactiveId = "";
    public MatrimonialMember matrimonialMember;
    public ArrayList<MatrimonialMember> matrimonialMemberArray = new ArrayList<>();
    public ArrayList<String> strMatrimonialMemberArray = new ArrayList<>();
    public ProgressBar pbar;
    public Button btn_add, btn_cancel;
    public EditText et_height, et_weight, et_handi_detail, et_other_info;
    public ArrayList<String> activateList = new ArrayList<>();
    Uri selectedUri;

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_matrimonial);
        instance = AddMatrimonialActivity.this;
        jParser = new JsonParserUniversal();
        toolbar = findViewById(R.id.toolbar);
        pbar = findViewById(R.id.pbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        img_profile = toolbar.findViewById(R.id.img_profile);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("ADD MATRIMONIAL");
        Validation();
        Utils.setRoundedImage(instance,user.getPhotoURL(),300,300,img_profile,null);
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });
    }

    private void Validation() {
        //  et_name = findViewById(R.id.et_name);
        // et_name.setVisibility(View.GONE);
        et_height = findViewById(R.id.et_height);
        et_weight = findViewById(R.id.et_weight);
        et_handi_detail = findViewById(R.id.et_handi_detail);
        et_other_info = findViewById(R.id.et_other_info);
        img_profile_local = findViewById(R.id.img_profile_local);
        sp_skin_color = findViewById(R.id.sp_skin_color);
        sp_handicap = findViewById(R.id.sp_handicap);
        sp_active = findViewById(R.id.sp_active);
        sp_names = findViewById(R.id.sp_names);
        sp_manglik = findViewById(R.id.sp_manglik);
        btn_add = findViewById(R.id.btn_add);
        btn_cancel = findViewById(R.id.btn_cancel);
        skinColorList.clear();
        skinColorList.add("Select Skin Color*");
        skinColorList.add("Fair");
        skinColorList.add("Moderate");
        skinColorList.add("Brownish");
        skinColorList.add("Blackish");
        skinColorList.add("Dark");
        sp_skin_color.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, skinColorList));
        sp_skin_color.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));

                        if (position == 0) {
                            skinColorId = "";
                        } else if (position == 1) {
                            skinColorId = "Fair";
                        } else if (position == 2) {
                            skinColorId = "Moderate";
                        } else if (position == 3) {
                            skinColorId = "Brownish";
                        } else if (position == 4) {
                            skinColorId = "Blackish";
                        } else if (position == 5) {
                            skinColorId = "Dark";
                        }
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        activateList.clear();
        activateList.add("Select Activation*");
        activateList.add("No");
        activateList.add("Yes");
        sp_active.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, activateList));
        sp_active.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (i > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        if (i == 0) {
                            isactiveId = "";
                        } else if (i == 1) {
                            isactiveId = "No";
                        } else if (i == 2) {
                            isactiveId = "Yes";
                        }
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        handicapList.clear();
        handicapList.add("Select Handicap");
        handicapList.add("No");
        handicapList.add("Yes");

        sp_handicap.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, handicapList));
        sp_handicap.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (i > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        if (i == 0) {
                            handicapId = "";
                        } else if (i == 1) {
                            handicapId = "No";
                        } else if (i == 2) {
                            handicapId = "Yes";
                        }
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        manglikList.clear();
        manglikList.add("Select Manglik");
        manglikList.add("No");
        manglikList.add("Yes");

        sp_manglik.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, manglikList));

        sp_manglik.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (i > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        if (i == 0) {
                            manglikId = "";
                        } else if (i == 1) {
                            manglikId = "No";
                        } else if (i == 2) {
                            manglikId = "Yes";
                        }
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        img_profile_local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });
        matrimonialMember = new MatrimonialMember();
        new CallRequest(AddMatrimonialActivity.this).getMatrimonialMembers(user);
        sp_names.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        FfamilyDetailId = String.valueOf(matrimonialMemberArray.get(position).getFamilyDetailID());
                    } else {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (FfamilyDetailId.equalsIgnoreCase("")) {
                    Utils.showToast("Please select member", AddMatrimonialActivity.this);
                } else if (et_height.getText().toString().isEmpty()) {
                    et_height.requestFocus();
                    et_height.setError("Please enter height");
                } else if (et_weight.getText().toString().isEmpty()) {
                    et_weight.requestFocus();
                    et_weight.setError("Please enter weight");
                } else if (skinColorId.equalsIgnoreCase("")) {
                    Utils.showToast("Please select skin color", AddMatrimonialActivity.this);
                } else if (logoPath.equalsIgnoreCase("")) {
                    Utils.showToast("Please select image", AddMatrimonialActivity.this);
                } else {

                    new CallRequest(AddMatrimonialActivity.this).addMatrimonialDetails(user,FfamilyDetailId, et_height.getText().toString(), et_weight.getText().toString(),
                            skinColorId, handicapId, et_handi_detail.getText().toString(), manglikId, logoPath, filename, et_other_info.getText().toString(), isactiveId);
                }
            }
        });


    }

    public void selectImage() {
        CropImage.startPickImageActivity(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);

            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                selectedUri = imageUri;
                logoPath = selectedUri.getPath().toString();
                filename = (logoPath.substring(logoPath.lastIndexOf('/') + 1));

                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    img_profile_local.setImageURI(result.getUri());
                    selectedUri = result.getUri();
                    logoPath = selectedUri.getPath().toString();
                    filename = (logoPath.substring(logoPath.lastIndexOf('/') + 1));
                    Log.i("TAG", "logoPath :-> " + logoPath);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }

    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .setFixAspectRatio(true)

                .setMultiTouchEnabled(true)
                .start(this);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case GetMatrimonialMember:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strMatrimonialMemberArray.clear();
                            matrimonialMemberArray.clear();
                            matrimonialMember.setFName("Select Members*");
                            matrimonialMember.setFamilyDetailID(0);
                            strMatrimonialMemberArray.add(matrimonialMember.getFName());
                            matrimonialMemberArray.add(matrimonialMember);
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        matrimonialMember = (MatrimonialMember) jParser.parseJson(jSurnameArray.getJSONObject(i), new MatrimonialMember());
                                        matrimonialMemberArray.add(matrimonialMember);
                                        strMatrimonialMemberArray.add(matrimonialMember.getFName());
                                    }
                                    sp_names.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strMatrimonialMemberArray));

                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            } else {
                                strMatrimonialMemberArray.clear();
                                matrimonialMemberArray.clear();
                                matrimonialMember.setFName("Select Name*");
                                matrimonialMember.setFamilyDetailID(0);
                                strMatrimonialMemberArray.add(matrimonialMember.getFName());
                                matrimonialMemberArray.add(matrimonialMember);

                                sp_names.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strMatrimonialMemberArray));

                            }

                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;

                case AddMatrimonialDetails:
                    Utils.hideProgressDialog();
                    JSONObject jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            Utils.showToast(jObj.getString("UMessage"), this);
                            onBackPressed();

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }


}
