package com.skgjst.activities;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.App;
import com.skgjst.fragment.OrganizationDetailsAdapter;
import com.skgjst.fragment.OrganizationList;
import com.skgjst.R;
import com.skgjst.utils.ImagePopup;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.PicassoTrustAll;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class OraganizationDetailActivity  extends BaseActivity {

    public Toolbar toolbar;
    public TextView tv_title;
    public CircleImageView img_profile, img_profile_local, ivOrganization;
    public OraganizationDetailActivity instance;
    public JsonParserUniversal jParser;
    public OrganizationList organizationList;
    public TextView tvOrganizationName, tvOrganizationDescription, tvAddress,
            tvContactPerson, tvContactNumber, tvEmailAddress, tvWebsiteURL, tvNoDetails;
    public RecyclerView rvTrustee;
    public LinearLayoutManager layoutManager;
    public LinearLayout lin_empty;

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oraganization_detail);
        instance = OraganizationDetailActivity.this;
        toolbar = findViewById(R.id.toolbar);
        lin_empty = findViewById(R.id.lin_empty);
        tv_title = toolbar.findViewById(R.id.tv_title);
        jParser = new JsonParserUniversal();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("ORGANIZATION DETAIL");
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_profile_local = toolbar.findViewById(R.id.img_profile_local);

        Utils.setRoundedImage(instance,user.getPhotoURL(),300,300,img_profile,null);

        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });
        Intent intent = getIntent();
        ivOrganization = findViewById(R.id.ivOrganization);
        tvOrganizationName = findViewById(R.id.tvOrganizationName);
        tvOrganizationDescription = findViewById(R.id.tvOrganizationDescription);
        tvAddress = findViewById(R.id.tvAddress);
        tvContactPerson = findViewById(R.id.tvContactPerson);
        tvContactNumber = findViewById(R.id.tvContactNumber);
        tvEmailAddress = findViewById(R.id.tvEmailAddress);
        tvWebsiteURL = findViewById(R.id.tvWebsiteURL);
        tvNoDetails = findViewById(R.id.tvNoDetails);
        rvTrustee = findViewById(R.id.rvTrustee);

        layoutManager = new LinearLayoutManager(instance);
        rvTrustee.setLayoutManager(layoutManager);


        if (intent.hasExtra("obj")) {
            organizationList = (OrganizationList) getIntent().getExtras().getSerializable("obj");
            setData(organizationList);
        }
    }

    private void setData(final OrganizationList organizationList) {
        if (!TextUtils.isEmpty(organizationList.getOrgLogo())) {
            try {
                PicassoTrustAll.getInstance(instance)
                        .load(organizationList.getOrgLogo().replace(" ", "%20"))
                        .placeholder(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        //.resize(256, 256)
                        .into(ivOrganization, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        final ImagePopup imagePopup = new ImagePopup(instance);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.BLACK);  // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(true);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional

        imagePopup.initiatePopupWithPicasso(organizationList.getOrgLogo().replace(" ", "%20"));

        ivOrganization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagePopup.viewPopup();
            }
        });

        if (organizationList.getOrgName() != null && !TextUtils.isEmpty(organizationList.getOrgName())) {
            tvOrganizationName.setText(organizationList.getOrgName());
        }

        if (organizationList.getOrgDesc() != null && !TextUtils.isEmpty(organizationList.getOrgDesc()) && !organizationList.getOrgDesc().equalsIgnoreCase("null")) {
            tvOrganizationDescription.setText(organizationList.getOrgDesc());
        }

        if (organizationList.getOrgAddress() != null && !TextUtils.isEmpty(organizationList.getOrgAddress()) && !organizationList.getOrgAddress().equalsIgnoreCase("null")) {
            tvAddress.setText(organizationList.getOrgAddress());
        }

        if (organizationList.getOrgCntPersonName() != null && !TextUtils.isEmpty(organizationList.getOrgCntPersonName()) && !organizationList.getOrgCntPersonName().equalsIgnoreCase("null")) {
            tvContactPerson.setText(organizationList.getOrgCntPersonName());
        }

        if (organizationList.getOrgContctNo() != null && !TextUtils.isEmpty(organizationList.getOrgContctNo()) && !organizationList.getOrgContctNo().equalsIgnoreCase("null")) {
            tvContactNumber.setText(organizationList.getOrgContctNo());
            tvContactNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", organizationList.getOrgContctNo(), null));
                    startActivity(intent);
                }
            });
        }

        if (organizationList.getOrgContEmail() != null && !TextUtils.isEmpty(organizationList.getOrgContEmail()) && !organizationList.getOrgContEmail().equalsIgnoreCase("null")) {
            tvEmailAddress.setText(organizationList.getOrgContEmail());
            tvEmailAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + organizationList.getOrgContEmail()));
                        intent.putExtra(Intent.EXTRA_SUBJECT, "");
                        intent.putExtra(Intent.EXTRA_TEXT, "");
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        //TODO smth
                        Utils.showToast("There is no app for email", instance);
                    }
                }
            });
        }

        if (organizationList.getWebsiteUrl() != null && !TextUtils.isEmpty(organizationList.getWebsiteUrl()) && !organizationList.getWebsiteUrl().equalsIgnoreCase("null")) {
            tvWebsiteURL.setText(organizationList.getWebsiteUrl());
            tvWebsiteURL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW);
                        browserIntent.setData(Uri.parse("http://" + organizationList.getWebsiteUrl()));
                        startActivity(browserIntent);
                    } catch (ActivityNotFoundException e) {
                        Utils.showToast("There is no app for url", instance);
                    }
                }
            });
        }

        System.out.println("trustee size:::::" + organizationList.getTrustee_Details().size());
        if (organizationList.getTrustee_Details().size() > 0) {
         //   tvNoDetails.setVisibility(View.GONE);
            lin_empty.setVisibility(View.GONE); rvTrustee.setVisibility(View.VISIBLE);

            OrganizationDetailsAdapter organizationDetailsAdapter = new OrganizationDetailsAdapter(instance, organizationList.getTrustee_Details());
            rvTrustee.setAdapter(organizationDetailsAdapter);
            rvTrustee.setNestedScrollingEnabled(false);

        } else {
            lin_empty.setVisibility(View.VISIBLE);

            rvTrustee.setVisibility(View.GONE);
        }
    }
}
