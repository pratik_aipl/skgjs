package com.skgjst.activities.events;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.activities.viewpager.HorizontalInfiniteCycleViewPager;
import com.skgjst.adapter.ImageDisplayPagerAdapter;
import com.skgjst.App;
import com.skgjst.fragment.event.EventDetailsAdapter;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.EventImages;
import com.skgjst.model.UpcomingEvent;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class EventDetailActivity  extends BaseActivity implements AsynchTaskListner {
    public Toolbar toolbar;
    public TextView tv_title, tv_no_image;
    public CircleImageView img_profile, img_profile_local;
    public EventDetailActivity instance;
    public UpcomingEvent upcomingEvent;
    public TextView tvDate, tvLocation, tvEventTitle, tvDescription;
    // RecyclerView rvImages;
    public JsonParserUniversal jParser;
    public HorizontalInfiniteCycleViewPager mViewPager;
    private RecyclerView.LayoutManager layoutManager;
    private ArrayList<EventImages> imageList = new ArrayList<>();
    private EventDetailsAdapter eventDetailsAdapter;

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        instance = this;
        toolbar = findViewById(R.id.toolbar);
        tv_no_image = findViewById(R.id.tv_no_image);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if(getIntent().getStringExtra("title").equalsIgnoreCase("Past")){
            tv_title.setText("PAST EVENT DETAILS");

        }else{
            tv_title.setText("UPCOMING EVENT DETAILS");

        }
        img_profile = toolbar.findViewById(R.id.img_profile);
        jParser = new JsonParserUniversal();
        Utils.setRoundedImage(instance,user.getPhotoURL(),300,300,img_profile,null);
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });
        mViewPager = findViewById(R.id.hicvp);

        tvEventTitle = findViewById(R.id.tvEventTitle);
        tvDate = findViewById(R.id.tvDate);
        tvLocation = findViewById(R.id.tvLocation);
        tvDescription = findViewById(R.id.tvDescription);
        //  rvImages = findViewById(R.id.rvImages);
        layoutManager = new LinearLayoutManager(instance, LinearLayoutManager.HORIZONTAL, false);
        //   rvImages.setLayoutManager(layoutManager);
        if (getIntent().hasExtra("obj")) {
            upcomingEvent = (UpcomingEvent) getIntent().getExtras().getSerializable("obj");
            new CallRequest(instance).GetEventImagesList(upcomingEvent.getEventID());
        }
        tvEventTitle.setText(upcomingEvent.getEventTitle());
        tvDate.setText(upcomingEvent.getEventDate());
        tvLocation.setText(upcomingEvent.getVenue());
        tvDescription.setText(upcomingEvent.getEventDescription());


    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            Utils.hideProgressDialog();
            switch (request) {
                case GetEventImages:
                    imageList.clear();

                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {
                            if (jObj != null) {
                                if (jObj.getJSONArray("Imagedata") != null && jObj.getJSONArray("Imagedata").length() > 0) {
                                    JSONArray jBranchArray = jObj.getJSONArray("Imagedata");
                                    for (int i = 0; i < jBranchArray.length(); i++) {
                                        JSONObject jPackag = jBranchArray.getJSONObject(i);
                                        EventImages pastEvent = (EventImages) jParser.parseJson(jPackag, new EventImages());
                                        imageList.add(pastEvent);
                                    }
                                }


                                System.out.println("size" + imageList.size());

                                eventDetailsAdapter = new EventDetailsAdapter(instance, imageList);
                                //      rvImages.setAdapter(eventDetailsAdapter);
                                //      rvImages.setVisibility(View.GONE);
                                if (imageList.size() > 0) {
                                    ImageDisplayPagerAdapter photoViewAdapter = new ImageDisplayPagerAdapter(EventDetailActivity.this, imageList);
                                    mViewPager.setAdapter(photoViewAdapter);
                                  } else {
                                    tv_no_image.setVisibility(View.VISIBLE);
                                    mViewPager.setVisibility(View.GONE);

                                }

                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        tv_no_image.setVisibility(View.VISIBLE);
                        mViewPager.setVisibility(View.GONE);
                    }
                    break;
            }
        }
    }

    private void setData() {
    }
}
