package com.skgjst.activities.matrimonial;

import java.io.Serializable;

/**
 * Created by empiere-vaibhav on 10/8/2018.
 */

public class MatriProfileList implements Serializable {
    public String FName = "";
    public String MName = "";
    public String SurName = "";
    public String Education = "";
    public String BusinessDetail = "";
    public String ContactNo = "";
    public String EmailID = "";
    public String PhotoURL = "";
    public String MaritalStatus = "";
    public String DateOfBirth = "";
    public String IndustryName = "";
    public String Gender = "";
    public String isactive = "";
    public int FamilyID = -1;
    public String BloodGroupName = "";
    public String Profession = "";
    public String NativePlace = "";
    public int MatrimonialID = -1;
    public int FamilydetailID = -1;
    public int FamilyDetailID = -1;
    public String Height = "";
    public String Weight = "";
    public String Skincolor = "";
    public String Handicap = "";
    public String Manglik = "";
    public String HandicapDetail = "";
    public String Photourl = "";
    public String otherinfo = "";
    public int matrimonialid = -1;
    public int selectionid = -1;

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    public int getFamilyID() {
        return FamilyID;
    }

    public void setFamilyID(int familyID) {
        FamilyID = familyID;
    }

    public int getMatrimonialID() {
        return MatrimonialID;
    }

    public void setMatrimonialID(int matrimonialID) {
        MatrimonialID = matrimonialID;
    }

    public int getFamilydetailID() {
        return FamilydetailID;
    }

    public void setFamilydetailID(int familydetailID) {
        FamilydetailID = familydetailID;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        DateOfBirth = dateOfBirth;
    }

    public String getIndustryName() {
        return IndustryName;
    }

    public void setIndustryName(String industryName) {
        IndustryName = industryName;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getBloodGroupName() {
        return BloodGroupName;
    }

    public void setBloodGroupName(String bloodGroupName) {
        BloodGroupName = bloodGroupName;
    }

    public String getProfession() {
        return Profession;
    }

    public void setProfession(String profession) {
        Profession = profession;
    }

    public String getFName() {
        return FName;
    }

    public void setFName(String FName) {
        this.FName = FName;
    }

    public String getMName() {
        return MName;
    }

    public void setMName(String MName) {
        this.MName = MName;
    }

    public String getSurName() {
        return SurName;
    }

    public void setSurName(String surName) {
        SurName = surName;
    }

    public String getEducation() {
        return Education;
    }

    public void setEducation(String education) {
        Education = education;
    }

    public String getBusinessDetail() {
        return BusinessDetail;
    }

    public void setBusinessDetail(String businessDetail) {
        BusinessDetail = businessDetail;
    }

    public String getContactNo() {
        return ContactNo;
    }

    public void setContactNo(String contactNo) {
        ContactNo = contactNo;
    }

    public String getEmailID() {
        return EmailID;
    }

    public void setEmailID(String emailID) {
        EmailID = emailID;
    }

    public String getPhotoURL() {
        return PhotoURL;
    }

    public void setPhotoURL(String photoURL) {
        PhotoURL = photoURL;
    }

    public String getMaritalStatus() {
        return MaritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        MaritalStatus = maritalStatus;
    }

    public String getNativePlace() {
        return NativePlace;
    }

    public void setNativePlace(String nativePlace) {
        NativePlace = nativePlace;
    }

    public int getFamilyDetailID() {
        return FamilyDetailID;
    }

    public void setFamilyDetailID(int familyDetailID) {
        FamilyDetailID = familyDetailID;
    }

    public String getHeight() {
        return Height;
    }

    public void setHeight(String height) {
        Height = height;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public String getSkincolor() {
        return Skincolor;
    }

    public void setSkincolor(String skincolor) {
        Skincolor = skincolor;
    }

    public String getHandicap() {
        return Handicap;
    }

    public void setHandicap(String handicap) {
        Handicap = handicap;
    }

    public String getManglik() {
        return Manglik;
    }

    public void setManglik(String manglik) {
        Manglik = manglik;
    }

    public String getHandicapDetail() {
        return HandicapDetail;
    }

    public void setHandicapDetail(String handicapDetail) {
        HandicapDetail = handicapDetail;
    }

    public String getPhotourl() {
        return Photourl;
    }

    public void setPhotourl(String photourl) {
        Photourl = photourl;
    }

    public String getOtherinfo() {
        return otherinfo;
    }

    public void setOtherinfo(String otherinfo) {
        this.otherinfo = otherinfo;
    }

    public int getMatrimonialid() {
        return matrimonialid;
    }

    public void setMatrimonialid(int matrimonialid) {
        this.matrimonialid = matrimonialid;
    }

    public int getSelectionid() {
        return selectionid;
    }

    public void setSelectionid(int selectionid) {
        this.selectionid = selectionid;
    }
}
