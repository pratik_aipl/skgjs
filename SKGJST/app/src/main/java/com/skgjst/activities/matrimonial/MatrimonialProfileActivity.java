package com.skgjst.activities.matrimonial;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.activities.membersearch.fragment.MemberContactFragment;
import com.skgjst.activities.membersearch.fragment.MemberFamilyFragment;
import com.skgjst.activities.membersearch.fragment.MemberProfileFragment;
import com.skgjst.activities.profile.FamilyTreeActivity;
import com.skgjst.App;
import com.skgjst.fonts.LatoRegularTextView;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.MatrimonialList;
import com.skgjst.model.Profile;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.CustPagerTransformer;
import com.skgjst.utils.ImagePopup;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MatrimonialProfileActivity  extends BaseActivity implements AsynchTaskListner {
    public static final String FRAGMENT_FIRST = "Family Member";
    public static final String FRAGMENT_SECOND = "Profile";
    public static final String FRAGMENT_THIRD = "Contact";
    public static FloatingActionButton float_add, float_tree;
    public static MatrimonialList matrimonialList;
    public Toolbar toolbar;
    public TextView tv_title, tv_name, tv_city;
    public MatrimonialProfileActivity instance;
    public ViewPager viewPager;
    public TabLayout tabLayout;
    public ProgressBar pbar;
    public CircleImageView img_profile, img_profile_local,img_tree;
    public Profile obj;
    public JsonParserUniversal jParser;
    ViewPagerAdapter adapter;
    public String profileUrl;
    public LinearLayout lin_tree;
    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        jParser = new JsonParserUniversal();
        Intent intent = getIntent();
        if (intent.hasExtra("obj")) {
            matrimonialList = (MatrimonialList) getIntent().getExtras().getSerializable("obj");
            // matriProfileList = (MatriProfileList) getIntent().getExtras().getSerializable("obj");
            App.memberFamilyDetailID = String.valueOf(matrimonialList.getFamilyDetailID());
            App.memberfamilyId = String.valueOf(matrimonialList.getFamilyID());
            //  App.memberFamilyDetailID = String.valueOf(matriProfileList.getFamilydetailID());
            //  App.memberfamilyId = String.valueOf(matriProfileList.getFamilyID());
          //  profileUrl = matrimonialList.getPhotoURL();
        }

        instance = this;


        lin_tree= findViewById(R.id.lin_tree);
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        pbar = findViewById(R.id.pbar);
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_tree = findViewById(R.id.img_tree);
        img_profile.setVisibility(View.GONE);
        img_profile_local = findViewById(R.id.img_profile_local);
        tv_name = findViewById(R.id.tv_name);
        tv_city = findViewById(R.id.tv_city);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("MEMBER PROFILE");
        viewPager = findViewById(R.id.viewPager);
        float_add = findViewById(R.id.float_add);
        float_tree = findViewById(R.id.float_tree);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        setupViewPager(viewPager);
        tv_name.setText(matrimonialList.getFName() + " " + matrimonialList.getMName() + " " + matrimonialList.getSurName());
        tv_city.setText(matrimonialList.getNativePlace());
        Log.i("Image Url", "==>" + matrimonialList.getPhotoURL());

        float_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.title = "Add Member";
                //  startActivity(new Intent(instance, AddMemberActivity.class));

            }
        });
        float_tree.setVisibility(View.GONE);
        float_add.setVisibility(View.GONE);
        lin_tree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(instance, FamilyTreeActivity.class).putExtra("FamilyDetailID",App.memberFamilyDetailID));

            }
        });
       /* float_tree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (float_add.getVisibility() == View.GONE) {
                    App.title = "EditProfile";
                    // startActivity(new Intent(instance, EditProfileActivity.class));
                    startActivity(new Intent(instance, EditProfileActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)); finish();

                } else {
                    App.title = "Family Tree";
                    startActivity(new Intent(instance, CommingSoonActivity.class));  }


            }
        });*/
        new CallRequest(instance).getMyprofilesActiviti(matrimonialList.getFamilyDetailID());

    }

    public void openImage() {
        final ImagePopup imagePopup = new ImagePopup(MatrimonialProfileActivity.this);
        imagePopup.setWindowHeight(800); // Optional
        imagePopup.setWindowWidth(800); // Optional
        imagePopup.setBackgroundColor(Color.BLACK);  // Optional
        imagePopup.setFullScreen(true); // Optional
        imagePopup.setHideCloseIcon(true);  // Optional
        imagePopup.setImageOnClickClose(true);  // Optional
        imagePopup.initiatePopupWithPicasso(profileUrl);
        img_profile_local.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagePopup.viewPopup();

            }
        });


    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new MemberFamilyFragment().newInstance(), "Family Member");
        adapter.addFragment(new MemberProfileFragment().newInstance(), "Profile");
        adapter.addFragment(new MemberContactFragment().newInstance(), "Contact");
        viewPager.setAdapter(adapter);
        viewPager.setPageTransformer(false, new CustPagerTransformer(this));

        tabLayout.setupWithViewPager(viewPager);
        setupTabFont();


        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

            @Override
            public void onPageSelected(int position) {


            }
        });

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            //    Log.i("TAG", "TAG Result : " + result);
            switch (request) {
                case getMyprofiles:
                    obj = new Profile();
                    try {
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {

                            if (jObj.getJSONArray("Profile") != null && jObj.getJSONArray("Profile").length() > 0) {
                                JSONArray userData = jObj.getJSONArray("Profile");

                                for (int i = 0; i < userData.length(); i++) {
                                    JSONObject jobj = userData.getJSONObject(i);
                                    matrimonialList = (MatrimonialList) jParser.parseJson(jobj, new MatrimonialList());
                                    matrimonialList.setPhotoURL(matrimonialList.getPhotoURL());
                                }
                                Utils.setRoundedImage(instance,matrimonialList.getPhotoURL(),300,300,img_profile_local,pbar);


                                img_profile_local.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        openImage();
                                    }
                                });

                                Utils.hideProgressDialog();
                            } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("UMessage"), instance);
                            }
                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), instance);
                        }
                    } catch (JSONException e) {
                        Utils.hideProgressDialog();
                        Utils.showToast("Something getting wrong! Please try again later.", instance);

                        e.printStackTrace();
                    }
            }
        }
    }

    private void setupTabFont() {
        LatoRegularTextView firsttab = (LatoRegularTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        firsttab.setText(FRAGMENT_FIRST);
        tabLayout.getTabAt(0).setCustomView(firsttab);

        LatoRegularTextView secondtab = (LatoRegularTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        secondtab.setText(FRAGMENT_SECOND);
        tabLayout.getTabAt(1).setCustomView(secondtab);

        LatoRegularTextView thirdtab = (LatoRegularTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        thirdtab.setText(FRAGMENT_THIRD);
        tabLayout.getTabAt(2).setCustomView(thirdtab);
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}

