package com.skgjst.activities.job;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.R;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.model.JobList;
import com.skgjst.utils.Utils;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyJobDetailActivity extends BaseActivity {
    public MyJobDetailActivity instance;
    public TextView tv_job_title, tv_exp_date, tv_title, tv_description, tv_location, tv_experience, tv_company_name;
    public JobList obj;
    public Toolbar toolbar;
    public Button btn_edit, btn_candidates;
    public CircleImageView img_profile;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_job_detail);
        instance = this;
        Intent intent = getIntent();

        tv_job_title = findViewById(R.id.tv_job_title);
        tv_description = findViewById(R.id.tv_description);
        tv_location = findViewById(R.id.tv_location);
        tv_experience = findViewById(R.id.tv_experience);
        tv_exp_date = findViewById(R.id.tv_exp_date);
        tv_company_name = findViewById(R.id.tv_company_name);
        btn_edit = findViewById(R.id.btn_edit);
        btn_candidates = findViewById(R.id.btn_candidates);
        toolbar = findViewById(R.id.toolbar);
        img_profile = toolbar.findViewById(R.id.img_profile);
        if (user.getPhotoURL() != null)
            Utils.setRoundedImage(instance, user.getPhotoURL(), 300, 300, img_profile, null);
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (intent.hasExtra("obj")) {
            obj = (JobList) getIntent().getExtras().getSerializable("obj");
            setData();
        }
        tv_title.setText(obj.getJobpost());
        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, EditJobActivity.class)
                        .putExtra("obj", obj));
            }
        });

        btn_candidates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, CandidatesAppliedActivity.class)
                        .putExtra("obj", obj));
            }
        });
    }

    public void setData() {
        tv_job_title.setText(obj.getJobpost());
        tv_company_name.setText(obj.getCompanyname());
        tv_description.setText(obj.getJobdescription());
        tv_experience.setText(obj.getExperience());
        tv_location.setText(obj.getJoblocation());
        tv_exp_date.setText(obj.getExpiryDate());
    }

}