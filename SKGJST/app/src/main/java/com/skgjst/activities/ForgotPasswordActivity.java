package com.skgjst.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class ForgotPasswordActivity  extends BaseActivity implements AsynchTaskListner {
    public ForgotPasswordActivity instance;
    public EditText et_mobile;
    public FloatingActionButton btn_next;
    public TextView tv_login;

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        instance = this;
        et_mobile = findViewById(R.id.et_mobile);
        btn_next = findViewById(R.id.btn_next);
        tv_login = findViewById(R.id.tv_login);

        tv_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_mobile.getText().toString().isEmpty()) {
                    et_mobile.setError("Please enter your registed mobile number");
                    et_mobile.setFocusable(true);
                } else {
                    new CallRequest(instance).forgotPassword(et_mobile.getText().toString());
                }
            }
        });

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            try {
                Utils.hideProgressDialog();

                switch (request) {

                    case forgotPassword:
                        Utils.hideProgressDialog();
                        JSONObject jObj = new JSONObject(result);
                        if (jObj.getBoolean("Success")) {

                            Utils.showToast(jObj.getString("UMessage"), this);

                            onBackPressed();

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }

                        break;
                }
            } catch (JSONException e) {
                Utils.hideProgressDialog();
                Utils.showToast("Something getting wrong! Please try again later.", instance);
                e.printStackTrace();
            }
        }
    }
}
