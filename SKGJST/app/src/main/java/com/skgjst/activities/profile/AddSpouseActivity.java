package com.skgjst.activities.profile;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.R;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddSpouseActivity extends BaseActivity implements AsynchTaskListner {
    public Toolbar toolbar;
    public AddSpouseActivity instance;
    public TextView tv_title;
    public ProgressBar pbar;
    public CircleImageView img_profile, img_profile_local;
    public EditText et_spouse_name, et_s_f_name, et_s_m_name, et_s_f_surname, et_s_f_village;
    public Button btn_submit;
    public String FamilyDetailID;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_spouse);
        instance = AddSpouseActivity.this;
        pbar = findViewById(R.id.pbar);
        FamilyDetailID = getIntent().getStringExtra("FamilyDetailId");
        et_spouse_name = findViewById(R.id.et_spouse_name);
        et_s_f_name = findViewById(R.id.et_s_f_name);
        et_s_m_name = findViewById(R.id.et_s_m_name);
        et_s_f_surname = findViewById(R.id.et_s_f_surname);
        et_s_f_village = findViewById(R.id.et_s_f_village);
        btn_submit = findViewById(R.id.btn_submit);
        toolbar = findViewById(R.id.toolbar);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("ADD SPOUSE");
        img_profile = toolbar.findViewById(R.id.img_profile);
        if (user.getPhotoURL() != null)
            Utils.setRoundedImage(instance, user.getPhotoURL(), 300, 300, img_profile, pbar);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_spouse_name.getText().toString().isEmpty()) {
                    et_spouse_name.requestFocus();
                    et_spouse_name.setError("Please enter spouse name");
                    return;

                } else if (et_s_f_name.getText().toString().isEmpty()) {
                    et_s_f_name.requestFocus();
                    et_s_f_name.setError("Please enter spouse father name");
                    return;

                } else if (et_s_m_name.getText().toString().isEmpty()) {
                    et_s_m_name.requestFocus();
                    et_s_m_name.setError("Please enter spouse mother name");
                    return;

                } else if (et_s_f_surname.getText().toString().isEmpty()) {
                    et_s_f_surname.requestFocus();
                    et_s_f_surname.setError("Please enter spouse father surname");
                    return;

                } else if (et_s_f_village.getText().toString().isEmpty()) {
                    et_s_f_village.requestFocus();
                    et_s_f_village.setError("Please enter spouse father village");
                    return;

                } else {
                    new CallRequest(instance).addSpouseNew(user, et_spouse_name.getText().toString(), et_s_f_name.getText().toString(),
                            et_s_m_name.getText().toString(), et_s_f_surname.getText().toString(), et_s_f_village.getText().toString(), FamilyDetailID);
                }
            }
        });


    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {

                case addSpouse:
                    Utils.hideProgressDialog();
                    JSONObject jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            Utils.showToast(jObj.getString("UMessage"), this);
                            onBackPressed();

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;


            }
        }
    }
}

