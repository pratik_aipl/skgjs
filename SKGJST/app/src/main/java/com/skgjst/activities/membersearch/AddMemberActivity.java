package com.skgjst.activities.membersearch;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.skgjst.App;
import com.skgjst.BaseActivity;
import com.skgjst.fonts.MyCustomTypeface;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.model.MaritalStatus;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.JsonParserUniversal;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddMemberActivity  extends BaseActivity implements AsynchTaskListner {
    public Toolbar toolbar;
    public TextView tv_title, tv_dob, et_suburb_name, et_village_name;
    public AddMemberActivity instance;
    public JsonParserUniversal jParser;
    public LinearLayout lin_brother, lin_sister, lin_spouse, lin_son, lin_daughter;
    public EditText et_brother_name, et_ssplace, et_ssvillage, et_sssurname, et_ssmn,
            et_ssfn, et_sister_name, et_sn, et_sfplace, et_sfvillage, et_sfsurname, et_smn, et_sfn, et_spouse_name,
            et_son_name, et_daughter_name;
    public Button btn_submit, btn_cancel;
    public CircleImageView img_profile, img_profile_local;
    public ProgressBar pbar;
    public MaritalStatus maritalStatus;
    public ArrayList<MaritalStatus> maritalStatusArray = new ArrayList<>();
    public ArrayList<String> strMaritalStatusArray = new ArrayList<>();
    public String marital_status_id = "";
    public Spinner sp_mat_status;
    public LinearLayout lin_married;

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_member);
        instance = AddMemberActivity.this;
        jParser = new JsonParserUniversal();
        toolbar = findViewById(R.id.toolbar);
        lin_married = findViewById(R.id.lin_married);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText(App.title);
        lin_brother = findViewById(R.id.lin_brother);
        lin_sister = findViewById(R.id.lin_sister);
        lin_spouse = findViewById(R.id.lin_spouse);
        lin_son = findViewById(R.id.lin_son);
        lin_daughter = findViewById(R.id.lin_daughter);
        pbar = findViewById(R.id.pbar);
        img_profile = toolbar.findViewById(R.id.img_profile);
        img_profile_local = findViewById(R.id.img_profile_local);
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        Utils.setRoundedImage(instance,user.getPhotoURL(),300,300,img_profile,pbar);

        if (getIntent().getStringExtra("memberType").equalsIgnoreCase("brother")) {
            lin_brother.setVisibility(View.VISIBLE);
            lin_sister.setVisibility(View.GONE);
            lin_spouse.setVisibility(View.GONE);
            lin_daughter.setVisibility(View.GONE);
            lin_son.setVisibility(View.GONE);
            new CallRequest(instance).getPrefilbrother(user);
        } else if (getIntent().getStringExtra("memberType").equalsIgnoreCase("sister")) {

            lin_brother.setVisibility(View.GONE);
            lin_sister.setVisibility(View.VISIBLE);
            lin_spouse.setVisibility(View.GONE);
            lin_daughter.setVisibility(View.GONE);
            lin_son.setVisibility(View.GONE);
            new CallRequest(instance).getMaritalStatus();
        } else if (getIntent().getStringExtra("memberType").equalsIgnoreCase("spouse")) {
            lin_brother.setVisibility(View.GONE);
            lin_sister.setVisibility(View.GONE);
            lin_spouse.setVisibility(View.VISIBLE);
            lin_daughter.setVisibility(View.GONE);
            lin_son.setVisibility(View.GONE);

        } else if (getIntent().getStringExtra("memberType").equalsIgnoreCase("son")) {
            lin_brother.setVisibility(View.GONE);
            lin_sister.setVisibility(View.GONE);
            lin_daughter.setVisibility(View.GONE);
            lin_spouse.setVisibility(View.GONE);
            lin_son.setVisibility(View.VISIBLE);

        } else if (getIntent().getStringExtra("memberType").equalsIgnoreCase("daughter")) {
            lin_brother.setVisibility(View.GONE);
            lin_sister.setVisibility(View.GONE);
            lin_daughter.setVisibility(View.VISIBLE);
            lin_spouse.setVisibility(View.GONE);
            lin_son.setVisibility(View.GONE);

        }
        maritalStatus = new MaritalStatus();

        Validation();
    }

    private void Validation() {
        et_daughter_name = findViewById(R.id.et_daughter_name);
        et_son_name = findViewById(R.id.et_son_name);
        et_suburb_name = findViewById(R.id.et_suburb_name);
        et_brother_name = findViewById(R.id.et_brother_name);
        et_ssplace = findViewById(R.id.et_ssplace);
        et_ssvillage = findViewById(R.id.et_ssvillage);
        et_sssurname = findViewById(R.id.et_sssurname);
        et_ssmn = findViewById(R.id.et_ssmn);
        et_ssfn = findViewById(R.id.et_ssfn);
        et_sister_name = findViewById(R.id.et_sister_name);
        et_sn = findViewById(R.id.et_sn);
        et_sfplace = findViewById(R.id.et_sfplace);
        et_sfvillage = findViewById(R.id.et_sfvillage);
        et_sfsurname = findViewById(R.id.et_sfsurname);
        et_smn = findViewById(R.id.et_smn);
        et_sfn = findViewById(R.id.et_sfn);
        et_spouse_name = findViewById(R.id.et_spouse_name);
        et_village_name = findViewById(R.id.et_village_name);
        btn_submit = findViewById(R.id.btn_submit);
        btn_cancel = findViewById(R.id.btn_cancel);
        sp_mat_status = findViewById(R.id.sp_mat_status);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        sp_mat_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
                try {
                    ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.text_color_hint));
                    ((TextView) parent.getChildAt(0)).setTextSize(14);
                    if (position > 0) {
                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                        ((TextView) parent.getChildAt(0)).setTypeface((MyCustomTypeface.getTypeFace(instance, "fonts/Lato-Regular_0.ttf")));
                        marital_status_id = String.valueOf(maritalStatusArray.get(position).getMaritalstatusID());
                        if (marital_status_id.equalsIgnoreCase("5")) {
                            lin_married.setVisibility(View.VISIBLE);
                        } else {
                            lin_married.setVisibility(View.GONE);
                        }
                    } else {
                        marital_status_id = "0";
                        lin_married.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getIntent().getStringExtra("memberType").equalsIgnoreCase("brother")) {
                    if (et_brother_name.getText().toString().isEmpty()) {
                        et_brother_name.requestFocus();
                        et_brother_name.setError("Please enter brother name");
                        return;

                    } else {
                        new CallRequest(instance).addBrother(et_brother_name.getText().toString(),user);
                    }
                } else if (getIntent().getStringExtra("memberType").equalsIgnoreCase("son")) {
                    if (et_son_name.getText().toString().isEmpty()) {
                        et_son_name.requestFocus();
                        et_son_name.setError("Please enter son name");
                        return;

                    } else {
                        new CallRequest(instance).addSon(user,et_son_name.getText().toString());
                    }
                } else if (getIntent().getStringExtra("memberType").equalsIgnoreCase("daughter")) {
                    if (et_daughter_name.getText().toString().isEmpty()) {
                        et_daughter_name.requestFocus();
                        et_daughter_name.setError("Please enter daughter name");
                        return;

                    } else {
                        new CallRequest(instance).addDaughter(et_daughter_name.getText().toString(),user);
                    }
                } else if (getIntent().getStringExtra("memberType").equalsIgnoreCase("sister")) {

                    if (marital_status_id.equalsIgnoreCase("5")) {
                        if (et_sister_name.getText().toString().isEmpty()) {
                            et_sister_name.requestFocus();
                            et_sister_name.setError("Please enter sister name");
                            return;

                        } else if (marital_status_id.equalsIgnoreCase("") || marital_status_id.equals("0")) {
                            Utils.showToast("Please select marital status", instance);
                        } else if (et_sn.getText().toString().isEmpty()) {
                            et_sn.requestFocus();
                            et_sn.setError("Please enter sister spouse name");
                            return;

                        } else if (et_sssurname.getText().toString().isEmpty()) {
                            et_sssurname.requestFocus();
                            et_sssurname.setError("Please enter sister spouse surname");
                            return;

                        } else if (et_ssfn.getText().toString().isEmpty()) {
                            et_ssfn.requestFocus();
                            et_ssfn.setError("Please enter sister spouse father name");
                            return;

                        } else if (et_ssmn.getText().toString().isEmpty()) {
                            et_ssmn.requestFocus();
                            et_ssmn.setError("Please enter sister spouse mother name");
                            return;

                        } else if (et_ssvillage.getText().toString().isEmpty()) {
                            et_ssvillage.requestFocus();
                            et_ssvillage.setError("Please enter sister spouse village");
                            return;

                        } else {
                            new CallRequest(instance).addSister(user,et_sister_name.getText().toString(),
                                    et_sn.getText().toString(), et_sssurname.getText().toString(), et_ssfn.getText().toString(),
                                    et_ssmn.getText().toString(),
                                    et_ssvillage.getText().toString(), marital_status_id);
                        }
                    } else {
                        if (et_sister_name.getText().toString().isEmpty()) {
                            et_sister_name.requestFocus();
                            et_sister_name.setError("Please enter sister name");
                            return;

                        } else if (marital_status_id.equalsIgnoreCase("") || marital_status_id.equals("0")) {
                            Utils.showToast("Please select marital status", instance);
                        } else {
                            new CallRequest(instance).addSister(user, et_sister_name.getText().toString(),
                                    et_sn.getText().toString(), et_sssurname.getText().toString(), et_ssfn.getText().toString(), et_ssmn.getText().toString(),
                                    et_ssvillage.getText().toString(), marital_status_id);
                        }

                    }
                } else if (getIntent().getStringExtra("memberType").equalsIgnoreCase("spouse")) {
                    if (et_spouse_name.getText().toString().isEmpty()) {
                        et_spouse_name.requestFocus();
                        et_spouse_name.setError("Please enter spouse's name");
                        return;

                    } else if (et_sfn.getText().toString().isEmpty()) {
                        et_sfn.requestFocus();
                        et_sfn.setError("Please enter spouse's father name");
                        return;

                    } else if (et_smn.getText().toString().isEmpty()) {
                        et_smn.requestFocus();
                        et_smn.setError("Please enter spouse's mother name");
                        return;

                    } else if (et_sfsurname.getText().toString().isEmpty()) {
                        et_sfsurname.requestFocus();
                        et_sfsurname.setError("Please enter spouse's father surname");
                        return;

                    } else if (et_sfvillage.getText().toString().isEmpty()) {
                        et_sfvillage.requestFocus();
                        et_sfvillage.setError("Please enter spouse's father village");
                        return;

                    } else {
                        new CallRequest(instance).addSpouse(user,et_spouse_name.getText().toString(), et_sfn.getText().toString(), et_smn.getText().toString(),
                                et_sfsurname.getText().toString(), et_sfvillage.getText().toString());

                    }
                }
            }
        });

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.equalsIgnoreCase("")) {
            Log.i("RESULT", result);
            switch (request) {
                case getMaritalStatus:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            strMaritalStatusArray.clear();
                            maritalStatusArray.clear();
                            if (jObj.getJSONArray("data") != null && jObj.getJSONArray("data").length() > 0) {
                                JSONArray jSurnameArray = jObj.getJSONArray("data");

                                maritalStatus.setMaritalstatus(getResources().getString(R.string.MaritalStatus));
                                maritalStatus.setMaritalstatusID(0);
                                strMaritalStatusArray.add(maritalStatus.getMaritalstatus());
                                maritalStatusArray.add(maritalStatus);
                                if (jSurnameArray != null && jSurnameArray.length() > 0) {
                                    for (int i = 0; i < jSurnameArray.length(); i++) {
                                        maritalStatus = (MaritalStatus) jParser.parseJson(jSurnameArray.getJSONObject(i), new MaritalStatus());
                                        maritalStatusArray.add(maritalStatus);
                                        strMaritalStatusArray.add(maritalStatus.getMaritalstatus());
                                    }
                                    sp_mat_status.setAdapter(new ArrayAdapter<String>(instance, android.R.layout.simple_spinner_dropdown_item, strMaritalStatusArray));
                                } else {
                                    Utils.showToast(jObj.getString("message"), instance);
                                }
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;


                case getPrefilbrother:
                    Utils.hideProgressDialog();
                    try {
                        JSONObject jObj = null;
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            if (jObj.getJSONArray("Prefilbrotherdata") != null && jObj.getJSONArray("Prefilbrotherdata").length() > 0) {
                                JSONArray userData = jObj.getJSONArray("Prefilbrotherdata");

                                for (int i = 0; i < userData.length(); i++) {
                                    JSONObject jobj = userData.getJSONObject(i);
                                    et_village_name.setText(jobj.getString("Village"));
                                    et_suburb_name.setText(jobj.getString("Suburb"));
                                }

                                Utils.hideProgressDialog();
                            } else {
                                Utils.hideProgressDialog();
                                Utils.showToast(jObj.getString("UMessage"), instance);
                            }
                        } else {
                            Utils.showToast(jObj.getString("message"), instance);
                        }

                    } catch (JSONException e) {
                        Utils.showToast("Something getting wrong! Please try again later.", instance);
                        e.printStackTrace();
                    }
                    break;

                case addBrother:
                    Utils.hideProgressDialog();
                    JSONObject jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            Utils.showToast(jObj.getString("UMessage"), this);
                            onBackPressed();

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case addSon:
                    Utils.hideProgressDialog();
                    jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            Utils.showToast(jObj.getString("UMessage"), this);
                            onBackPressed();

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case addDaughter:
                    Utils.hideProgressDialog();
                    jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            Utils.showToast(jObj.getString("UMessage"), this);
                            onBackPressed();

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case addSister:
                    Utils.hideProgressDialog();
                    jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            Utils.showToast(jObj.getString("UMessage"), this);
                            onBackPressed();

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
                case addSpouse:
                    Utils.hideProgressDialog();
                    jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            Utils.showToast(jObj.getString("UMessage"), this);
                            onBackPressed();

                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    }
}
