package com.skgjst.activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.skgjst.BaseActivity;
import com.skgjst.activities.profile.MyProfileActivity;
import com.skgjst.App;
import com.skgjst.callinterface.AsynchTaskListner;
import com.skgjst.R;
import com.skgjst.utils.CallRequest;
import com.skgjst.utils.Constant;
import com.skgjst.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChangePassActivity extends BaseActivity implements AsynchTaskListner {
    public Toolbar toolbar;
    public TextView tv_title;
    public CircleImageView img_profile, img_profile_local;
    public ChangePassActivity instance;
    public EditText et_confirm_pass, et_new_pass, et_old_pass;
    public Button btn_submit;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);
        instance = this;
        toolbar = findViewById(R.id.toolbar);
        et_confirm_pass = findViewById(R.id.et_confirm_pass);
        et_new_pass = findViewById(R.id.et_new_pass);
        et_old_pass = findViewById(R.id.et_old_pass);
        btn_submit = findViewById(R.id.btn_submit);
        tv_title = toolbar.findViewById(R.id.tv_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tv_title.setText("CHANGE PASSWORD");
        img_profile = toolbar.findViewById(R.id.img_profile);
        Utils.setRoundedImage(instance,user.getPhotoURL(),300,300,img_profile,null);
        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(instance, MyProfileActivity.class));
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (et_old_password.getText().toString().equals("")) {
                    et_old_password.setError("Please enter old password");
                    et_old_password.setFocusable(true);
                }else*/
                if (et_new_pass.getText().toString().equals("")) {
                    et_new_pass.setError("Please enter valid password");
                    et_new_pass.setFocusable(true);
                } else if (et_new_pass.getText().toString().length() < 6) {
                    et_new_pass.setFocusable(true);
                    et_new_pass.setError("Please enter the password of atlest 6 characters");
                } else if (et_confirm_pass.getText().toString().equals("")) {
                    et_confirm_pass.setError("Please enter confirm password");
                    et_confirm_pass.setFocusable(true);
                } else if (!et_confirm_pass.getText().toString().equals(et_new_pass.getText().toString())) {
                    et_confirm_pass.setError("Password and Confirm password not match");
                    et_confirm_pass.setFocusable(true);
                } else {
                    new CallRequest(instance).changePassword(user,et_new_pass.getText().toString());

                }
            }
        });

    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        if (result != null && !result.isEmpty()) {
            Log.i("TAG", "TAG Result : " + result);
            switch (request) {


                case changePassword:
                    Utils.hideProgressDialog();
                    JSONObject jObj = null;
                    try {
                        jObj = new JSONObject(result);

                        if (jObj.getBoolean("Success")) {
                            Utils.showToast(jObj.getString("UMessage"), this);
                            // JSONArray jChangePass =jObj.getJSONArray("Changepassword");
                            if (!jObj.getString("Changepassword").equals("")) {
                                mySharedPref.clearApp();
                                startActivity(new Intent(instance, LoginActivity.class)
                                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                                finish();
                            }


                        } else {
                            Utils.hideProgressDialog();
                            Utils.showToast(jObj.getString("UMessage"), this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;


            }
        }
    }
}
