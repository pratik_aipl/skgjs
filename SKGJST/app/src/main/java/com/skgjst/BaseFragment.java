package com.skgjst;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.google.gson.Gson;
import com.skgjst.model.MainUser;
import com.skgjst.utils.MySharedPref;

public class BaseFragment extends Fragment {
    public MainUser user;
    public Gson gson;
    public MySharedPref mySharedPref;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        gson = new Gson();
        mySharedPref = new MySharedPref(getActivity());
        user = gson.fromJson(mySharedPref.getUserModel(), MainUser.class);
    }
}
