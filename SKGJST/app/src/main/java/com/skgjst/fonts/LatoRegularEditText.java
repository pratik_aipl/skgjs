package com.skgjst.fonts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.EditText;

@SuppressLint("AppCompatCustomView")
public class LatoRegularEditText extends EditText {


    public LatoRegularEditText(Context context) {
        super(context);
        this.setTypeface((MyCustomTypeface.getTypeFace(context, "fonts/Lato-Regular_0.ttf")));
        this.setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }

    public LatoRegularEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setTypeface((MyCustomTypeface.getTypeFace(context, "fonts/Lato-Regular_0.ttf")));
        this.setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }

    public LatoRegularEditText(Context context, AttributeSet attrs, int defStyle) {

        super(context, attrs, defStyle);
        this.setTypeface((MyCustomTypeface.getTypeFace(context, "fonts/Lato-Regular_0.ttf")));
        this.setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }
}