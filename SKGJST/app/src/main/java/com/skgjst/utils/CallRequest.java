package com.skgjst.utils;

import android.content.Context;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;

import com.skgjst.App;
import com.skgjst.BuildConfig;
import com.skgjst.model.MainUser;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;

public class CallRequest {

//old url
    // public static String COMMON_URL = "http://clientworksarea.com/SKGJST/Webservice2.asmx/";

    //  http://skgjsedirectory.org/WebService2.asmx/
    public static String COMMON_URL = BuildConfig.API_URL + "Webservice2.asmx/";

    public static String CLInt_URL = BuildConfig.API_URL + "Webservice2.asmx/";

    //  public static String COMMON_URL = "http://clientworksarea.com/SKGJST/WebService2.asmx/";
    public App app;
    public Context ct;
    public Fragment ft;
    private static final String TAG = "CallRequest";

    public CallRequest(Fragment ft) {
        app = App.getInstance();
        this.ft = ft;
    }

    public CallRequest(Context ct) {
        this.ct = ct;
        app = App.getInstance();
    }

    public void NewsList(String userId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "NewsList");
        //map.put("UserID", userId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.NewsList, Constant.POST_TYPE.GET, map);
    }

    public void JarVidhi(String villageId, String surnameId) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "JarVidhi");
        map.put("villageid", villageId);
        map.put("surnameid", surnameId);


        new AsynchHttpRequest(ct, Constant.REQUESTS.JarVidhi, Constant.POST_TYPE.POST, map);
    }

//    public void UpdatePlayerId(String playerID) {
//        Map<String, String> map = new HashMap<String, String>();
//        map.put("url", COMMON_URL + "UpdatePlayerId");
//        map.put("FamilyDetailID", String.valueOf(user.getFamilyDetailID()));
//        map.put("playerID", playerID);
//        new AsynchHttpRequest(ct, Constant.REQUESTS.UpdatePlayerId, Constant.POST_TYPE.POST, map);
//    }

    public void getOrganizationList() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "GetOrganization");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getOrganizationList, Constant.POST_TYPE.GET, map);
    }

    public void Relationformembers() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "Relationformembers");

        new AsynchHttpRequest(ft, Constant.REQUESTS.Relationformembers, Constant.POST_TYPE.GET, map);
    }

    public void editMember() {
        Map map = new HashMap();

        map.put("url", COMMON_URL + "Editmemberdetails");
        map.put("FamilyDetailID", App.FamilyDetailID);
        map.put("firstname", App.memberDetails.get("firstName"));
        map.put("middlename", App.memberDetails.get("middleName"));
        map.put("surname", App.memberDetails.get("surname_id"));
        map.put("relationid", App.memberDetails.get("relation_id"));
        map.put("dob", App.memberDetails.get("dob"));
        map.put("gender", App.memberDetails.get("genderId"));
        map.put("bloodgroupid", App.memberDetails.get("bloodGroup_id"));
        map.put("marriagestatusid", App.memberDetails.get("maritalStatusId"));
        map.put("marriagedate", App.memberDetails.get("marriageDate"));
        map.put("contactno", App.memberDetails.get("mobileNo"));
        map.put("education", App.memberDetails.get("education"));
        map.put("workprofession", App.memberDetails.get("profession_id"));
        map.put("businesstype", App.memberDetails.get("business_nature_id"));
        map.put("bussdetails", App.memberDetails.get("businessDetail"));
        map.put("officename", App.memberDetails.get("companyName"));
        map.put("officeaddress", App.memberDetails.get("companyAddress"));
        map.put("officecontactno", App.memberDetails.get("officeContact"));
        map.put("EmailID", App.memberDetails.get("emailId"));
        map.put("websiteurl", App.memberDetails.get("websiteUrl"));
        map.put("countrycode", "84");
        map.put("PhotoUrl", App.memberDetails.get("PhotoUrl"));
        map.put("filename", App.memberDetails.get("filename"));


        new AsynchHttpRequest(ft, Constant.REQUESTS.editMember, Constant.POST_TYPE.POST, map);
    }

    public void RelationList() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "RelationList");
        new AsynchHttpRequest(ft, Constant.REQUESTS.RelationList, Constant.POST_TYPE.GET, map);
    }

    public void BloodGroup() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "BloodGroup");
        new AsynchHttpRequest(ft, Constant.REQUESTS.BloodGroup, Constant.POST_TYPE.GET, map);
    }

    public void SurNameList() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "SurNameList");
        new AsynchHttpRequest(ft, Constant.REQUESTS.SurNameList, Constant.POST_TYPE.GET, map);
    }

    public void registerMemberDetails() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "RegisterMember");
        map.put("Familyid", App.familyId);
        map.put("firstname", App.memberDetails.get("firstName"));
        map.put("middlename", App.memberDetails.get("middleName"));
        map.put("surname", App.memberDetails.get("surname_id"));
        map.put("relationid", App.memberDetails.get("relation_id"));
        map.put("dob", App.memberDetails.get("dob"));
        map.put("gender", App.memberDetails.get("genderId"));
        map.put("bloodgroupid", App.memberDetails.get("bloodGroup_id"));
        map.put("marriagestatusid", App.memberDetails.get("maritalStatusId"));
        map.put("marriagedate", App.memberDetails.get("marriageDate"));
        map.put("contactno", App.memberDetails.get("mobileNo"));
        map.put("education", App.memberDetails.get("education"));
        map.put("workprofession", App.memberDetails.get("profession_id"));
        map.put("businesstype", App.memberDetails.get("business_nature_id"));
        map.put("bussdetails", App.memberDetails.get("businessDetail"));
        map.put("officename", App.memberDetails.get("companyName"));
        map.put("officeaddress", App.memberDetails.get("companyAddress"));
        map.put("officecontactno", App.memberDetails.get("officeContact"));
        map.put("EmailID", App.memberDetails.get("emailId"));
        map.put("websiteurl", App.memberDetails.get("websiteUrl"));
        map.put("countrycode", "84");
        map.put("PhotoUrl", App.memberDetails.get("PhotoUrl"));
        map.put("filename", App.memberDetails.get("filename"));
        new AsynchHttpRequest(ft, Constant.REQUESTS.RegisterMember, Constant.POST_TYPE.POST, map);
    }

    public void Matrimonialbysearch(String isManglik, String handicap, String fromage, String toage, String villageId, String gender, String skincolor) {


        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "Matrimonialbysearch");
        map.put("IsManglik", isManglik);
        map.put("handicap", handicap);
        map.put("Fromage", fromage);
        map.put("Toage", toage);
        map.put("Village", villageId);
        map.put("Gender", gender);
        map.put("skincolor", skincolor);

        new AsynchHttpRequest(ft, Constant.REQUESTS.Matrimonialbysearch, Constant.POST_TYPE.POST, map);

    }

    public void Gender_List() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "Gender_List");
        new AsynchHttpRequest(ft, Constant.REQUESTS.Gender_List, Constant.POST_TYPE.GET, map);
    }

    public void MaritalStatus() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "MaritalStatus");
        new AsynchHttpRequest(ft, Constant.REQUESTS.MaritalStatus, Constant.POST_TYPE.POST, map);
    }

    public void VillageList() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "VillageList");
        new AsynchHttpRequest(ft, Constant.REQUESTS.VillageList, Constant.POST_TYPE.POST, map);
    }

    public void editMatrimonialDetails(Map<String, String> map) {
        new AsynchHttpRequest(ft, Constant.REQUESTS.EditMatrimonialDetails, Constant.POST_TYPE.POST, map);
    }

    public void MatrimonialList() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "matrilist");
        new AsynchHttpRequest(ft, Constant.REQUESTS.MatrimonialList, Constant.POST_TYPE.GET, map);
    }

    public void MatrimonialListActivity(MainUser user) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "matrilist");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        new AsynchHttpRequest(ct, Constant.REQUESTS.MatrimonialList, Constant.POST_TYPE.POST, map);
    }

    public void myMatrimonial(MainUser user) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "mymatrimonial");
        map.put("ufamilydetailid", String.valueOf(user.getFamilyDetailID()));
        new AsynchHttpRequest(ct, Constant.REQUESTS.myMatrimonial, Constant.POST_TYPE.POST, map);
    }

    public void myprofiles(MainUser user) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "myprofiles");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        new AsynchHttpRequest(ct, Constant.REQUESTS.myprofiles, Constant.POST_TYPE.POST, map);
    }

    public void fillMatrimonial(String matrimonialid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "fillmatrimonial");
        map.put("matrimonialid", matrimonialid);
        new AsynchHttpRequest(ct, Constant.REQUESTS.fillMatrimonial, Constant.POST_TYPE.POST, map);
    }

    public void removeProfile(MainUser user, int matrimonialid, int selectionid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "removeprofile");
        map.put("matrimonialid", String.valueOf(matrimonialid));
        map.put("selectionid", String.valueOf(selectionid));
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        new AsynchHttpRequest(ct, Constant.REQUESTS.removeProfile, Constant.POST_TYPE.POST, map);
    }

    public void getMatrimonialMembers(MainUser user) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "namesformatrimonial");
        map.put("FamilyID", String.valueOf(user.getFamilyID()));

        new AsynchHttpRequest(ct, Constant.REQUESTS.GetMatrimonialMember, Constant.POST_TYPE.POST, map);
    }

    public void GetEditProfileParameters(MainUser user) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "GetEditProfileParameters");
        map.put("familyid", String.valueOf(user.getFamilyID()));
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));

        new AsynchHttpRequest(ct, Constant.REQUESTS.GetEditProfileParameters, Constant.POST_TYPE.POST, map);
    }

    public void GetEditProfileParametersUser(MainUser user, String familyid, String familydetailid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "GetEditProfileParameters");
        map.put("familyid", String.valueOf(user.getFamilyID()));
        map.put("familydetailid", familydetailid);

        new AsynchHttpRequest(ct, Constant.REQUESTS.GetEditProfileParameters, Constant.POST_TYPE.POST, map);
    }

    public void addMatrimonialDetails(MainUser user, String familydetailid, String height, String weight
            , String skincolor, String handicap, String handicapdetails, String Manglik,
                                      String photourl, String filename, String otherinfo, String isactive) {
        Map<String, String> map = new HashMap<>();
        map.put("url", COMMON_URL + "Addmatrimonial");
        map.put("familydetailid", familydetailid);
        map.put("ufamilydetailid", String.valueOf(user.getFamilyDetailID()));
        map.put("height", height);
        map.put("weight", weight);
        map.put("skincolor", skincolor);
        map.put("handicap", handicap);
        map.put("handicapdetails", handicapdetails);
        map.put("Manglik", Manglik);
        map.put("photourl", ImageConvert(photourl));
        map.put("filename", filename);
        map.put("otherinfo", otherinfo);
        map.put("isactive", isactive);
        new AsynchHttpRequest(ct, Constant.REQUESTS.AddMatrimonialDetails, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void editMatrimonialDetails(String matrimonialid, String height, String weight
            , String skincolor, String handicap, String handicapdetails, String Manglik,
                                       String photourl, String filename, String otherinfo, String isactive) {
        Map<String, String> map = new HashMap<>();
        map.put("url", COMMON_URL + "editmymatrimonial");
        map.put("height", height);
        map.put("weight", weight);
        map.put("skincolor", skincolor);
        map.put("handicap", handicap);
        map.put("handicapdetails", handicapdetails);
        map.put("Manglik", Manglik);
        map.put("photourl", ImageConvert(photourl));
        map.put("filename", filename);
        map.put("otherinfo", otherinfo);
        map.put("isactive", isactive);
        map.put("matrimonialid", matrimonialid);
        new AsynchHttpRequest(ct, Constant.REQUESTS.editMatrimonialDetails, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void matriSearch(MainUser user, String nativeplace, String fromage, String toage
            , String skincolor, String handicap, String gender, String Manglik, String CityID, String maritalstatusid) {
        Map<String, String> map = new HashMap<>();
        map.put("url", COMMON_URL + "matrisearch");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        map.put("manglik", Manglik);
        map.put("handicap", handicap);
        map.put("fromage", fromage);
        map.put("toage", toage);
        map.put("skincolor", skincolor);
        map.put("nativeplace", nativeplace);
        map.put("gender", gender);
        map.put("CityID", CityID);
        map.put("maritalstatusid", maritalstatusid);
        new AsynchHttpRequest(ct, Constant.REQUESTS.MatrimonialList, Constant.POST_TYPE.POST, map);
    }

    public void addMatri(MainUser user, String matrimonialid, String isactive) {
        Map<String, String> map = new HashMap<>();
        map.put("url", COMMON_URL + "addmatri");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        map.put("matrimonialid", matrimonialid);
        map.put("isactive", isactive);
        new AsynchHttpRequest(ct, Constant.REQUESTS.addMatri, Constant.POST_TYPE.POST, map);
    }

    public void GetEventList() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "Eventinformation");
        new AsynchHttpRequest(ft, Constant.REQUESTS.GetEventList, Constant.POST_TYPE.GET, map);
    }

    public void GetEventImagesList(int eventId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "eventimages");
        map.put("eventid", String.valueOf(eventId));
        new AsynchHttpRequest(ct, Constant.REQUESTS.GetEventImages, Constant.POST_TYPE.POST, map);
    }

    public void getLogin(String user_name, String password, String playerID) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "MemberLogin");
        map.put("user_name", user_name);
        map.put("password", password);
        map.put("playerID", playerID);

        new AsynchHttpRequest(ct, Constant.REQUESTS.getLogin, Constant.POST_TYPE.POST, map);
    }

    public void AddDeviceLogin(String MUserID, String DeviceIdorIPAddr) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "Securelogin");
        map.put("MUserID", MUserID);
        map.put("DeviceIdorIPAddr", DeviceIdorIPAddr);

        new AsynchHttpRequest(ct, Constant.REQUESTS.addDeviceLogin, Constant.POST_TYPE.POST, map);
    }

    public void LogOutStatus(String MUserID, String SecureLoginID, String DeviceIdorIPAddr) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "Securelogout");
        map.put("MUserID", MUserID);
        map.put("SecureLoginID", SecureLoginID);
        map.put("DeviceIdorIPAddr", DeviceIdorIPAddr);

        new AsynchHttpRequest(ct, Constant.REQUESTS.logOutStatus, Constant.POST_TYPE.POST, map);
    }

    public void CheckSecurelogin(String MUserID, String SecureLoginID, String DeviceIdorIPAddr) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "CheckSecurelogin");
        map.put("MUserID", MUserID);
        map.put("SecureLoginID", SecureLoginID);
        map.put("DeviceIdorIPAddr", DeviceIdorIPAddr);

        new AsynchHttpRequest(ct, Constant.REQUESTS.loginStatus, Constant.POST_TYPE.POST, map);
    }

    public void changePassword(MainUser user, String newpassword) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "changepass");
        map.put("userid", String.valueOf(user.getMUserID()));
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        map.put("newpassword", newpassword);

        new AsynchHttpRequest(ct, Constant.REQUESTS.changePassword, Constant.POST_TYPE.POST, map);
    }

    public void GetProfile(String familyId) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "GetProfile");
        map.put("FamilyID", familyId);
        new AsynchHttpRequest(ft, Constant.REQUESTS.GetProfile, Constant.POST_TYPE.POST, map);
    }

    public void generateOtp(String number) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "generateotp");
        map.put("number", number);

        new AsynchHttpRequest(ct, Constant.REQUESTS.generateOtp, Constant.POST_TYPE.POST, map);
    }

    public void verifyOtp(String FamilyID, String FamilyDetailID, String playerID) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "GetLoginDetailsAfterOTPSuccess");
        map.put("FamilyID", FamilyID);
        map.put("FamilyDetailID", FamilyDetailID);
        map.put("playerID", playerID);

        new AsynchHttpRequest(ct, Constant.REQUESTS.verifyOtp, Constant.POST_TYPE.POST, map);
    }

    public void forgotPassword(String number) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "forgotpass");
        map.put("number", number);

        new AsynchHttpRequest(ct, Constant.REQUESTS.forgotPassword, Constant.POST_TYPE.POST, map);
    }

    public void Validatenumber(String number) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "ValidatenumberForRegistration");
        map.put("contactno", number);
        new AsynchHttpRequest(ct, Constant.REQUESTS.Validatenumber, Constant.POST_TYPE.POST, map);
    }

    public void Validatenumber(String number, String FamilyDetailID) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "ValidatenumberForLogin");
        map.put("contactno", number);
        map.put("FamilyDetailID", FamilyDetailID);
        new AsynchHttpRequest(ct, Constant.REQUESTS.Validatenumber, Constant.POST_TYPE.POST, map);
    }

    public void validatenumber(String FamilyDetailID, String number) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "validatenumber");
        map.put("number", number);
        map.put("familydetailid", String.valueOf(FamilyDetailID));

        new AsynchHttpRequest(ct, Constant.REQUESTS.validatenumber, Constant.POST_TYPE.POST, map);
    }

    public void getSurnameList() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "SurNameList");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getSurnameList, Constant.POST_TYPE.GET, map);
    }

    public void getCity() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "city");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getCity, Constant.POST_TYPE.GET, map);
    }

    public void getSurnameListFragment() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "SurNameList");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getSurnameList, Constant.POST_TYPE.GET, map);
    }

    public void getDemiseList() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "Demiselist");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getDemiseList, Constant.POST_TYPE.GET, map);
    }

    public void deleteDemise(MainUser user, String demiseid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", CLInt_URL + "DeletePrathnaDemise");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        map.put("DemisePrathnaID", String.valueOf(demiseid));
        new AsynchHttpRequest(ct, Constant.REQUESTS.deleteDemise, Constant.POST_TYPE.POST, map);
    }

    public void deletePrathna(MainUser user, String prathnaid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "prathnadelete");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        map.put("prathnaid", String.valueOf(prathnaid));
        new AsynchHttpRequest(ct, Constant.REQUESTS.deletePrathna, Constant.POST_TYPE.POST, map);
    }

    public void getPrathanaList() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "Prathnalist");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getPrathanaList, Constant.POST_TYPE.GET, map);
    }

    public void getNotifications(MainUser user) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "notifications");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        new AsynchHttpRequest(ft, Constant.REQUESTS.getNotifications, Constant.POST_TYPE.POST, map);
    }

    public void getcount(int familyDetailID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "getcount");
        map.put("familydetailid", String.valueOf(familyDetailID));

        // if (user. != null)
        //  map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        new AsynchHttpRequest(ct, Constant.REQUESTS.getcount, Constant.POST_TYPE.POST, map);

    }

    public void getNews() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "NewsList");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getNews, Constant.POST_TYPE.GET, map);
    }

    public void getOraganization() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "GetOrganization");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getOraganization, Constant.POST_TYPE.GET, map);
    }

//    public void getaaNotifications() {
//        Map<String, String> map = new HashMap<String, String>();
//        map.put("url", COMMON_URL + "notifications");
//        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
//        new AsynchHttpRequest(ct, Constant.REQUESTS.getNotifications, Constant.POST_TYPE.POST, map);
//    }

    public void getMyDemiseList(MainUser user) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "getpersonaldemise");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        new AsynchHttpRequest(ft, Constant.REQUESTS.getMyDemiseList, Constant.POST_TYPE.POST, map);
    }

    public void getMyPrathanaList(MainUser user) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "myprathna");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        new AsynchHttpRequest(ct, Constant.REQUESTS.getMyPrathanaList, Constant.POST_TYPE.POST, map);
    }

    public void getSurnameListFra() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "SurNameList");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getSurnameList, Constant.POST_TYPE.GET, map);
    }

    public void getVillageList() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "VillageList");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getVillageList, Constant.POST_TYPE.GET, map);
    }

    public void getVillageListFragment() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "VillageList");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getVillageList, Constant.POST_TYPE.GET, map);
    }

    public void getBloodGroup() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "BloodGroup");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getBloodGroup, Constant.POST_TYPE.GET, map);
    }

    public void getBloodGroupFragment() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "BloodGroup");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getBloodGroup, Constant.POST_TYPE.GET, map);
    }

    public void getMaritalStatus() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "MaritalStatus");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getMaritalStatus, Constant.POST_TYPE.GET, map);
    }

    public void getMaritalStatusFragment() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "MaritalStatus");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getMaritalStatus, Constant.POST_TYPE.GET, map);
    }

    public void getCountry() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "Country_List");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getCountry, Constant.POST_TYPE.GET, map);
    }

    public void getState(String countryid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "State");
        map.put("countryid", countryid);
        new AsynchHttpRequest(ct, Constant.REQUESTS.getState, Constant.POST_TYPE.POST, map);
    }

    public void getCity(String stateid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "citylist");
        map.put("stateid", stateid);
        new AsynchHttpRequest(ct, Constant.REQUESTS.getCity, Constant.POST_TYPE.POST, map);
    }

    public void getProfession() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "Profession_List");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getProfession, Constant.POST_TYPE.GET, map);
    }

    public void getBusinessNature() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "NatureOfBusiness_List");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getBusinessNature, Constant.POST_TYPE.GET, map);
    }

    public void getWatched(MainUser user, String notification_id) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "watched");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        map.put("notification_id", notification_id);
        new AsynchHttpRequest(ct, Constant.REQUESTS.getWatched, Constant.POST_TYPE.POST, map);
    }

    public void getMemberSearch(String firstname, String area, String surname, String maritalstatusid,
                                String bloodgroupid, String village, String gender, String minage, String maxAge, String locationid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "memberfilter");
        map.put("firstname", firstname);
        map.put("area", area);
        map.put("surname", surname);
        map.put("maritalstatusid", maritalstatusid);
        map.put("bloodgroupid", bloodgroupid);
        map.put("village", village);
        map.put("gender", gender);
        map.put("minage", minage);
        map.put("maxage", maxAge);
        map.put("CityID", locationid);

        new AsynchHttpRequest(ct, Constant.REQUESTS.getMemberSearch, Constant.POST_TYPE.POST, map);
    }

    public void getBusinesssearch(MainUser user, String firstname, String natureofbusiness, String professionname) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "Businesssearch");
        map.put("fname", firstname);
        map.put("natureofbusiness", natureofbusiness);
        map.put("professionname", professionname);
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));

        new AsynchHttpRequest(ct, Constant.REQUESTS.getBusinesssearch, Constant.POST_TYPE.POST, map);
    }

    public void getRegister(String Surname, String Firstname, String Middlename, String NativePlace, String PhotoUrl,
                            String filename, String gender, String dob, String bloodgroupid,
                            String maritalstatusid, String marriagedate, String contactno, String Education,
                            String workprofession, String busstype, String bussdetails, String officename, String offaddress,
                            String officecontactNo, String EmailID, String Websiteurl, String wing, String roomno,
                            String PlotNo, String Buildingname, String roadname, String landmark, String suburbname,
                            String homecontactno, String districtname, String countryid, String stateid, String cityid,
                            String pincode, String international, String countrycode, String fathername, String mothername,
                            String grandfathername, String grandmothername, String mmothername, String mfathersurname,
                            String mfathervillage, String fathersurname, String fathervillage, String spousename, String spousefathername,
                            String spousemothername, String wifefathersurname, String wifefathervillage, String yourfathername) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "RegistrationNewWithMiddleName");
        map.put("Surname", Surname);
        map.put("mothername",fathername );
        map.put("grandfathername",mothername );
        map.put("grandmothername",grandfathername );
//        map.put("mfathername", yourfathername);
        map.put("mfathername", grandmothername);
        map.put("mmothername", mmothername);
        map.put("mfathersurname", mfathersurname);
        map.put("mfathervillage", mfathervillage);
        map.put("fathersurname", fathersurname);
        map.put("fathervillage", fathervillage);
        map.put("spousename", spousename);
        map.put("spousefathername", spousefathername);
        map.put("spousemothername", spousemothername);
        map.put("wifefathervillage", wifefathervillage);
        map.put("wifefathersurname", wifefathersurname);
        map.put("Firstname", Firstname);
        map.put("Middlename", Middlename);
        map.put("NativePlace", NativePlace);
        map.put("filename", filename);
        map.put("Relationid", "1");
        map.put("gender", gender);
        map.put("dob", dob);
        map.put("bloodgroupid", bloodgroupid);
        map.put("maritalstatusid", maritalstatusid);
        map.put("marriagedate", marriagedate);
        map.put("contactno", contactno);
        map.put("Education", Education);
        map.put("workprofession", workprofession);
        map.put("busstype", busstype);
        map.put("bussdetails", bussdetails);
        map.put("officename", officename);
        map.put("offaddress", offaddress);
        map.put("officecontactNo", officecontactNo);
        map.put("EmailID", EmailID);
        map.put("Websiteurl", Websiteurl);
        map.put("wing", wing);
        map.put("roomno", roomno);
        map.put("PlotNo", PlotNo);
        map.put("Buildingname", Buildingname);
        map.put("roadname", roadname);
        map.put("landmark", landmark);
        map.put("suburbname", suburbname);
        map.put("homecontactno", homecontactno);
        map.put("countryid", countryid);
        map.put("stateid", stateid);
        map.put("cityid", cityid);
        map.put("pincode", pincode);
        map.put("international", international);
        map.put("countrycode", countrycode);
        map.put("districtname", districtname);
        map.put("fathername", yourfathername);
        map.put("PlatformName", "Android");

        Log.d(TAG, "getRegister:>>>" + map.toString());
        map.put("PhotoUrl", ImageConvert(PhotoUrl));

        new AsynchHttpRequest(ct, Constant.REQUESTS.getRegister, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void editProfile(MainUser user, String dob, String bloodgroupid, String education,
                            String wing, String flatno, String plotno, String building,
                            String roadname, String landmark, String suburbname,
                            String district, String countryid, String stateid, String cityid,
                            String pincode, String Profession, String busstypeid,
                            String bussdetail, String companyname, String websiteurl, String officecontactno, String officeaddress,
                            String Emailid, String photourl, String filename, String number, String familydetailid, String IsAlive) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", CLInt_URL + "editprofile");
        map.put("dob", Utils.changeDateToMMDDYYYY(dob));
        map.put("bloodgroupid", bloodgroupid);
        map.put("familyid", String.valueOf(user.getFamilyID()));
        map.put("familydetailid", familydetailid);
        map.put("LoggedInFamilyDetailID", String.valueOf(user.getFamilyDetailID()));
        map.put("number", number);
        map.put("education", education);
        map.put("flatno", flatno);
        map.put("plotno", plotno);
        map.put("wing", wing);
        map.put("building", building);
        map.put("district", district);
        map.put("Profession", Profession);
        map.put("busstypeid", busstypeid);
        map.put("bussdetail", bussdetail);
        map.put("Emailid", Emailid);
        map.put("companyname", companyname);
        map.put("websiteurl", websiteurl);
        map.put("officecontactno", officecontactno);
        map.put("officeaddress", officeaddress);
        map.put("roadname", roadname);
        map.put("landmark", landmark);
        map.put("suburbname", suburbname);
        map.put("countryid", countryid);
        map.put("stateid", stateid);
        map.put("cityid", cityid);
        map.put("pincode", pincode);
        map.put("photourl", ImageConvert(photourl));
        map.put("filename", filename);
        map.put("IsAlive", IsAlive);


        new AsynchHttpRequest(ct, Constant.REQUESTS.editProfile, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void editprofileForNonActiveMember(MainUser user, String dob, String bloodgroupid, String education,
                                              String wing, String flatno, String plotno, String building,
                                              String roadname, String landmark, String suburbname,
                                              String district, String countryid, String stateid, String cityid,
                                              String pincode, String Profession, String busstypeid,
                                              String bussdetail, String companyname, String websiteurl, String officecontactno, String officeaddress,
                                              String Emailid, String photourl, String filename, String number, String familydetailid, String IsAlive) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", CLInt_URL + "editprofileForNonActiveMember");
        map.put("dob", Utils.changeDateToMMDDYYYY(dob));
        map.put("bloodgroupid", bloodgroupid);
        map.put("familyid", String.valueOf(user.getFamilyID()));
        map.put("familydetailid", familydetailid);
        map.put("LoggedInFamilyDetailID", String.valueOf(user.getFamilyDetailID()));
        map.put("number", number);
        map.put("education", education);
        map.put("flatno", flatno);
        map.put("plotno", plotno);
        map.put("wing", wing);
        map.put("building", building);
        map.put("district", district);
        map.put("Profession", Profession);
        map.put("busstypeid", busstypeid);
        map.put("bussdetail", bussdetail);
        map.put("Emailid", Emailid);
        map.put("companyname", companyname);
        map.put("websiteurl", websiteurl);
        map.put("officecontactno", officecontactno);
        map.put("officeaddress", officeaddress);
        map.put("roadname", roadname);
        map.put("landmark", landmark);
        map.put("suburbname", suburbname);
        map.put("countryid", countryid);
        map.put("stateid", stateid);
        map.put("cityid", cityid);
        map.put("pincode", pincode);
        map.put("photourl", ImageConvert(photourl));
        map.put("filename", filename);
        map.put("IsAlive", IsAlive);


        new AsynchHttpRequest(ct, Constant.REQUESTS.editProfile, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void Business_Advertisements() {

        Map map = new HashMap();
        map.put("url", COMMON_URL + "Business_Advertisements");
        map.put("adtype", "Platinum");
        new AsynchHttpRequest(ct, Constant.REQUESTS.Business_Advertisements, Constant.POST_TYPE.POST, map);
    }

    public void getBusinessAd() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "business_advertisementss");
        new AsynchHttpRequest(ct, Constant.REQUESTS.getBusinessAd, Constant.POST_TYPE.GET, map);
    }

    public void getBusinessAdFragment() {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "business_advertisementss");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getBusinessAd, Constant.POST_TYPE.GET, map);
    }

    public void getDimondAd(String adid) {

        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "SponsoredAds");
        map.put("adid", adid);
        new AsynchHttpRequest(ct, Constant.REQUESTS.getDimondAd, Constant.POST_TYPE.POST, map);

    }

    public void birthdayList(MainUser user) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "getbirthdaylist");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        new AsynchHttpRequest(ft, Constant.REQUESTS.BirthdayList, Constant.POST_TYPE.POST, map);
    }

    public void updateNotifications(MainUser user, String notid, String Noti_Type) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "updatenotifications");
        map.put("notid", notid);
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        map.put("notification_type", Noti_Type);

        new AsynchHttpRequest(ct, Constant.REQUESTS.updateNotifications, Constant.POST_TYPE.POST, map);
    }

    public void getBirthdayWishes(MainUser user, String message, int familydetailid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "getbirthdaywishes");
        map.put("Sfamilydetailid", String.valueOf(user.getFamilyDetailID()));
        map.put("familydetailid", String.valueOf(familydetailid));
        map.put("message", message);
        map.put("notificationtypeid", "3");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getBirthdayWishes, Constant.POST_TYPE.POST, map);
    }

    public void getAnniversarywishes(MainUser user, String message, int HFamilydetailID, int WFamilyDetailID) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "getanniversarywishes");
        map.put("FamilyDetailID", String.valueOf(user.getFamilyDetailID()));
        map.put("WFamilyDetailID", String.valueOf(WFamilyDetailID));
        map.put("HFamilydetailID", String.valueOf(HFamilydetailID));
        map.put("message", message);
        map.put("notificationtypeid", "2");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getAnniversarywishes, Constant.POST_TYPE.POST, map);
    }

    public void getAnniversarylist(MainUser user) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "getanniversarylist");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        new AsynchHttpRequest(ft, Constant.REQUESTS.getAnniversarylist, Constant.POST_TYPE.POST, map);
    }

    public void getJoblist(MainUser user) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "Joblist");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        new AsynchHttpRequest(ct, Constant.REQUESTS.getJoblist, Constant.POST_TYPE.POST, map);
    }

    public void jobapply(int FamilyID, int familydetailid, int jobid, String firstname,
                         String lastname, String experience, String presentlocation,
                         String noticeperiod, String currentctc, String expectedctc, String file, String filename) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "JobApply");
        map.put("FamilyID", String.valueOf(FamilyID));
        map.put("familydetailid", String.valueOf(familydetailid));
        map.put("jobid", String.valueOf(jobid));
        map.put("firstname", firstname);

        map.put("lastname", lastname);
        map.put("experience", experience);
        map.put("presentlocation", presentlocation);
        map.put("noticeperiod", noticeperiod);
        map.put("currentctc", currentctc);
        map.put("expectedctc", expectedctc);
        map.put("Base64file", ImageConvert(file));
        map.put("filename", filename);

        new AsynchHttpRequest(ct, Constant.REQUESTS.jobapply, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void GetCityList() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "city");
        new AsynchHttpRequest(ct, Constant.REQUESTS.GetCityList, Constant.POST_TYPE.GET, map);
    }

    public void createjob(int FamilyID, int familydetailid, String expirydate,
                          String companyname, String jobpost, String location,
                          String description, String experience) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "createjob");
        map.put("familyid", String.valueOf(FamilyID));
        map.put("Familydetailid", String.valueOf(familydetailid));
        map.put("expirydate", expirydate);
        map.put("companyname", companyname);
        map.put("jobpost", jobpost);
        map.put("location", location);
        map.put("description", description);
        map.put("experience", experience);
        new AsynchHttpRequest(ct, Constant.REQUESTS.createjob, Constant.POST_TYPE.POST, map);
    }

    public void editJob(int jobid, String expirydate,
                        String companyname, String jobpost, String location,
                        String description, String experience) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "editmyjob");
        map.put("jobid", String.valueOf(jobid));
        map.put("expirydate", Utils.changeDateToMMDDYYYY(expirydate));
        map.put("companyname", companyname);
        map.put("jobpost", jobpost);
        map.put("location", location);
        map.put("description", description);
        map.put("experience", experience);
        new AsynchHttpRequest(ct, Constant.REQUESTS.editJob, Constant.POST_TYPE.POST, map);
    }

    public void AddPrathnaNew(MainUser user, String Name, String FatherName,
                              String GrandFatherName, String Surname, String NativePlace,
                              String CurrentPlace, String DemiseDate, String ImageURL, String filename,
                              String ContactPerson, String ContactNumber, String OtherInfo, String PDType,
                              String PDDate, String PDPlace, String PDTime, String PostedByFDID, String Age) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", CLInt_URL + "AddPrathnaNew");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        map.put("Name", Name);
        map.put("FatherName", FatherName);
        map.put("GrandFatherName", GrandFatherName);
        map.put("Surname", Surname);
        map.put("NativePlace", NativePlace);
        map.put("CurrentPlace", CurrentPlace);
        map.put("DemiseDate", DemiseDate);
        map.put("filename", filename);
        map.put("ContactPerson", ContactPerson);
        map.put("ContactNumber", ContactNumber);
        map.put("OtherInfo", OtherInfo);
        map.put("PDType", PDType);
        map.put("PDDate", PDDate);
        map.put("PDPlace", PDPlace);
        map.put("PDTime", PDTime);
        map.put("PostedByFDID", PostedByFDID);
        map.put("Age", Age);
        Log.d(TAG, "AddPrathnaNew: " + map.toString());
        map.put("ImageURL", ImageConvert(ImageURL));
        new AsynchHttpRequest(ct, Constant.REQUESTS.Adddemise, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void EditPrathnaDemise(MainUser user, String DemisePrathnaID, String Name, String FatherName,
                                  String GrandFatherName, String Surname, String NativePlace,
                                  String CurrentPlace, String DemiseDate, String ImageURL, String filename,
                                  String ContactPerson, String ContactNumber, String OtherInfo, String PDType,
                                  String PDDate, String PDPlace, String PDTime, String PostedByFDID, String Age) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", CLInt_URL + "EditPrathnaDemise");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        map.put("Name", Name);
        map.put("DemisePrathnaID", DemisePrathnaID);
        map.put("FatherName", FatherName);
        map.put("GrandFatherName", GrandFatherName);
        map.put("Surname", Surname);
        map.put("NativePlace", NativePlace);
        map.put("CurrentPlace", CurrentPlace);
        map.put("DemiseDate", Utils.changeDateToMMDDYYYY(DemiseDate));
        map.put("ImageURL", ImageConvert(ImageURL));
        map.put("filename", filename);
        map.put("ContactPerson", ContactPerson);
        map.put("ContactNumber", ContactNumber);
        map.put("OtherInfo", OtherInfo);
        map.put("PDType", PDType);
        map.put("PDDate", Utils.changeDateToMMDDYYYY(PDDate));
        map.put("PDPlace", PDPlace);
        map.put("PDTime", PDTime);
        map.put("PostedByFDID", PostedByFDID);
        map.put("Age", Age);
        new AsynchHttpRequest(ct, Constant.REQUESTS.EditPrathnaDemise, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void AddPrathna(MainUser user, String name, String prathnadate, String prathnaplace, String prathnainfo) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "addprathna");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        map.put("name", name);
        map.put("prathnadate", prathnadate);
        map.put("prathnaplace", prathnaplace);
        map.put("prathnainfo", prathnainfo);
        new AsynchHttpRequest(ct, Constant.REQUESTS.AddPrathna, Constant.POST_TYPE.POST, map);
    }

    public void getMyJoblist(int familydetailid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "myjobs");
        map.put("familydetailid", String.valueOf(familydetailid));
        new AsynchHttpRequest(ct, Constant.REQUESTS.getMyJoblist, Constant.POST_TYPE.POST, map);
    }

    public void getCandidateslist(int familydetailid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "Candidates");
        map.put("jobid", String.valueOf(familydetailid));
        new AsynchHttpRequest(ct, Constant.REQUESTS.getCandidateslist, Constant.POST_TYPE.POST, map);
    }

   /* public void getFamilymembers(int familyid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "familymembers");
        map.put("familyid", String.valueOf(familyid));
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        new AsynchHttpRequest(ft, Constant.REQUESTS.getFamilymembers, Constant.POST_TYPE.POST, map);
    }*/

    public void getFamilymembers(int familyid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", CLInt_URL + "familymembersListByRelationShip");
        map.put("familydetailid", String.valueOf(familyid));
        new AsynchHttpRequest(ft, Constant.REQUESTS.getFamilymembers, Constant.POST_TYPE.POST, map);
    }

    public void familymembers(int familyid, int familydetailid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "familymembersListByRelationShip");
        map.put("familydetailid", String.valueOf(familydetailid));
        new AsynchHttpRequest(ft, Constant.REQUESTS.getFamilymembers, Constant.POST_TYPE.POST, map);
    }

    public void ConnectionTreeCount(int SourceFamilyDetailID, int Destinationfamilydetailid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "ConnectionTreeCount");
        map.put("src", String.valueOf(SourceFamilyDetailID));
        map.put("dst", String.valueOf(Destinationfamilydetailid));
        new AsynchHttpRequest(ft, Constant.REQUESTS.getFamilymembersCount, Constant.POST_TYPE.POST, map);
    }

    //    public void getMyprofiles(MainUser user) {
//        Map<String, String> map = new HashMap<String, String>();
//        map.put("url", CLInt_URL + "myprofile");
//        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
//        map.put("LoggedInFamilyDetailID", String.valueOf(user.getFamilyDetailID()));
//        new AsynchHttpRequest(ft, Constant.REQUESTS.getMyprofiles, Constant.POST_TYPE.POST, map);
//    }
    public void getMyprofiles(MainUser user, int familydetailid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", CLInt_URL + "myprofile");
        map.put("familydetailid", String.valueOf(familydetailid));
        if (user != null)
            map.put("LoggedInFamilyDetailID", String.valueOf(user.getFamilyDetailID()));
        new AsynchHttpRequest(ft, Constant.REQUESTS.getMyprofiles, Constant.POST_TYPE.POST, map);
    }

    public void getMyprofile(MainUser user, int familydetailid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", CLInt_URL + "myprofile");
        map.put("familydetailid", String.valueOf(familydetailid));
        if (user != null)
            map.put("LoggedInFamilyDetailID", String.valueOf(user.getFamilyDetailID()));
        new AsynchHttpRequest(ct, Constant.REQUESTS.getMyprofiles, Constant.POST_TYPE.POST, map);
    }

    public void getMyprofilesActiviti(int familydetailid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", CLInt_URL + "myprofile");
        map.put("familydetailid", String.valueOf(familydetailid));
        new AsynchHttpRequest(ct, Constant.REQUESTS.getMyprofiles, Constant.POST_TYPE.POST, map);
    }

    public void getMyprofilesMember(MainUser user, int familydetailid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", CLInt_URL + "myprofile");
        map.put("familydetailid", String.valueOf(familydetailid));
        map.put("LoggedInFamilyDetailID", String.valueOf(user.getFamilyDetailID()));
        new AsynchHttpRequest(ct, Constant.REQUESTS.getMyprofilesMember, Constant.POST_TYPE.POST, map);
    }

    public void getPrefilbrother(MainUser user) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "prefilbrother");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        new AsynchHttpRequest(ct, Constant.REQUESTS.getPrefilbrother, Constant.POST_TYPE.POST, map);
    }

    public void addBrother(String name, MainUser user) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "addbrother");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        map.put("name", name);
        new AsynchHttpRequest(ct, Constant.REQUESTS.addBrother, Constant.POST_TYPE.POST, map);
    }

    public void addDaughter(String name, MainUser user) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "adddaughter");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        map.put("name", name);
        new AsynchHttpRequest(ct, Constant.REQUESTS.addDaughter, Constant.POST_TYPE.POST, map);
    }

    public void addDaughterNew(MainUser user, String DaughterName, String maritalstatusid, String FatherName, String FathersSurname, String FathersVillage,
                               String Dhusbandname, String Dhsurname, String Dhfathername, String Dhmothername, String Dhvillage,
                               String familydetailid, String FatherNameForDivorced, String FathersVillageForDivorced, String FathersSurnameForDivorced) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", CLInt_URL + "AddDaughterNew");
        map.put("familydetailid", familydetailid);
        map.put("DaughterName", DaughterName);
        map.put("maritalstatusid", maritalstatusid);
        map.put("PlatformName", "Android");
        map.put("FatherName", FatherName);
        map.put("FathersSurname", FathersSurname);
        map.put("FathersVillage", FathersVillage);
        map.put("Dhusbandname", Dhusbandname);
        map.put("LoggedInFamilyDetailID", String.valueOf(user.getFamilyDetailID()));
        map.put("Dhsurname", Dhsurname);
        map.put("Dhfathername", Dhfathername);
        map.put("Dhmothername", Dhmothername);
        map.put("Dhvillage", Dhvillage);
        map.put("FatherNameForDivorced", FatherNameForDivorced);
        map.put("FathersVillageForDivorced", FathersVillageForDivorced);
        map.put("FathersSurnameForDivorced", FathersSurnameForDivorced);
        new AsynchHttpRequest(ct, Constant.REQUESTS.addDaughter, Constant.POST_TYPE.POST, map);
    }

    public void addSisterNew(MainUser user, String sistername, String maritalstatusid, String FatherName, String FathersSurname, String FathersVillage,
                             String Dhusbandname, String Dhsurname, String Dhfathername, String Dhmothername, String Dhvillage,
                             String familydetailid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", CLInt_URL + "AddSisterNew");
        map.put("familydetailid", familydetailid);
        map.put("sistername", sistername);
        map.put("maritalstatusid", maritalstatusid);
        map.put("PlatformName", "Android");
        map.put("FatherName", FatherName);
        map.put("FathersSurname", FathersSurname);
        map.put("FathersVillage", FathersVillage);
        map.put("shusbandname", Dhusbandname);
        map.put("ssurname", Dhsurname);
        map.put("LoggedInFamilyDetailID", String.valueOf(user.getFamilyDetailID()));
        map.put("shfathername", Dhfathername);
        map.put("shmothername", Dhmothername);
        map.put("shvillage", Dhvillage);
        new AsynchHttpRequest(ct, Constant.REQUESTS.addSister, Constant.POST_TYPE.POST, map);
    }

    public void addSonNew(MainUser user, String name, String FatherName, String FathersSurname, String FathersVillage, String familydetailid,
                          String FatherNameForDivorced, String FathersVillageForDivorced, String FathersSurnameForDivorced) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", CLInt_URL + "AddSonNew");
        map.put("familydetailid", familydetailid);
        map.put("name", name);
        map.put("PlatformName", "Android");
        map.put("FatherName", FatherName);
        map.put("LoggedInFamilyDetailID", String.valueOf(user.getFamilyDetailID()));
        map.put("FathersSurname", FathersSurname);
        map.put("FathersVillage", FathersVillage);
        map.put("FatherNameForDivorced", FatherNameForDivorced);
        map.put("FathersVillageForDivorced", FathersVillageForDivorced);
        map.put("FathersSurnameForDivorced", FathersSurnameForDivorced);
        new AsynchHttpRequest(ct, Constant.REQUESTS.addSon, Constant.POST_TYPE.POST, map);
    }

    public void addBrotherNew(MainUser user, String name, String FatherName, String FathersSurname, String FathersVillage, String familydetailid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", CLInt_URL + "AddBrotherNew");
        map.put("familydetailid", familydetailid);
        map.put("LoggedInFamilyDetailID", String.valueOf(user.getFamilyDetailID()));
        map.put("name", name);
        map.put("PlatformName", "Android");
        map.put("FatherName", FatherName);
        map.put("FathersSurname", FathersSurname);
        map.put("FathersVillage", FathersVillage);
        new AsynchHttpRequest(ct, Constant.REQUESTS.addBrother, Constant.POST_TYPE.POST, map);
    }

    public void addFatherMother(MainUser user, String Mothername, String FatherName, String FathersSurname, String FathersVillage, String familydetailid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", CLInt_URL + "AddFatherMotherNew");
        map.put("LoggedInFamilyDetailID", String.valueOf(user.getFamilyDetailID()));
        map.put("FatherName", FatherName);
        map.put("MotherName", Mothername);
        map.put("FathersSurname", FathersSurname);
        map.put("FathersVillage", FathersVillage);
        map.put("PlatformName", "Android");
        map.put("familydetailid", familydetailid);
        new AsynchHttpRequest(ct, Constant.REQUESTS.addFatherMother, Constant.POST_TYPE.POST, map);
    }

    public void addSpouseNew(MainUser user, String spousename, String sfathername, String smothername, String sfathersurname, String sfathervillage, String familydetailid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "AddSpouseNew");
        map.put("familydetailid", familydetailid);
        map.put("LoggedInFamilyDetailID", String.valueOf(user.getFamilyDetailID()));
        map.put("spousename", spousename);
        map.put("PlatformName", "Android");
        map.put("sfathername", sfathername);
        map.put("smothername", smothername);
        map.put("sfathersurname", sfathersurname);
        map.put("sfathervillage", sfathervillage);
        new AsynchHttpRequest(ct, Constant.REQUESTS.addSpouse, Constant.POST_TYPE.POST, map);
    }

    public void addSon(MainUser user, String name) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "addbrother");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        map.put("name", name);
        new AsynchHttpRequest(ct, Constant.REQUESTS.addSon, Constant.POST_TYPE.POST, map);
    }

    public void addSpouse(MainUser user, String spousename, String sfathername, String smothername, String sfathersurname, String sfathervillage) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "addspouse");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        map.put("spousename", spousename);
        map.put("sfathername", sfathername);
        map.put("smothername", smothername);
        map.put("sfathersurname", sfathersurname);
        map.put("sfathervillage", sfathervillage);
        new AsynchHttpRequest(ct, Constant.REQUESTS.addSpouse, Constant.POST_TYPE.POST, map);
    }

    public void addSister(MainUser user, String sistername, String shusbandname, String ssurname, String shfathername, String shmothername, String shvillage, String maritalstatusid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "addsister");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        map.put("sistername", sistername);
        map.put("shusbandname", shusbandname);
        map.put("ssurname", ssurname);
        map.put("shfathername", shfathername);
        map.put("shmothername", shmothername);
        map.put("shvillage", shvillage);
        map.put("maritalstatusid", maritalstatusid);
        new AsynchHttpRequest(ct, Constant.REQUESTS.addSister, Constant.POST_TYPE.POST, map);
    }

    public void getContact(int familydetailid) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "contact");
        map.put("familydetailid", String.valueOf(familydetailid));
        new AsynchHttpRequest(ft, Constant.REQUESTS.getContact, Constant.POST_TYPE.POST, map);
    }

    public void locationsforjob(MainUser user) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "joblocation");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        new AsynchHttpRequest(ct, Constant.REQUESTS.locationsforjob, Constant.POST_TYPE.POST, map);
    }

    /* public void locationsforjob() {
         Map<String, String> map = new HashMap<String, String>();
         map.put("url", COMMON_URL + "locationsforjob");
         map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
         new AsynchHttpRequest(ct, Constant.REQUESTS.locationsforjob, Constant.POST_TYPE.POST, map);
     }*/
    public void jobsearch(MainUser user, String fromyear, String toyear, String location) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "jobsearch");
        map.put("fromyear", fromyear);
        map.put("toyear", toyear);
        map.put("location", location);
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        new AsynchHttpRequest(ct, Constant.REQUESTS.jobsearch, Constant.POST_TYPE.POST, map);
    }

    public void Profession_List() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "Profession_List");
        new AsynchHttpRequest(ft, Constant.REQUESTS.Profession_List, Constant.POST_TYPE.GET, map);
    }

    public void NatureOfBusiness_List() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "NatureOfBusiness_List");

        new AsynchHttpRequest(ft, Constant.REQUESTS.NatureOfBusiness_List, Constant.POST_TYPE.GET, map);
    }

    public void BusinessSearch(String fname, String profession, String business) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", COMMON_URL + "BusinessSearch");
        map.put("FirstName", fname);
        map.put("ProfessionName", profession);
        map.put("NatureOfBusiness", business);
        new AsynchHttpRequest(ft, Constant.REQUESTS.BusinessSearch, Constant.POST_TYPE.POST, map);
    }
/*

    public String ImageConvert(String ImageUrl) {
        String imageAsString = "";
        try {
            if (ImageUrl != null && !ImageUrl.equalsIgnoreCase("")) {
                Uri uriString = Uri.parse(ImageUrl);
                File file = new File(uriString.getPath());
                FileInputStream objFileIS;
                objFileIS = new FileInputStream(file);
                ByteArrayOutputStream objByteArrayOS = new ByteArrayOutputStream();
                byte[] byteBufferString = new byte[1024];
                for (int readNum; (readNum = objFileIS.read(byteBufferString)) != -1; ) {
                    objByteArrayOS.write(byteBufferString, 0, readNum);
                }

                imageAsString = Base64.encodeToString(objByteArrayOS.toByteArray(), Base64.DEFAULT);


                Bitmap bm = BitmapFactory.decodeFile(ImageUrl);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 50, baos); //bm is the bitmap object
                byte[] b = baos.toByteArray();
                imageAsString.replaceAll("\n","");
                imageAsString = Base64.encodeToString(b, Base64.DEFAULT);

            } else {
                imageAsString = "";
            }
            return imageAsString;

        } catch (Exception e) {
            e.printStackTrace();
            return imageAsString;
        }
    }
*/

    /*  public String ImageConvert(String ImageUrl) {
          String imageAsString = "";
          InputStream inputStream = null;//You can get an inputStream using any IO API
          try {
              inputStream = new FileInputStream(ImageUrl);
          } catch (FileNotFoundException e) {
              e.printStackTrace();
          }
          byte[] bytes;
          byte[] buffer = new byte[8192];
          int bytesRead;
          ByteArrayOutputStream output = new ByteArrayOutputStream();
          try {
              while ((bytesRead = inputStream.read(buffer)) != -1) {
                  output.write(buffer, 0, bytesRead);
              }
          } catch (IOException e) {
              e.printStackTrace();
          }
          bytes = output.toByteArray();
          String encodedString = Base64.encodeToString(bytes, Base64.DEFAULT);
          return encodedString;


         *//* try {
            if (ImageUrl != null && !ImageUrl.equalsIgnoreCase("")) {
                Uri uriString = Uri.parse(ImageUrl);
                File file = new File(uriString.getPath());
                FileInputStream objFileIS;
                objFileIS = new FileInputStream(file);
                ByteArrayOutputStream objByteArrayOS = new ByteArrayOutputStream();
                byte[] byteBufferString = new byte[1024];
                for (int readNum; (readNum = objFileIS.read(byteBufferString)) != -1; ) {
                    objByteArrayOS.write(byteBufferString, 0, readNum);
                }
                imageAsString = Base64.encodeToString(objByteArrayOS.toByteArray(), Base64.DEFAULT);
              //  String converted = Base64.encodeToString(objByteArrayOS.toByteArray(), Base64.DEFAULT).replace('-', '+');
               // imageAsString = converted.replace('_', '/');
            } else {
                imageAsString = "";
            }
            return imageAsString;

        } catch (Exception e) {
            e.printStackTrace();
            return imageAsString;
        }*//*
    }
   */
    public String ImageConvert(String ImageUrl) {
        String imageAsString = "";
        try {
            if (ImageUrl != null && !ImageUrl.equalsIgnoreCase("")) {
           /*   Bitmap bm = BitmapFactory.decodeFile(ImageUrl);
              ByteArrayOutputStream baos = new ByteArrayOutputStream();
              bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
              byte[] b = baos.toByteArray();
              imageAsString = Base64.encodeToString(b, Base64.DEFAULT);
         */
                Uri uriString = Uri.parse(ImageUrl);
                File file = new File(uriString.getPath());
                FileInputStream objFileIS;
                objFileIS = new FileInputStream(file);
                ByteArrayOutputStream objByteArrayOS = new ByteArrayOutputStream();
                byte[] byteBufferString = new byte[1024];
                for (int readNum; (readNum = objFileIS.read(byteBufferString)) != -1; ) {
                    objByteArrayOS.write(byteBufferString, 0, readNum);
                }
             /* String converted = Base64.encodeToString(objByteArrayOS.toByteArray(), Base64.DEFAULT).replace('-', '+');
              imageAsString = converted.replace('_', '/');
         */
                imageAsString = Base64.encodeToString(objByteArrayOS.toByteArray(), Base64.DEFAULT);
            } else {
                imageAsString = "";
            }
            return imageAsString;

        } catch (Exception e) {
            e.printStackTrace();
            return imageAsString;
        }
    }

    public void getPrathnaDemiseList() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", CLInt_URL + "GetPrathnaDemiseList");
        new AsynchHttpRequest(ft, Constant.REQUESTS.getPrathnaDemiseList, Constant.POST_TYPE.GET, map);
    }

    public void getPersonallyAddedPrathnaDemiseList(MainUser user) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("url", CLInt_URL + "GetPersonallyAddedPrathnaDemiseList");
        map.put("familydetailid", String.valueOf(user.getFamilyDetailID()));
        new AsynchHttpRequest(ft, Constant.REQUESTS.getPersonallyAddedPrathnaDemiseList, Constant.POST_TYPE.POST, map);
    }


}
