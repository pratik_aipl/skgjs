package com.skgjst.utils;

import com.skgjst.BuildConfig;

import java.util.Arrays;
import java.util.List;

/**
 * Created by admin on 10/12/2016.
 */
public class Constant {

    public static final int READ_WRITE_EXTERNAL_STORAGE_CAMERA = 2355;
    public static final String IMAGE_PREFIX = "";
    public static final int ACCESS_FINE_COARSE_LOCATION = 820;
    public static final int ACCESS_FINE_LOCATION = 821;
    public static final int NUM_OF_COLUMNS = 3;
    public static String BASIC_URL = BuildConfig.API_URL;
    // Gridview image padding
    public static final int GRID_PADDING = 8; // in dp

    // SD card image directory
    public static final String PHOTO_ALBUM = "DCIM/Camera";

    // supported file formats
    public static final List<String> FILE_EXTN = Arrays.asList("jpg", "jpeg",
            "png");

    public enum POST_TYPE {
        GET, POST, POST_WITH_IMAGE;
    }

    public enum REQUESTS {
        EditPrathnaDemise, Business_Advertisements, BirthdayList, getJoblist, jobapply, GetCityList, createjob, getMyJoblist, editmyjob, getCandidateslist, editJob, getFamilymembers,getFamilymembersCount, getMyprofiles, getContact, Profession_List, NatureOfBusiness_List, locationsforjob, jobsearch, getRegister, generateOtp, verifyOtp, forgotPassword, Validatenumber, getSurnameList, getVillageList, getBloodGroup, getMaritalStatus, getCountry, getState, getCity, getProfession, getBusinessNature, BusinessSearch, GetEventImages, GetEventList, GetMatrimonialMember, AddMatrimonialDetails, GetProfile, EditMatrimonialDetails, MatrimonialList, VillageList, MaritalStatus, Gender_List, Matrimonialbysearch, RelationList, SurNameList, BloodGroup, RegisterMember, editMember, Relationformembers, MemberSearch, getMember, JarVidhi, getOrganizationList, NewsList, GetEditProfileParameters, editProfile, getDemiseList, Adddemise, getMyDemiseList, validatenumber, UpdatePlayerId, getMemberSearch, getPrefilbrother, addBrother,addFatherMother, getBirthdayWishes, getAnniversarylist, getAnniversarywishes, matriSearch, myMatrimonial, editMatrimonialDetails, myprofiles, addMatri, fillMatrimonial, removeProfile, addSpouse, addSister, getNotifications, getBusinesssearch, getWatched, changePassword, addSon, addDaughter, getNews, getOraganization, getBusinessAd, getSponeserAd, getDimondAd, getcount, updateNotifications, deleteDemise, getPrathanaList, getMyPrathanaList, AddPrathna, deletePrathna, getLogin, addDeviceLogin, logOutStatus, loginStatus, getPrathnaDemiseList, getPersonallyAddedPrathnaDemiseList, getMyprofilesMember;


    }
}
