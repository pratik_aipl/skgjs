package com.skgjst.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;

import com.skgjst.callinterface.AsynchTaskListner;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 10/11/2016.
 */
public class AsynchHttpRequest {
    public Fragment ft;
    public Constant.REQUESTS request;
    public Map<String, String> map;
    public AsynchTaskListner aListner;
    public StringBuilder sb = new StringBuilder();
    public Constant.POST_TYPE post_type;

    public AsynchHttpRequest(Fragment ft, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ft = ft;
        this.request = request;
        this.map = map;
        this.aListner = (AsynchTaskListner) ft;
        this.post_type = post_type;

        if (Utils.isNetworkAvailable(ft.getActivity())) {
            if (!map.containsKey("show")) {
                Utils.showProgressDialog(ft.getActivity());
            } else {
                this.map.remove("show");
            }

            new MyRequest().execute(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
            Utils.showAlert("No Internet, Please try again later", ft.getActivity());
        }
    }

    public Context ct;

    public AsynchHttpRequest(Context ct, Constant.REQUESTS request, Constant.POST_TYPE post_type, Map<String, String> map) {
        this.ct = ct;
        this.request = request;
        this.map = map;
        this.aListner = (AsynchTaskListner) ct;
        this.post_type = post_type;
        if (Utils.isNetworkAvailable(ct)) {
            if (!map.containsKey("show")) {
                Utils.showProgressDialog(ct);
            } else {
                this.map.remove("show");
            }
            new MyRequest().execute(this.map);
        } else {
            aListner.onTaskCompleted(null, request);
            Utils.showAlert("No Internet, Please try again later", ct);
        }
    }

    class MyRequest extends AsyncTask<Map<String, String>, Void, String> {
        MyCustomMultiPartEntity reqEntity;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Map<String, String>... map) {
            System.out.println("::urls ::" + map[0].get("url"));
            String responseBody = "";
            try {
                switch (post_type) {
                    case GET:
                        DefaultHttpClient httpClient =new DefaultHttpClient();
                        String query = map[0].get("url");
                        System.out.println();
                        map[0].remove("url");
                        List<String> values = new ArrayList<String>(map[0].values());
                        List<String> keys = new ArrayList<String>(map[0].keySet());
                        for (int i = 0; i < values.size(); i++) {
                            System.out.println();
                            System.out.println(keys.get(i) + "====>" + values.get(i));
                            query = query + keys.get(i) + "=" + values.get(i).replace(" ", "%20");
                            ;
                            if (i < values.size() - 1) {
                                query += "&";
                            }
                        }
                        System.out.println("URL" + "====>" + query);
                        HttpGet httpGet = new HttpGet(query);
                        HttpResponse httpResponse = httpClient.execute(httpGet);
                        HttpEntity httpEntity = httpResponse.getEntity();
                        responseBody = EntityUtils.toString(httpEntity);
                        System.out.println("Responce" + "====>" + responseBody);
                        return responseBody;

                    case POST:
                        System.out.println("new Requested URL >> " + map[0].get("url"));
                        HttpPost postRequest = new HttpPost(map[0].get("url"));

                        java.net.URL url = new URL(map[0].get("url"));
                        map[0].remove("url");

                        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("POST");
                        conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
                        conn.setRequestProperty("Accept", "application/json");
                        conn.setDoOutput(true);
                        conn.setDoInput(true);
                        String jsonValue = new JSONObject(map[0]).toString();//String.valueOf(new JSONObject(map[0]));
                   //    android.util.Log.i("Json Value : ", jsonValue);
                       longInfo(jsonValue,"Jso Value");
                        jsonValue = jsonValue.replaceAll("\\\\", "");
                        DataOutputStream os = new DataOutputStream(conn.getOutputStream());
                        //os.writeBytes(URLEncoder.encode(jsonValue.toString(), "UTF-8"));
                        os.writeBytes(String.valueOf(jsonValue));

                        os.flush();
                        os.close();

                        android.util.Log.i("STATUS", String.valueOf(conn.getResponseCode()));


                        int HttpResult = conn.getResponseCode();

                        if (HttpResult == HttpURLConnection.HTTP_OK) {
                            BufferedReader br = new BufferedReader(new InputStreamReader(
                                    conn.getInputStream(), "utf-8"));
                            String line = null;
                            while ((line = br.readLine()) != null) {
                                sb.append(line + "\n");
                            }
                            br.close();


                            android.util.Log.i("MSG", sb.toString());
                            return sb.toString();

                        } else {
                            System.out.println(conn.getResponseMessage());
                            return "";
                        }


                    case POST_WITH_IMAGE:
                        System.out.println("new Requested url >> " + map[0].get("url"));
                         postRequest = new HttpPost(map[0].get("url"));

                         url = new URL(map[0].get("url"));
                        map[0].remove("url");

                         conn = (HttpURLConnection) url.openConnection();
                        conn.setRequestMethod("POST");
/*conn.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
conn.setRequestProperty("Accept","application/json");*/
                        conn.setDoOutput(true);
                        conn.setDoInput(true);

                        values = new ArrayList<String>(map[0].values());
                        keys = new ArrayList<String>(map[0].keySet());
                        for (int i = 0; i < values.size(); i++) {

                            if(values.get(i) == null){
                                map[0].put(keys.get(i),"");
                            }

                        }


                         jsonValue = String.valueOf(new JSONObject(map[0]));
                         android.util.Log.i("Json Value : ", jsonValue);


                        os = new DataOutputStream(conn.getOutputStream());
//os.writeBytes(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                        os.writeBytes(String.valueOf(jsonValue));

                        os.flush();
                        os.close();

                        Log.d("STATUS", String.valueOf(conn.getResponseCode()));

                         HttpResult =conn.getResponseCode();

                        if(HttpResult ==HttpURLConnection.HTTP_OK){
                            BufferedReader br = new BufferedReader(new InputStreamReader(
                                    conn.getInputStream(),"utf-8"));
                            String line = null;
                            while ((line = br.readLine()) != null) {
                                sb.append(line + "\n");
                            }
                            br.close();


                            Log.i("MSG" , sb.toString());
                            return sb.toString();

                        }else{
                            System.out.println(conn.getResponseMessage());
                            return "";
                        }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Utils.hideProgressDialog();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
          /*  if (Utils.isNetworkAvailable(ct)) {
                if (result.equalsIgnoreCase("")) {
                    Utils.showProgressDialog(ct);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Utils.removeWorkingDialog(ct);
                        }
                    }, 5000);
                } else {
              */
            aListner.onTaskCompleted(result, request);
               /* }
            }*/
            super.onPostExecute(result);
        }
    }
    public static void longInfo(String str, String tag) {
        if (str.length() > 8000) {
            Utils.Log("TAG " + tag + " -->", str.substring(0, 8000));
            longInfo(str.substring(8000), tag);
        } else {
            Utils.Log("TAG " + tag + " -->", str);
        }
    }

    public boolean matchKeysForImages(String key) {
      /*  profile_picture_1
                profile_picture_2
        profile_picture_3
                profile_picture_4
        profile_picture_5*/

        if (key.equalsIgnoreCase("file") ||
                key.equalsIgnoreCase("PhotoUrl") ||
                key.equalsIgnoreCase("profile_picture_3") ||
                key.equalsIgnoreCase("profile_picture_4") ||
                key.equalsIgnoreCase("profile_picture_5") ||
                key.equalsIgnoreCase("profile_picture") ||
                key.equalsIgnoreCase("user_image") ||
                key.equalsIgnoreCase("event_image") ||
                key.equalsIgnoreCase("critiques_image") ||
                key.equalsIgnoreCase("classified_image") ||
                key.equalsIgnoreCase("lerning_image") ||
                key.equalsIgnoreCase("video") ||
                key.equalsIgnoreCase("fund_image") ||
                key.equalsIgnoreCase("project_image") ||
                key.equalsIgnoreCase("portfolio_image") ||
                key.equalsIgnoreCase("portfolio_category_image") ||
                key.equalsIgnoreCase("photo_id") || key.equalsIgnoreCase("profile_pic")) {
            return true;
        } else {
            return false;
        }
    }

    static class Log {

        public static void d(String TAG, String message) {
            int maxLogSize = 2000;
            for (int i = 0; i <= message.length() / maxLogSize; i++) {
                int start = i * maxLogSize;
                int end = (i + 1) * maxLogSize;
                end = end > message.length() ? message.length() : end;
                android.util.Log.d(TAG, message.substring(start, end));
            }
        }

        public static void i(String TAG, String message) {
            int maxLogSize = 2000;
            for (int i = 0; i <= message.length() / maxLogSize; i++) {
                int start = i * maxLogSize;
                int end = (i + 1) * maxLogSize;
                end = end > message.length() ? message.length() : end;
                android.util.Log.i(TAG, message.substring(start, end));
            }
        }


        public static void longInfo(String str, String tag) {
            if (str.length() > 4000) {
                Log.i("TAG " + tag + " -->", str.substring(0, 4000));
                //  longInfo(str.substring(4000), tag);
            } else {
                Log.i("TAG " + tag + " -->", str);
            }
        }
    }

    public DefaultHttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }
}
