package com.skgjst.callinterface;

import com.skgjst.utils.Constant;

public interface AsynchTaskListner {
    public void onTaskCompleted(String result, Constant.REQUESTS request);
}
