package com.skgjst.callinterface;

/**
 * Created by empiere-vaibhav on 10/10/2018.
 */

public interface NotificationListener  {
    public void notificationWatch(int notification_id);
}
