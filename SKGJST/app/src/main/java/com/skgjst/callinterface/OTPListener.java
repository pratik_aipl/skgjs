package com.skgjst.callinterface;

public interface OTPListener {

    public void otpReceived(String messageText);
}