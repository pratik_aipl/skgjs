package com.skgjst.callinterface;

/**
 * Created by empiere-vaibhav on 11/2/2018.
 */

public interface DemiseDeleteListener {
    public void onDeleteDemise(int demiseId);
}
