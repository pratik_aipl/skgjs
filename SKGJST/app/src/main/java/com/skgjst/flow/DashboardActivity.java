package com.skgjst.flow;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.skgjst.BaseActivity;
import com.skgjst.R;
import com.skgjst.utils.DotsIndicator;
import com.skgjst.utils.FragmentDrawer;

import java.util.ArrayList;
import java.util.List;

public class DashboardActivity  extends BaseActivity implements FragmentDrawer.FragmentDrawerListener {
    private ViewPager viewPager;
    private List<PackegFragment> fragments = new ArrayList<>();
    private int dotsCount = 3;
    private ImageView[] dots;
    public LinearLayout pager_indicator;

    @Override
     public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        DotsIndicator dotsIndicator = findViewById(R.id.dots_indicator);
        ViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.setPageTransformer(false, new CustPagerTransformer(this));
        pager_indicator = (LinearLayout) findViewById(R.id.viewPagerCountDots);
        setUiPageViewController();

        for (int i = 0; i < 3; i++) {

            fragments.add(new PackegFragment());
        }
        // 1. viewPager添加parallax效果，使用PageTransformer就足够了

        viewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                PackegFragment fragment = fragments.get(position % 10);
                return fragment;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return super.isViewFromObject(view, object);
            }

            @Override
            public int getCount() {
                return 3;
            }
        });

        viewPager.setCurrentItem(0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < 3; i++) {
                    dots[i].setImageDrawable(getResources().getDrawable(R.drawable.non_selected_item));
                }
                dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selected_item));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        TabLayout dots = findViewById(R.id.dots);
        dots.setupWithViewPager(viewPager, true);
       dotsIndicator.setViewPager(viewPager);
    }

    public void setUiPageViewController() {

        dotsCount = 3;
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.non_selected_item));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(8, 0, 8, 0);

            pager_indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selected_item));
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {

    }
}
