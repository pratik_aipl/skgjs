package com.skgjst.flow;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.skgjst.BaseFragment;
import com.skgjst.R;

/**
 * Created by Karan - Empiere on 9/24/2018.
 */


public class PackegFragment  extends BaseFragment {

    public View view;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_packeg, container, false);
        return view;
    }


}
